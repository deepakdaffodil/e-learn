app.factory('focus', function($timeout) {
    return function(id) {
      $timeout(function() {
        var element = document.getElementById(id);
        if(element){
          element.focus();
        }
      },400);
    };
});
app.filter('capitalize', function(){
   return function(input){
        if(input){
            return input[0].toUpperCase() + input.slice(1);
        }
   };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});
    
app.directive('confirmClick', function() {
    return {
        link: function (scope, element, attrs) {
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {

              msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteActivity ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickDelete', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteAccount ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickComment', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteComment ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickModule', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteModule ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
 //Displaying the loading image form for loading
  app.directive('showProgressBar', function() {
    return {
      restrict: 'E',
      template: '<img src="assets/img/proceed.gif" alt="processing..." />'
    };
  });

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "dd/mm/yy",
         changeMonth: true,
         changeYear: true,
         yearRange: '1920:2016',
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  };
});