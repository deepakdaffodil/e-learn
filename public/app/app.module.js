var app = angular.module('eLearning', ['ui.router', 'ngMessages', 'ngAnimate', 'ui.bootstrap', 'ngResource', 'file-model', 'djds4rce.angular-socialshare', 'ngTagsInput', 'angular-svg-round-progressbar', 'infinite-scroll', 'youtube-embed','ngFileUpload']).run(function($FB) {
    $FB.init('1684515721812917');
});

app.controller("AppController", ['$scope', '$rootScope', 'logoutService', '$state', 'getStatistics', 'isAuthenticated', 'profileService', 'getNotificationService', '$location','$sce', function($scope, $rootScope, logoutService, $state, getStatistics, isAuthenticated, profileService, getNotificationService, $location,$sce) {
    window.localStorage.location = '';
    if ($location.url().split('/').length > 2) {
        var id = $location.url().split('/')[$location.url().split('/').length - 1];

        if(id.length >= 20)
            window.localStorage['location'] = id;
    }

    $scope.bodyClasses = 'default';
    $scope.getTotalNotification = 0;
    $rootScope.isLoggedIn = false;
    $rootScope.isAdminLoggedIn = false;
    $scope.baseUrl = APP.url;
    $scope.showTotalNotification = false;
    $rootScope.showEditProfile = false;
    if (!localStorage.getItem('loggedInUser')) {
        $rootScope.isLoggedIn = false;
        $rootScope.isAdminLoggedIn = false;
        APP.currentUser = {};
    } else {
        //$rootScope.isLoggedIn = true;
        APP.currentUser = JSON.parse(localStorage.getItem('loggedInUser'));
        if (APP.currentUser.local.ambassdor) {
            $rootScope.isAdminLoggedIn = true;
        } else {
            $rootScope.isLoggedIn = true;
        }
    };
    $scope.isAuthenticated = function() {
        isAuthenticated.query(function(data) {
            if (data.statusCode === 200 && data.error === false && data.message === "ok") {
                profileService.query(function(data) {
                    if(localStorage.getItem('loggedInUser') !== JSON.stringify(data.result) && data.result.local.password)
                        return $scope.logout();

                    localStorage.setItem("loggedInUser", JSON.stringify(data.result));
                    //$rootScope.isLoggedIn = true;
                    APP.currentUser = JSON.parse(localStorage.getItem('loggedInUser'));
                    if (APP.currentUser.hasOwnProperty("local") === true) {
                        if (APP.currentUser.local.ambassdor) {
                            $rootScope.isAdminLoggedIn = true;
                            $rootScope.isLoggedIn = false;
                            $rootScope.count = 1;
                        } else {
                            $rootScope.isLoggedIn = true;
                        }
                        if (APP.currentUser.local.hasOwnProperty("token") === true) {
                            if (APP.currentUser.local.token !== '' && APP.currentUser.local.token !== null && APP.currentUser.local.token !== undefined) {
                                $rootScope.showEditProfile = true;
                            }
                        }
                    }
                     $state.go('home');
                });
            } else {
                APP.currentUser = {};
                localStorage.removeItem('loggedInUser');
                $rootScope.isLoggedIn = false;
                $rootScope.isAdminLoggedIn = false;
            }
        }, function(error) {});
    };
    $scope.isAuthenticated();
    $scope.logout = function() {
        logoutService.query(function() {
            APP.currentUser = {};
            localStorage.removeItem('loggedInUser');
            $rootScope.isLoggedIn = false;
            $rootScope.isAdminLoggedIn = false;
            $rootScope.showEditProfile = false;
            // window.location.reload(true);
            $state.go('home', {}, {
                reload: true
            });

        }, function(error) {

        });
    };
    //from controller when user is deleted by admmin
    $rootScope.$on('logoutLoggedInUser',function(event, args) {
        $scope.logout();
    });
    // get Statistics
    $rootScope.getStatistics = function() {
        getStatistics.query(function(data) {
            if (data.statusCode == 200 && data.message === 'ok') {
                $scope.statistics = data.result.statistics;
            } else {

            }
        }, function(error) {});
    };
    $rootScope.$on("callTotalNotification", function() {
        $rootScope.getAllNotification(APP.currentUser._id);
    });
    $rootScope.getAllNotification = function(id) {
        $scope.getTotalNotification = 0;
        $scope.showTotalNotification = false;
        if (id !== '') {
            getNotificationService.query({
                id: id
            }, function(data) {
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.getTotalNotification = data.result.newNoticationCount;
                    if (data.result.totalCount > 0) {
                        $scope.showTotalNotification = true;
                    };
                } else {
                    $scope.getTotalNotification = 0;
                }
            }, function(error) {
            });
        };
    };


    $rootScope.getStatistics();
    //$rootScope.getAllNotification();

}]);
app.run(function($rootScope, $state) {

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        $rootScope.containerClass = toState.containerClass;
        $rootScope.getStatistics();
        if (!Object.keys(APP.currentUser).length) {
            if (toState.name !== "reset" && toState.name !== "/") {
                $state.go('home');
            }
        } else {
            $rootScope.getAllNotification(APP.currentUser._id);
            if (toState.name === "reset" || toState.name === "/")
                $state.go('home');
        }
    });
});
/*app.run(function($rootScope){
  $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
	  $rootScope.containerClass = toState.containerClass;
	});
});*/
app.config(['tagsInputConfigProvider', function (tagsInputConfigProvider) {
    tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });
}]);
app.config(function($sceProvider) {
  // Completely disable SCE.  For demonstration purposes only!
  // Do not use in new projects.
  $sceProvider.enabled(false);
});
