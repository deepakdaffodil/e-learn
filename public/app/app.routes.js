app.config(['$stateProvider', '$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    // Now set up the states
    $stateProvider
        // For any unmatched url, redirect to /
        .state('/', {
            url: '/',
            templateUrl: 'app/components/landing/landingView.html',
            controller: 'landingController',
            containerClass: 'home-background'
        })
        .state('home',{
            url: '/home',
            templateUrl : 'app/components/home/homeView.html',
            controller: 'homeController',
            containerClass: 'stable-screen'
        })
        .state('home.groups',{
            url: '/groups',
            templateUrl : 'app/shared/group/groupView.html',
            controller : 'myGroupsController'
        })
        .state('home.groupActivity',{
            url: '/groupActivity',
            templateUrl : 'app/shared/groupActivities/groupActivitiesView.html',
            controller : 'groupActivitiesController',
            params: { obj: null }
        })
        .state('home.community',{
            url: '/community',
            templateUrl : 'app/shared/community/communityView.html',
            controller : 'communityController',
            params: { obj: null }
        })
        .state('home.space',{
            url: '/space',
            templateUrl : 'app/shared/myspace/myspaceView.html',
            controller : 'myspaceController'
        })
        .state('favourites',{
            url: '/favourites',
            templateUrl : 'app/components/favourites/favouritesView.html',
            controller : 'myFavouritesController'
        })
       .state('landing',{
            url: '/landing',
            templateUrl : 'app/components/landing/landingView.html',
            controller: 'homeController'
        })
        .state('profile',{
            url: '/profile',
            templateUrl : 'app/components/profile/profileView.html',
            controller: 'profileController'
        })
        .state('action',{
            url: '/action',
            templateUrl : 'app/components/action/actionView.html',
            controller: 'actionController'
        })
        .state('manage-module',{
            url: '/manage/modules',
            templateUrl : 'app/components/lessonModule/lessonView.html',
            controller: 'lessonModuleController'
        })
        .state('manage-user',{
            url: '/manage/user',
            templateUrl : 'app/components/adminUser/adminUserView.html',
            controller: 'adminUserController'
        })
        .state('reset',{
            url: '/reset?user',
            templateUrl : 'app/components/resetPassword/resetPassword.html',
            controller: 'resetPasswordController'
        })
        .state('activity',{
            url: '/home/:activityId',
            templateUrl : 'app/components/home/homeView.html',
            controller: 'homeController',
            containerClass: 'stable-screen'
        })
        $locationProvider.html5Mode(true).hashPrefix('!');
}]);
