app.factory('actionService', function ($resource) {
	return  $resource(APP.endpoints.actions);
});
app.factory('actionUserService', function ($resource) {
	return  $resource(APP.endpoints.actionUser,  { id: '@id',actionId:'@actionId' },{query: { method: "PUT"}});
});
app.factory('createActivity', function($resource){
	return $resource(APP.endpoints.createActivity);
});
app.factory('moduleCommunityService', function($resource){
	return $resource(APP.endpoints.moduleCommunity);
});
app.factory('lessonCommunityService', function($resource){
	return $resource(APP.endpoints.lessonCommunity, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
app.factory('completedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson);
});
app.factory('getAllCompletedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson,  {query :{method : "GET", isArray :false}});
});
app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
app.factory('lessonActivityService', function ($resource) {
   return  $resource(APP.endpoints.lessonActivity, { id: '@id', lessonId : '@lessonId'}, {query :{method : "GET", isArray :false}});
});

app.factory('getCompletedLessonService', function ($resource) {
   return  $resource(APP.endpoints.getCompletedLesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
