app.controller("modulesController", ['$scope', '$rootScope', '$timeout', 'moduleService', 'lessonService', 'lessonDetailService', 'actionService', 'actionUserService', 'createActivity', '$uibModal', '$state', 'moduleCommunityService', 'lessonCommunityService', 'completedLessonService', '$window', 'lessonActivityService','getCompletedLessonService', function($scope, $rootScope, $timeout, moduleService, lessonService, lessonDetailService, actionService, actionUserService, createActivity, $uibModal, $state, moduleCommunityService, lessonCommunityService, completedLessonService, $window, lessonActivityService, getCompletedLessonService) {
    $scope.modules = [];
    $scope.lesson = '';
    $scope.lessonDetail = '';
    $scope.loader = false;
    $scope.actionlist = false;
    $scope.showActivity = false;
    $scope.activityFormSubmitted = false;
    $scope.lessonSeeMore = false;
    $scope.selectAction = [];
    $scope.activity = {};
    $scope.shareLoader = false;
    $scope.module_id = '';
    $scope.mycommListShow = false;
    $scope.noFile = '';
    $scope.successAct = false;
    $scope.activityError = false;
    $scope.lenAction = false;
    $scope.uploadLoader = false;
    $scope.showLesson = false;
    $scope.delImage = false;
    $scope.uploadSuccess = false;

    $scope.showModule = {};
    $scope.lessons = [];
    $scope.modulesView = true;
    $scope.progressBar = false;
    $scope.current = 0;
    $scope.max = 0;
    $scope.onceClicked = [];
    $scope.showActivity = false;
    $scope.hasActivity = false;
    $scope.completedLessons = [];
    $scope.hasLessons = true;

    //AWS
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });

    $scope.$on("callModuleController", function(event, args) {
        $scope.showModule = args.module;
        $scope.getAllLessons();
        // $scope.getCompletedLessons();

    });
    $scope.getAllLessons = function(module) {
        lessonService.query({
            id: $scope.showModule._id
        }, function(data) {

            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                  $scope.lessons = data.result;
                  $scope.max = data.result.length;
                  $scope.hasLessons = true;
                } else {
                    $scope.hasLessons = false;
                }
            }
        }, function(error) {
        });
    };


    $scope.onNext = function(index, id, lesson) {
      $scope.completedLessons = [];
        if ($rootScope.isLoggedIn === false && $rootScope.isAdminLoggedIn === false) {
            $scope.$emit("ShowModalLogin", {});
        } else if(lesson.completed) {
            var opts = {};
            opts.user = APP.currentUser._id;
            opts.lesson = id;
            $timeout(function() {
                    angular.forEach($scope.lessons, function(lesson, key) {
                        if (lesson.completed) {
                            $scope.completedLessons.push(lesson);
                        }
                    });
            }, 0);
            $timeout(function() {
              var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'app/shared/progressBar/progressBarView.html',
                    controller: 'progressBarController',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sm',
                    resolve: {
                        max: function() {
                            return $scope.max;
                        },
                        current: function() {
                            return $scope.completedLessons;
                        },
                        lesson: function() {
                            return $scope.lesson;
                        },
                        lessonIndex: function() {
                            return index;
                        },
                        module: function() {
                            return $scope.showModule;
                         }
                    }
                });
            }, 100);

        } else {
            $scope.lessonForActivity = lesson;
            $scope.onceClicked[id] = id;
            //reddirectng to activity page at homeController
            $rootScope.$emit("callHomeControllerForActivity", {
                activity: true,
                lesson: lesson,
                index: index
            });
        }
    };

    $scope.progressModal = function(lesson) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/progressBar/progressBarView.html',
            controller: 'progressBarController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            text: {
              value : "text",
               color: '#f00',
               autoStyle: true
            },
            resolve: {
                max: function() {
                    return $scope.max;
                },
                current: function() {
                    return $scope.current;
                },
                lesson: function() {
                    return lesson;
                }
            }
        });
    };
    //close detail of lesson
    $scope.$on("CallParamsCloseMethod", function() {
        $scope.closeLessonModal();
    });
    $scope.closeLessonModal = function() {
        $scope.actionlist = false;
        $scope.showActivity = false;

    };

    // Api call for module list
    var module = moduleService.query(function(data) {
        $scope.modules = data.result.module;
    });
  $scope.getlessonDetail = function($event, id, completeAttr) {
        $scope.noFile = '';
        $scope.activity = {};
        angular.element('#fileUploaded').val(null);
        $scope.activityList = [];
        $scope.mycommListShow = false;
        $scope.activityFormSubmitted = false;
        $event.stopPropagation();
        angular.element('#fadein').addClass('color-overlay');
        $scope.showActivity = true;
        $scope.actionlist = false;
        if (completeAttr === true) {
            $("#completeDft").attr("disabled", "disabled");
        } else {
            $("#completeDft").removeAttr("disabled");
        }
        lessonDetailService.get({
            id: id
        }, function(data) {
            if (data.statusCode == 200) {
                $scope.lessonDetail = data.result;
                $scope.module_id = $scope.lessonDetail.module;
                $scope.Keytakeaways = $scope.lessonDetail.Keytakeaways ? $scope.lessonDetail.Keytakeaways.split(".") : [];
                lessonCommunityService.get({
                    id: id
                }, function(data) {
                    if (data.result.length > 0) {
                        $scope.activityList = data.result;
                        $scope.mycommListShow = false;
                    } else {
                        $scope.mycommListShow = true;
                        $scope.activityList = data.result;
                    }
                });
            } else {
            }
        });
    };

    $(window).click(function(e) {
        try {
            if(!$(e.target).attr('class').includes('video-bg'))
                $scope.modulePlayer.stopVideo();
        }
        catch(e1) {}
    });
}]);
