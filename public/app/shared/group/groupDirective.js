app.directive('myGroups', function () {
	return {
		templateUrl: 'app/shared/group/groupView.html',
		restrict: 'E',
		controller: 'myGroupsController'
	};
});