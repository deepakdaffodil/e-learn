app.controller("myGroupsController", ['$scope', '$rootScope', 'myGroupsService', '$uibModal', 'delGroupService','$state', function($scope, $rootScope, myGroupsService, $uibModal, delGroupService,$state) {
    $scope.spaceloader = true;
    $scope.myGroupsList = [];
    $scope.mySpaceMsg = true;
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.grpId = '';
    $scope.index = '';
    $scope.search = '';
    $scope.hasGroups = false;
    $rootScope.activeSection = "groups";
    $rootScope.myTabs = true;

    $scope.showGroupsList = function() {
        var limit_start = $scope.myGroupsList.length;
        var limit = 100;
        var id = APP.currentUser._id;
        //if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.moduleLoader = true;
            myGroupsService.query({
                id: id,
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.spaceloader = false;
                if (data.statusCode === 200 && data.message === "ok") {
                    if (data.result.totalCount === 0) {
                        $scope.hasGroups = false;
                    } else {
                        $scope.hasGroups = true;
                        $scope.moduleLoader = false;
                        $scope.allTotal = data.result.totalCount;
                        $scope.myGroupsList = $scope.myGroupsList.concat(data.result.groups);
                    }
                } else {
                    $scope.moduleLoader = false;
                    $scope.myGroupsList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {});
      //  }
    };

    $scope.showGroupsList();

    $scope.loadMore = function() {
        $scope.showGroupsList();
    };
    //for creating new group
    $scope.createGroup = function() {
        $scope.myGroupsList = $scope.myGroupsList;
        $scope.hasGroups = $scope.hasGroups;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/createGroup/createGroupView.html',
            controller: 'createGroupController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
    $rootScope.$on('groupsComing',function(event, args) {
      $scope.showGroupsList();
    });

    $scope.editGroup = function(id, $event) {
        //$scope.showGroupsList = $scope.showGroupsList;
        $event.stopPropagation();
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/editGroup/editGroupView.html',
            controller: 'editGroupController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return id;
                }
            }
        });
    };

    $scope.deleteMyGroup = function(index, id, $event) {
        $event.stopPropagation();
        $scope.index = index;
        $scope.myGroupsList = $scope.myGroupsList;
        $scope.grpId = id;
        $scope.hasGroups = $scope.hasGroups;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure to delete this group?";
                },
                item: function() {
                    return 'delete-group';
                }
            }
        });

    };
    $rootScope.$on('noGroupsPressent', function(event, args) {
      $scope.hasGroups = false;
    });

    $scope.searchGroup = function() {
      $scope.myGroupsList=[];
        // var limit_start = 0;
        // var limit = 30;
        var id = APP.currentUser._id;

        myGroupsService.query({
            id : id,
            name: $scope.search
        }, function(data) {
            $scope.myGroupsList = data.result.groups;
        });
    };
    $scope.groupActivitiesz = function(id, name) {
      var obj = {};
      obj.id = id;
      obj.name = name;
      $state.go('home.groupActivity', {obj:obj});
      //$state.go('home.community');
        // $rootScope.$emit("groupActivitiesPage", {
        //     id : id,
        //     name : name
        // });
    };
    $rootScope.$on("CallGroupsMethod", function() {
        $scope.myGroupsList = [];
        $scope.showGroupsList();
    });
}]);
