app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroups,{},{query: { method: "GET", isArray: false }});
});
app.factory('delGroupService', function ($resource) {
   return  $resource(APP.endpoints.delGroup, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});
