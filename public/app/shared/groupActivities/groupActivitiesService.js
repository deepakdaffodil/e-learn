app.factory('groupActivitiesService', function ($resource) {
   return  $resource(APP.endpoints.groupActivities,{id : '@id'},{query: { method: "GET", isArray: false }});
});
