app.controller("groupActivitiesController", ['$rootScope', '$scope', '$log', 'groupActivitiesService','$state',function($rootScope, $scope, $log, groupActivitiesService, $state) {
    $scope.activityloader = false;
    $scope.hasActivities = false;

    if($state.params.obj) {
      $rootScope.name =  $state.params.obj.name;
      $rootScope.id = $state.params.obj.id;
    }else {
      $rootScope.name = $rootScope.name;
      $rootScope.id = $rootScope.id;
    }
    $scope.name = $rootScope.name;
    $scope.id = $rootScope.id;
    
    $scope.getGroupActivities = function() {
        $scope.activityloader = true;
        groupActivitiesService.query({
            id: $rootScope.id
        }, function(data) {
            $scope.activityloader = false;
            if (data.result.totalCount) {

                $scope.hasActivities = true;
                $scope.groupActivities = data.result.activities;
            } else {
                $scope.hasActivities = false;
                $scope.groupActivities = [];
            }
          });
    };
    $scope.getGroupActivities();
}]);
