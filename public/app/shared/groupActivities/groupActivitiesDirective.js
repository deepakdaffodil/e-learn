app.directive('groupActivities', function () {
	return {
		templateUrl: 'app/shared/groupActivities/groupActivitiesView.html',
		restrict: 'E',
		controller: 'groupActivitiesController'
	};
});
