app.controller("customBrowseController", ['$rootScope', '$scope', '$uibModalInstance', '$log', function($rootScope, $scope, $uibModalInstance, $log) {

    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.selectImage = function($event) {
        $rootScope.$emit("uploadGroupAvatar", {
            avatar_url: $event.target.src
        });
        $scope.closeModal();
    };


}]);
