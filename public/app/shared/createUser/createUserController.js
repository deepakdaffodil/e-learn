app.controller("createUserController", ['$scope', '$uibModalInstance', 'moduleId', 'createUserService', 'registerService','$timeout','$rootScope', function($scope, $uibModalInstance, moduleId, createUserService, registerService,$timeout, $rootScope) {
    $scope.user = {};
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        $scope.$emit("CallModulesMethod", {});
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.convert = function(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [day, mnth, date.getFullYear()].join("/");
    };

    $scope.emailValidate = function() {
        $scope.checkingEmail = false;
        $scope.showErrEmail = true;
        $scope.errMsg = frontendSettings.checkingEmail;
        //$scope.showErrEmail = false;
        //$scope.errMsg = '';
        if ($scope.user.email === undefined) {} else {
            registerService.save({
                email: $scope.user.email
            }, function(data) {
                if (data.statusCode == 200 && data.message == "ok" && data.result.message == 'verified') {
                    $scope.showErrEmail = false;
                    $scope.checkingEmail = false;
                } else if (data.statusCode == 422 && data.message == 'email already registered') {

                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.userregistered;
                } else if (data.statusCode == 422 && data.message == 'email should be registered with any email provider') {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                } else {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                }
            });
        }
    };

    $scope.submitRegistration = function() {
        $scope.userSubmitted = true;
        $scope.sucessMessage = false;

        $scope.dob = '';
        if ($scope.dt) {
            $scope.dob = $scope.convert($scope.dt);
        }
        if ($scope.user.name === undefined || $scope.user.name === '') {
            focus('uname');
            return false;
        } else if ($scope.dob === undefined || $scope.dob === '') {
            focus('dob');
            return false;
        } else if ($scope.user.email === undefined || $scope.user.email === '') {
            focus('userEmail');
            return false;
        } else if ($scope.user.password === undefined || $scope.user.password === '') {
            focus('userPassword');
            return false;
        } else if ($scope.showErrEmail === true) {
            $scope.checkingEmail = true;
            focus('email');
            return false;
        }

        $scope.signupStart = true;
        var opts = {};
        opts.email = $scope.user.email;
        opts.password = $scope.user.password;
        opts.dob = $scope.dob;
        opts.name = $scope.user.name;

        createUserService.save(opts, function(data) {
            if (data.statusCode == 200 && data.message === 'ok') {
                $scope.sucessMessage = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.signupStart = false;
                $rootScope.$emit('updateUserList',{});
                $timeout(function() {
                    $scope.closeModal();
                }, 2000);
            } else if (data.statusCode == 422 && data.message.email === false) {
                $scope.signupStart = false;
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.userregistered;
            } else {
                $scope.signupStart = true;
            }
        }, function(error) {
        });

    };
}]);
