app.factory('createUserService', function ($resource) {
	return  $resource(APP.endpoints.createUser);
});
app.factory('registerService', function ($resource) {
	return  $resource(APP.endpoints.validateEmail);
});
