app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
