app.directive('progressBar', function () {
	return {
		templateUrl: 'app/shared/progressBar/progressBarView.html',
		restrict: 'E',
		controller: 'progressBarController'
	};
});
//
