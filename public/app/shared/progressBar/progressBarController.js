app.controller("progressBarController", ['$rootScope', '$scope', '$state', '$uibModalInstance', 'max', 'current', 'lesson', 'lessonIndex', 'module', 'lessonService', '$timeout', function($rootScope, $scope, $state, $uibModalInstance, max, current, lesson, lessonIndex, module, lessonService, $timeout) {
    $scope.max = max;
    $scope.current = current.length;
    $scope.lesson = lesson;
    $scope.module = module;
    $scope.lessonIndex = lessonIndex;
    $scope.nextLesson = {};
    $scope.lessons = [];
    $scope.inCompleteLessons = [];
    $scope.percent = "";

    $scope.getAllLessons = function() {
        $timeout(function() {
            lessonService.query({
                id: $scope.module._id
            }, function(data) {
                $scope.lessons = data.result;
                angular.forEach(data.result, function(lesson, key) {
                    if (!lesson.completed) {
                        $scope.inCompleteLessons.push(lesson);
                    }
                });
                $scope.percent = parseInt((($scope.max-$scope.inCompleteLessons.length)*100)/$scope.max);
            });
        }, 0);

    };
    $scope.getAllLessons();
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };
    $rootScope.$on("closingModal", function(event, args){
        $scope.closeModal();
    });
    $scope.onProgressNext = function() {

        if ($scope.max == $scope.current) {

            $rootScope.$emit("callHomeConLastActiviyComponent", {
                module: $scope.module
            });

        } else {

            $scope.nextLesson = $scope.inCompleteLessons[0];
                $rootScope.$emit("callHomeControllerForActivity", {
                    activity: true,
                    lesson: $scope.nextLesson,
                    module: $scope.module,
                    index: $scope.lessonIndex + 1,
                });


        }
    };


}]);
