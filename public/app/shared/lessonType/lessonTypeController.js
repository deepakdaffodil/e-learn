app.controller("lessonTypeController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'moduleId', 'createLessonService', 'type', 'editLessonService', function($rootScope, $scope, $timeout, $uibModalInstance, $log, moduleId, createLessonService, type, editLessonService) {
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $scope.type = type;
    if (type == 'Edit Lesson') {
        $scope.optionModule = moduleId.module;
        $scope.lesson = moduleId;
        $scope.videoTitle = "Change video";
        $scope.videoId = moduleId.url;
        $scope.lesson.location = moduleId.url;
    } else {
        $scope.optionModule = moduleId;
        $scope.lesson = {};
        $scope.videoTitle = "Add video";
    }

    $scope.showVideoButton = true;
    $scope.message = '';

    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.upload = function(element) {
        $scope.noFile = '';
        $scope.delImage = false;
        $scope.imag = '';
        $scope.uploadSuccess = false;
        $scope.showProgressBar = false;
        //$scope.showProgressBar = false;
        $scope.lesson.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.lesson.fileModel) {
            if ($scope.lesson.fileModel.type === 'image/jpeg' || $scope.lesson.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.lesson.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.lesson.fileModel.type,
                    Body: $scope.lesson.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {
                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.uploadSuccess = true;
                        $scope.lesson.location = data.Location;
                        if ($scope.delImage) {
                            $scope.hidePreview();
                            $scope.lesson.location = '';
                            $scope.lesson.fileModel = null;
                        }
                        $scope.showProgressBar = true;
                        //$scope.noFile = 'Image uploaded successfully';
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);


            }
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }

        return $scope.lesson.location;
    };

    $scope.hidePreview = function() {
        $scope.lesson.location = '';
        $scope.lesson.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };
    //videooooooooo
    $scope.addVideo = function() {
        $scope.showVideoFile = true;
        $scope.showVideo = true;

    };


    $scope.doneClick = function($event) {
        $scope.createLessonFormSubmitted = true;
        if ($scope.videoId === null || $scope.videoId === '' || $scope.videoId === undefined) {
            focus('videoIdField');
            return false;
        } else {
            $scope.createLessonFormSubmitted = false;
        }
        $scope.videoTitle = "Change video";
        $scope.showVideo = false;
    };
    $scope.cancelVideo = function() {
        $scope.lesson.url = '';
        $scope.videoTitle = "Add video";
        $scope.videoId = '';
        $scope.showVideo = false;
        $scope.showVideoFile = false;
    };

    //video submit createLessonTypeVideo
    $scope.createLessonTypeVideo = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.videoId === undefined || $scope.videoId === '') {
            focus('videoIdField');
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.url = $scope.videoId;
        opts.type = 'video';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };

    $scope.createLessonTypeImage = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.lesson.location === undefined || $scope.lesson.location === '') {
            $scope.noFile = "Please upload a file first";
            $timeout(function() {
                $scope.noFile = "";
            }, 2000);
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.url = $scope.lesson.location;
        opts.type = 'image';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };
    $scope.createLessonTypeText = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.type = 'text';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };

    $scope.lessonServiceType = function(data, message) {
        if (data.statusCode === 200 && data.message === "ok") {
            $scope.createLessonFormSubmitted = false;
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = message;
            $scope.msgClass = 'success-green';
            $scope.lesson = {};
            $rootScope.$emit("updatelessons", {
                moduleIndex: $scope.moduleIndex,
                moduleId: $scope.optionModule
            });

            $timeout(function() {
                $scope.lessonResultMsg = false;
                $scope.sucessMsg = '';
                $scope.closeModal();

            }, 2000);
        } else if (data.statusCode === 422 && data.message === "lesson name must be unique") {
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = frontendSettings.uniqueLesson;
            $scope.msgClass = 'error-red';
            $scope.createLessonFormSubmitted = false;
            $timeout(function() {
                $scope.resultMsg = false;
                $scope.sucessMsg = '';
            }, 3000);
        } else {
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = frontendSettings.errorOccured;
            $scope.msgClass = 'error-red';
            $scope.createLessonFormSubmitted = false;
            $timeout(function() {
                $scope.lessonResultMsg = false;
                $scope.sucessMsg = '';
            }, 4000);
        }
    };

}]);
