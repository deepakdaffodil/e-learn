app.factory('editLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{update: { method: "PUT"}});
});
