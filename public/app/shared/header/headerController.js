app.controller("headerController", ['$rootScope', '$scope', '$uibModal', '$log', 'getNotificationService', 'viewAllNotificationService', 'viewOneNotificationService', 'rejectGroupInviteService', 'acceptGroupInviteService', '$state', function($rootScope, $scope, $uibModal, $log, getNotificationService, viewAllNotificationService, viewOneNotificationService, rejectGroupInviteService, acceptGroupInviteService, $state) {
    $scope.showRegForm = false;
    $scope.animationsEnabled = true;
    $scope.showDropMenu = false;
    $scope.skip = 0;
    $scope.showAllNotificationList = [];
    $scope.showDp = true;
    $scope.avatar_url = "";
    $scope.showSeeMore = true;

    $scope.rejectId = "";
    //Code for notification section
    $scope.notificationloader = false;

    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        $scope.avatar_url = APP.currentUser.local.avatar_url;
        $scope.userName = APP.currentUser.local.name;
        if ($scope.avatar_url === null || $scope.avatar_url === "undefined" || $scope.avatar_url === "") {
            $scope.showDp = false;
        }
    }
    $rootScope.$on("updateHeaderDp", function(event, args) {
        $scope.avatar_url = args.avatar_url;

    });
    $rootScope.$on("updateName", function(event, args) {
        $scope.userName = args.name;

    });
    // show registration form
    $scope.showRegisterForm = function() {
        $scope.$broadcast("CallParamsCloseMethod", {});
        $scope.showRegForm = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/register/registerView.html',
            controller: 'registerController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });

    };
    $scope.menuAccount = function() {
        $scope.showDropMenu = true;
    };
    $scope.cancel = function() {
        $uibModal.dismiss('cancel');
    };
    // $on in parent method for show login form
    $scope.$on("ShowModalLogin", function() {
        $scope.showRegisterForm();
    });
    $rootScope.$on("ShowModalLogin", function() {
        $scope.showRegisterForm();
    });
    $rootScope.$on("callHeaderController", function(event, value) {
        $scope.backToLessons = value;
    });
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    // Function for get the all notification in list
    $scope.getAllNotificationShow = function(loadMore) {

        if(!loadMore && $scope.allTotal > 0){
          return;
        }

        var limit_start = $scope.showAllNotificationList.length;
        var limit = 5;
         if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && APP.currentUser._id) {

            $scope.listResponse = 0;
            $scope.notificationloader = true;
            getNotificationService.query({
                id: APP.currentUser._id,
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.notificationloader = false;
                    $scope.getNotificationShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.showAllNotificationList = $scope.showAllNotificationList.concat(data.result.notifications);
                    if($scope.allTotal <= $scope.showAllNotificationList.length)
                        $scope.showSeeMore = false;
                    else
                        $scope.showSeeMore = true;
                } else {
                    $scope.notificationloader = false;
                    $scope.getNotificationShow = 0;
                    $scope.showAllNotificationList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
                console.log("error", error);
            });
        }
    };
    $scope.getAllNotificationShow();
    $rootScope.$on("updateHeader", function(event, args) {
        $scope.avatar_url = args.user.local.avatar_url;
        $scope.userName = args.user.local.name;
        $scope.showAllNotificationList = [];
        $scope.getAllNotificationShow(true);

    });

    $scope.seenAllMessage = function() {
        if ($scope.getTotalNotification > 0) {
            viewAllNotificationService.update({
                id: APP.currentUser._id
            }, function(data) {
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.$emit("callTotalNotification", {});
                }
            });
        }
    };
    $scope.removeNotification = function(index, grpId, grpOwner, notiId) {
        $scope.showAllNotificationList.splice(index, 1);
        $scope.allTotal = $scope.allTotal - 1;
        var opts = {};
        opts.group = grpId;
        opts.groupOwner = grpOwner;
        opts.notificationId = notiId;
        rejectGroupInviteService.update({
            id: grpId
        }, opts, function(data) {});

        if($scope.allTotal === 0)
            $("#testing-dropdown").dropdown("toggle");
    };

    $scope.acceptNotification = function(index, grpId, grpOwner, notiId) {
        $scope.showAllNotificationList.splice(index, 1);
        $scope.allTotal = $scope.allTotal - 1;
        //$scope.rejectId = grpId;
        var opts = {};
        opts.group = grpId;
        opts.groupOwner = grpOwner;
        opts.notificationId = notiId;
        acceptGroupInviteService.update({
            id: grpId
        }, opts, function(data) {});

        if($scope.allTotal === 0)
            $("#testing-dropdown").dropdown("toggle");
    };

    $scope.viewLikeActivity = function(index, id, notiId) {
        $scope.getNotificationShow = true;
        $scope.showAllNotificationList[index].status = 'seen';
        $("#activity-noti-" + index).removeClass("unread-noti").addClass("read-noti");
        $scope.$broadcast('callViewActivityLike', {
            id: id
        });
        viewOneNotificationService.update({
            id: APP.currentUser._id,
            nid: notiId
        }, function(data) {}, function() {});
    };

    $scope.viewGroup = function(index, notificationList, notiId) {
        $("#testing-dropdown").dropdown("toggle");
        $scope.getNotificationShow = true;
        $scope.showAllNotificationList[index].status = 'seen';
        $("#activity-noti-" + index).removeClass("unread-noti").addClass("read-noti");
        $rootScope.$emit('showMyGroups');
          viewOneNotificationService.update({
            id: APP.currentUser._id,
            nid: notiId
        }, function(data) {}, function() {});
    };

    $scope.loadMoreNoti = function($event) {
        $event.stopPropagation();
        $scope.getAllNotificationShow(true);
    };
    $scope.stopEvent = function($event) {
      $event.stopPropagation();
    };

    $scope.logoClick = function() {
        $state.go('home', {}, { reload: true });
    };

}]);
