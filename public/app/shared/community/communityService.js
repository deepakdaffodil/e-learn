app.factory('getCommunityService', function ($resource) {
	return  $resource(APP.endpoints.createActivity,{},{query: { method: "GET", isArray: false }});
});
app.factory('getViewActivityService', function ($resource) {
	return  $resource(APP.endpoints.viewActivity,{},{query: { method: "GET", isArray: false }});
});
app.factory('likeActivityService', function($resource){
	return $resource(APP.endpoints.likeActivity, { id: '@id'}, {update: { method: "PUT"}});
});
app.factory('unlikeActivityService', function($resource){
	return $resource(APP.endpoints.unlikeActivity, { id: '@id'}, {update: { method: "PUT"}});
});
app.factory('removeActivityService', function ($resource) {
	return  $resource(APP.endpoints.deleteActivity,{id: '@id'},{remove: { method: "DELETE"}});
});
app.factory('editActivityService', function ($resource) {
	return  $resource(APP.endpoints.editActivity,{id: '@id'},{query: { method: "PUT"}});
});
app.factory('viewLikeUserService', function ($resource) {
	return  $resource(APP.endpoints.getListLike,{}, {query: { method: "GET",  isArray: false }});
});
app.factory('notificationShareService', function ($resource) {
	return  $resource(APP.endpoints.notiShare);
});
app.factory('moduleActivitiesService', function ($resource) {
	return  $resource(APP.endpoints.moduleActivities,{id : '@id', moduleId : '@moduleId'}, {query :{method : "GET", isArray :false}});
});
app.factory('getActivityCommentService', function ($resource) {
	return  $resource(APP.endpoints.getActivityComments,{},{query: { method: "GET", isArray: false }});
});
app.factory('getlessonDetailService', function ($resource) {
	return  $resource(APP.endpoints.lessonDetail,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getModuleDetailService', function ($resource) {
	return  $resource(APP.endpoints.actionModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('allActivitiesByModule', function ($resource) {
	return  $resource(APP.endpoints.allActivitiesByModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('byTagNameService', function ($resource) {
	return  $resource(APP.endpoints.byTagNameService,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('byUserNameService', function ($resource) {
	return  $resource(APP.endpoints.byUserNameService,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('followUserService', function ($resource) {
	return  $resource(APP.endpoints.followUser,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('unfollowUserService', function ($resource) {
	return  $resource(APP.endpoints.unfollowUser,{id: '@id'},{update: { method: "PUT"}});
});
