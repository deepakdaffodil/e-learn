
app.controller("communityController", ['$scope', '$rootScope', 'getCommunityService', '$uibModal', 'getViewActivityService', 'moduleActivitiesService', 'removeActivityService', 'getActivityCommentService', '$timeout', 'getlessonDetailService', 'getModuleDetailService', 'allActivitiesByModule', '$location','byTagNameService','byUserNameService','followUserService', function($scope, $rootScope, getCommunityService, $uibModal, getViewActivityService, moduleActivitiesService, removeActivityService, getActivityCommentService, $timeout, getlessonDetailService, getModuleDetailService, allActivitiesByModule, $location, byTagNameService, byUserNameService, followUserService) {
    $scope.activityList = [];
    $scope.mycommListShow = false;
    $scope.comments = '';
    $scope.createdAt = [];
    $scope.lessonsz = [];
    $scope.modulesz = [];
    $scope.id = null;
    $scope.url = "";
    $scope.lastLengthComm = -1;
    $scope.lastLengthComm1 = -1;
    $scope.moduleIndex = 0;
    $scope.isLast = null;
    $scope.index = '';
    $scope.name = allActivitiesByModule;
    $scope.currentIndex = null;

    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $rootScope.activeSection = "community";
    $rootScope.myTabs = false;
    // if (!$rootScope.isLast)
    //     $rootScope.activeModule[$rootScope.moduleIndex] = true;
    // else
    //     $rootScope.activeModuleLast[$rootScope.moduleIndex] = true;

    $scope.listResponse = 1;
    $scope.allTotal = 0;

    $scope.showCommunityList = function() {

        if ($scope.lastLengthComm >= $scope.activityList.length && $scope.lastLengthComm != 8 && $scope.activityList.length != 8 && $scope.activityList.length !==0 ){
          return;
        }

        $scope.commloader = true;
        var limit_start = $scope.activityList.length;
        $scope.lastLengthComm = limit_start;
        var limit = 8;
        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse) {
            $scope.listResponse = 0;
            $scope.commloader = true;
            getCommunityService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                // $scope.commloader = false;
                if (data.statusCode === 200 && data.message === 'ok')
                    if (data.result.activities.length > 0) {
                        $scope.ago(data.result.activities);
                        $scope.commloader = false;
                        $scope.mycommListShow = false;
                        $scope.allTotal = data.result.totalCount;
                        $scope.activityList = $scope.activityList.concat(data.result.activities);
                    } else {
                        $scope.commloader = false;
                        $scope.mycommListShow = true;
                        $scope.activityList = [];
                        $scope.allTotal = 0;
                    }
            }, function(error) {});
        } else
            $scope.commloader = false;
    };

    $scope.ago = function(activities) {
        angular.forEach(activities, function(activity, key) {
            $timeout(function() {
                var a = new Date(activity.createdAt);
                var b = new Date();
                var agoSpan = $scope.getActualTime(b - a);
                $scope.createdAt.push(agoSpan);
            }, 0);

        });
    };

    $scope.getActualTime = function(t) {
        var showSpan = {};
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000,
            d = Math.floor(t / cd),
            h = Math.floor((t - d * cd) / ch),
            m = Math.round((t - d * cd - h * ch) / 60000),
            pad = function(n) {
                return n < 10 ? '0' + n : n;
            };
        if (m === 60) {
            h++;
            m = 0;
        }
        if (h === 24) {
            d++;
            h = 0;
        }
        if (d === 0) {
            showSpan.numberDate = d;
            showSpan.numberSpan = "day";
            return showSpan;
        } else if (d > 0 && d < 30) {
            if (d == 1) {
                showSpan.numberDate = d;
                showSpan.numberSpan = "day";
                return showSpan;
            } else {
                showSpan.numberDate = d;
                showSpan.numberSpan = "days";
                return showSpan;
            }
        } else if (d > 30 || d == 30) {
            var months = parseInt(d / 30);
            if (months > 12 || months == 12) {
                var years = parseInt(months / 12);
                if (years == 1) {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "year";
                    return showSpan;
                } else {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "years";
                    return showSpan;
                }
            } else {
                if (months == 1) {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "month";
                    return showSpan;
                } else {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "months";
                    return showSpan;
                }

            }

        }
    };

    $scope.allTotal1 = 0;
    $scope.listResponse1 = 1;

    $scope.getAllActivities = function(args) {
        if ($scope.lastLengthComm1 >= $scope.activityList.length && $scope.activityList.length !== 0) {
          return;
        }
        if (args !== null && args !== undefined) {
            $scope.module = JSON.parse(JSON.stringify(args.module));
        } else
            args = {
                module: $scope.module
            };

        // $scope.lessonsz = [];
        // $scope.modulesz = [];
        var limit_start = $scope.activityList.length;
        $scope.lastLengthComm1 = limit_start;
        var limit = 8;
        $scope.commloader1 = true;

        if ((($scope.allTotal1 > limit_start) || $scope.allTotal1 === 0) && $scope.listResponse1) {
            $scope.listResponse1 = 0;
            var opts = {};
            opts.skip = limit_start;
            opts.limit = limit;

            $scope.name.query({
                id: args.module._id,
                skip: limit_start,
                limit: limit
            }, opts, function(data) {
                // $scope.commloader1 = false;
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.commloader1 = false;
                    $scope.listResponse1 = 1;
                    if (data.result.activities.length > 0) {
                        $scope.mycommListShow = false;
                        $scope.ago(data.result.activities);
                        $scope.commloader1 = false;
                        $scope.mycommListShow = false;
                        $scope.allTotal1 = data.result.totalCount;
                        $scope.activityList = $scope.activityList.concat(data.result.activities);
                    } else {
                        $scope.commloader1 = false;
                        $scope.mycommListShow = true;
                        $scope.activityList = [];
                        $scope.allTotal1 = 0;
                    }
                }

            }, function(error) {});
        } else {
            $scope.commloader1 = false;
        }

    };

    if ((!$rootScope.moduleSelected || $rootScope.isAdminLoggedIn) && !$rootScope.filterActive) {
        $scope.showCommunityList();
     }else {
       if(!$rootScope.filterActive) {
          $scope.getAllActivities();
       }

     }

    $rootScope.$on('updatingLikes', function(event, args) {
        //$scope.activityList = [];
        //$scope.getAllActivities();
    });

    // if ($location.url() == '/home/community' && $rootScope.isLoggedIn) {
    //     $scope.getAllActivities();
    // }
    $rootScope.$on('updateMyCommunity', function(event, args) {
        $scope.activityList = [];
        $scope.module = args.module;
        //$scope.getAllActivities();
    });

    // function for get the community

    $rootScope.$on("callMyCommunity", function(event, args) {

        $scope.showCommunityList();
    });

    $rootScope.$on("callViewActivityFromHome", function(event, args) {
        $scope.viewActivity(args.activityId);
    });

    // Open modal for view full detail of activity in my community
    $scope.viewActivity = function(index, activityId, videoId) {
        $scope.currentIndex = index;
        $scope.activityList = $scope.activityList;
        getActivityCommentService.query({
            id: activityId
        }, function(data) {

            $scope.comments = data.result.totalCount;
        });

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            size: 'lg',
            templateUrl: 'app/shared/activityDetail/viewSharedActivity.html',
            controller: 'viewSharedController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                activityId: function() {
                    return angular.copy(activityId);
                }
            }
        });
    };

    // $on on click on like
    $scope.$on('callViewActivityLike', function(event, args) {
        $scope.id = args.id;

        $scope.viewActivity($scope.id);
    });

    // Scrolling in page
    $scope.loadMoreCommunity = function() {
        if ($rootScope.activeSection !== 'community' && ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)){
            return;
        }

        if ($rootScope.isAdminLoggedIn) {
            $scope.showCommunityList();
        } else if ($rootScope.isLoggedIn && $rootScope.moduleSelected) {
          $scope.getAllActivities();
          // if($rootScope.moduleSelected ) {
          //   $scope.getAllActivities();
          // }else {
          //   $scope.showCommunityList();
          // }
        //   if($rootScope.activeModuleLast.length > 0 || $rootScope.activeModule.length > 0)
        //      $scope.getAllActivities();
        //  else
        //      $scope.showCommunityList();

        } else {
          if(!$rootScope.filterActive)
            $scope.showCommunityList();
            else
              $scope.getAllActivities();

        }
    };

    $scope.confirmModal = function(id, url, $event, index) {
        $event.stopPropagation();
        $scope.id = id;
        $scope.url = url;
        $scope.mySpaceList = $scope.mySpaceList;
        $scope.activityList = $scope.activityList;
        $scope.index = index;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return "Are you sure to delete this activity?";
                },
                item: function() {
                    return 'delete-activity';
                }
            }
        });
    };

    $scope.goToModule = function ($event, module) {
        $event.stopPropagation();
        if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)
            $rootScope.$emit("callMyModule", { module: module });
        else
            $rootScope.$broadcast('ShowModalLogin', {});
    };

    $rootScope.$on("whenNoTag", function (event, args) {
      $scope.listResponse = 1;
      $scope.allTotal = 0;
      $scope.activityList = [];
      $scope.showCommunityList();

    });

    $rootScope.$on("byModuleName", function (event, args) {
      $scope.listResponse1 = 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = allActivitiesByModule;
      $scope.getAllActivities(args);
    });

    $rootScope.$on("byUserName", function (event, args) {
      $scope.listResponse1 = 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = byUserNameService;
      $scope.getAllActivities(args);
    });

    $rootScope.$on("byTagName", function (event, args) {
      $scope.listResponse1= 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = byTagNameService;
      $scope.getAllActivities(args);
    });

}]);
