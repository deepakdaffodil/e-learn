app.factory('getspaceService', function ($resource) {
	return  $resource(APP.endpoints.mySpace,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getlessonDetailService', function ($resource) {
	return  $resource(APP.endpoints.lessonDetail,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getModuleDetailService', function ($resource) {
	return  $resource(APP.endpoints.actionModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('removeActivityService', function ($resource) {
	return  $resource(APP.endpoints.deleteActivity,{id: '@id'},{remove: { method: "DELETE"}});
});
