app.controller("myspaceController", ['$rootScope', '$scope', 'getspaceService', 'getViewActivityService', '$uibModal', 'removeActivityService', '$timeout', 'getlessonDetailService', 'getModuleDetailService', function($rootScope, $scope, getspaceService, getViewActivityService, $uibModal, removeActivityService, $timeout, getlessonDetailService, getModuleDetailService) {
    $scope.spaceloader = true;
    $scope.mySpaceList = [];
    $scope.url = "";
    $scope.id = "";
    $scope.numberDate = "";
    $scope.numberSpan = "";
    $scope.createdAt = [];
    $scope.lessonsz = [];
    $scope.modulesz = [];
    $scope.index = 0;
    $scope.mySpaceMsg = true;
    $scope.lastLength = -1;
    $rootScope.activeSection = "space";
    $rootScope.myTabs = true;
    $scope.currentIndex = null;

    // function for get the community
    $scope.showSpaceList = function() {
        if ($scope.lastLength >= $scope.mySpaceList.length)
            return;

        var limit_start = $scope.mySpaceList.length;
        $scope.lastLength = limit_start;
        var limit = 8;
        var opts = {};
        opts.id = APP.currentUser._id;
        opts.limit = limit;
        opts.skip = limit_start;

        $scope.lessonsz = [];
        $scope.modulesz = [];

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0)) {
            $scope.spaceloader = true;
            getspaceService.query(opts, function(data) {
                $scope.spaceloader = false;
                if (data.statusCode === 200 && data.message === 'ok') {
                    if (data.result.activities.length > 0) {
                        $scope.ago(data.result.activities);
                        $scope.spaceloader = false;
                        $scope.mySpaceList = $scope.mySpaceList.concat(data.result.activities);
                        $scope.allTotal = data.result.totalCount;
                        $scope.mySpaceMsg = false;
                    } else {
                        $scope.spaceloader = false;
                        $scope.allTotal = 0;
                        $scope.mySpaceList = [];
                    }
                }
            }, function(error) {});
        } else
            $scope.spaceloader = false;
    };

    $rootScope.$on('updatingLikes', function(event, args) {
        $scope.mySpaceList = [];
        $scope.showSpaceList();
    });

    $scope.ago = function(activities) {
        angular.forEach(activities, function(activity, key) {
            $timeout(function() {
                var a = new Date(activity.createdAt);
                var b = new Date();
                var agoSpan = $scope.getActualTime(b - a);
                $scope.createdAt.push(agoSpan);
            }, 0);

        });
    };

    $scope.getActualTime = function(t) {
        var showSpan = {};
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000,
            d = Math.floor(t / cd),
            h = Math.floor((t - d * cd) / ch),
            m = Math.round((t - d * cd - h * ch) / 60000),
            pad = function(n) {
                return n < 10 ? '0' + n : n;
            };
        if (m === 60) {
            h++;
            m = 0;
        }
        if (h === 24) {
            d++;
            h = 0;
        }
        if (d === 0) {
            showSpan.numberDate = d;
            showSpan.numberSpan = "day";
            return showSpan;
        } else if (d > 0 && d < 30) {
            if (d == 1) {
                showSpan.numberDate = d;
                showSpan.numberSpan = "day";
                return showSpan;
            } else {
                showSpan.numberDate = d;
                showSpan.numberSpan = "days";
                return showSpan;
            }
        } else if (d > 30 || d == 30) {
            var months = parseInt(d / 30);
            if (months > 12 || months == 12) {
                var years = parseInt(months / 12);
                if (years == 1) {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "year";
                    return showSpan;
                } else {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "years";
                    return showSpan;
                }
            } else {
                if (months == 1) {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "month";
                    return showSpan;
                } else {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "months";
                    return showSpan;
                }

            }

        }
    };

    $scope.showSpaceList();
    $scope.$on("CallUpdatedSpaceMethod", function() {
        $scope.showSpaceList();
    });
    // Open modal for view full detail of activity in my space
    $scope.viewActivitySpace = function(activityId, index) {
        $scope.currentIndex = index;
        $scope.activityList = $scope.mySpaceList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            size: 'lg',
            templateUrl: 'app/shared/activityDetail/viewSharedActivity.html',
            controller: 'viewSharedController',
            scope: $scope,
            resolve: {
                activityId: function() {
                    return angular.copy(activityId);
                }
            }
        });
    };
    $scope.deleteMyActivity = function(index, id, url, $event) {
        $event.stopPropagation();
        $scope.id = id;
        $scope.url = url;
        $scope.index = index;
        $scope.mySpaceList = $scope.mySpaceList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this activity?";
                },
                item: function() {
                    return "delete-activity-space";
                }
            }
        });

    };

    $scope.loadMoreSpace = function() {
        if ($rootScope.activeSection !== 'space')
            return;

        $scope.showSpaceList();
    };

}]);
