app.controller("dialogController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'message', function($rootScope, $scope, $timeout, $uibModalInstance, $log, message) {
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };
    if (message == "Your activity is submitted successfully") {
        $scope.message = message;
        $scope.confirm = false;
        $timeout(function() {
            $scope.closeModal();
        }, 500);
        $timeout(function() {
            $rootScope.$emit("moveToProgressBar", {});
        }, 1000);


    }
    $scope.message = message;
    $scope.confirm = false;
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

}]);
app.controller("confirmController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'message', 'deluserService', 'item', 'deleteAccountService', '$state', 'deleteLessonService', 'removeActivityService', 'moduleDelService', 'deleteCommentService', 'delGrpMemberService', 'delGroupService', 'unfollowService', function($rootScope, $scope, $timeout, $uibModalInstance, $log, message, deluserService, item, deleteAccountService, $state, deleteLessonService, removeActivityService, moduleDelService, deleteCommentService, delGrpMemberService, delGroupService, unfollowService) {

    $scope.item = item;
    $scope.message = message;
    $scope.confirm = true;
    $scope.closeModal = function(param) {
        $uibModalInstance.dismiss('cancel');
        // param 1 for press ok button
        if (param === 1) {
            if ($scope.item === 'users') {
                deluserService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.userList.splice($scope.index, 1);
                    }
                });
            }
            if ($scope.item === 'user') {
                deleteAccountService.delete({
                    id: APP.currentUser._id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok' && data.result.message === 'user deleted') {
                        localStorage.removeItem('loggedInUser');
                        $rootScope.isLoggedIn = false;
                        $state.go('home', {}, {
                            reload: true
                        });
                    } else {
                        $scope.errorDelete = true;
                        $scope.delError = frontendSettings.delError;
                        $timeout(function() {
                            $scope.errorDelete = false;
                        }, 2000);
                    }
                });
            }
            if ($scope.item === 'delete-lesson') {
                deleteLessonService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.lesson[$scope.parentId].splice($scope.index, 1);
                        if ($scope.lesson[$scope.parentId].length === 0) {
                            $scope.isnoLesson[$scope.parentId] = true;
                        }
                    } else {
                        $scope.errorDelete = true;
                        $scope.delError = frontendSettings.delError;
                        $timeout(function() {
                            $scope.errorDelete = false;
                        }, 2000);
                    }
                });
            }
            if ($scope.item === 'delete-activity') {
                removeActivityService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        //$scope.mySpaceList.splice($scope.index, 1);
                        $scope.activityList.splice($scope.index, 1);
                        if ($scope.url) {
                            var res = $scope.url.split("com/");
                            var bucketInstance = new AWS.S3();
                            var params = {
                                Bucket: 'mindmaxdaffo',
                                Key: res[1]
                            };
                            bucketInstance.deleteObject(params, function(err, data) {});
                        }
                    }

                });
            }
            if ($scope.item === 'delete-activity-space') {
                removeActivityService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.mySpaceList.splice($scope.index, 1);
                        if ($scope.url) {
                            var res = $scope.url.split("com/");
                            var bucketInstance = new AWS.S3();
                            var params = {
                                Bucket: 'mindmaxdaffo',
                                Key: res[1]
                            };
                            bucketInstance.deleteObject(params, function(err, data) {});
                        }
                    }

                });
            }
            if ($scope.item === 'delete-module') {
                moduleDelService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.showAllModuleList.splice($scope.index, 1);
                    }
                });
            }
            if ($scope.item === 'delete-comment') {
                deleteCommentService.delete({
                    id: $scope.activityId,
                    commentId: $scope.commentId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.commentList.splice($scope.index, 1);
                        $scope.loader[$scope.index] = true;

                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-grpMember') {
                var opts = {};
                opts.user = $scope.userId;
                delGrpMemberService.delete({
                    id: $scope.delMemberId,
                    mid: $scope.userId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.grpMembers.splice($scope.index, 1);

                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-follower') {
                unfollowService.update({
                    id: $scope.id,
                    follower: $scope.follower
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        if ($scope.follower)
                            $scope.followers.splice($scope.index, 1);
                        else
                            $scope.followings.splice($scope.index, 1);
                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-group') {
                delGroupService.delete({
                    id: $scope.grpId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.myGroupsList.splice($scope.index, 1);
                        $rootScope.$emit('groupsComingInCreatActivity', {});
                        if ($scope.myGroupsList.length === 0 || $scope.myGroupsList.length < 1) {
                            $rootScope.$emit('noGroupsPressent', {});
                            $scope.hasGroups = false;
                        }
                    }
                }, function(error) {});
            }

            if ($scope.item === 'post-without-image') {
                $scope.postActivity();
            }
        } else { // param 2 for press ok button
            angular.element('#fadein').removeClass('color-overlay');
        }
    };

}]);
