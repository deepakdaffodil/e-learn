app.controller("lastComponentController", ['$rootScope', '$scope', '$uibModal', '$log', 'moduleActivitiesService', function($rootScope, $scope, $uibModal, $log, moduleActivitiesService) {
    $scope.module = {};
    $scope.allActivities = [];
    $scope.commloader = true;
    $scope.noActivities = false;
    $rootScope.$on("callLastComponent", function(event, args) {
        $scope.module = args.module;
        $scope.getAllActivitiesz();
    });
    $scope.getAllActivitiesz = function() {
        $scope.commloader = true;
        moduleActivitiesService.query({
            id: APP.currentUser._id,
            moduleId: $scope.module._id
        }, function(data) {
            if (data.statusCode == 200 && data.message == "ok") {
                if (data.result.totalCount === 0) {
                    $scope.commloader = false;
                    $scope.noActivities = true;
                } else {
                    $scope.commloader = false;
                    $scope.allActivities = data.result.activities;
                }
            }
        });
    };
}]);
