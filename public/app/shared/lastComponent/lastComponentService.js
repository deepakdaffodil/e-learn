app.factory('moduleActivitiesService', function ($resource) {
	return  $resource(APP.endpoints.moduleActivities,{id : '@id', moduleId : '@moduleId'}, {query :{method : "GET", isArray :false}});
});
