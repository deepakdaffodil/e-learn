app.directive('lastComponent', function () {
	return {
		templateUrl: 'app/shared/lastComponent/lastComponentView.html',
		restrict: 'E',
		controller: 'lastComponentController'
	};
});
