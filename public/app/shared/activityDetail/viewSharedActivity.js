app.controller('viewSharedController', ['$scope', '$uibModalInstance', 'getViewActivityService', 'editActivityService', 'likeActivityService', 'unlikeActivityService', 'activityId', '$uibModal', '$timeout', 'notificationShareService', '$rootScope', 'followUserService', 'unfollowUserService',
    function($scope, $uibModalInstance, getViewActivityService, editActivityService, likeActivityService, unlikeActivityService, activityId, $uibModal, $timeout, notificationShareService, $rootScope, followUserService, unfollowUserService) {
        //fab
        $scope.url = 'http://google.com';
        $scope.text = 'testing share';
        $scope.title = 'title1';
        $scope.callback = function(response) {
            if (response !== undefined) {
                if (response.post_id) {
                    var opts = {};
                    opts.activity = activityId;
                    opts.activityOwner = $scope.viewDetailActivity.user._id;
                    opts.shareWith = "facebook";
                    notificationShareService.save(opts, function(data) {});
                }
            }
        };
        $scope.isLike = false;
        $scope.isUnlike = false;
        $scope.editDescription = false;
        $scope.editDescriptionValue = '';
        $scope.activityFormEditSubmitted = false;
        $scope.showLike = false;
        $scope.showEdit = false;
        $scope.fixedComment = false;
        $scope.viewDetailActivity = {};
        $scope.followText = 'Follow';
        $scope.followState = 'hide';
        $scope.activityLoader = true;

        $scope.callViewActivityService = function (callId) {
            $scope.activityLoader = true;
            var opts = {};
            opts.id = callId;

            getViewActivityService.query(opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.viewDetailActivity = data.result;

                    if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn)
                        $scope.followState = 'hide';
                    else if ($scope.viewDetailActivity.user._id === APP.currentUser._id)
                        $scope.followState = 'hide';
                    else if (APP.currentUser.local.following.filter(function (v) {
                        return v === $scope.viewDetailActivity.user._id;
                    }).length > 0) {
                        $scope.followText = 'Unfollow';
                        $scope.followState = 'show';
                    }
                    else {
                        $scope.followText = 'Follow';
                        $scope.followState = 'show';
                    }

                    $scope.getLike = data.result.likes.indexOf(APP.currentUser._id);
                    if ($scope.getLike === -1) {
                        $scope.showLike = false;
                    } else {
                        $scope.showLike = true;
                    }
                    if (APP.currentUser._id === data.result.user._id) $scope.showEdit = true;
                    $scope.likedPeople = data.result.likes.length;
                    $scope.activityLoader = false;
                }
            });
        };
        $scope.callViewActivityService(activityId);

        //$scope.showLikeView = true;
        $scope.closeModal = function() {
            $scope.viewDetailActivity = {};
            $scope.editDescription = false;
            $scope.editDescriptionValue = '';
            $scope.$emit("CallUpdatedCommunityMethod", {});
            $scope.$emit("CallUpdatedSpaceMethod", {});
            $uibModalInstance.dismiss('cancel');
        };

        /*$timeout(function(){
        $scope.loadMoreComments();
        }, 3000);  */
        $scope.$on("CallHeightMethod", function() {
            $scope.loadMoreComments();
        });
        $scope.loadMoreComments = function() {
            var elmnt = document.getElementById("getHeight");
            var txt = elmnt.scrollHeight;
            if (txt > 365) {
                $scope.fixedComment = true;
            }
        };


        // Like Activity
        $scope.likeActivity = function(id, ownerId, likedPeople) {
            var opts = {};
            opts.activityOwner = ownerId;
            likeActivityService.update({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.isLike = true;
                    $scope.showLike = true;
                    $scope.isUnlike = false;
                    $scope.likedPeople = likedPeople + 1;
                    $scope.activityList[$scope.currentIndex].likes.push(APP.currentUser);
                } else {}
            }, function(error) {});
        };
        //Unlike Activity
        $scope.unlikeActivity = function(id, owner_id, likedPeople) {
            var opts = {};
            opts.activityOwner = owner_id;
            unlikeActivityService.update({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.isLike = false;
                    $scope.showLike = false;
                    $scope.isUnlike = true;
                    $rootScope.$emit('updatingLikes', {});
                    $scope.activityList[$scope.currentIndex].likes=$scope.activityList[$scope.currentIndex].likes.filter(function (v) {
                      return v !== APP.currentUser._id;
                    });

                    if ($scope.likedPeople > 0) {
                        $scope.likedPeople = likedPeople - 1;
                    } else {
                        $scope.likedPeople = likedPeople;
                    }
                } else {}
            }, function(error) {});
        };

        $scope.editDetail = function() {
            $scope.editDescription = true;
            $scope.editDescriptionValue = $scope.viewDetailActivity.description;
        };
        $scope.saveDetail = function(form, id) {
            $scope.activityFormEditSubmitted = true;
            //autosize(document.querySelectorAll('.editpostbox'));
            if ($scope.editDescriptionValue === undefined || $scope.editDescriptionValue === '') {
                focus('description');
                return false;
            }
            var opts = {};
            opts.description = $scope.editDescriptionValue;
            editActivityService.query({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.editDescription = false;
                    $scope.viewDetailActivity.description = $scope.editDescriptionValue;
                }
            });
        };
        $scope.cancel = function() {
            $scope.editDescriptionValue = '';
            $scope.editDescription = false;
        };
        $scope.openModal = function(id) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                size: 'lg',
                templateUrl: 'app/shared/activityDetail/detail.html',
                controller: 'findPeopleController',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    activityId: function() {
                        return id;
                    }
                }

            });
        };

        $scope.goToModuleViewSharedActivity = function($event, module) {
            $scope.closeModal();
            if ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)
                $rootScope.$emit("callMyModule", {
                    module: module
                });
            else
                $rootScope.$broadcast('ShowModalLogin', {});
        };

        $scope.follow = function() {
            $scope.followState = 'disable';

            if ($scope.followText === 'Follow') {
                followUserService.update({ id: $scope.viewDetailActivity.user._id }, {}, function (data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.followText = 'Unfollow';
                        APP.currentUser.local.following.push($scope.viewDetailActivity.user._id);
                        $scope.followState = 'show';
                    }
                }, function (error) {
                    $scope.followState = 'show';
                });
            }
            else if ($scope.followText === 'Unfollow') {
                unfollowUserService.update({ id: $scope.viewDetailActivity.user._id }, {}, function (data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.followText = 'Follow';
                        APP.currentUser.local.following = APP.currentUser.local.following.filter(function (v) {
                            return v !== $scope.viewDetailActivity.user._id;
                        });
                        $scope.followState = 'show';
                    }
                }, function (error) {
                    $scope.followState = 'show';
                });
            }
        };

        $scope.nextActivity = function () {
            $scope.currentIndex++;
            $scope.viewDetailActivity = $scope.activityList[$scope.currentIndex];
            $rootScope.$broadcast('refreshComments', {
                activityId: $scope.viewDetailActivity._id
            });
            $scope.callViewActivityService($scope.viewDetailActivity._id);
        };

        $scope.prevActivity = function () {
            $scope.currentIndex--;
            $scope.viewDetailActivity = $scope.activityList[$scope.currentIndex];
            $rootScope.$broadcast('refreshComments', {
                activityId: $scope.viewDetailActivity._id
            });
            $scope.callViewActivityService($scope.viewDetailActivity._id);
        };
}]);
app.controller("findPeopleController", ['$rootScope', '$uibModalInstance', '$scope', '$uibModal', '$log', 'viewLikeUserService', 'activityId', function($rootScope, $uibModalInstance, $scope, $uibModal, $log, viewLikeUserService, activityId) {
    $scope.listLikeUser = [];
    $scope.showUserLoader = true;
    viewLikeUserService.query({
        id: activityId
    }, function(data) {
        if (data.statusCode === 200 && data.message === "ok") {
            $scope.listLikeUser = data.result[0].likes;
            $scope.showUserLoader = false;
        }
    }, function() {});
    $scope.closeModal2 = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);
