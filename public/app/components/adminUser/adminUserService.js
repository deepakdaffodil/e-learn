app.factory('userService', function ($resource) {
   return  $resource(APP.endpoints.users,{},{query: { method: "GET", isArray: false }});
});
app.factory('deluserService', function ($resource) {
   return  $resource(APP.endpoints.delUser);
});
app.factory('updateUserService',function($resource){
	return $resource(APP.endpoints.editUser,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('userSearchService', function ($resource) {
   return  $resource(APP.endpoints.searchUser,{},{query: { method: "GET", isArray: false }});
});