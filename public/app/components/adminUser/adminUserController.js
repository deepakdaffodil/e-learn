app.controller("adminUserController", ['$rootScope', '$uibModal', '$scope', 'userService', 'deluserService', '$timeout', 'updateUserService', 'registerService', 'userSearchService', '$filter', function($rootScope, $uibModal, $scope, userService, deluserService, $timeout, updateUserService, registerService, userSearchService, $filter) {
    $scope.loader = false;
    $scope.userList = [];
    $scope.edit = [];
    $scope.formData = {};
    $scope.saveLoader = false;
    $scope.uploadLoader = [];
    $scope.noFile = [];
    $scope.location = [];
    $scope.showProgressBar = [];
    $scope.showErrEmail = false;
    $scope.getOriginalEmail = '';
    $scope.sortUser = false;
    $scope.change = [];
    $scope.localuser = null;

    //AWS
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    // calender scope define
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'dd-MMMM-yyyy';
    $scope.disabled = function(date, mode) {
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.maxDate = new Date();
    $scope.toggleMin = function() {
        $scope.minDate = null;
    };
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.toggleMin();
    $scope.userList = [];
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.getUserList = function() {
        var limit_start = $scope.userList.length;
        var limit = 20;
        $scope.loader = true;

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.loader = true;
            userService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.loader = false;
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.moduleLoader = false;
                    $scope.getUserShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.userList = $scope.userList.concat(data.result.users);
                } else {
                    $scope.moduleLoader = false;
                    $scope.getUserShow = false;
                    $scope.userList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
            });
        }
    };
    $scope.getUserList();
    $rootScope.$on('updateUserList',function(event, args){
      $scope.userList = [];
      $scope.getUserList();
    });

    $scope.deleteUser = function(index, id) {
        deluserService.delete({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.userList.splice(index, 1);
            }
        });
    };
    $scope.activeUserEdit = [];

    $scope.editUser = function(index, user) {
        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');
        $('#' + index).addClass('user-list-scroll');

        user.local.password = '';
        angular.forEach($scope.change, function(user, key) {
            if (user.changing) {
                user.real.local.name = user.username;
                user.real.local.email = user.useremail;
                user.real.local.dob = user.userdob;
                user.real.local.password = user.userpassword;
            }
        });

        var opts = {
            real: user,
            username: user.local.name,
            useremail: user.local.email,
            userdob: user.local.userdob,
            userpassword: user.local.password,
            index: index,
            changing: true
        };
        $scope.change[index] = opts;

        if (user.local.token) {
            $scope.openModal(frontendSettings.singlesignup);
        } else {
            $scope.username = user.local.name;
            $scope.useremail = user.local.email;
            $scope.userdob = user.local.dob;
            $scope.userpassword = user.local.password;
            $scope.edit[index] = true;
            $scope.activeUserEdit = [];
            $scope.activeUserEdit[index] = user._id;
            $scope.uploadLoader[index] = false;
            $scope.noFile[index] = false;
            $scope.location[index] = '';
            $scope.getOriginalEmail = user.local.email;
        }
    };

    $scope.cancelSave = function(index, user) {

        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');

        angular.forEach($scope.change, function(user, key) {
            if (user.index == index) {
                user.changing = false;
            }
        });
        user.local.name = $scope.username;
        user.local.email = $scope.useremail;
        user.local.dob = $scope.userdob;
        user.local.password = $scope.userpassword;

        $scope.edit[index] = false;
        //$scope.userList[index]  = $scope.localuser;
        $scope.activeUserEdit = [];
    };
    $scope.updateUser = function(index, user) {
        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');

        angular.forEach($scope.change, function(user, key) {
            if (user.index == index) {
                user.changing = false;
            }
        });
        $scope.submittedEditForm = true;
        if (user.local.name === null || user.local.name === '' || user.local.name === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
        if (user.local.email === null || user.local.email === '' || user.local.email === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
        if (user.local.dob === null || user.local.dob === '' || user.local.dob === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
            if (user.local.password.length !== 0 && user.local.password.length < 4) {
                $scope.openModal('Password length should be greater than 4');
                return false;
            }
        $scope.saveLoader = true;
        if ($scope.getOriginalEmail === user.local.email) {
            $scope.updateSuccess(index, user);
        } else {
            $scope.emailValidate(index, user);
        }


    };
    $scope.updateSuccess = function(index, user) {
        var opts = {};
        opts.name = user.local.name;
        opts.email = user.local.email;
        opts.dob = user.local.dob;
        opts.password = user.local.password;
        if ($scope.location[index] === '' || $scope.location[index] === null) {
            opts.avatar_url = user.local.avatar_url;
        } else {
            opts.avatar_url = $scope.location[index];
        }
        opts.rev = user.local.rev.toString();
        //$scope.saveLoader = false;
        updateUserService.update({
            id: user._id
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.activeUserEdit = [];
                $scope.userList[index] = data.result;
                $scope.location[index] = '';
                $scope.saveLoader = false;
            } else {
                $scope.openModal('Some errorOccured please try again');
                $scope.saveLoader = false;
            }
        });
    };
    //search user by name
    $scope.searchUser = function() {
        userSearchService.get({
            name: $scope.search
        }, function(data) {
            $scope.userList = data.result.users;
        });
    };

    // Open modal for alert messgae for validation
    $scope.openModal = function(msg) {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'dialogController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return msg;
                }
            }
        });
    };
    // Confirm box
    // Open modal for alert messgae for validation
    $scope.openConfirmModal = function(index, id) {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        $scope.index = index;
        $scope.id = id;
        $scope.userList = $scope.userList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return "Are you sure to delete this user?";
                },
                item: function() {
                    return 'users';
                }
            }
        });
    };
    // sort user
    $scope.sortMyUser = function() {
        $scope.sortUser = !$scope.sortUser;
        $scope.userList = $scope.userList;
        if ($scope.sortUser === true) {
            $scope.userList = $filter('orderBy')($scope.userList, 'local.name');
        } else {
            $scope.userList = $filter('orderBy')($scope.userList, 'createdAt').reverse();
        }
    };


    // Validate email
    $scope.emailValidate = function(index, user) {
        $scope.showErrEmail = false;
        $scope.errMsg = '';
        registerService.save({
            email: user.local.email
        }, function(data) {
            if (data.statusCode == 200 && data.message == "ok" && data.result.message == 'verified') {
                $scope.updateSuccess(index, user);
            } else if (data.statusCode == 422 && data.message == 'email already registered') {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.userregistered;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            } else if (data.statusCode == 422 && data.message == 'email should be registered with any email provider') {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.domainnotValid;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            } else {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.errorOccured;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            }
        });

    };
    // Upload image in profile data for  logged user

    $scope.upload = function(element, index) {
        $scope.noFile[index] = false;
        $scope.imag = '';
        $scope.showProgressBar[index] = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader[index] = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {
                $scope.uploadLoader[index] = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader[index] = false;
                        $scope.noFile[index] = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile[index] = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader[index] = false;
                        $scope.showProgressBar[index] = true;
                        //$scope.noFile = 'Image uploaded successfully';

                        $timeout(function() {
                            $scope.noFile[index] = '';
                        }, 3000);
                        $scope.location[index] = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile[index] = 'Please upload a image with jpeg and png format';
                $timeout(function () {
                  $scope.noFile[index] = '';
                }, 3000);
            }
        } else {
            $scope.noFile[index] = 'Please upload a file first';
            $timeout(function () {
              $scope.noFile[index] = '';
            }, 3000);
        }
    };
    //Hide preview of image
    $scope.hidePreview = function($index) {
        $scope.location[$index] = '';
        $scope.openeditImage = false;
        $scope.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };
    // Function call on load more
    $scope.loadMoreUsers = function() {
        $scope.getUserList();
    };
    //creating a standard user from admin
    $scope.createUser = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/createUser/createUserView.html',
            controller: 'createUserController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
}]);
