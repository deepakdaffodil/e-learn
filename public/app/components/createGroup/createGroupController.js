app.controller("createGroupController", ['$rootScope', '$scope', '$timeout', 'createGroupService', 'userService', '$uibModal', '$uibModalInstance', '$log', 'moduleId', '$filter', 'getInviteMembersService', function($rootScope, $scope, $timeout, createGroupService, userService, $uibModal, $uibModalInstance, $log, moduleId, $filter, getInviteMembersService) {
    $scope.createFormSubmitted = false;
    $scope.group = {};
    /*$scope.group.name = "hello";*/
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLesson = false;
    $scope.editFormSubmitted = false;
    $scope.emails = [];
    $scope.values = [];
    $scope.log = [];
    $scope.chooseFile = false;
    $scope.groupImage = false;
    $scope.location = '';
    $scope.members = [];
    $scope.uploadLoader = false;
    $scope.list = [];
    $scope.showPlus = true;
    //$scope.avatar_url = "";

    $scope.createGroup = function() {
        $scope.createFormSubmitted = true;
        if ($scope.group.name === undefined || $scope.group.name === '' || $scope.group.name === null) {
            focus('gname');
            return false;
        }
        if ($scope.group.description === undefined || $scope.group.description === '' || $scope.group.description === null) {
            focus('gdescription');
            return false;
        }
        var opts = {};
        opts.name = $scope.group.name;
        opts.description = $scope.group.description;
        opts.owner = APP.currentUser._id;
        opts.avatar_url = $scope.location;
        opts.members = $scope.list;

        createGroupService.save(opts, function(data) {
            /*$scope.loader = false;*/
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.members = [];
                var object = {};
                object.group = data.result;
                $scope.hasGroups = true;
                $scope.createFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.group = {};
                $scope.location = "";
                $scope.hidePreview();
                $scope.myGroupsList.unshift(object);
                $rootScope.$emit('groupsComing', {});
                $rootScope.$emit('groupsComingInCreatActivity', {});
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                }, 1000);

            } else if (data.statusCode === 409 && data.message === "group name should be unique for a user") {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueGroup;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 3000);
            } else {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });
    };

    $scope.loadMatchingEmails = function(query) {
        return new Promise(function(resolve, reject) {
            getInviteMembersService.get({
                term: query
            }, function(data) {
                resolve(data.result);
            });
        });
    };

    $scope.tagAdded = function(tag) {
        $scope.list.push(tag.text);
    };

    $scope.tagRemoved = function(tag) {
        $scope.list.pop(tag.text);
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    //edit avatar con
    $scope.editAvatar = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/customBrowse/customBrowseView.html',
            controller: 'customBrowseController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return "";
                }
            }
        });
    };


    $scope.upload = function(element) {
        $scope.noFile = false;
        $scope.imag = '';
        $scope.showProgressBar = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {

                $scope.uploadLoader = true;
                $scope.chooseFile = false;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.showProgressBar = true;
                        $scope.groupImage = true;
                        $scope.chooseFile = false;
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.location = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
            }
        } else {
            $scope.noFile = 'Please upload a file first';
        }
    };

    $scope.hidePreview = function() {
        $scope.showPlus = true;
        $scope.location = '';
        $scope.openeditImage = false;

    };

    $rootScope.$on("uploadGroupAvatar", function(event, args) {
        $scope.groupImage = true;
        $scope.showPlus = false;
        $scope.location = args.avatar_url;
    });
}]);
