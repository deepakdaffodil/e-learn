/*app.directive('myGroups', function () {
return {
templateUrl: 'app/shared/group/groupView.html',
restrict: 'E',
controller: 'myGroupsController'
};
});*/

/*<ul>
	<li ng-repeat="email in emails" data-ng-bind="email"></li>
</ul>*/
/*<input-Dropdown></input-Dropdown>
*/app.directive('inputDropdown', function(userService) {

var template =
'<input ng-model="search" ng-change="searchMembers()" placeholder="INVITE MEMBERSss" tabindex="1" class="form-control">' +
'<div class="dropdown">' +
'<div ng-repeat="email in emails">' +
'<div ng-mousedown="select($event, value)">{{value}}</div>' +
'</div>' +
'</div>';

return {
restrict: 'E',
scope: {
ngModel: '=',
list: '=',
onSelect: '&'
},
template: template,
/*link: function(scope, element, attrs) {
element.addClass('input-dropdown');
scope.select = function(e, value) {
scope.ngModel = value;
scope.onSelect({$event: e, value: value});
};
}*/
link: function (scope, iElement, iAttrs, documentController) {
// Allow the controller here to access the document controller
scope.documentController = documentController;
},
controller: function ($scope) {
/*$scope.save = function (data) {
// Assuming the document controller exposes a function "getUrl"
var url = $scope.documentController.getUrl();

myService.saveComments(url, data).then(function (result) {
// Do something
});
};*/
	$scope.emails = [];
	$scope.searchMembers = function(){
		userService.get({name : $scope.search}, function(data) {
			var users = data.result.users;

			angular.forEach(users, function(user, key) {
				$scope.emails.push(user.local.email);
			});

		});
	};
	$scope.searchMembers();

}
};
});
