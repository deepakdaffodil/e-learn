
app.factory('createGroupService', function ($resource) {
   return  $resource(APP.endpoints.createGroup,{},{query: { method: "POST", isArray: false }});
});
app.factory('userService', function ($resource) {
   return  $resource(APP.endpoints.users,{},{query: { method: "GET", isArray: false }});
});
app.factory('getInviteMembersService', function ($resource) {
   return  $resource(APP.endpoints.getInviteMembers,{},{query: { method: "GET", isArray: false }});
});
