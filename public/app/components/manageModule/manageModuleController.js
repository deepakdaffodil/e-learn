app.controller("manageModuleController", ['$rootScope', '$scope', '$timeout', 'moduleAddService', '$uibModalInstance', '$log', 'moduleId', 'moduleDelService', 'updateModuleService', 'lessonService', 'createLessonService', 'getLessonService', 'editLessonService','$uibModal', function($rootScope, $scope, $timeout, moduleAddService, $uibModalInstance, $log, moduleId, moduleDelService, updateModuleService, lessonService, createLessonService, getLessonService, editLessonService, $uibModal) {
    $scope.createFormSubmitted = false;
    $scope.module = {};
    $scope.lesson1 = {};
    $scope.lesson = {};
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLessonView = false;
    $scope.editFormSubmitted = false;
    $scope.createLessonFormSubmitted = false;
    $scope.lessonResultMsg = false;
    $scope.pattern = "activity";
    $scope.getTotalLesson = function(id) {
        lessonService.get({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.totalLesson = data.result;
            } else {
                $scope.totalLesson = [];
            }
        });
    };
    // create module
    $scope.createModule = function() {
        $scope.createFormSubmitted = true;
        if ($scope.module.name === undefined || $scope.module.name === '' || $scope.module.name === null) {
            focus('mname');
            return false;
        }
        if ($scope.module.description === undefined || $scope.module.description === '' || $scope.module.description === null) {
            focus('mdescription');
            return false;
        }
        $scope.loader = true;
        var opts = {};
        opts.name = $scope.module.name;
        opts.description = $scope.module.description;
        $scope.createFormSubmitted = false;
        moduleAddService.save(opts, function(data) {
            $scope.loader = false;
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.showAllModuleList.unshift($scope.module);

                $scope.createFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.module = {}; //
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    //$scope.createFormSubmitted = false;
                    $scope.closeModal();
                    $scope.showCreateForm = false;
                    $scope.firstForm = false;
                }, 3000);
                //
            } else if (data.statusCode === 422 && data.message.message === "Module name must be unique") {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueModule;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            } else {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        $scope.$emit("CallModulesMethod", {});
        angular.element('#fadein').removeClass('color-overlay');
    };
    // Edit form
    $scope.editForm = function(id) {
        moduleDelService.get({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editFormData = data.result;
            }
        });
    };
    $scope.getLesson = function(moduleId) {
        getLessonService.query({
            id: moduleId
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.lesson1 = data.result;
            }
        });
    };
    if (moduleId) {
        $scope.getLesson(moduleId);
        $scope.editForm(moduleId);
        $scope.getTotalLesson(moduleId);
    }
    // update module
    $scope.updateModule = function() {
        $scope.editFormSubmitted = true;
        if ($scope.editFormData.name === undefined || $scope.editFormData.name === '') {
            focus('ename');
            return false;
        }
        if ($scope.editFormData.description === undefined || $scope.editFormData.description === '') {
            focus('edescription');
            return false;
        }
        $scope.loader = true;
        var opts = {};
        opts.name = $scope.editFormData.name;
        opts.description = $scope.editFormData.description;
        updateModuleService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.loader = false;
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.editSucessMessage;
                $scope.msgClass = 'success-green';
                $scope.module = {};
                $rootScope.$emit("CallModulesMethod", {});
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();

                }, 2000);
            } else {
                $scope.editFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.msgClass = '';
                }, 4000);
            }
        });
    };
    // open create modal form
    $scope.createModuleForm = function() {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
    };
    // open edit module form
    $scope.editModuleForm = function() {
        if (moduleId) {
            $scope.showCreateForm = false;
            $scope.showEditForm = true;
        }
    };
    // open create lesson form
    $scope.showCreateLessonForm = function() {
        switch ($scope.pattern) {
            case 'image':
                $scope.whenImage();
                break;
            case 'video':
                $scope.whenVideo();
                break;
            case 'text':
                $scope.whenText();
                break;
            case 'activity':
                $scope.whenActivity();
                break;
            default:
                $scope.whenActivity();
                break;
        }
        $scope.lesson = [];
    };

    $scope.whenImage = function() {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeImageView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });
    };
    $scope.whenVideo = function () {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeVideoView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });

    };
    $scope.whenText = function () {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeTextView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });
    };

    $scope.whenActivity = function () {
        $scope.createLessonView = true;
    };

    $scope.hideLessonForm = function() {
        $scope.createLessonView = false;
    };
    //for adding lessons
    $scope.createLesson = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.lesson.detail_description === undefined || $scope.lesson.detail_description === '') {
            focus('lessonDDes');
            return false;
        }
        if ($scope.lesson.activity === undefined || $scope.lesson.activity === '') {
            focus('lessonActivity');
            return false;
        }
        if ($scope.lesson.Keytakeaways === undefined || $scope.lesson.Keytakeaways === '') {
            focus('Keytakeaways');
            return false;
        }

        var opts = {};
        opts.module = moduleId;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.detail_description = $scope.lesson.detail_description;
        opts.activity = $scope.lesson.activity;
        opts.Keytakeaways = $scope.lesson.Keytakeaways;
        opts.type = $scope.pattern;
        //$scope.loader = true;
        createLessonService.save(opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.createLessonFormSubmitted = false;
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.totalLesson.push($scope.lesson);
                $rootScope.$emit("updatelessons", {
                    moduleIndex: $scope.moduleIndex,
                    moduleId: moduleId
                });

                $timeout(function() {
                    $scope.lessonResultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.hideLessonForm();
                }, 2000);
            } else if (data.statusCode === 422 && data.message === "lesson name must be unique") {
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueLesson;
                $scope.msgClass = 'error-red';
                $scope.createLessonFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 3000);
            } else {
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createLessonFormSubmitted = false;
                $timeout(function() {
                    $scope.lessonResultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });


    };
    $scope.lessonEditMsg = false;
    $scope.editLesson = function() {
        $scope.editLessonFormSubmitted = true;
        if ($scope.lesson1.name === undefined || $scope.lesson1.name === '') {
            focus('lessonEName');
            return false;
        }
        if ($scope.lesson1.basic_description === undefined || $scope.lesson1.basic_description === '') {
            focus('lessonEDes1');
            return false;
        }
        if ($scope.lesson1.detail_description === undefined || $scope.lesson1.description === '') {
            focus('lessonEDes2');
            return false;
        }
        if ($scope.lesson1.activity === undefined || $scope.lesson1.activity === '') {
            focus('lessonEActivity');
            return false;
        }

        if ($scope.lesson1.Keytakeaways === undefined || $scope.lesson1.Keytakeaways === '') {
            focus('EKeytakeaways');
            return false;
        }
        var opts = $scope.lesson1;
        editLessonService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editLessonFormSubmitted = true;
                $scope.lessonEditMsg = true;
                $scope.msgClass = 'success-green';
                $scope.sucessMsg = frontendSettings.editSucessMessage;
                $scope.showLesson($rootScope.lessonIndex, $rootScope.lessonId);
                $timeout(function() {
                    $scope.lessonEditMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                    $scope.secondForm = false;
                }, 2000);
            } else {}
        });


    };
    //selectLessonType
    $scope.selectLessonType = function(index) {
        switch (index) {
            case 0:
                $scope.pattern = "activity";
                break;
            case 1:
                $scope.pattern = "image";
                $scope.createLessonView = false;
                break;
            case 2:
                $scope.pattern = "video";
                $scope.createLessonView = false;
                break;
            case 3:
                $scope.pattern = "text";
                $scope.createLessonView = false;
                break;
            default:

        }
    };



}]);
