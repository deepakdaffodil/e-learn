app.factory('moduleAddService', function ($resource) {
   return  $resource(APP.endpoints.module);
});
app.factory('updateModuleService',function($resource){
	return $resource(APP.endpoints.actionModule,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('createLessonService', function ($resource) {
   return  $resource(APP.endpoints.createLesson,{},{query: { method: "POST", isArray: false }});
});
app.factory('getLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('editLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{update: { method: "PUT"}});
});
