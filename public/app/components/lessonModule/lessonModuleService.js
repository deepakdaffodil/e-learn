app.factory('moduleDelService', function ($resource) {
   return  $resource(APP.endpoints.actionModule, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});
app.factory('moduleSearchService', function ($resource) {
   return  $resource(APP.endpoints.searchModule,{},{query: { method: "GET", isArray: false }});
});
app.factory('deleteLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});
app.factory('moduleService', function ($resource) {
   return  $resource(APP.endpoints.module,{},{query: { method: "GET", isArray: false }});
});
