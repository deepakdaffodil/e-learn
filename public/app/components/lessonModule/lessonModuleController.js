app.controller("lessonModuleController", ['$rootScope', '$scope', '$timeout', 'getActionService', 'saveCompleteActionService', 'moduleService', 'lessonService', 'moduleDelService', '$uibModal', '$log', 'moduleSearchService', 'deleteLessonService', '$filter', function($rootScope, $scope, $timeout, getActionService, saveCompleteActionService, moduleService, lessonService, moduleDelService, $uibModal, $log, moduleSearchService, deleteLessonService, $filter) {
    $scope.modules = [];
    $scope.loader = [];
    $scope.lesson = [];
    $scope.isshowLesson = [];
    $scope.isnoLesson = [];
    $scope.showCreateForm = false;
    $scope.showEditForm = false;
    $scope.showAllModuleList = [];
    $scope.id = null;
    $scope.index = '';
    $scope.firstForm = true;
    $scope.secondForm = false;
    $scope.sortModules = false;
    // Get module list
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.moduleIndex = '';
    $scope.parentId = '';

    $scope.getModuleList = function() {
        var limit_start = $scope.showAllModuleList.length;
        var limit = 30;

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.moduleLoader = true;
            moduleService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.moduleLoader = false;
                    $scope.getModuleShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.showAllModuleList = $scope.showAllModuleList.concat(data.result.module);
                } else {
                    $scope.moduleLoader = false;
                    $scope.getModuleShow = false;
                    $scope.showAllModuleList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
                console.log("error", error);
            });
        }
    };
    $scope.getModuleList();
    $rootScope.$on("CallModulesMethod", function() {

        $scope.showAllModuleList = [];
        $scope.getModuleList();
    });
    //search user by name
    $scope.searchModule = function() {
        var limit_start = 0;
        var limit = 30;

        moduleService.get({
            name: $scope.search,
            skip: limit_start,
            limit: limit
        }, function(data) {
            $scope.showAllModuleList = data.result.module;
        });
    };
    // Show lesson form module ID
    $scope.showLesson = function(index, id) {
        $rootScope.lessonIndex = index;
        $rootScope.lessonId = id;
        $scope.loader[index] = true;
        $scope.isshowLesson[index] = true;
        lessonService.get({
            id: id
        }, function(data) {
            $scope.loader[index] = false;
            if (data.statusCode === 200 && data.message === "ok") {
                if (data.result.length > 0) {
                    $scope.lesson[index] = data.result;
                    $scope.isnoLesson[index] = false;
                } else {
                    $scope.isnoLesson[index] = true;
                }
            }
        });
    };
    $scope.sortMyModules = function() {
        $scope.sortModules = !$scope.sortModules;
        $scope.showAllModuleList = $scope.showAllModuleList;
        if ($scope.sortModules === true) {
            $scope.showAllModuleList = $filter('orderBy')($scope.showAllModuleList, 'name').reverse();
        } else {
            $scope.showAllModuleList = [];
            $scope.getModuleList();
            // $scope.showAllModuleList = $filter('orderBy')($scope.showAllModuleList, 'createdAt');
        }
    };
    // Hide lesson form module ID
    $scope.hideLesson = function(index) {
        $scope.isshowLesson[index] = false;
        $scope.lesson[index] = '';
        $scope.isnoLesson[index] = false;
        $scope.loader[index] = false;
    };
    // delete module by module ID
    $scope.deleteModule = function(index, id) {
        $scope.showAllModuleList = $scope.showAllModuleList;
        $scope.id = id;
        for(var i = 0; i < $scope.showAllModuleList.length; i++) {
            if($scope.showAllModuleList[i]._id === id)
                $scope.index = i;
        }

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this module?";
                },
                item: function() {
                    return "delete-module";
                }
            }
        });
    };
    //Create module
    $scope.createModule = function() {
        $scope.firstForm = true;
        $scope.secondForm = false;
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        $scope.showAllModuleList = $scope.showAllModuleList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/manageModule/moduleView.html',
            controller: 'manageModuleController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
    // Edit module

    $scope.editModule = function(id, index) {
        $scope.firstForm = true;
        $scope.secondForm = false;
        $scope.showEditForm = true;
        $scope.showCreateForm = false;
        $scope.moduleIndex = index;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/manageModule/moduleView.html',
            controller: 'manageModuleController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return id;
                },
                lesson: function() {
                    return 'lesson';
                }
            }
        });

    };
    // Function call on loadmore
    $scope.loadMore = function() {
        $scope.getModuleList();
    };
    //to show edit lesson form
    $scope.editLesson = function(lesson) {
        $scope.firstForm = false;
        $scope.secondForm = true;
        $scope.lesson = $scope.lesson;
        if(lesson.type == 'image') {
          var modalInstance1 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeImageView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else if(lesson.type == 'video'){
          var modalInstance2 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeVideoView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else if(lesson.type == 'text'){
          var modalInstance3 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeTextView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else{
          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/components/manageModule/moduleView.html',
              controller: 'manageModuleController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson._id;
                  },
                  lesson: function() {
                      return lesson;
                  }
              }
          });
        }


    };

    $scope.deleteLesson = function(lesson, index, parentIndex) {
        $scope.parentId = parentIndex;
        $scope.id = lesson._id;
        $scope.index = index;
        $scope.lesson = $scope.lesson;
        $scope.isnoLesson = $scope.isnoLesson;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this lesson?";
                },
                item: function() {
                    return "delete-lesson";
                }
            }
        });
    };
    //for updating lessonService
    $rootScope.$on("updatelessons", function(event, args) {
        $scope.showLesson(args.moduleIndex, args.moduleId);
    });
}]);
