app.controller("editGroupController", ['$rootScope', '$scope', '$timeout', 'editGroupService', 'userService', '$uibModal', '$uibModalInstance', '$log', 'moduleId', '$filter', 'getGroupService', 'editGroupAvatarService', 'editGroupNameService', 'editGroupDescriptionService', 'editGroupMembersService', 'getInviteMembersService', 'groupMembersService', 'delGrpMemberService', function($rootScope, $scope, $timeout, createGroupService, userService, $uibModal, $uibModalInstance, $log, moduleId, $filter, getGroupService, editGroupAvatarService, editGroupNameService, editGroupDescriptionService, editGroupMembersService, getInviteMembersService, groupMembersService, delGrpMemberService) {
    $scope.createFormSubmitted = false;
    $scope.group = {};
    /*$scope.group.name = "hello";*/
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLesson = false;
    $scope.editFormSubmitted = false;
    $scope.emails = [];
    $scope.values = [];
    $scope.log = [];
    $scope.chooseFile = false;
    $scope.groupImage = false;
    $scope.location = '';
    $scope.members = [];
    $scope.uploadLoader = false;
    $scope.list = [];
    $scope.showPlus = "";
    $scope.showPlusPencil = true;
    $scope.pencil = false;
    $scope.inviteEnabled = false;
    //this file varilables
    $scope.group = {};
    $scope.openeditName = false;
    $scope.openeditDes = false;
    $scope.location = '';
    $scope.editForm = true;
    $scope.memberForm = false;
    $scope.grpDetailClass = "col-xs-6 tab-reg active";
    $scope.grpMemberClass = "col-xs-6 tab-reg";
    $scope.grpMembers = [];
    $scope.delMemberId = '';
    $scope.delMemberIndex = '';
    $scope.userId = '';
    $scope.showUrl = "";

    $scope.getGroup = function() {
        getGroupService.query({
            id: moduleId
        }, function(data) {
            $scope.group = data.result;
            $scope.location = data.result.avatar_url;
            if($scope.group.owner == APP.currentUser._id) {
              $scope.pencil = true;
            }else {
              $scope.pencil = false;
            }
            if ($scope.location  === null || $scope.location  === "" || $scope.location === undefined) {
                $scope.showUrl = false;
                $scope.showPlus = true;
                $scope.group.avatar_url = "";
                $scope.location = "";
            } else {
                $scope.showPlus = false;
                $scope.showUrl = true;
            }
        });
    };
    $scope.getGroup();

    // $scope.editGroupService = function() {
    //     $scope.members = [];
    //     $scope.createFormSubmitted = true;
    //     if ($scope.group.name === undefined || $scope.group.name === '' || $scope.group.name === null) {
    //         focus('gname');
    //         return false;
    //     }
    //     if ($scope.group.description === undefined || $scope.group.description === '' || $scope.group.description === null) {
    //         focus('gdescription');
    //         return false;
    //     }
    //     var opts = {};
    //     opts.name = $scope.group.name;
    //     opts.description = $scope.group.description;
    //     opts.owner = APP.currentUser._id;
    //     opts.avatar_url = $scope.location;
    //     opts.members = $scope.list;
    //
    //     createGroupService.save(opts, function(data) {
    //         /*$scope.loader = false;*/
    //         if (data.statusCode === 200 && data.message === "ok") {
    //             $scope.createFormSubmitted = false;
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.sucessMessage;
    //             $scope.msgClass = 'success-green';
    //             $scope.group = {};
    //             $rootScope.$emit("CallGroupsMethod", {});
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 3000);
    //             //
    //         } else if (data.statusCode === 409 && data.message === "group name should be unique for a user") {
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.uniqueGroup;
    //             $scope.msgClass = 'error-red';
    //             $scope.createFormSubmitted = false;
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 3000);
    //         } else {
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.errorOccured;
    //             $scope.msgClass = 'error-red';
    //             $scope.createFormSubmitted = false;
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 4000);
    //         }
    //     });
    // };

    $scope.loadMatchingEmails = function(query) {
        return new Promise(function(resolve, reject) {
            getInviteMembersService.get({
                term: query
            }, function(data) {
                resolve(data.result);
            });
        });
    };

    $scope.tagAdded = function(tag) {
        $scope.list.push(tag.text);
        $scope.inviteEnabled = true;
    };

    $scope.tagRemoved = function(tag) {
        for(var i = 0; i < $scope.list.length; i++) {
            if($scope.list[i] === tag.text)
                $scope.list.splice(i, 1);
        }
        if($scope.list.length <= 0)
            $scope.inviteEnabled = false;
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    //edit avatar con
    $scope.editAvatar = function($event) {
        $event.stopPropagation();
        // $scope.showPlus = false;
        // $scope.chooseFile = true;
        // $scope.uploadLoader = false;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/customBrowse/customBrowseView.html',
            controller: 'customBrowseController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return "";
                }
            }
        });

    };


    $scope.upload = function(element) {

        $scope.noFile = false;
        $scope.imag = '';
        $scope.showProgressBar = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                $scope.chooseFile = false;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.showProgressBar = true;
                        $scope.groupImage = true;
                        $scope.chooseFile = false;
                        $scope.openeditImage = true;
                        //$scope.noFile = 'Image uploaded successfully';

                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.location = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
            }
        } else {
            $scope.noFile = 'Please upload a file first';
        }
    };

    $scope.hidePreview = function() {
        $scope.groupImage = true;
        $scope.openeditImage = false;
        $scope.showPlus = false;
        $scope.fileModel = null;
        $scope.showPlusPencil = true;
        if ($scope.group.avatar_url === null || $scope.group.avatar_url === "") {
            $scope.showPlus = true;
        } else {
            $scope.showPlus = false;
        }
    };

    //edit group-name
    $scope.openEditForm = function(index) {
        $scope.openeditImage = false;
        $scope.openeditName = false;
        $scope.openeditDob = false;
        $scope.openeditEmail = false;
        $scope.openeditPassword = false;
        switch (index) {
            case 1:
                $scope.openeditImage = true;
                break;
            case 2:
                $scope.openeditName = true;
                focus('gname');
                break;
            case 3:
                $scope.openeditDes = true;
                focus('gdes');
                break;
            case 4:
                $scope.openeditEmail = true;
                focus('email');
                $scope.showErrEmail = false;
                $scope.errMsg = '';
                break;
            case 5:
                $scope.openeditPassword = true;
                $scope.editUser.password = '';
                focus('password');
                break;
        }
    };

    $scope.changeImage = function() {
        if ($scope.location === '' || $scope.location === undefined) {
            $scope.noFile = 'Please upload a file first';
            focus('image');
            $timeout(function() {
                $scope.noFile = '';
            }, 3000);
            return false;
        }
        var opts = {};
        opts.avatar_url = $scope.location;
        editGroupAvatarService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.openeditImage = false;
            $scope.group.avatar_url = $scope.location;
            $scope.location = '';
            $scope.hidePreview();
            $scope.getGroup();
            // }
        });
    };

    // Edit change Name
    $scope.changeName = function() {
        $scope.nameSubmit = true;
        if ($scope.group.name === undefined || $scope.group.name === '') {
            focus('gname');
            return false;
        }
        var opts = {};
        opts.name = $scope.group.name;
        editGroupNameService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.openeditName = false;
            $scope.nameSubmit = false;
            $scope.getGroup();
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.openeditName = false;
                $scope.nameSubmit = false;
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
            }
        });
    };

    // Edit change description
    $scope.changeDes = function() {
        $scope.desSubmit = true;
        if ($scope.group.description === undefined || $scope.group.description === '') {
            focus('gdes');
            return false;
        }
        var opts = {};
        opts.description = $scope.group.description;
        editGroupDescriptionService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.desSubmit = false;
                $scope.openeditDes = false;
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
            }
        });
    };

    $scope.changeMembers = function() {
        var opts = {};
        opts.members = $scope.list;
        editGroupMembersService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.members = [];
                $scope.resultMsg = true;
                $scope.sucessMsg = "Invitations sent successfully";
                $scope.msgClass = 'success-green';
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                }, 2000);

            }
        });
    };

    //Cancel edit  box
    $scope.cancelSave = function(index) {
        switch (index) {
            case 1:
                $scope.openeditImage = false;
                $scope.hidePreview();
                $scope.getGroup();
                break;
            case 2:
                $scope.openeditName = false;
                $scope.getGroup();
                break;
            case 3:
                $scope.openeditDes = false;
                $scope.getGroup();
                break;
            case 4:
                $scope.openeditEmail = false;
                $scope.getGroup();
                break;
            case 5:
                $scope.openeditPassword = false;
                $scope.getGroup();
                break;
        }
    };

    // second form, members form
    $scope.membersForm = function() {
        $scope.grpDetailClass = "col-xs-6 tab-reg";
        $scope.grpMemberClass = "col-xs-6 tab-reg active";
        $scope.editForm = false;
        $scope.memberForm = true;
    };

    $scope.detailsForm = function() {
        $scope.grpDetailClass = "col-xs-6 tab-reg active";
        $scope.grpMemberClass = "col-xs-6 tab-reg";
        $scope.editForm = true;
        $scope.memberForm = false;

    };
    $scope.memberList = [];
    $scope.groupMembers = function() {
        groupMembersService.get({
            id: moduleId
        }, function(data) {
            $scope.grpMembers = data.result.members;
        });
        angular.forEach($scope.grpMembers, function(member, key) {
            $scope.memberList.push(member.user.local.email);
        });
    };
    $scope.groupMembers();

    $scope.confirmModal = function(index, id, userId) {
        $scope.delMemberId = id;
        $scope.delMemberIndex = index;
        $scope.userId = userId;
        $scope.grpMembers = $scope.grpMembers;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure to delete this member?";
                },
                item: function() {
                    return 'delete-grpMember';
                }
            }
        });

    };

    $rootScope.$on("uploadGroupAvatar", function(event, args) {
        $scope.groupImage = true;
        $scope.showPlus = false;
        $scope.openeditImage = true;
        $scope.location = args.avatar_url;
    });

}]);
