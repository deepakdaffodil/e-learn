app.factory('editGroupService', function ($resource) {
   return  $resource(APP.endpoints.editGroup,{id: '@id'},{update: { method: "PUT"}});
});

app.factory('getGroupService', function ($resource) {
   return  $resource(APP.endpoints.editGroup,{id: '@id'},{query: { method: "GET", isArray: false}});
});
app.factory('changeGroupNameService', function ($resource) {
   return  $resource(APP.endpoints.editGroup);
});
app.factory('changeGroupDesService', function ($resource) {
   return  $resource(APP.endpoints.editGroup);
});
app.factory('editGroupAvatarService', function ($resource) {
   return  $resource(APP.endpoints.editGroupAvatar,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupNameService', function ($resource) {
   return  $resource(APP.endpoints.editGroupName,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupDescriptionService', function ($resource) {
   return  $resource(APP.endpoints.editGroupDescription,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupMembersService', function ($resource) {
   return  $resource(APP.endpoints.editGroupMembers,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('groupMembersService', function ($resource) {
   return  $resource(APP.endpoints.groupMembers,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('delGrpMemberService', function ($resource) {
   return  $resource(APP.endpoints.delGrpMember, {id : '@id', mid : '@mid'},{query : { method:  "DELETE", isArray: false}});
});
