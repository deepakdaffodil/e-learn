app.factory('createActivity', function($resource){
	return $resource(APP.endpoints.createActivity);
});

app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});

app.factory('completedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson);
});
app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroups,{},{query: { method: "GET", isArray: false }});
});

app.factory('getSearchTagsService', function ($resource) {
   return  $resource(APP.endpoints.getSearchTags,{},{query: { method: "GET", isArray: false }});
});
