app.controller("createActivityController", ['$rootScope', '$scope', '$timeout', '$log', 'createActivity', '$uibModal', '$window', 'lessonService', 'completedLessonService', 'myGroupsService', 'getSearchTagsService', function($rootScope, $scope, $timeout, $log, createActivity, $uibModal, $window, lessonService, completedLessonService, myGroupsService, getSearchTagsService) {
    $scope.showVideoButton = false;
    $scope.lesson = {};
    $scope.Keytakeaways = [];
    $scope.delImage = false;
    $scope.activityFormSubmitted = false;
    $scope.activity = {};
    $scope.activity.description = '';
    $scope.noFile = '';
    $scope.myCommunityCheck = true;
    $scope.mySpaceCheck = true;
    $scope.myGroupsCheck = '';
    $scope.uploadLoader = false;
    $scope.module = {};
    $scope.lessonIndex = null;
    $scope.max = null;
    $scope.completedLesson = [];
    $scope.completedLessons = [];
    $scope.groupCheck = [];
    $scope.showVideo = false;
    $scope.videoTitle = "Add video";
    $scope.showVideoFile = false;
    $scope.videoId = '';
    $scope.tags = [];
    $scope.tagList = [];
    $scope.videoIdForm = false;

    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    if ($rootScope.isAdminLoggedIn) {
        $scope.showVideoButton = true;
    }
    $rootScope.$on('callCreateActivityController', function(event, args) {
        $scope.lesson = args.lesson;
        $scope.module = args.module;
        $scope.lessonIndex = args.lessonIndex;
        $scope.Keytakeaways = $scope.lesson.Keytakeaways ? $scope.lesson.Keytakeaways.split(".") : [];
        $scope.getAllLessons();
    });

    $scope.getAllLessons = function() {
        lessonService.query({
            id: $scope.module._id
        }, function(data) {
            $scope.lessons = data.result;
            $scope.max = data.result.length;
        });
    };

    $scope.Keytakeaways = $scope.lesson.Keytakeaways ? $scope.lesson.Keytakeaways.split(".") : [];
    // Function for uplad the image on s3 server
    $scope.upload = function(element) {
        $scope.noFile = '';
        $scope.delImage = false;
        $scope.imag = '';
        $scope.uploadSuccess = false;
        $scope.showProgressBar = false;
        //$scope.showProgressBar = false;
        $scope.activity.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.activity.fileModel) {
            if ($scope.activity.fileModel.type === 'image/jpeg' || $scope.activity.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.activity.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.activity.fileModel.type,
                    Body: $scope.activity.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {
                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.uploadSuccess = true;
                        $scope.activity.location = data.Location;
                        if ($scope.delImage) {
                            $scope.hidePreview();
                            $scope.activity.location = '';
                            $scope.activity.fileModel = null;
                        }
                        $scope.showProgressBar = true;
                        //$scope.noFile = 'Image uploaded successfully';
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);


            }
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }

        return $scope.activity.location;
    };

    $scope.hidePreview = function() {
        $scope.activity.location = '';
        $scope.activity.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };

    $scope.createActivityItem = function(i, lesson_id, module_Id) {
        $scope.delImage = false;
        if ($rootScope.isLoggedIn === false && $rootScope.isAdminLoggedIn === false) {
            $scope.actionlist = false;
            $scope.showActivity = false;
            $scope.$emit("ShowModalLogin", {});
        } else {
            $scope.activityFormSubmitted = true;
            if ($scope.activity.description === undefined || $scope.activity.description === '') {
                focus('description');
                return false;
            } else {
                $scope.activityFormSubmitted = false;
            }
            if ($scope.uploadLoader === false && !$scope.activity.location && $scope.tagList.length < 1 && !$scope.videoId) {
                $scope.confirmBox(frontendSettings.confirmPostActivityTagsImage);
            } else if ($scope.uploadLoader === false && !$scope.activity.location && !$scope.videoId) {
                $scope.confirmBox(frontendSettings.confirmPostActivityImage );
            } else if ($scope.tagList.length < 1 && $scope.uploadLoader === true) {
                $scope.noFile = 'Please wait while the file is uploading';
                $timeout(function() {
                    $scope.noFile = '';
                }, 2000);
            } else if ($scope.tagList.length < 1 && $scope.uploadLoader === false) {
                $scope.confirmBox(frontendSettings.confirmPostActivityTags);
            }else {
              $scope.postActivity();
            }

            if ($scope.uploadLoader === true) {
                $scope.noFile = 'Please wait while the file is uploading';
                $timeout(function() {
                    $scope.noFile = '';
                }, 2000);
            }
            if ($scope.uploadLoader === false) {
                $scope.noFile = '';
            }


        }
    };
    // Function for post the activity
    $scope.postActivity = function() {

        var opts = {};
        opts.user = APP.currentUser._id;
        opts.lesson = $scope.lesson._id;
        opts.description = $scope.activity.description;
        opts.groups = $scope.groupCheck;
        opts.tags = $scope.tagList;
        if ($scope.videoId === "" || $scope.videoId === null) {
            opts.type = 'image';
        } else {
            opts.type = 'video';
        }
        if (($scope.activity.location === '' || $scope.activity.location === undefined) && ($scope.videoId === "" || $scope.videoId === null)) {
            opts.url = '';
        } else {
            if ($scope.videoId)
                opts.url = $scope.videoId;
            else
                opts.url = $scope.activity.location;
        }
        opts.module = $scope.lesson.module;
        if ($scope.myCommunityCheck) {
            opts.my_community = "true";
        }

        createActivity.save(opts, function(data) {

            $scope.activityFormSubmitted = false;
            $scope.shareLoader = false;
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.tags = [];
                var options = {};
                options.user = APP.currentUser._id;
                options.lesson = $scope.lesson._id;
                options.module = $scope.module._id;
                $timeout(function() {
                    completedLessonService.save(options, function(data) {
                        //$state.go('home',{}, {reload : true});
                        if (data.message === 'ok' && data.statusCode === 200) {
                          $rootScope.$emit("updateProgressBar",{moduleId : $scope.module._id});
                        }
                    }, function(error) {});
                }, 0);
                $timeout(function() {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'app/shared/dialogBox/alertView.html',
                        controller: 'dialogController',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sm',
                        resolve: {
                            message: function() {
                                return "Your activity is submitted successfully";
                            }
                        }
                    });
                }, 0);

                $scope.completedLesson[$scope.lessonIndex] = $scope.lesson;
                $scope.module_id = '';
                $scope.activity = {};
                $scope.showProgressBar = false;
                angular.element('#fileUploaded').val(null);
                $timeout(function() {
                    $scope.successAct = false;
                    // modalInstance.dismiss('cancel');
                    // $scope.expandLesson(module_Id);
                    // $scope.$broadcast("CallUpdatedCommunityMethod", {});
                    // $scope.$broadcast("CallUpdatedSpaceMethod", {});
                    $rootScope.getStatistics();
                    //$state.go('home',{id:module_Id},{ reload: true });
                }, 1800);

            } else {
                $scope.activityError = true;
                $scope.showProgressBar = false;
                $timeout(function() {
                    $scope.activityError = false;
                    // modalInstance.dismiss('cancel');
                    // $scope.expandLesson(module_Id);
                }, 1800);
            }
        }, function(error) {
            $scope.activityError = true;
            $timeout(function() {
                $scope.activityError = false;
                // modalInstance.dismiss('cancel');
                $state.go('home', {}, {
                    reload: true
                });
            }, 1800);
        });
    };
    $scope.confirmBox = function(msg) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return msg;
                },
                item: function() {
                    return 'post-without-image';
                }
            }
        });
    };
    $rootScope.$on("moveToProgressBar", function(event, args) {
        $scope.onNext();
    });
    // $scope.closeModal = function() {
    //     $uibModalInstance.dismiss('cancel');
    //     angular.element('#fadein').removeClass('color-overlay');
    // };
    //on next click of the lesson to show next lesson of hhte module
    $scope.onNext = function() {
        $scope.completedLessons = [];
        $timeout(function() {
            lessonService.query({
                id: $scope.module._id
            }, function(data) {
                $scope.lessons = data.result;
                $scope.max = data.result.length;
                angular.forEach(data.result, function(lesson, key) {
                    if (lesson.completed) {
                        $scope.completedLessons.push(lesson);
                    }
                });
                if ($scope.completedLessons.length == $scope.max) {
                    $scope.openProgressModal();

                } else {
                    $scope.openProgressModal();
                }
            });


        }, 0);


    };
    //for opening modal
    $scope.openProgressModal = function() {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/progressBar/progressBarView.html',
            controller: 'progressBarController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            resolve: {
                max: function() {
                    return $scope.max;
                },
                current: function() {
                    return $scope.completedLessons;
                },
                lesson: function() {
                    return $scope.lesson;
                },
                lessonIndex: function() {
                    return $scope.lessonIndex;
                },
                module: function() {
                    return $scope.module;
                }
            }
        });
    };
    $scope.allTotal = 0;
    $scope.myGroupsList = [];
    $scope.listResponse = 1;
    $scope.getGroups = function() {
        var limit_start = $scope.myGroupsList.length;
        var limit = 100;
        var id = APP.currentUser._id;
        // if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
        $scope.listResponse = 0;
        //$scope.moduleLoader = true;
        myGroupsService.query({
            id: id,
            skip: limit_start,
            limit: limit
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.allTotal = data.result.totalCount;
                $scope.myGroupsList = $scope.myGroupsList.concat(data.result.groups);
            } else {
                //$scope.moduleLoader = false;
                $scope.myGroupsList = [];
                $scope.allTotal = 0;
            }
        }, function(error) {});
        // }
    };
    $scope.getGroups();
    $rootScope.$on('groupsComingInCreatActivity', function(event, args) {
        $scope.myGroupsList = [];
        $scope.getGroups();
    });
    $scope.updateSelection = function($event, id) {
        $event.stopPropagation();
        var checkbox = $event.target;
        if (checkbox.checked)
            $scope.groupCheck.push(id);
    };

    $scope.allGroupCheck = function($event) {
        $scope.allChecked = $event.target.checked;
        $scope.groupCheck = [];
        for (var i = 0; i < $scope.myGroupsList.length; i++) {
            $('#' + $scope.myGroupsList[i].group._id).val($event.target.checked);
            if ($event.target.checked)
                $scope.groupCheck.push($scope.myGroupsList[i].group._id);
        }
    };

    $scope.stopEvent = function($event) {
        $event.stopPropagation();
    };

    $(window).click(function(e) {
        try {
            if (!$(e.target).attr('class').includes('video-bg'))
                $scope.activityPlayer.stopVideo();
        } catch (e1) {}
    });

    $scope.addVideo = function() {
        $scope.showVideoFile = true;
        $scope.showVideo = true;

    };

    $scope.doneClick = function() {
        $scope.videoIdForm = true;
        if ($scope.videoId === null || $scope.videoId === '' || $scope.videoId === undefined) {
            focus('videoIdField');
            return false;
        }
        $scope.videoTitle = "Change video";
        $scope.showVideo = false;
    };
    $scope.cancelVideo = function() {
        $scope.videoId = '';
        $scope.videoTitle = "Add video";
        $scope.showVideo = false;
        $scope.showVideoFile = false;
    };
    //tags
    $scope.tagAdded = function(tag) {
        $scope.tagList.push(tag.text);
    };

    $scope.tagRemoved = function(tag) {
        $scope.tagList.pop(tag.text);
    };

    $scope.loadMatchingTags = function(query) {
        return new Promise(function(resolve, reject) {
            getSearchTagsService.get({
                term: query
            }, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    resolve(data.result.tags);
                } else {
                    resolve();
                }

            });
        });
    };
}]);
