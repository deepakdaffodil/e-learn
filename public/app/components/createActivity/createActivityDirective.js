app.directive('lessonActivity', function() {
    return {
        templateUrl: 'app/components/createActivity/createActivityView.html',
        restrict: 'E',
        controller: 'createActivityController',
        scope: {
            lesson: '='
        }
    };
});
