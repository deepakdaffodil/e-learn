app.controller("myFavouritesController", ['$scope', '$rootScope', 'getFollowersService', 'getFollowingService', '$uibModal', 'Upload', '$timeout', '$sce', function($scope, $rootScope, getFollowersService, getFollowingService, $uibModal, Upload, $timeout, $sce) {
    $scope.followingsLoader = true;
    $scope.followersLoader = true;
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $scope.followings = function() {
        getFollowingService.get(function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.followingsLoader = false;
                $scope.followings = data.result;
            }
        });
    };
    $scope.followings();
    $scope.followers = function() {
        getFollowersService.get(function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.followersLoader = false;
                $scope.followers = data.result;
            }
        });
    };
    $scope.followers();
    $scope.unfollow = function(index, id, follower) {
        $scope.id = id;
        $scope.index = index;
        $scope.follower = follower;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    if ($scope.follower)
                        return 'Are you sure to delete this follower?';
                    else
                        return 'Are you sure to delete this favourite?';
                },
                item: function() {
                    return 'delete-follower';
                }
            }
        });

    };


    // $scope.uploadFiles = function(files) {
    //     $scope.files = files;
    //     if (files && files.length) {
    //         Upload.upload({
    //             url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
    //             data: {
    //                 files: files
    //             }
    //         }).then(function(response) {
    //             $timeout(function() {
    //                 $scope.result = response.data;
    //             });
    //         }, function(response) {
    //             if (response.status > 0) {
    //                 $scope.errorMsg = response.status + ': ' + response.data;
    //             }
    //         }, function(evt) {
    //             $scope.progress =
    //                 Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    //         });
    //     }
    // };


    // $scope.$watch(function(scope) {
    //         return scope.locationVideo;
    //     },
    //     function(newVal, oldVal) {
    //         var player = document.getElementById('videoPlayer');
    //         var videoSource = document.getElementById('videoSource');
    //
    //         player.pause();
    //         videoSource.src = newVal;
    //         player.load();
    //         player.play();
    //     });

    $scope.uploadFiles = function(files) {
        $scope.files = files[0];
        if (files) {
            // if ($scope.activity.fileModel.type === 'image/jpeg' || $scope.activity.fileModel.type === 'image/png') {
            $scope.uploadLoader = true;
            var bucket = new AWS.S3({
                params: {
                    Bucket: 'mindmaxdaffo'
                }
            });
            var imageName = $scope.files.name + Math.floor(Date.now() / 1000);
            $scope.imag = imageName;
            var params = {
                Key: imageName,
                ContentType: $scope.files.type,
                Body: $scope.files
            };
            bucket.upload(params).on('httpUploadProgress', function(evt) {
                $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                $scope.$digest();
            }).send(function(err, data) {
                $scope.uploadLoader = false;
                $scope.uploadSuccess = true;
                $scope.locationVideo = $sce.trustAsResourceUrl(data.Location);
                if ($scope.delImage) {
                    $scope.hidePreview();
                    $scope.activity.location = '';
                    $scope.activity.fileModel = null;
                }
                $scope.showProgressBar = true;
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);
                $scope.barShow = '0%';
                $scope.$digest();
            });
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }
        return $scope.locationVideo;
    };

    // $scope.$watch(function (scope) {
    // return $scope.locationVideo;
    // }, function (newVal, oldval) {
    //
    // });

}]);
