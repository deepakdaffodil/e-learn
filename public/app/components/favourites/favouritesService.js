app.factory('getFollowingService', function ($resource) {
    return $resource(APP.endpoints.followings, {}, { query: { method: "GET", isArray: false } });
});
app.factory('getFollowersService', function ($resource) {
    return $resource(APP.endpoints.followers, {}, { query: { method: "GET", isArray: false } });
});
app.factory('unfollowService', function ($resource) {
    return $resource(APP.endpoints.unfollowUser, { id: '@id', follower: '@follower' }, { update: { method: 'PUT' } });
});