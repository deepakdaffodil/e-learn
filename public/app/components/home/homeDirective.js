app.directive('headerNav', function() {
    return {
        templateUrl: 'app/components/home/homeView.html',
        restrict: 'E',
        controller: 'headerNav',
        scope: {
         title: '@'
        },

    };
});

app.directive('enforceMaxTags', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngCtrl) {
      var maxTags = attrs.maxTags ? parseInt(attrs.maxTags, '10') : null;

      ngCtrl.$parsers.push(function(value) {
        if (value && maxTags && value.length > maxTags) {
          value.splice(value.length - 1, 1);
        }
        return value;
      });
    }
  };
});

app.directive('limitTags', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModel) {
            var maxTags = parseInt(attrs.maxTags, 10);
            ngModel.$parsers.unshift(function(value) {
                if (value && value.length > maxTags) {
                    value.splice(value.length - 1, 1);
                }
                return value;
            });
        }
    };
});
