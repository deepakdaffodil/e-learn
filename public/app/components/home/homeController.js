app.controller("homeController", ['$rootScope', '$scope', 'moduleService', 'lessonDetailService', 'getCommunityService', 'myGroupsService', '$location', '$uibModal', '$state', '$timeout', '$filter', 'userByIdService', 'getSearchTagsService', 'searchUserNameService', 'searchModuleNameService', 'getCompletedLessonService', 'lessonService', function($rootScope, $scope, moduleService, lessonDetailService, getCommunityService, myGroupsService, $location, $uibModal, $state, $timeout, $filter, userByIdService, getSearchTagsService, searchUserNameService, searchModuleNameService, getCompletedLessonService, lessonService) {
    // $scope.getLoggedInUser = function() {
    //   userByIdService.get({  id: APP.currentUser._id},{},function (data) {
    //     if(data.statusCode===200 && data.message==='ok') {
    //       if(APP.currentUser.local.deleted===true || data.result[0].local.password !== APP.currentUser.local.password || data.result[0].local.name !== APP.currentUser.local.name) {
    //         $rootScope.$emit('logoutLoggedInUser',{});
    //       }
    //     }
    //   });
    // };
    // $scope.getLoggedInUser();

    window.onpopstate = function(event) {
        if ($location.url() == '/home' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $scope.moduleDetails = true;
            $scope.showLessonActivity = false;
            $rootScope.activeSection = "";
            $rootScope.myTabs = false;
            $scope.$broadcast('callModuleController', {
                module: $rootScope.module
            });
            $rootScope.$broadcast('updateMyCommunity', {
                module: $rootScope.module
            });
            for (var i = 0; i < $rootScope.activeModule.length; i++)
                $rootScope.activeModule[i] = false;
            for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
                $rootScope.activeModuleLast[j] = false;

            if (!$rootScope.isLast)
                $rootScope.activeModule[$rootScope.moduleIndex] = true;
            else
                $rootScope.activeModuleLast[$rootScope.moduleIndex] = true;
        }
        if ($location.url() != '/home' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $scope.moduleDetails = false;
            $scope.showLastComponent = false;
            $scope.showLessonActivity = false;
        }
        if ($location.url() != '/home' && $location.url() != '/' && $location.url() != '/home/groups' && $location.url() != '/home/space' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $state.go('home', {}, {
                reload: true
            });
        }
        if ($location.url() == '/home/groups') {
            $state.go('home.groups', {}, {

            });
        }
        if ($location.url() == '/home/space') {
            $state.go('home.space', {}, {

            });
        }
    };
    // if logged in...
    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        // set the google tracking user id
        ga('set', 'userId', APP.currentUser._id);

        // update the header
        $rootScope.$broadcast('updateName', {
            name: APP.currentUser.local.name
        });
        $rootScope.$broadcast('updateHeaderDp', {
            avatar_url: APP.currentUser.local.avatar_url
        });
    }

    // Open modal for view full detail of activity in my community
    $scope.viewActivity = function(activityId) {
        $scope.comments = 0;

        if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn) {

            $scope.$emit("ShowModalLogin", {});
        } else {
            $timeout(function() {
                $rootScope.$broadcast("callViewActivityFromHome", {
                    activityId: activityId
                });
            }, 500);
        }
    };

    if (window.localStorage.location) {
        if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn)
            $scope.$emit('ShowModalLogin', {});
        else {
            $location.url('/home/' + window.localStorage.location);
            window.localStorage.location = '';

            if (window.location.pathname !== $location.url())
                window.location.pathname = $location.url();
            else {
                $scope.viewActivity($location.url().split('/')[$location.url().split('/').length - 1]);
            }
        }
    }

    $scope.oneAtATime = true;
    $scope.showCommunity = false;
    $scope.showSpace = false;
    $scope.showGroup = false;
    $scope.showLessonActivity = false;
    $scope.showLastComponent = false;
    $scope.showGrpActivities = false;
    $scope.showMore = true;
    $scope.moduleDetails = false;
    $scope.progressBar = false;
    $scope.adminCommunity = false;
    $rootScope.activeModule = [];
    $rootScope.activeModuleLast = [];
    $scope.lessonForActivity = {};
    $scope.module = {};
    $rootScope.activeSection = "";
    $rootScope.myTabs = false;
    $rootScope.moduleSelected = false;
    $rootScope.filterActive = false;
    $scope.patternSelected = "Module";
    $rootScope.showProgress = false;
    $scope.max = 0;
    $scope.current = 0;
    $scope.percent = 0;
    $scope.tags = [];

    //when user is not logged in
    if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn) {
        $scope.moduleDetails = false;
        //$rootScope.$broadcast('callMyCommunity',{});
        $scope.showCommunity = true;

    } else if ($rootScope.isAdminLoggedIn && $rootScope.count == 1) {
        $state.go('manage-module');
        $rootScope.count = 2;
    } else if ($rootScope.isAdminLoggedIn && $rootScope.count == 2) {
        $scope.moduleDetails = true;
        $scope.showCommunity = false;
    }
    $rootScope.$emit("callHeaderController", false);

    $rootScope.$on("callHomeControllerForActivity", function(event, args) {
        $rootScope.$broadcast("closingModal", {});
        $rootScope.$broadcast("callCreateActivityController", {
            lesson: args.lesson,
            module: $scope.module,
            lessonIndex: args.index
        });
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.moduleDetails = false;
        $scope.showGrpActivities = false;
        $scope.showLessonActivity = true;

    });

    $rootScope.$on("callHomeConLastActiviyComponent", function(event, args) {
        $rootScope.$broadcast("closingModal", {});
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.moduleDetails = false;
        $scope.showLessonActivity = false;
        $scope.showGrpActivities = false;
        $scope.showLastComponent = true;
        $rootScope.$broadcast("callLastComponent", {
            module: args.module
        });
    });
    $scope.modules = [];
    $scope.getModules = function() {
        moduleService.query({
            skip: 0,
            limit: 60
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.modules = data.result.module;
                $scope.modules = $filter('orderBy')($scope.modules, 'name');
                if ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)
                    $scope.showMycommunity();
                // else
                //     $scope.moduleClicked(0, $scope.modules[0]);
                // $scope.firstModules = $scope.modules.slice(0, 6);
                $scope.lastModules = $scope.modules;
                $scope.totalModules = data.result.totalCount;
                // if ($scope.totalModules < 6 || $scope.totalModules == 6) {
                //     $scope.showMore = false;
                // }
            }
        }, function(error) {});
    };
    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        $scope.getModules();
    }

    $scope.getAllLessons1 = function(module, current) {
        lessonService.query({
            id: module
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                    $scope.lessons = data.result;
                    $scope.max = data.result.length;
                    $scope.percent = (current / $scope.max) * 100;
                    $scope.hasLessons = true;
                } else {
                    $scope.hasLessons = false;
                }
            }
        }, function(error) {});
    };

    $scope.getCompletedLessons = function(id) {
        getCompletedLessonService.query({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                    $scope.current = data.result.length;
                    $timeout(function() {
                        $scope.getAllLessons1(id, data.result.length);
                    }, 0);
                } else {
                    $scope.percent = 0;
                    $scope.current = 0;
                }
            }
        }, function(error) {});
    };
    $rootScope.$on("updateProgressBar", function(event, args) {
        $scope.getCompletedLessons(args.moduleId);
    });
    $scope.moduleClicked = function(index, module, isLast) {
        $rootScope.showProgress = true;
        $scope.tagRemoved();
        $scope.getCompletedLessons(module._id);
        $rootScope.moduleSelected = true;
        $scope.adminCommunity = false;
        $rootScope.moduleIndex = index;
        $rootScope.isLast = isLast;
        $rootScope.activeSection = "";
        $rootScope.myTabs = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        $scope.moduleDetails = true;

        for (var i = 0; i < $rootScope.activeModule.length; i++)
            $rootScope.activeModule[i] = false;
        for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
            $rootScope.activeModuleLast[j] = false;

        if (!isLast)
            $rootScope.activeModule[index] = true;
        else {
            $("#module-toggle").dropdown("toggle");
            $rootScope.activeModuleLast[index] = true;
        }

        $scope.showModule = 'module';
        $scope.whenClicked = true;
        $rootScope.module = module;
        $scope.$broadcast('callModuleController', {
            module: module
        });
        $rootScope.$broadcast('updateMyCommunity', {
            module: module
        });
        $state.go('home');

    };
    //showing module when clicked from activity item
    $rootScope.$on("callMyModule", function(event, args) {
        $scope.showingModule(args.module);
    });
    $scope.showingModule = function(module) {
        $rootScope.moduleSelected = true;
        $scope.adminCommunity = false;
        // $rootScope.moduleIndex = index;
        // $rootScope.isLast = isLast;
        $rootScope.activeSection = "";
        $rootScope.myTabs = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        $scope.moduleDetails = true;

        // for (var i = 0; i < $rootScope.activeModule.length; i++)
        //     $rootScope.activeModule[i] = false;
        // for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
        //     $rootScope.activeModuleLast[j] = false;
        //
        // if (!isLast)
        //     $rootScope.activeModule[index] = true;
        // else {
        //     $("#module-toggle").dropdown("toggle");
        //     $rootScope.activeModuleLast[index] = true;
        // }

        $scope.showModule = 'module';
        $scope.whenClicked = true;
        $rootScope.module = module;
        $scope.$broadcast('callModuleController', {
            module: module
        });
        $rootScope.$broadcast('updateMyCommunity', {
            module: module
        });
        $state.go('home');

    };

    $scope.showMyspace = function() {
        // $scope.tags = [];
        $scope.tagRemoved();
        $rootScope.showProgress = false;
        $rootScope.activeSection = "space";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        //$scope.showSpace = true;
        for (var i = 0; i < $rootScope.activeModule.length; i++)
            $rootScope.activeModule[i] = false;
        for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
            $rootScope.activeModuleLast[j] = false;

        $scope.$broadcast("CallUpdatedSpaceMethod", {});
        $state.go('home.space');

    };

    $scope.showMycommunity = function() {

        $scope.tagRemoved();
        $rootScope.activeSection = "community";
        if ($rootScope.isAdminLoggedIn)
            $scope.adminCommunity = true;
        $rootScope.showProgress = false;
        $rootScope.myTabs = false;
        $scope.moduleDetails = false;
        $scope.showGroups = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        // $scope.showCommunity = true;
        // if ($rootScope.isLoggedIn) {
        //   $timeout(function () {
        //     $rootScope.$broadcast('updateMyCommunity', {
        //         module: $rootScope.module
        //     });
        //   }, 100);

        // }
        // $scope.tags = [];
        // $scope.tagRemoved();
        $state.go('home.community');
    };

    $scope.showMyGroups = function() {
        // $scope.tags = [];
        $scope.tagRemoved();
        $rootScope.showProgress = false;
        $rootScope.activeSection = "groups";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        //$scope.showGroups = true;
        $state.go('home.groups');

    };

    $rootScope.$on('showMyGroups', function(event, args) {
        $scope.showMyGroups();
    });

    //show group Activities
    $rootScope.$on("groupActivitiesPage", function(event, args) {
        $rootScope.activeSection = "groups";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGroups = false;
        $scope.showGrpActivities = true;
        $rootScope.$broadcast("groupActivitiesPageId", {
            id: args.id,
            name: args.name
        });

    });

    $scope.stopEvent = function($event) {
        $event.stopPropagation();
    };
    //To select pattern --module/tag/user
    $scope.pattern = function(type) {
        $("#search-toggle").dropdown("toggle");
        // $scope.tags = [];
        $scope.tagRemoved($scope.tags.length <= 0);
        switch (type) {
            case 1:
                $scope.patternSelected = "Module";
                break;
            case 2:
                $scope.patternSelected = "Tag";
                break;
            case 3:
                $scope.patternSelected = "User";
                break;
        }
    };

    $scope.loadMatching = function(query) {
        if ($scope.patternSelected == "Tag") {
            return new Promise(function(resolve, reject) {
                getSearchTagsService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result.tags);
                    } else {
                        resolve();
                    }

                });
            });
        } else if ($scope.patternSelected == "User") {
            return new Promise(function(resolve, reject) {
                searchUserNameService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result);
                    } else {
                        resolve();
                    }

                });
            });
        } else if ($scope.patternSelected == "Module") {
            return new Promise(function(resolve, reject) {
                searchModuleNameService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result);
                    } else {
                        resolve();
                    }

                });
            });
        }
    };
    $scope.tagAdding = function(tag) {
        $rootScope.filterActive = true;
        $scope.tags = [tag];
        if ($scope.patternSelected == "Module") {
            $rootScope.$broadcast("byModuleName", {
                module: tag
            });
        } else if ($scope.patternSelected == "Tag") {
            var module = {};
            module._id = tag.text;
            $rootScope.$broadcast("byTagName", {
                module: module
            });
        } else if ($scope.patternSelected == "User") {
            $rootScope.$broadcast("byUserName", {
                module: tag
            });
        }

        if ($rootScope.activeSection !== 'community')
            $scope.showMycommunity();
        
        return false;
    };

    $scope.tagRemoved = function(noReload) {
        $rootScope.filterActive = false;
        $scope.tags = [];
        if (noReload === true)
            return;
        $rootScope.$broadcast("whenNoTag", {});
    };

}]);
