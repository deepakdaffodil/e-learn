app.factory('moduleService', function ($resource) {
   return  $resource(APP.endpoints.module,{},{query: { method: "GET", isArray: false }});
});
app.factory('lessonDetailService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail);
});
app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroupsService,{},{query: { method: "GET", isArray: false }});
});
app.factory('userByIdService', function ($resource) {
   return  $resource(APP.endpoints.userById,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('searchUserNameService', function ($resource) {
   return  $resource(APP.endpoints.searchForUserName,{},{query: { method: "GET", isArray: false }});
});
app.factory('searchModuleNameService', function ($resource) {
   return  $resource(APP.endpoints.searchModuleName,{},{query: { method: "GET", isArray: false }});
});
