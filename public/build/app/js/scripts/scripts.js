function doGet($resource, url, opt, callback){
	var data = {
  "id": 1,
  "name": "Leanne Graham",
  "username": "Bret",
  "email": "Sincere@april.biz",
  "address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
    "geo": {
      "lat": "-37.3159",
      "lng": "81.1496"
    }
  },
  "phone": "1-770-736-8031 x56442",
  "website": "hildegard.org",
  "company": {
    "name": "Romaguera-Crona",
    "catchPhrase": "Multi-layered client-server neural-net",
    "bs": "harness real-time e-markets"
  }
};
callback(data);
$resource(url,{}, {query : {method : "GET" , isArray :  true} });
/* $resource("api/StudentsApi", {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET", url: "/api/StudentsApi?id=:id" },
            remove: { method: "DELETE", url: "/api/StudentsApi?id=:id" },
            update: { method: "PUT", url: "/api/StudentsApi?id=:id" }
       });
*/


}

app.factory('focus', function($timeout) {
    return function(id) {
      $timeout(function() {
        var element = document.getElementById(id);
        if(element){
          element.focus();
        }
      },400);
    };
});
app.filter('capitalize', function(){
   return function(input){
        if(input){
            return input[0].toUpperCase() + input.slice(1);
        }
   };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});
    
app.directive('confirmClick', function() {
    return {
        link: function (scope, element, attrs) {
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {

              msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteActivity ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickDelete', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteAccount ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickComment', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteComment ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
app.directive('confirmClickModule', function() {
    return {
        link: function (scope, element, attrs) {
           
            // setup a confirmation action on the scope
            scope.confirmClick = function(msg) {
               msg = msg || attrs.confirmClick || frontendSettings.confirmDeleteModule ;
                // return true/false to continue/stop the ng-click
                return confirm(msg);
            };
        }
    };
});
 //Displaying the loading image form for loading
  app.directive('showProgressBar', function() {
    return {
      restrict: 'E',
      template: '<img src="assets/img/proceed.gif" alt="processing..." />'
    };
  });

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "dd/mm/yy",
         changeMonth: true,
         changeYear: true,
         yearRange: '1920:2016',
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  };
});
app.controller("actionController",['$rootScope','$scope','$timeout','getActionService','saveCompleteActionService', function($rootScope, $scope, $timeout, getActionService, saveCompleteActionService) {
	
	$scope.getAction = {};
	$scope.showActionList = false;
	$scope.loaderShowA = true;
	$scope.selectSaveActionList = [];
	$scope.activeAction = true;
	$scope.activeProfile = false;
	$scope.actionLoader = false;
	$scope.resultAction = false;
	$rootScope.$emit("callHeaderController",true);
	// get user save actions
	$scope.getUserActions = function(){
		getActionService.query(function(data) {
		$scope.loaderShowA = false;
		if(data.statusCode === 200 && data.message === 'ok'){
			if(data.result.length > 0){
			/*	for(var i = 0; i<data.result.length; i++){
					if(data.result[i].status == 1){
						$scope.showActionList = true;
						break;
					}
				}*/
				$scope.showActionList = true;
				$scope.getAction = data.result;
			}
		} 
	}, function(error){
	});
	};
	$scope.getUserActions();
	//toggle checkbox for actions save
	$scope.toggleCheckSaveAction = function(id){
		$scope.actionLoader = true;
		/*saveCompleteActionService.update({id:id}, function(data){
			$scope.actionLoader = false;
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.resultAction = true;
				$scope.errMsg = frontendSettings.successActionSaved;
				$timeout(function() {
					$scope.resultAction = false;
					$scope.getUserActions();
				}, 2000);
			} else {
				$scope.resultAction = true;
				$scope.errMsg = frontendSettings.errorOccured;
				$timeout(function() {
					$scope.resultAction = false;
					}, 3000);
			}
			},function(error){
		});*/
	};

}]);
/*app.factory('getActionUserService', function ($resource) {
   return  $resource(APP.endpoints.actionSave);
});*/
/*app.factory('getActionService', function ($resource) {
	return  $resource(APP.endpoints.actionSave);
});*/
app.factory('getActionService', function ($resource) {
   return  $resource(APP.endpoints.actionSave,{},{query: { method: "GET", isArray: false }});
});
app.factory('saveCompleteActionService',function($resource){
	return $resource(APP.endpoints.saveCompleteAction,{ id: '@id'}, {update: { method: "PUT"}});
});

app.controller("adminUserController", ['$rootScope', '$uibModal', '$scope', 'userService', 'deluserService', '$timeout', 'updateUserService', 'registerService', 'userSearchService', '$filter', function($rootScope, $uibModal, $scope, userService, deluserService, $timeout, updateUserService, registerService, userSearchService, $filter) {
    $scope.loader = false;
    $scope.userList = [];
    $scope.edit = [];
    $scope.formData = {};
    $scope.saveLoader = false;
    $scope.uploadLoader = [];
    $scope.noFile = [];
    $scope.location = [];
    $scope.showProgressBar = [];
    $scope.showErrEmail = false;
    $scope.getOriginalEmail = '';
    $scope.sortUser = false;
    $scope.change = [];
    $scope.localuser = null;

    //AWS
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    // calender scope define
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.format = 'dd-MMMM-yyyy';
    $scope.disabled = function(date, mode) {
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.maxDate = new Date();
    $scope.toggleMin = function() {
        $scope.minDate = null;
    };
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };
    $scope.toggleMin();
    $scope.userList = [];
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.getUserList = function() {
        var limit_start = $scope.userList.length;
        var limit = 20;
        $scope.loader = true;

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.loader = true;
            userService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.loader = false;
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.moduleLoader = false;
                    $scope.getUserShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.userList = $scope.userList.concat(data.result.users);
                } else {
                    $scope.moduleLoader = false;
                    $scope.getUserShow = false;
                    $scope.userList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
            });
        }
    };
    $scope.getUserList();
    $rootScope.$on('updateUserList',function(event, args){
      $scope.userList = [];
      $scope.getUserList();
    });

    $scope.deleteUser = function(index, id) {
        deluserService.delete({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.userList.splice(index, 1);
            }
        });
    };
    $scope.activeUserEdit = [];

    $scope.editUser = function(index, user) {
        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');
        $('#' + index).addClass('user-list-scroll');

        user.local.password = '';
        angular.forEach($scope.change, function(user, key) {
            if (user.changing) {
                user.real.local.name = user.username;
                user.real.local.email = user.useremail;
                user.real.local.dob = user.userdob;
                user.real.local.password = user.userpassword;
            }
        });

        var opts = {
            real: user,
            username: user.local.name,
            useremail: user.local.email,
            userdob: user.local.userdob,
            userpassword: user.local.password,
            index: index,
            changing: true
        };
        $scope.change[index] = opts;

        if (user.local.token) {
            $scope.openModal(frontendSettings.singlesignup);
        } else {
            $scope.username = user.local.name;
            $scope.useremail = user.local.email;
            $scope.userdob = user.local.dob;
            $scope.userpassword = user.local.password;
            $scope.edit[index] = true;
            $scope.activeUserEdit = [];
            $scope.activeUserEdit[index] = user._id;
            $scope.uploadLoader[index] = false;
            $scope.noFile[index] = false;
            $scope.location[index] = '';
            $scope.getOriginalEmail = user.local.email;
        }
    };

    $scope.cancelSave = function(index, user) {

        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');

        angular.forEach($scope.change, function(user, key) {
            if (user.index == index) {
                user.changing = false;
            }
        });
        user.local.name = $scope.username;
        user.local.email = $scope.useremail;
        user.local.dob = $scope.userdob;
        user.local.password = $scope.userpassword;

        $scope.edit[index] = false;
        //$scope.userList[index]  = $scope.localuser;
        $scope.activeUserEdit = [];
    };
    $scope.updateUser = function(index, user) {
        for(var i = 0; i < $scope.userList.length; i++)
            $('#' + i).removeClass('user-list-scroll');

        angular.forEach($scope.change, function(user, key) {
            if (user.index == index) {
                user.changing = false;
            }
        });
        $scope.submittedEditForm = true;
        if (user.local.name === null || user.local.name === '' || user.local.name === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
        if (user.local.email === null || user.local.email === '' || user.local.email === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
        if (user.local.dob === null || user.local.dob === '' || user.local.dob === undefined) {
            $scope.openModal('Please enter all fields');
            return false;
        }
            if (user.local.password.length !== 0 && user.local.password.length < 4) {
                $scope.openModal('Password length should be greater than 4');
                return false;
            }
        $scope.saveLoader = true;
        if ($scope.getOriginalEmail === user.local.email) {
            $scope.updateSuccess(index, user);
        } else {
            $scope.emailValidate(index, user);
        }


    };
    $scope.updateSuccess = function(index, user) {
        var opts = {};
        opts.name = user.local.name;
        opts.email = user.local.email;
        opts.dob = user.local.dob;
        opts.password = user.local.password;
        if ($scope.location[index] === '' || $scope.location[index] === null) {
            opts.avatar_url = user.local.avatar_url;
        } else {
            opts.avatar_url = $scope.location[index];
        }
        opts.rev = user.local.rev.toString();
        //$scope.saveLoader = false;
        updateUserService.update({
            id: user._id
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.activeUserEdit = [];
                $scope.userList[index] = data.result;
                $scope.location[index] = '';
                $scope.saveLoader = false;
            } else {
                $scope.openModal('Some errorOccured please try again');
                $scope.saveLoader = false;
            }
        });
    };
    //search user by name
    $scope.searchUser = function() {
        userSearchService.get({
            name: $scope.search
        }, function(data) {
            $scope.userList = data.result.users;
        });
    };

    // Open modal for alert messgae for validation
    $scope.openModal = function(msg) {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'dialogController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return msg;
                }
            }
        });
    };
    // Confirm box
    // Open modal for alert messgae for validation
    $scope.openConfirmModal = function(index, id) {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        $scope.index = index;
        $scope.id = id;
        $scope.userList = $scope.userList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return "Are you sure to delete this user?";
                },
                item: function() {
                    return 'users';
                }
            }
        });
    };
    // sort user
    $scope.sortMyUser = function() {
        $scope.sortUser = !$scope.sortUser;
        $scope.userList = $scope.userList;
        if ($scope.sortUser === true) {
            $scope.userList = $filter('orderBy')($scope.userList, 'local.name');
        } else {
            $scope.userList = $filter('orderBy')($scope.userList, 'createdAt').reverse();
        }
    };


    // Validate email
    $scope.emailValidate = function(index, user) {
        $scope.showErrEmail = false;
        $scope.errMsg = '';
        registerService.save({
            email: user.local.email
        }, function(data) {
            if (data.statusCode == 200 && data.message == "ok" && data.result.message == 'verified') {
                $scope.updateSuccess(index, user);
            } else if (data.statusCode == 422 && data.message == 'email already registered') {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.userregistered;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            } else if (data.statusCode == 422 && data.message == 'email should be registered with any email provider') {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.domainnotValid;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            } else {
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.errorOccured;
                $scope.openModal($scope.errMsg);
                $scope.saveLoader = false;
                return false;
            }
        });

    };
    // Upload image in profile data for  logged user

    $scope.upload = function(element, index) {
        $scope.noFile[index] = false;
        $scope.imag = '';
        $scope.showProgressBar[index] = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader[index] = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {
                $scope.uploadLoader[index] = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader[index] = false;
                        $scope.noFile[index] = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile[index] = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader[index] = false;
                        $scope.showProgressBar[index] = true;
                        //$scope.noFile = 'Image uploaded successfully';

                        $timeout(function() {
                            $scope.noFile[index] = '';
                        }, 3000);
                        $scope.location[index] = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile[index] = 'Please upload a image with jpeg and png format';
                $timeout(function () {
                  $scope.noFile[index] = '';
                }, 3000);
            }
        } else {
            $scope.noFile[index] = 'Please upload a file first';
            $timeout(function () {
              $scope.noFile[index] = '';
            }, 3000);
        }
    };
    //Hide preview of image
    $scope.hidePreview = function($index) {
        $scope.location[$index] = '';
        $scope.openeditImage = false;
        $scope.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };
    // Function call on load more
    $scope.loadMoreUsers = function() {
        $scope.getUserList();
    };
    //creating a standard user from admin
    $scope.createUser = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/createUser/createUserView.html',
            controller: 'createUserController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
}]);

app.factory('userService', function ($resource) {
   return  $resource(APP.endpoints.users,{},{query: { method: "GET", isArray: false }});
});
app.factory('deluserService', function ($resource) {
   return  $resource(APP.endpoints.delUser);
});
app.factory('updateUserService',function($resource){
	return $resource(APP.endpoints.editUser,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('userSearchService', function ($resource) {
   return  $resource(APP.endpoints.searchUser,{},{query: { method: "GET", isArray: false }});
});
app.controller("createActivityController", ['$rootScope', '$scope', '$timeout', '$log', 'createActivity', '$uibModal', '$window', 'lessonService', 'completedLessonService', 'myGroupsService', 'getSearchTagsService', function($rootScope, $scope, $timeout, $log, createActivity, $uibModal, $window, lessonService, completedLessonService, myGroupsService, getSearchTagsService) {
    $scope.showVideoButton = false;
    $scope.lesson = {};
    $scope.Keytakeaways = [];
    $scope.delImage = false;
    $scope.activityFormSubmitted = false;
    $scope.activity = {};
    $scope.activity.description = '';
    $scope.noFile = '';
    $scope.myCommunityCheck = true;
    $scope.mySpaceCheck = true;
    $scope.myGroupsCheck = '';
    $scope.uploadLoader = false;
    $scope.module = {};
    $scope.lessonIndex = null;
    $scope.max = null;
    $scope.completedLesson = [];
    $scope.completedLessons = [];
    $scope.groupCheck = [];
    $scope.showVideo = false;
    $scope.videoTitle = "Add video";
    $scope.showVideoFile = false;
    $scope.videoId = '';
    $scope.tags = [];
    $scope.tagList = [];
    $scope.videoIdForm = false;

    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    if ($rootScope.isAdminLoggedIn) {
        $scope.showVideoButton = true;
    }
    $rootScope.$on('callCreateActivityController', function(event, args) {
        $scope.lesson = args.lesson;
        $scope.module = args.module;
        $scope.lessonIndex = args.lessonIndex;
        $scope.Keytakeaways = $scope.lesson.Keytakeaways ? $scope.lesson.Keytakeaways.split(".") : [];
        $scope.getAllLessons();
    });

    $scope.getAllLessons = function() {
        lessonService.query({
            id: $scope.module._id
        }, function(data) {
            $scope.lessons = data.result;
            $scope.max = data.result.length;
        });
    };

    $scope.Keytakeaways = $scope.lesson.Keytakeaways ? $scope.lesson.Keytakeaways.split(".") : [];
    // Function for uplad the image on s3 server
    $scope.upload = function(element) {
        $scope.noFile = '';
        $scope.delImage = false;
        $scope.imag = '';
        $scope.uploadSuccess = false;
        $scope.showProgressBar = false;
        //$scope.showProgressBar = false;
        $scope.activity.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.activity.fileModel) {
            if ($scope.activity.fileModel.type === 'image/jpeg' || $scope.activity.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.activity.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.activity.fileModel.type,
                    Body: $scope.activity.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {
                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.uploadSuccess = true;
                        $scope.activity.location = data.Location;
                        if ($scope.delImage) {
                            $scope.hidePreview();
                            $scope.activity.location = '';
                            $scope.activity.fileModel = null;
                        }
                        $scope.showProgressBar = true;
                        //$scope.noFile = 'Image uploaded successfully';
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);


            }
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }

        return $scope.activity.location;
    };

    $scope.hidePreview = function() {
        $scope.activity.location = '';
        $scope.activity.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };

    $scope.createActivityItem = function(i, lesson_id, module_Id) {
        $scope.delImage = false;
        if ($rootScope.isLoggedIn === false && $rootScope.isAdminLoggedIn === false) {
            $scope.actionlist = false;
            $scope.showActivity = false;
            $scope.$emit("ShowModalLogin", {});
        } else {
            $scope.activityFormSubmitted = true;
            if ($scope.activity.description === undefined || $scope.activity.description === '') {
                focus('description');
                return false;
            } else {
                $scope.activityFormSubmitted = false;
            }
            if ($scope.uploadLoader === false && !$scope.activity.location && $scope.tagList.length < 1 && !$scope.videoId) {
                $scope.confirmBox(frontendSettings.confirmPostActivityTagsImage);
            } else if ($scope.uploadLoader === false && !$scope.activity.location && !$scope.videoId) {
                $scope.confirmBox(frontendSettings.confirmPostActivityImage );
            } else if ($scope.tagList.length < 1 && $scope.uploadLoader === true) {
                $scope.noFile = 'Please wait while the file is uploading';
                $timeout(function() {
                    $scope.noFile = '';
                }, 2000);
            } else if ($scope.tagList.length < 1 && $scope.uploadLoader === false) {
                $scope.confirmBox(frontendSettings.confirmPostActivityTags);
            }else {
              $scope.postActivity();
            }

            if ($scope.uploadLoader === true) {
                $scope.noFile = 'Please wait while the file is uploading';
                $timeout(function() {
                    $scope.noFile = '';
                }, 2000);
            }
            if ($scope.uploadLoader === false) {
                $scope.noFile = '';
            }


        }
    };
    // Function for post the activity
    $scope.postActivity = function() {

        var opts = {};
        opts.user = APP.currentUser._id;
        opts.lesson = $scope.lesson._id;
        opts.description = $scope.activity.description;
        opts.groups = $scope.groupCheck;
        opts.tags = $scope.tagList;
        if ($scope.videoId === "" || $scope.videoId === null) {
            opts.type = 'image';
        } else {
            opts.type = 'video';
        }
        if (($scope.activity.location === '' || $scope.activity.location === undefined) && ($scope.videoId === "" || $scope.videoId === null)) {
            opts.url = '';
        } else {
            if ($scope.videoId)
                opts.url = $scope.videoId;
            else
                opts.url = $scope.activity.location;
        }
        opts.module = $scope.lesson.module;
        if ($scope.myCommunityCheck) {
            opts.my_community = "true";
        }

        createActivity.save(opts, function(data) {

            $scope.activityFormSubmitted = false;
            $scope.shareLoader = false;
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.tags = [];
                var options = {};
                options.user = APP.currentUser._id;
                options.lesson = $scope.lesson._id;
                options.module = $scope.module._id;
                $timeout(function() {
                    completedLessonService.save(options, function(data) {
                        //$state.go('home',{}, {reload : true});
                        if (data.message === 'ok' && data.statusCode === 200) {
                          $rootScope.$emit("updateProgressBar",{moduleId : $scope.module._id});
                        }
                    }, function(error) {});
                }, 0);
                $timeout(function() {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'app/shared/dialogBox/alertView.html',
                        controller: 'dialogController',
                        scope: $scope,
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sm',
                        resolve: {
                            message: function() {
                                return "Your activity is submitted successfully";
                            }
                        }
                    });
                }, 0);

                $scope.completedLesson[$scope.lessonIndex] = $scope.lesson;
                $scope.module_id = '';
                $scope.activity = {};
                $scope.showProgressBar = false;
                angular.element('#fileUploaded').val(null);
                $timeout(function() {
                    $scope.successAct = false;
                    // modalInstance.dismiss('cancel');
                    // $scope.expandLesson(module_Id);
                    // $scope.$broadcast("CallUpdatedCommunityMethod", {});
                    // $scope.$broadcast("CallUpdatedSpaceMethod", {});
                    $rootScope.getStatistics();
                    //$state.go('home',{id:module_Id},{ reload: true });
                }, 1800);

            } else {
                $scope.activityError = true;
                $scope.showProgressBar = false;
                $timeout(function() {
                    $scope.activityError = false;
                    // modalInstance.dismiss('cancel');
                    // $scope.expandLesson(module_Id);
                }, 1800);
            }
        }, function(error) {
            $scope.activityError = true;
            $timeout(function() {
                $scope.activityError = false;
                // modalInstance.dismiss('cancel');
                $state.go('home', {}, {
                    reload: true
                });
            }, 1800);
        });
    };
    $scope.confirmBox = function(msg) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return msg;
                },
                item: function() {
                    return 'post-without-image';
                }
            }
        });
    };
    $rootScope.$on("moveToProgressBar", function(event, args) {
        $scope.onNext();
    });
    // $scope.closeModal = function() {
    //     $uibModalInstance.dismiss('cancel');
    //     angular.element('#fadein').removeClass('color-overlay');
    // };
    //on next click of the lesson to show next lesson of hhte module
    $scope.onNext = function() {
        $scope.completedLessons = [];
        $timeout(function() {
            lessonService.query({
                id: $scope.module._id
            }, function(data) {
                $scope.lessons = data.result;
                $scope.max = data.result.length;
                angular.forEach(data.result, function(lesson, key) {
                    if (lesson.completed) {
                        $scope.completedLessons.push(lesson);
                    }
                });
                if ($scope.completedLessons.length == $scope.max) {
                    $scope.openProgressModal();

                } else {
                    $scope.openProgressModal();
                }
            });


        }, 0);


    };
    //for opening modal
    $scope.openProgressModal = function() {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/progressBar/progressBarView.html',
            controller: 'progressBarController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            resolve: {
                max: function() {
                    return $scope.max;
                },
                current: function() {
                    return $scope.completedLessons;
                },
                lesson: function() {
                    return $scope.lesson;
                },
                lessonIndex: function() {
                    return $scope.lessonIndex;
                },
                module: function() {
                    return $scope.module;
                }
            }
        });
    };
    $scope.allTotal = 0;
    $scope.myGroupsList = [];
    $scope.listResponse = 1;
    $scope.getGroups = function() {
        var limit_start = $scope.myGroupsList.length;
        var limit = 100;
        var id = APP.currentUser._id;
        // if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
        $scope.listResponse = 0;
        //$scope.moduleLoader = true;
        myGroupsService.query({
            id: id,
            skip: limit_start,
            limit: limit
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.allTotal = data.result.totalCount;
                $scope.myGroupsList = $scope.myGroupsList.concat(data.result.groups);
            } else {
                //$scope.moduleLoader = false;
                $scope.myGroupsList = [];
                $scope.allTotal = 0;
            }
        }, function(error) {});
        // }
    };
    $scope.getGroups();
    $rootScope.$on('groupsComingInCreatActivity', function(event, args) {
        $scope.myGroupsList = [];
        $scope.getGroups();
    });
    $scope.updateSelection = function($event, id) {
        $event.stopPropagation();
        var checkbox = $event.target;
        if (checkbox.checked)
            $scope.groupCheck.push(id);
    };

    $scope.allGroupCheck = function($event) {
        $scope.allChecked = $event.target.checked;
        $scope.groupCheck = [];
        for (var i = 0; i < $scope.myGroupsList.length; i++) {
            $('#' + $scope.myGroupsList[i].group._id).val($event.target.checked);
            if ($event.target.checked)
                $scope.groupCheck.push($scope.myGroupsList[i].group._id);
        }
    };

    $scope.stopEvent = function($event) {
        $event.stopPropagation();
    };

    $(window).click(function(e) {
        try {
            if (!$(e.target).attr('class').includes('video-bg'))
                $scope.activityPlayer.stopVideo();
        } catch (e1) {}
    });

    $scope.addVideo = function() {
        $scope.showVideoFile = true;
        $scope.showVideo = true;

    };

    $scope.doneClick = function() {
        $scope.videoIdForm = true;
        if ($scope.videoId === null || $scope.videoId === '' || $scope.videoId === undefined) {
            focus('videoIdField');
            return false;
        }
        $scope.videoTitle = "Change video";
        $scope.showVideo = false;
    };
    $scope.cancelVideo = function() {
        $scope.videoId = '';
        $scope.videoTitle = "Add video";
        $scope.showVideo = false;
        $scope.showVideoFile = false;
    };
    //tags
    $scope.tagAdded = function(tag) {
        $scope.tagList.push(tag.text);
    };

    $scope.tagRemoved = function(tag) {
        $scope.tagList.pop(tag.text);
    };

    $scope.loadMatchingTags = function(query) {
        return new Promise(function(resolve, reject) {
            getSearchTagsService.get({
                term: query
            }, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    resolve(data.result.tags);
                } else {
                    resolve();
                }

            });
        });
    };
}]);

app.directive('lessonActivity', function() {
    return {
        templateUrl: 'app/components/createActivity/createActivityView.html',
        restrict: 'E',
        controller: 'createActivityController',
        scope: {
            lesson: '='
        }
    };
});

app.factory('createActivity', function($resource){
	return $resource(APP.endpoints.createActivity);
});

app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});

app.factory('completedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson);
});
app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroups,{},{query: { method: "GET", isArray: false }});
});

app.factory('getSearchTagsService', function ($resource) {
   return  $resource(APP.endpoints.getSearchTags,{},{query: { method: "GET", isArray: false }});
});

app.controller("createGroupController", ['$rootScope', '$scope', '$timeout', 'createGroupService', 'userService', '$uibModal', '$uibModalInstance', '$log', 'moduleId', '$filter', 'getInviteMembersService', function($rootScope, $scope, $timeout, createGroupService, userService, $uibModal, $uibModalInstance, $log, moduleId, $filter, getInviteMembersService) {
    $scope.createFormSubmitted = false;
    $scope.group = {};
    /*$scope.group.name = "hello";*/
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLesson = false;
    $scope.editFormSubmitted = false;
    $scope.emails = [];
    $scope.values = [];
    $scope.log = [];
    $scope.chooseFile = false;
    $scope.groupImage = false;
    $scope.location = '';
    $scope.members = [];
    $scope.uploadLoader = false;
    $scope.list = [];
    $scope.showPlus = true;
    //$scope.avatar_url = "";

    $scope.createGroup = function() {
        $scope.createFormSubmitted = true;
        if ($scope.group.name === undefined || $scope.group.name === '' || $scope.group.name === null) {
            focus('gname');
            return false;
        }
        if ($scope.group.description === undefined || $scope.group.description === '' || $scope.group.description === null) {
            focus('gdescription');
            return false;
        }
        var opts = {};
        opts.name = $scope.group.name;
        opts.description = $scope.group.description;
        opts.owner = APP.currentUser._id;
        opts.avatar_url = $scope.location;
        opts.members = $scope.list;

        createGroupService.save(opts, function(data) {
            /*$scope.loader = false;*/
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.members = [];
                var object = {};
                object.group = data.result;
                $scope.hasGroups = true;
                $scope.createFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.group = {};
                $scope.location = "";
                $scope.hidePreview();
                $scope.myGroupsList.unshift(object);
                $rootScope.$emit('groupsComing', {});
                $rootScope.$emit('groupsComingInCreatActivity', {});
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                }, 1000);

            } else if (data.statusCode === 409 && data.message === "group name should be unique for a user") {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueGroup;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 3000);
            } else {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });
    };

    $scope.loadMatchingEmails = function(query) {
        return new Promise(function(resolve, reject) {
            getInviteMembersService.get({
                term: query
            }, function(data) {
                resolve(data.result);
            });
        });
    };

    $scope.tagAdded = function(tag) {
        $scope.list.push(tag.text);
    };

    $scope.tagRemoved = function(tag) {
        $scope.list.pop(tag.text);
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    //edit avatar con
    $scope.editAvatar = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/customBrowse/customBrowseView.html',
            controller: 'customBrowseController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return "";
                }
            }
        });
    };


    $scope.upload = function(element) {
        $scope.noFile = false;
        $scope.imag = '';
        $scope.showProgressBar = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {

                $scope.uploadLoader = true;
                $scope.chooseFile = false;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.showProgressBar = true;
                        $scope.groupImage = true;
                        $scope.chooseFile = false;
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.location = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
            }
        } else {
            $scope.noFile = 'Please upload a file first';
        }
    };

    $scope.hidePreview = function() {
        $scope.showPlus = true;
        $scope.location = '';
        $scope.openeditImage = false;

    };

    $rootScope.$on("uploadGroupAvatar", function(event, args) {
        $scope.groupImage = true;
        $scope.showPlus = false;
        $scope.location = args.avatar_url;
    });
}]);

/*app.directive('myGroups', function () {
return {
templateUrl: 'app/shared/group/groupView.html',
restrict: 'E',
controller: 'myGroupsController'
};
});*/

/*<ul>
	<li ng-repeat="email in emails" data-ng-bind="email"></li>
</ul>*/
/*<input-Dropdown></input-Dropdown>
*/app.directive('inputDropdown', function(userService) {

var template =
'<input ng-model="search" ng-change="searchMembers()" placeholder="INVITE MEMBERSss" tabindex="1" class="form-control">' +
'<div class="dropdown">' +
'<div ng-repeat="email in emails">' +
'<div ng-mousedown="select($event, value)">{{value}}</div>' +
'</div>' +
'</div>';

return {
restrict: 'E',
scope: {
ngModel: '=',
list: '=',
onSelect: '&'
},
template: template,
/*link: function(scope, element, attrs) {
element.addClass('input-dropdown');
scope.select = function(e, value) {
scope.ngModel = value;
scope.onSelect({$event: e, value: value});
};
}*/
link: function (scope, iElement, iAttrs, documentController) {
// Allow the controller here to access the document controller
scope.documentController = documentController;
},
controller: function ($scope) {
/*$scope.save = function (data) {
// Assuming the document controller exposes a function "getUrl"
var url = $scope.documentController.getUrl();

myService.saveComments(url, data).then(function (result) {
// Do something
});
};*/
	$scope.emails = [];
	$scope.searchMembers = function(){
		userService.get({name : $scope.search}, function(data) {
			var users = data.result.users;

			angular.forEach(users, function(user, key) {
				$scope.emails.push(user.local.email);
			});

		});
	};
	$scope.searchMembers();

}
};
});


app.factory('createGroupService', function ($resource) {
   return  $resource(APP.endpoints.createGroup,{},{query: { method: "POST", isArray: false }});
});
app.factory('userService', function ($resource) {
   return  $resource(APP.endpoints.users,{},{query: { method: "GET", isArray: false }});
});
app.factory('getInviteMembersService', function ($resource) {
   return  $resource(APP.endpoints.getInviteMembers,{},{query: { method: "GET", isArray: false }});
});

app.controller("editGroupController", ['$rootScope', '$scope', '$timeout', 'editGroupService', 'userService', '$uibModal', '$uibModalInstance', '$log', 'moduleId', '$filter', 'getGroupService', 'editGroupAvatarService', 'editGroupNameService', 'editGroupDescriptionService', 'editGroupMembersService', 'getInviteMembersService', 'groupMembersService', 'delGrpMemberService', function($rootScope, $scope, $timeout, createGroupService, userService, $uibModal, $uibModalInstance, $log, moduleId, $filter, getGroupService, editGroupAvatarService, editGroupNameService, editGroupDescriptionService, editGroupMembersService, getInviteMembersService, groupMembersService, delGrpMemberService) {
    $scope.createFormSubmitted = false;
    $scope.group = {};
    /*$scope.group.name = "hello";*/
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLesson = false;
    $scope.editFormSubmitted = false;
    $scope.emails = [];
    $scope.values = [];
    $scope.log = [];
    $scope.chooseFile = false;
    $scope.groupImage = false;
    $scope.location = '';
    $scope.members = [];
    $scope.uploadLoader = false;
    $scope.list = [];
    $scope.showPlus = "";
    $scope.showPlusPencil = true;
    $scope.pencil = false;
    $scope.inviteEnabled = false;
    //this file varilables
    $scope.group = {};
    $scope.openeditName = false;
    $scope.openeditDes = false;
    $scope.location = '';
    $scope.editForm = true;
    $scope.memberForm = false;
    $scope.grpDetailClass = "col-xs-6 tab-reg active";
    $scope.grpMemberClass = "col-xs-6 tab-reg";
    $scope.grpMembers = [];
    $scope.delMemberId = '';
    $scope.delMemberIndex = '';
    $scope.userId = '';
    $scope.showUrl = "";

    $scope.getGroup = function() {
        getGroupService.query({
            id: moduleId
        }, function(data) {
            $scope.group = data.result;
            $scope.location = data.result.avatar_url;
            if($scope.group.owner == APP.currentUser._id) {
              $scope.pencil = true;
            }else {
              $scope.pencil = false;
            }
            if ($scope.location  === null || $scope.location  === "" || $scope.location === undefined) {
                $scope.showUrl = false;
                $scope.showPlus = true;
                $scope.group.avatar_url = "";
                $scope.location = "";
            } else {
                $scope.showPlus = false;
                $scope.showUrl = true;
            }
        });
    };
    $scope.getGroup();

    // $scope.editGroupService = function() {
    //     $scope.members = [];
    //     $scope.createFormSubmitted = true;
    //     if ($scope.group.name === undefined || $scope.group.name === '' || $scope.group.name === null) {
    //         focus('gname');
    //         return false;
    //     }
    //     if ($scope.group.description === undefined || $scope.group.description === '' || $scope.group.description === null) {
    //         focus('gdescription');
    //         return false;
    //     }
    //     var opts = {};
    //     opts.name = $scope.group.name;
    //     opts.description = $scope.group.description;
    //     opts.owner = APP.currentUser._id;
    //     opts.avatar_url = $scope.location;
    //     opts.members = $scope.list;
    //
    //     createGroupService.save(opts, function(data) {
    //         /*$scope.loader = false;*/
    //         if (data.statusCode === 200 && data.message === "ok") {
    //             $scope.createFormSubmitted = false;
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.sucessMessage;
    //             $scope.msgClass = 'success-green';
    //             $scope.group = {};
    //             $rootScope.$emit("CallGroupsMethod", {});
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 3000);
    //             //
    //         } else if (data.statusCode === 409 && data.message === "group name should be unique for a user") {
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.uniqueGroup;
    //             $scope.msgClass = 'error-red';
    //             $scope.createFormSubmitted = false;
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 3000);
    //         } else {
    //             $scope.resultMsg = true;
    //             $scope.sucessMsg = frontendSettings.errorOccured;
    //             $scope.msgClass = 'error-red';
    //             $scope.createFormSubmitted = false;
    //             $timeout(function() {
    //                 $scope.resultMsg = false;
    //                 $scope.sucessMsg = '';
    //             }, 4000);
    //         }
    //     });
    // };

    $scope.loadMatchingEmails = function(query) {
        return new Promise(function(resolve, reject) {
            getInviteMembersService.get({
                term: query
            }, function(data) {
                resolve(data.result);
            });
        });
    };

    $scope.tagAdded = function(tag) {
        $scope.list.push(tag.text);
        $scope.inviteEnabled = true;
    };

    $scope.tagRemoved = function(tag) {
        for(var i = 0; i < $scope.list.length; i++) {
            if($scope.list[i] === tag.text)
                $scope.list.splice(i, 1);
        }
        if($scope.list.length <= 0)
            $scope.inviteEnabled = false;
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    //edit avatar con
    $scope.editAvatar = function($event) {
        $event.stopPropagation();
        // $scope.showPlus = false;
        // $scope.chooseFile = true;
        // $scope.uploadLoader = false;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/customBrowse/customBrowseView.html',
            controller: 'customBrowseController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return "";
                }
            }
        });

    };


    $scope.upload = function(element) {

        $scope.noFile = false;
        $scope.imag = '';
        $scope.showProgressBar = false;
        $scope.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.fileModel) {
            if ($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                $scope.chooseFile = false;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.fileModel.type,
                    Body: $scope.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {

                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.showProgressBar = true;
                        $scope.groupImage = true;
                        $scope.chooseFile = false;
                        $scope.openeditImage = true;
                        //$scope.noFile = 'Image uploaded successfully';

                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.location = data.Location;
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
            }
        } else {
            $scope.noFile = 'Please upload a file first';
        }
    };

    $scope.hidePreview = function() {
        $scope.groupImage = true;
        $scope.openeditImage = false;
        $scope.showPlus = false;
        $scope.fileModel = null;
        $scope.showPlusPencil = true;
        if ($scope.group.avatar_url === null || $scope.group.avatar_url === "") {
            $scope.showPlus = true;
        } else {
            $scope.showPlus = false;
        }
    };

    //edit group-name
    $scope.openEditForm = function(index) {
        $scope.openeditImage = false;
        $scope.openeditName = false;
        $scope.openeditDob = false;
        $scope.openeditEmail = false;
        $scope.openeditPassword = false;
        switch (index) {
            case 1:
                $scope.openeditImage = true;
                break;
            case 2:
                $scope.openeditName = true;
                focus('gname');
                break;
            case 3:
                $scope.openeditDes = true;
                focus('gdes');
                break;
            case 4:
                $scope.openeditEmail = true;
                focus('email');
                $scope.showErrEmail = false;
                $scope.errMsg = '';
                break;
            case 5:
                $scope.openeditPassword = true;
                $scope.editUser.password = '';
                focus('password');
                break;
        }
    };

    $scope.changeImage = function() {
        if ($scope.location === '' || $scope.location === undefined) {
            $scope.noFile = 'Please upload a file first';
            focus('image');
            $timeout(function() {
                $scope.noFile = '';
            }, 3000);
            return false;
        }
        var opts = {};
        opts.avatar_url = $scope.location;
        editGroupAvatarService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.openeditImage = false;
            $scope.group.avatar_url = $scope.location;
            $scope.location = '';
            $scope.hidePreview();
            $scope.getGroup();
            // }
        });
    };

    // Edit change Name
    $scope.changeName = function() {
        $scope.nameSubmit = true;
        if ($scope.group.name === undefined || $scope.group.name === '') {
            focus('gname');
            return false;
        }
        var opts = {};
        opts.name = $scope.group.name;
        editGroupNameService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.openeditName = false;
            $scope.nameSubmit = false;
            $scope.getGroup();
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.openeditName = false;
                $scope.nameSubmit = false;
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
            }
        });
    };

    // Edit change description
    $scope.changeDes = function() {
        $scope.desSubmit = true;
        if ($scope.group.description === undefined || $scope.group.description === '') {
            focus('gdes');
            return false;
        }
        var opts = {};
        opts.description = $scope.group.description;
        editGroupDescriptionService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.desSubmit = false;
                $scope.openeditDes = false;
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
            }
        });
    };

    $scope.changeMembers = function() {
        var opts = {};
        opts.members = $scope.list;
        editGroupMembersService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.members = [];
                $scope.resultMsg = true;
                $scope.sucessMsg = "Invitations sent successfully";
                $scope.msgClass = 'success-green';
                $rootScope.$emit("CallGroupsMethod", {});
                $scope.getGroup();
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                }, 2000);

            }
        });
    };

    //Cancel edit  box
    $scope.cancelSave = function(index) {
        switch (index) {
            case 1:
                $scope.openeditImage = false;
                $scope.hidePreview();
                $scope.getGroup();
                break;
            case 2:
                $scope.openeditName = false;
                $scope.getGroup();
                break;
            case 3:
                $scope.openeditDes = false;
                $scope.getGroup();
                break;
            case 4:
                $scope.openeditEmail = false;
                $scope.getGroup();
                break;
            case 5:
                $scope.openeditPassword = false;
                $scope.getGroup();
                break;
        }
    };

    // second form, members form
    $scope.membersForm = function() {
        $scope.grpDetailClass = "col-xs-6 tab-reg";
        $scope.grpMemberClass = "col-xs-6 tab-reg active";
        $scope.editForm = false;
        $scope.memberForm = true;
    };

    $scope.detailsForm = function() {
        $scope.grpDetailClass = "col-xs-6 tab-reg active";
        $scope.grpMemberClass = "col-xs-6 tab-reg";
        $scope.editForm = true;
        $scope.memberForm = false;

    };
    $scope.memberList = [];
    $scope.groupMembers = function() {
        groupMembersService.get({
            id: moduleId
        }, function(data) {
            $scope.grpMembers = data.result.members;
        });
        angular.forEach($scope.grpMembers, function(member, key) {
            $scope.memberList.push(member.user.local.email);
        });
    };
    $scope.groupMembers();

    $scope.confirmModal = function(index, id, userId) {
        $scope.delMemberId = id;
        $scope.delMemberIndex = index;
        $scope.userId = userId;
        $scope.grpMembers = $scope.grpMembers;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure to delete this member?";
                },
                item: function() {
                    return 'delete-grpMember';
                }
            }
        });

    };

    $rootScope.$on("uploadGroupAvatar", function(event, args) {
        $scope.groupImage = true;
        $scope.showPlus = false;
        $scope.openeditImage = true;
        $scope.location = args.avatar_url;
    });

}]);

app.factory('editGroupService', function ($resource) {
   return  $resource(APP.endpoints.editGroup,{id: '@id'},{update: { method: "PUT"}});
});

app.factory('getGroupService', function ($resource) {
   return  $resource(APP.endpoints.editGroup,{id: '@id'},{query: { method: "GET", isArray: false}});
});
app.factory('changeGroupNameService', function ($resource) {
   return  $resource(APP.endpoints.editGroup);
});
app.factory('changeGroupDesService', function ($resource) {
   return  $resource(APP.endpoints.editGroup);
});
app.factory('editGroupAvatarService', function ($resource) {
   return  $resource(APP.endpoints.editGroupAvatar,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupNameService', function ($resource) {
   return  $resource(APP.endpoints.editGroupName,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupDescriptionService', function ($resource) {
   return  $resource(APP.endpoints.editGroupDescription,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('editGroupMembersService', function ($resource) {
   return  $resource(APP.endpoints.editGroupMembers,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('groupMembersService', function ($resource) {
   return  $resource(APP.endpoints.groupMembers,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('delGrpMemberService', function ($resource) {
   return  $resource(APP.endpoints.delGrpMember, {id : '@id', mid : '@mid'},{query : { method:  "DELETE", isArray: false}});
});

app.controller("myFavouritesController", ['$scope', '$rootScope', 'getFollowersService', 'getFollowingService', '$uibModal', 'Upload', '$timeout', '$sce', function($scope, $rootScope, getFollowersService, getFollowingService, $uibModal, Upload, $timeout, $sce) {
    $scope.followingsLoader = true;
    $scope.followersLoader = true;
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $scope.followings = function() {
        getFollowingService.get(function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.followingsLoader = false;
                $scope.followings = data.result;
            }
        });
    };
    $scope.followings();
    $scope.followers = function() {
        getFollowersService.get(function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.followersLoader = false;
                $scope.followers = data.result;
            }
        });
    };
    $scope.followers();
    $scope.unfollow = function(index, id, follower) {
        $scope.id = id;
        $scope.index = index;
        $scope.follower = follower;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    if ($scope.follower)
                        return 'Are you sure to delete this follower?';
                    else
                        return 'Are you sure to delete this favourite?';
                },
                item: function() {
                    return 'delete-follower';
                }
            }
        });

    };


    // $scope.uploadFiles = function(files) {
    //     $scope.files = files;
    //     if (files && files.length) {
    //         Upload.upload({
    //             url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
    //             data: {
    //                 files: files
    //             }
    //         }).then(function(response) {
    //             $timeout(function() {
    //                 $scope.result = response.data;
    //             });
    //         }, function(response) {
    //             if (response.status > 0) {
    //                 $scope.errorMsg = response.status + ': ' + response.data;
    //             }
    //         }, function(evt) {
    //             $scope.progress =
    //                 Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    //         });
    //     }
    // };


    // $scope.$watch(function(scope) {
    //         return scope.locationVideo;
    //     },
    //     function(newVal, oldVal) {
    //         var player = document.getElementById('videoPlayer');
    //         var videoSource = document.getElementById('videoSource');
    //
    //         player.pause();
    //         videoSource.src = newVal;
    //         player.load();
    //         player.play();
    //     });

    $scope.uploadFiles = function(files) {
        $scope.files = files[0];
        if (files) {
            // if ($scope.activity.fileModel.type === 'image/jpeg' || $scope.activity.fileModel.type === 'image/png') {
            $scope.uploadLoader = true;
            var bucket = new AWS.S3({
                params: {
                    Bucket: 'mindmaxdaffo'
                }
            });
            var imageName = $scope.files.name + Math.floor(Date.now() / 1000);
            $scope.imag = imageName;
            var params = {
                Key: imageName,
                ContentType: $scope.files.type,
                Body: $scope.files
            };
            bucket.upload(params).on('httpUploadProgress', function(evt) {
                $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                $scope.$digest();
            }).send(function(err, data) {
                $scope.uploadLoader = false;
                $scope.uploadSuccess = true;
                $scope.locationVideo = $sce.trustAsResourceUrl(data.Location);
                if ($scope.delImage) {
                    $scope.hidePreview();
                    $scope.activity.location = '';
                    $scope.activity.fileModel = null;
                }
                $scope.showProgressBar = true;
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);
                $scope.barShow = '0%';
                $scope.$digest();
            });
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }
        return $scope.locationVideo;
    };

    // $scope.$watch(function (scope) {
    // return $scope.locationVideo;
    // }, function (newVal, oldval) {
    //
    // });

}]);

app.filter("trustUrl", ['$sce', function($sce) {
    return function(recordingUrl) {
        return $sce.trustAsResourceUrl(recordingUrl);
    };
}]);

app.factory('getFollowingService', function ($resource) {
    return $resource(APP.endpoints.followings, {}, { query: { method: "GET", isArray: false } });
});
app.factory('getFollowersService', function ($resource) {
    return $resource(APP.endpoints.followers, {}, { query: { method: "GET", isArray: false } });
});
app.factory('unfollowService', function ($resource) {
    return $resource(APP.endpoints.unfollowUser, { id: '@id', follower: '@follower' }, { update: { method: 'PUT' } });
});
app.controller("homeController", ['$rootScope', '$scope', 'moduleService', 'lessonDetailService', 'getCommunityService', 'myGroupsService', '$location', '$uibModal', '$state', '$timeout', '$filter', 'userByIdService', 'getSearchTagsService', 'searchUserNameService', 'searchModuleNameService', 'getCompletedLessonService', 'lessonService', function($rootScope, $scope, moduleService, lessonDetailService, getCommunityService, myGroupsService, $location, $uibModal, $state, $timeout, $filter, userByIdService, getSearchTagsService, searchUserNameService, searchModuleNameService, getCompletedLessonService, lessonService) {
    // $scope.getLoggedInUser = function() {
    //   userByIdService.get({  id: APP.currentUser._id},{},function (data) {
    //     if(data.statusCode===200 && data.message==='ok') {
    //       if(APP.currentUser.local.deleted===true || data.result[0].local.password !== APP.currentUser.local.password || data.result[0].local.name !== APP.currentUser.local.name) {
    //         $rootScope.$emit('logoutLoggedInUser',{});
    //       }
    //     }
    //   });
    // };
    // $scope.getLoggedInUser();

    window.onpopstate = function(event) {
        if ($location.url() == '/home' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $scope.moduleDetails = true;
            $scope.showLessonActivity = false;
            $rootScope.activeSection = "";
            $rootScope.myTabs = false;
            $scope.$broadcast('callModuleController', {
                module: $rootScope.module
            });
            $rootScope.$broadcast('updateMyCommunity', {
                module: $rootScope.module
            });
            for (var i = 0; i < $rootScope.activeModule.length; i++)
                $rootScope.activeModule[i] = false;
            for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
                $rootScope.activeModuleLast[j] = false;

            if (!$rootScope.isLast)
                $rootScope.activeModule[$rootScope.moduleIndex] = true;
            else
                $rootScope.activeModuleLast[$rootScope.moduleIndex] = true;
        }
        if ($location.url() != '/home' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $scope.moduleDetails = false;
            $scope.showLastComponent = false;
            $scope.showLessonActivity = false;
        }
        if ($location.url() != '/home' && $location.url() != '/' && $location.url() != '/home/groups' && $location.url() != '/home/space' && ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)) {
            $state.go('home', {}, {
                reload: true
            });
        }
        if ($location.url() == '/home/groups') {
            $state.go('home.groups', {}, {

            });
        }
        if ($location.url() == '/home/space') {
            $state.go('home.space', {}, {

            });
        }
    };
    // if logged in...
    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        // set the google tracking user id
        ga('set', 'userId', APP.currentUser._id);

        // update the header
        $rootScope.$broadcast('updateName', {
            name: APP.currentUser.local.name
        });
        $rootScope.$broadcast('updateHeaderDp', {
            avatar_url: APP.currentUser.local.avatar_url
        });
    }

    // Open modal for view full detail of activity in my community
    $scope.viewActivity = function(activityId) {
        $scope.comments = 0;

        if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn) {

            $scope.$emit("ShowModalLogin", {});
        } else {
            $timeout(function() {
                $rootScope.$broadcast("callViewActivityFromHome", {
                    activityId: activityId
                });
            }, 500);
        }
    };

    if (window.localStorage.location) {
        if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn)
            $scope.$emit('ShowModalLogin', {});
        else {
            $location.url('/home/' + window.localStorage.location);
            window.localStorage.location = '';

            if (window.location.pathname !== $location.url())
                window.location.pathname = $location.url();
            else {
                $scope.viewActivity($location.url().split('/')[$location.url().split('/').length - 1]);
            }
        }
    }

    $scope.oneAtATime = true;
    $scope.showCommunity = false;
    $scope.showSpace = false;
    $scope.showGroup = false;
    $scope.showLessonActivity = false;
    $scope.showLastComponent = false;
    $scope.showGrpActivities = false;
    $scope.showMore = true;
    $scope.moduleDetails = false;
    $scope.progressBar = false;
    $scope.adminCommunity = false;
    $rootScope.activeModule = [];
    $rootScope.activeModuleLast = [];
    $scope.lessonForActivity = {};
    $scope.module = {};
    $rootScope.activeSection = "";
    $rootScope.myTabs = false;
    $rootScope.moduleSelected = false;
    $rootScope.filterActive = false;
    $scope.patternSelected = "Module";
    $rootScope.showProgress = false;
    $scope.max = 0;
    $scope.current = 0;
    $scope.percent = 0;
    $scope.tags = [];

    //when user is not logged in
    if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn) {
        $scope.moduleDetails = false;
        //$rootScope.$broadcast('callMyCommunity',{});
        $scope.showCommunity = true;

    } else if ($rootScope.isAdminLoggedIn && $rootScope.count == 1) {
        $state.go('manage-module');
        $rootScope.count = 2;
    } else if ($rootScope.isAdminLoggedIn && $rootScope.count == 2) {
        $scope.moduleDetails = true;
        $scope.showCommunity = false;
    }
    $rootScope.$emit("callHeaderController", false);

    $rootScope.$on("callHomeControllerForActivity", function(event, args) {
        $rootScope.$broadcast("closingModal", {});
        $rootScope.$broadcast("callCreateActivityController", {
            lesson: args.lesson,
            module: $scope.module,
            lessonIndex: args.index
        });
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.moduleDetails = false;
        $scope.showGrpActivities = false;
        $scope.showLessonActivity = true;

    });

    $rootScope.$on("callHomeConLastActiviyComponent", function(event, args) {
        $rootScope.$broadcast("closingModal", {});
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.moduleDetails = false;
        $scope.showLessonActivity = false;
        $scope.showGrpActivities = false;
        $scope.showLastComponent = true;
        $rootScope.$broadcast("callLastComponent", {
            module: args.module
        });
    });
    $scope.modules = [];
    $scope.getModules = function() {
        moduleService.query({
            skip: 0,
            limit: 60
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.modules = data.result.module;
                $scope.modules = $filter('orderBy')($scope.modules, 'name');
                if ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)
                    $scope.showMycommunity();
                // else
                //     $scope.moduleClicked(0, $scope.modules[0]);
                // $scope.firstModules = $scope.modules.slice(0, 6);
                $scope.lastModules = $scope.modules;
                $scope.totalModules = data.result.totalCount;
                // if ($scope.totalModules < 6 || $scope.totalModules == 6) {
                //     $scope.showMore = false;
                // }
            }
        }, function(error) {});
    };
    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        $scope.getModules();
    }

    $scope.getAllLessons1 = function(module, current) {
        lessonService.query({
            id: module
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                    $scope.lessons = data.result;
                    $scope.max = data.result.length;
                    $scope.percent = (current / $scope.max) * 100;
                    $scope.hasLessons = true;
                } else {
                    $scope.hasLessons = false;
                }
            }
        }, function(error) {});
    };

    $scope.getCompletedLessons = function(id) {
        getCompletedLessonService.query({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                    $scope.current = data.result.length;
                    $timeout(function() {
                        $scope.getAllLessons1(id, data.result.length);
                    }, 0);
                } else {
                    $scope.percent = 0;
                    $scope.current = 0;
                }
            }
        }, function(error) {});
    };
    $rootScope.$on("updateProgressBar", function(event, args) {
        $scope.getCompletedLessons(args.moduleId);
    });
    $scope.moduleClicked = function(index, module, isLast) {
        $rootScope.showProgress = true;
        $scope.tagRemoved();
        $scope.getCompletedLessons(module._id);
        $rootScope.moduleSelected = true;
        $scope.adminCommunity = false;
        $rootScope.moduleIndex = index;
        $rootScope.isLast = isLast;
        $rootScope.activeSection = "";
        $rootScope.myTabs = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        $scope.moduleDetails = true;

        for (var i = 0; i < $rootScope.activeModule.length; i++)
            $rootScope.activeModule[i] = false;
        for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
            $rootScope.activeModuleLast[j] = false;

        if (!isLast)
            $rootScope.activeModule[index] = true;
        else {
            $("#module-toggle").dropdown("toggle");
            $rootScope.activeModuleLast[index] = true;
        }

        $scope.showModule = 'module';
        $scope.whenClicked = true;
        $rootScope.module = module;
        $scope.$broadcast('callModuleController', {
            module: module
        });
        $rootScope.$broadcast('updateMyCommunity', {
            module: module
        });
        $state.go('home');

    };
    //showing module when clicked from activity item
    $rootScope.$on("callMyModule", function(event, args) {
        $scope.showingModule(args.module);
    });
    $scope.showingModule = function(module) {
        $rootScope.moduleSelected = true;
        $scope.adminCommunity = false;
        // $rootScope.moduleIndex = index;
        // $rootScope.isLast = isLast;
        $rootScope.activeSection = "";
        $rootScope.myTabs = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        $scope.moduleDetails = true;

        // for (var i = 0; i < $rootScope.activeModule.length; i++)
        //     $rootScope.activeModule[i] = false;
        // for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
        //     $rootScope.activeModuleLast[j] = false;
        //
        // if (!isLast)
        //     $rootScope.activeModule[index] = true;
        // else {
        //     $("#module-toggle").dropdown("toggle");
        //     $rootScope.activeModuleLast[index] = true;
        // }

        $scope.showModule = 'module';
        $scope.whenClicked = true;
        $rootScope.module = module;
        $scope.$broadcast('callModuleController', {
            module: module
        });
        $rootScope.$broadcast('updateMyCommunity', {
            module: module
        });
        $state.go('home');

    };

    $scope.showMyspace = function() {
        // $scope.tags = [];
        $scope.tagRemoved();
        $rootScope.showProgress = false;
        $rootScope.activeSection = "space";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showGroups = false;
        $scope.showCommunity = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        //$scope.showSpace = true;
        for (var i = 0; i < $rootScope.activeModule.length; i++)
            $rootScope.activeModule[i] = false;
        for (var j = 0; j < $rootScope.activeModuleLast.length; j++)
            $rootScope.activeModuleLast[j] = false;

        $scope.$broadcast("CallUpdatedSpaceMethod", {});
        $state.go('home.space');

    };

    $scope.showMycommunity = function() {

        $scope.tagRemoved();
        $rootScope.activeSection = "community";
        if ($rootScope.isAdminLoggedIn)
            $scope.adminCommunity = true;
        $rootScope.showProgress = false;
        $rootScope.myTabs = false;
        $scope.moduleDetails = false;
        $scope.showGroups = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        // $scope.showCommunity = true;
        // if ($rootScope.isLoggedIn) {
        //   $timeout(function () {
        //     $rootScope.$broadcast('updateMyCommunity', {
        //         module: $rootScope.module
        //     });
        //   }, 100);

        // }
        // $scope.tags = [];
        // $scope.tagRemoved();
        $state.go('home.community');
    };

    $scope.showMyGroups = function() {
        // $scope.tags = [];
        $scope.tagRemoved();
        $rootScope.showProgress = false;
        $rootScope.activeSection = "groups";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGrpActivities = false;
        //$scope.showGroups = true;
        $state.go('home.groups');

    };

    $rootScope.$on('showMyGroups', function(event, args) {
        $scope.showMyGroups();
    });

    //show group Activities
    $rootScope.$on("groupActivitiesPage", function(event, args) {
        $rootScope.activeSection = "groups";
        $rootScope.myTabs = true;
        $scope.moduleDetails = false;
        $scope.showCommunity = false;
        $scope.showSpace = false;
        $scope.showLessonActivity = false;
        $scope.showLastComponent = false;
        $scope.showGroups = false;
        $scope.showGrpActivities = true;
        $rootScope.$broadcast("groupActivitiesPageId", {
            id: args.id,
            name: args.name
        });

    });

    $scope.stopEvent = function($event) {
        $event.stopPropagation();
    };
    //To select pattern --module/tag/user
    $scope.pattern = function(type) {
        $("#search-toggle").dropdown("toggle");
        // $scope.tags = [];
        $scope.tagRemoved($scope.tags.length <= 0);
        switch (type) {
            case 1:
                $scope.patternSelected = "Module";
                break;
            case 2:
                $scope.patternSelected = "Tag";
                break;
            case 3:
                $scope.patternSelected = "User";
                break;
        }
    };

    $scope.loadMatching = function(query) {
        if ($scope.patternSelected == "Tag") {
            return new Promise(function(resolve, reject) {
                getSearchTagsService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result.tags);
                    } else {
                        resolve();
                    }

                });
            });
        } else if ($scope.patternSelected == "User") {
            return new Promise(function(resolve, reject) {
                searchUserNameService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result);
                    } else {
                        resolve();
                    }

                });
            });
        } else if ($scope.patternSelected == "Module") {
            return new Promise(function(resolve, reject) {
                searchModuleNameService.get({
                    term: query
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        resolve(data.result);
                    } else {
                        resolve();
                    }

                });
            });
        }
    };
    $scope.tagAdding = function(tag) {
        $rootScope.filterActive = true;
        $scope.tags = [tag];
        if ($scope.patternSelected == "Module") {
            $rootScope.$broadcast("byModuleName", {
                module: tag
            });
        } else if ($scope.patternSelected == "Tag") {
            var module = {};
            module._id = tag.text;
            $rootScope.$broadcast("byTagName", {
                module: module
            });
        } else if ($scope.patternSelected == "User") {
            $rootScope.$broadcast("byUserName", {
                module: tag
            });
        }

        if ($rootScope.activeSection !== 'community')
            $scope.showMycommunity();
        
        return false;
    };

    $scope.tagRemoved = function(noReload) {
        $rootScope.filterActive = false;
        $scope.tags = [];
        if (noReload === true)
            return;
        $rootScope.$broadcast("whenNoTag", {});
    };

}]);

app.directive('headerNav', function() {
    return {
        templateUrl: 'app/components/home/homeView.html',
        restrict: 'E',
        controller: 'headerNav',
        scope: {
         title: '@'
        },

    };
});

app.directive('enforceMaxTags', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngCtrl) {
      var maxTags = attrs.maxTags ? parseInt(attrs.maxTags, '10') : null;

      ngCtrl.$parsers.push(function(value) {
        if (value && maxTags && value.length > maxTags) {
          value.splice(value.length - 1, 1);
        }
        return value;
      });
    }
  };
});

app.directive('limitTags', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModel) {
            var maxTags = parseInt(attrs.maxTags, 10);
            ngModel.$parsers.unshift(function(value) {
                if (value && value.length > maxTags) {
                    value.splice(value.length - 1, 1);
                }
                return value;
            });
        }
    };
});

app.factory('moduleService', function ($resource) {
   return  $resource(APP.endpoints.module,{},{query: { method: "GET", isArray: false }});
});
app.factory('lessonDetailService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail);
});
app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroupsService,{},{query: { method: "GET", isArray: false }});
});
app.factory('userByIdService', function ($resource) {
   return  $resource(APP.endpoints.userById,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('searchUserNameService', function ($resource) {
   return  $resource(APP.endpoints.searchForUserName,{},{query: { method: "GET", isArray: false }});
});
app.factory('searchModuleNameService', function ($resource) {
   return  $resource(APP.endpoints.searchModuleName,{},{query: { method: "GET", isArray: false }});
});

app.controller("landingController",['$scope', function($scope) {
	$scope.landing  = true;
}]);

app.controller("lessonModuleController", ['$rootScope', '$scope', '$timeout', 'getActionService', 'saveCompleteActionService', 'moduleService', 'lessonService', 'moduleDelService', '$uibModal', '$log', 'moduleSearchService', 'deleteLessonService', '$filter', function($rootScope, $scope, $timeout, getActionService, saveCompleteActionService, moduleService, lessonService, moduleDelService, $uibModal, $log, moduleSearchService, deleteLessonService, $filter) {
    $scope.modules = [];
    $scope.loader = [];
    $scope.lesson = [];
    $scope.isshowLesson = [];
    $scope.isnoLesson = [];
    $scope.showCreateForm = false;
    $scope.showEditForm = false;
    $scope.showAllModuleList = [];
    $scope.id = null;
    $scope.index = '';
    $scope.firstForm = true;
    $scope.secondForm = false;
    $scope.sortModules = false;
    // Get module list
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.moduleIndex = '';
    $scope.parentId = '';

    $scope.getModuleList = function() {
        var limit_start = $scope.showAllModuleList.length;
        var limit = 30;

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.moduleLoader = true;
            moduleService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.moduleLoader = false;
                    $scope.getModuleShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.showAllModuleList = $scope.showAllModuleList.concat(data.result.module);
                } else {
                    $scope.moduleLoader = false;
                    $scope.getModuleShow = false;
                    $scope.showAllModuleList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
                console.log("error", error);
            });
        }
    };
    $scope.getModuleList();
    $rootScope.$on("CallModulesMethod", function() {

        $scope.showAllModuleList = [];
        $scope.getModuleList();
    });
    //search user by name
    $scope.searchModule = function() {
        var limit_start = 0;
        var limit = 30;

        moduleService.get({
            name: $scope.search,
            skip: limit_start,
            limit: limit
        }, function(data) {
            $scope.showAllModuleList = data.result.module;
        });
    };
    // Show lesson form module ID
    $scope.showLesson = function(index, id) {
        $rootScope.lessonIndex = index;
        $rootScope.lessonId = id;
        $scope.loader[index] = true;
        $scope.isshowLesson[index] = true;
        lessonService.get({
            id: id
        }, function(data) {
            $scope.loader[index] = false;
            if (data.statusCode === 200 && data.message === "ok") {
                if (data.result.length > 0) {
                    $scope.lesson[index] = data.result;
                    $scope.isnoLesson[index] = false;
                } else {
                    $scope.isnoLesson[index] = true;
                }
            }
        });
    };
    $scope.sortMyModules = function() {
        $scope.sortModules = !$scope.sortModules;
        $scope.showAllModuleList = $scope.showAllModuleList;
        if ($scope.sortModules === true) {
            $scope.showAllModuleList = $filter('orderBy')($scope.showAllModuleList, 'name').reverse();
        } else {
            $scope.showAllModuleList = [];
            $scope.getModuleList();
            // $scope.showAllModuleList = $filter('orderBy')($scope.showAllModuleList, 'createdAt');
        }
    };
    // Hide lesson form module ID
    $scope.hideLesson = function(index) {
        $scope.isshowLesson[index] = false;
        $scope.lesson[index] = '';
        $scope.isnoLesson[index] = false;
        $scope.loader[index] = false;
    };
    // delete module by module ID
    $scope.deleteModule = function(index, id) {
        $scope.showAllModuleList = $scope.showAllModuleList;
        $scope.id = id;
        for(var i = 0; i < $scope.showAllModuleList.length; i++) {
            if($scope.showAllModuleList[i]._id === id)
                $scope.index = i;
        }

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this module?";
                },
                item: function() {
                    return "delete-module";
                }
            }
        });
    };
    //Create module
    $scope.createModule = function() {
        $scope.firstForm = true;
        $scope.secondForm = false;
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        $scope.showAllModuleList = $scope.showAllModuleList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/manageModule/moduleView.html',
            controller: 'manageModuleController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
    // Edit module

    $scope.editModule = function(id, index) {
        $scope.firstForm = true;
        $scope.secondForm = false;
        $scope.showEditForm = true;
        $scope.showCreateForm = false;
        $scope.moduleIndex = index;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/manageModule/moduleView.html',
            controller: 'manageModuleController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return id;
                },
                lesson: function() {
                    return 'lesson';
                }
            }
        });

    };
    // Function call on loadmore
    $scope.loadMore = function() {
        $scope.getModuleList();
    };
    //to show edit lesson form
    $scope.editLesson = function(lesson) {
        $scope.firstForm = false;
        $scope.secondForm = true;
        $scope.lesson = $scope.lesson;
        if(lesson.type == 'image') {
          var modalInstance1 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeImageView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else if(lesson.type == 'video'){
          var modalInstance2 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeVideoView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else if(lesson.type == 'text'){
          var modalInstance3 = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/shared/lessonType/lessonTypeTextView.html',
              controller: 'lessonTypeController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson;
                  },
                  type: function() {
                    return "Edit Lesson";
                  }
              }
          });
        }else{
          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'app/components/manageModule/moduleView.html',
              controller: 'manageModuleController',
              scope: $scope,
              backdrop: 'static',
              keyboard: false,
              size: 'lg',
              resolve: {
                  moduleId: function() {
                      return lesson._id;
                  },
                  lesson: function() {
                      return lesson;
                  }
              }
          });
        }


    };

    $scope.deleteLesson = function(lesson, index, parentIndex) {
        $scope.parentId = parentIndex;
        $scope.id = lesson._id;
        $scope.index = index;
        $scope.lesson = $scope.lesson;
        $scope.isnoLesson = $scope.isnoLesson;

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this lesson?";
                },
                item: function() {
                    return "delete-lesson";
                }
            }
        });
    };
    //for updating lessonService
    $rootScope.$on("updatelessons", function(event, args) {
        $scope.showLesson(args.moduleIndex, args.moduleId);
    });
}]);

app.factory('moduleDelService', function ($resource) {
   return  $resource(APP.endpoints.actionModule, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});
app.factory('moduleSearchService', function ($resource) {
   return  $resource(APP.endpoints.searchModule,{},{query: { method: "GET", isArray: false }});
});
app.factory('deleteLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});
app.factory('moduleService', function ($resource) {
   return  $resource(APP.endpoints.module,{},{query: { method: "GET", isArray: false }});
});

app.controller("manageModuleController", ['$rootScope', '$scope', '$timeout', 'moduleAddService', '$uibModalInstance', '$log', 'moduleId', 'moduleDelService', 'updateModuleService', 'lessonService', 'createLessonService', 'getLessonService', 'editLessonService','$uibModal', function($rootScope, $scope, $timeout, moduleAddService, $uibModalInstance, $log, moduleId, moduleDelService, updateModuleService, lessonService, createLessonService, getLessonService, editLessonService, $uibModal) {
    $scope.createFormSubmitted = false;
    $scope.module = {};
    $scope.lesson1 = {};
    $scope.lesson = {};
    $scope.resultMsg = false;
    $scope.sucessMsg = '';
    $scope.msgClass = '';
    $scope.loader = false;
    $scope.totalLesson = [];
    $scope.createLessonView = false;
    $scope.editFormSubmitted = false;
    $scope.createLessonFormSubmitted = false;
    $scope.lessonResultMsg = false;
    $scope.pattern = "activity";
    $scope.getTotalLesson = function(id) {
        lessonService.get({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.totalLesson = data.result;
            } else {
                $scope.totalLesson = [];
            }
        });
    };
    // create module
    $scope.createModule = function() {
        $scope.createFormSubmitted = true;
        if ($scope.module.name === undefined || $scope.module.name === '' || $scope.module.name === null) {
            focus('mname');
            return false;
        }
        if ($scope.module.description === undefined || $scope.module.description === '' || $scope.module.description === null) {
            focus('mdescription');
            return false;
        }
        $scope.loader = true;
        var opts = {};
        opts.name = $scope.module.name;
        opts.description = $scope.module.description;
        $scope.createFormSubmitted = false;
        moduleAddService.save(opts, function(data) {
            $scope.loader = false;
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.showAllModuleList.unshift($scope.module);

                $scope.createFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.module = {}; //
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    //$scope.createFormSubmitted = false;
                    $scope.closeModal();
                    $scope.showCreateForm = false;
                    $scope.firstForm = false;
                }, 3000);
                //
            } else if (data.statusCode === 422 && data.message.message === "Module name must be unique") {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueModule;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            } else {
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });
    };
    // Close closeModal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        $scope.$emit("CallModulesMethod", {});
        angular.element('#fadein').removeClass('color-overlay');
    };
    // Edit form
    $scope.editForm = function(id) {
        moduleDelService.get({
            id: id
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editFormData = data.result;
            }
        });
    };
    $scope.getLesson = function(moduleId) {
        getLessonService.query({
            id: moduleId
        }, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.lesson1 = data.result;
            }
        });
    };
    if (moduleId) {
        $scope.getLesson(moduleId);
        $scope.editForm(moduleId);
        $scope.getTotalLesson(moduleId);
    }
    // update module
    $scope.updateModule = function() {
        $scope.editFormSubmitted = true;
        if ($scope.editFormData.name === undefined || $scope.editFormData.name === '') {
            focus('ename');
            return false;
        }
        if ($scope.editFormData.description === undefined || $scope.editFormData.description === '') {
            focus('edescription');
            return false;
        }
        $scope.loader = true;
        var opts = {};
        opts.name = $scope.editFormData.name;
        opts.description = $scope.editFormData.description;
        updateModuleService.update({
            id: moduleId
        }, opts, function(data) {
            $scope.loader = false;
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.editSucessMessage;
                $scope.msgClass = 'success-green';
                $scope.module = {};
                $rootScope.$emit("CallModulesMethod", {});
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();

                }, 2000);
            } else {
                $scope.editFormSubmitted = false;
                $scope.resultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.msgClass = '';
                }, 4000);
            }
        });
    };
    // open create modal form
    $scope.createModuleForm = function() {
        $scope.showCreateForm = true;
        $scope.showEditForm = false;
    };
    // open edit module form
    $scope.editModuleForm = function() {
        if (moduleId) {
            $scope.showCreateForm = false;
            $scope.showEditForm = true;
        }
    };
    // open create lesson form
    $scope.showCreateLessonForm = function() {
        switch ($scope.pattern) {
            case 'image':
                $scope.whenImage();
                break;
            case 'video':
                $scope.whenVideo();
                break;
            case 'text':
                $scope.whenText();
                break;
            case 'activity':
                $scope.whenActivity();
                break;
            default:
                $scope.whenActivity();
                break;
        }
        $scope.lesson = [];
    };

    $scope.whenImage = function() {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeImageView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });
    };
    $scope.whenVideo = function () {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeVideoView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });

    };
    $scope.whenText = function () {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'app/shared/lessonType/lessonTypeTextView.html',
          controller: 'lessonTypeController',
          scope: $scope,
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
              moduleId: function() {
                  return moduleId;
              },
              type: function() {
                return "Create Lesson";
              }
          }
      });
    };

    $scope.whenActivity = function () {
        $scope.createLessonView = true;
    };

    $scope.hideLessonForm = function() {
        $scope.createLessonView = false;
    };
    //for adding lessons
    $scope.createLesson = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.lesson.detail_description === undefined || $scope.lesson.detail_description === '') {
            focus('lessonDDes');
            return false;
        }
        if ($scope.lesson.activity === undefined || $scope.lesson.activity === '') {
            focus('lessonActivity');
            return false;
        }
        if ($scope.lesson.Keytakeaways === undefined || $scope.lesson.Keytakeaways === '') {
            focus('Keytakeaways');
            return false;
        }

        var opts = {};
        opts.module = moduleId;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.detail_description = $scope.lesson.detail_description;
        opts.activity = $scope.lesson.activity;
        opts.Keytakeaways = $scope.lesson.Keytakeaways;
        opts.type = $scope.pattern;
        //$scope.loader = true;
        createLessonService.save(opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.createLessonFormSubmitted = false;
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.msgClass = 'success-green';
                $scope.totalLesson.push($scope.lesson);
                $rootScope.$emit("updatelessons", {
                    moduleIndex: $scope.moduleIndex,
                    moduleId: moduleId
                });

                $timeout(function() {
                    $scope.lessonResultMsg = false;
                    $scope.sucessMsg = '';
                    $scope.hideLessonForm();
                }, 2000);
            } else if (data.statusCode === 422 && data.message === "lesson name must be unique") {
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.uniqueLesson;
                $scope.msgClass = 'error-red';
                $scope.createLessonFormSubmitted = false;
                $timeout(function() {
                    $scope.resultMsg = false;
                    $scope.sucessMsg = '';
                }, 3000);
            } else {
                $scope.lessonResultMsg = true;
                $scope.sucessMsg = frontendSettings.errorOccured;
                $scope.msgClass = 'error-red';
                $scope.createLessonFormSubmitted = false;
                $timeout(function() {
                    $scope.lessonResultMsg = false;
                    $scope.sucessMsg = '';
                }, 4000);
            }
        });


    };
    $scope.lessonEditMsg = false;
    $scope.editLesson = function() {
        $scope.editLessonFormSubmitted = true;
        if ($scope.lesson1.name === undefined || $scope.lesson1.name === '') {
            focus('lessonEName');
            return false;
        }
        if ($scope.lesson1.basic_description === undefined || $scope.lesson1.basic_description === '') {
            focus('lessonEDes1');
            return false;
        }
        if ($scope.lesson1.detail_description === undefined || $scope.lesson1.description === '') {
            focus('lessonEDes2');
            return false;
        }
        if ($scope.lesson1.activity === undefined || $scope.lesson1.activity === '') {
            focus('lessonEActivity');
            return false;
        }

        if ($scope.lesson1.Keytakeaways === undefined || $scope.lesson1.Keytakeaways === '') {
            focus('EKeytakeaways');
            return false;
        }
        var opts = $scope.lesson1;
        editLessonService.update({
            id: moduleId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.editLessonFormSubmitted = true;
                $scope.lessonEditMsg = true;
                $scope.msgClass = 'success-green';
                $scope.sucessMsg = frontendSettings.editSucessMessage;
                $scope.showLesson($rootScope.lessonIndex, $rootScope.lessonId);
                $timeout(function() {
                    $scope.lessonEditMsg = false;
                    $scope.sucessMsg = '';
                    $scope.closeModal();
                    $scope.secondForm = false;
                }, 2000);
            } else {}
        });


    };
    //selectLessonType
    $scope.selectLessonType = function(index) {
        switch (index) {
            case 0:
                $scope.pattern = "activity";
                break;
            case 1:
                $scope.pattern = "image";
                $scope.createLessonView = false;
                break;
            case 2:
                $scope.pattern = "video";
                $scope.createLessonView = false;
                break;
            case 3:
                $scope.pattern = "text";
                $scope.createLessonView = false;
                break;
            default:

        }
    };



}]);

app.factory('moduleAddService', function ($resource) {
   return  $resource(APP.endpoints.module);
});
app.factory('updateModuleService',function($resource){
	return $resource(APP.endpoints.actionModule,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('createLessonService', function ($resource) {
   return  $resource(APP.endpoints.createLesson,{},{query: { method: "POST", isArray: false }});
});
app.factory('getLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('editLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{update: { method: "PUT"}});
});

app.controller("profileController",['$rootScope','$scope','changeNameService','focus', 'profileService','changeEmailService','changePasswordService','changeUserDobService','$timeout','changeUserImageService','deleteAccountService','$state', 'registerService','$uibModal' , function($rootScope, $scope, changeNameService, focus, profileService, changeEmailService, changePasswordService, changeUserDobService, $timeout, changeUserImageService, deleteAccountService, $state, registerService, $uibModal) {
	$scope.basicProfile = function(){
		profileService.query(function(data){
			$scope.editUser = data.result.local;
			APP.currentUser = data.result;
		});
	};
	$scope.basicProfile();
	$scope.showErrEmail = false;
	$scope.errMsg = '';
	$scope.activeAction = false;
	$scope.activeProfile = true;
	$scope.nameSubmit = false;
	$scope.openeditName = false;
	$scope.openeditEmail = false;
	$scope.emailSubmit = false;
	$scope.openeditPassword = false;
	$scope.passwordSubmit = false;
	$scope.openeditDob = false;
	$scope.dobSubmit = false;
	$scope.uploadLoader = false;
	$scope.noFile = '';
	$scope.location = '';
	$scope.openeditImage = false;
	$scope.errorDelete = false;
	$scope.popup1 = {
		opened: false
	};
	//AWS
	AWS.config.region = 'ap-northeast-1'; // Region
	AWS.config.credentials = new AWS.CognitoIdentityCredentials({
		IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
	});
	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};
	$scope.format = 'dd-MMMM-yyyy';
	$scope.disabled = function(date, mode) {
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	};
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.maxDate = new Date();
	$scope.toggleMin = function() {
		$scope.minDate =  null;
	};
	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};
	/*$scope.emailValidate = function(){
		$scope.showErrEmail = false;
		$scope.errMsg = '';
		if($scope.user.email === undefined){
		} else {
			registerService.save({email : $scope.user.email }, function(data){
				if(data.statusCode == 200 &&  data.message == "ok" && data.result.message == 'verified'){

				} else if (data.statusCode == 422 && data.message == 'email already registered'){
					$scope.showErrEmail = true;
					$scope.errMsg = frontendSettings.userregistered;
				} else  if (data.statusCode == 422 && data.message == 'email should be registered with any email provider'){
					$scope.showErrEmail = true;
					$scope.errMsg = frontendSettings.domainnotValid;
				} else {
					$scope.showErrEmail = true;
					$scope.errMsg = frontendSettings.errorOccured;
				}
			});
		}
	};*/

	$scope.toggleMin();
	// Open edit form
	$scope.openEditForm = function(index){
		$scope.openeditImage = false;
		$scope.openeditName = false;
		$scope.openeditDob = false;
		$scope.openeditEmail = false;
		$scope.openeditPassword = false;
		switch(index){
			case 1 :
				$scope.openeditImage = true;
				break;
			case 2 :
				$scope.openeditName = true;
				focus('name');
				break;
			case 3 :
				$scope.openeditDob = true;
				focus('dob');
				break;
			case 4 :
				$scope.openeditEmail = true;
				focus('email');
				$scope.showErrEmail = false;
				$scope.errMsg = '';
				break;
			case 5 :
				$scope.openeditPassword = true;
				$scope.editUser.password = '';
				focus('password');
				break;
		}
	};
	// Edit change Name
	$scope.changeName = function(){
		$scope.nameSubmit = true;
		if($scope.editUser.name === undefined || $scope.editUser.name === ''){
			focus('name');
			return false;
		}
		var opts = {};
		opts.name = $scope.editUser.name;
		changeNameService.update({id: APP.currentUser._id}, opts, function(data){
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.openeditName = false;
				$scope.nameSubmit = false;
				$scope.basicProfile();
				$rootScope.$broadcast("updateName", { name: $scope.editUser.name });
			}
		});
	};
	// Edit change email
	$scope.changeEmail = function(){
		$scope.emailSubmit = true;
		if($scope.editUser.email === undefined || $scope.editUser.name === ''){
			focus('email');
			return false;
		} else if($scope.showErrEmail === true){
			focus('email');
			return false;
		}


		registerService.save({email : $scope.editUser.email }, function(data){
				if(data.statusCode == 200 &&  data.message == "ok" && data.result.message == 'verified'){
					var opts = {};
					opts.email = $scope.editUser.email;
					changeEmailService.update({id: APP.currentUser._id}, opts, function(data){
						if(data.statusCode === 200 && data.message === 'ok'){
							$scope.openeditEmail = false;
							$scope.emailSubmit = false;
							$scope.basicProfile();
						}
					});
				} else if (data.statusCode == 422 && data.message == 'email already registered'){
					$scope.showErrEmail = true;

					$timeout(function() {
						$scope.showErrEmail = false;
					}, 3000);
					$scope.errMsg = frontendSettings.userregistered;
				} else  if (data.statusCode == 422 && data.message == 'email should be registered with any email provider'){
					$scope.showErrEmail = true;
					$scope.errMsg = frontendSettings.domainnotValid;
					$timeout(function() {
						$scope.showErrEmail = false;
					}, 3000);
				} else {
					$scope.showErrEmail = true;
					$scope.errMsg = frontendSettings.domainnotValid;
					$timeout(function() {
						$scope.showErrEmail = false;
					}, 3000);
				}
		});



	};
	//Edit change password
	$scope.changePassword = function(){
		$scope.passwordSubmit = true;
		if($scope.editUser.password === undefined || $scope.editUser.password === ''){
			focus('password');
			return false;
		}
		var opts = {};
		opts.password = $scope.editUser.password;
		changePasswordService.update({id: APP.currentUser._id}, opts, function(data){
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.openeditPassword = false;
				$scope.passwordSubmit = false;
				$scope.basicProfile();
			}
		});
	};
	// covert calender date
	$scope.convert = function(str){
		var date = new Date(str),
		mnth = ("0" + (date.getMonth()+1)).slice(-2),
		day  = ("0" + date.getDate()).slice(-2);
		return [ day, mnth , date.getFullYear() ].join("/");
	};
	//Edit Date of birth
	$scope.changeDob = function(){
		$scope.dobSubmit = true;
		if($scope.dt === undefined || $scope.dt === ''){
			focus('dob');
			return false;
		}
		$scope.dob = $scope.convert($scope.dt);
		var opts = {};
		opts.dob = $scope.dob;
		changeUserDobService.update({id: APP.currentUser._id}, opts, function(data){
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.openeditDob = false;
				$scope.popup1.opened = false;
				$scope.dobSubmit = false;
				$scope.basicProfile();
			}
		});
	};
	// Upload image in profile data for  logged user

	$scope.upload = function (element) {
		$scope.noFile = '';
		$scope.imag = '';
		$scope.showProgressBar = false;
		$scope.fileModel =  element.files[0];
		$scope.uploadLoader = false;
		if ($scope.fileModel) {
			if($scope.fileModel.type === 'image/jpeg' || $scope.fileModel.type === 'image/png'){
				$scope.uploadLoader = true;
				var bucket = new AWS.S3({params: {Bucket: 'mindmaxdaffo'}});
				var imageName = $scope.fileModel.name + Math.floor(Date.now() / 1000);
				$scope.imag = imageName;
				var params = {Key: imageName, ContentType: $scope.fileModel.type, Body: $scope.fileModel};
				bucket.upload(params).on('httpUploadProgress', function(evt) {
				$scope.barShow =  parseInt((evt.loaded * 100) / evt.total)+'%';
				$scope.percentageStyle = {
					width : $scope.barShow
				};
				$scope.$digest();
				}).send(function(err, data) {
					if(err){
						$scope.uploadLoader = false;
						$scope.noFile = 'Error in uploading file, please try again';
						$scope.$digest();
						$timeout(function() {
							$scope.noFile = '';
						}, 3000);
					} else {
						$scope.uploadLoader = false;
						$scope.showProgressBar = true;
						//$scope.noFile = 'Image uploaded successfully';
						$timeout(function() {
							$scope.noFile = '';
						}, 3000);
						$scope.location = data.Location;
						$scope.barShow = '0%';
						element.value = null;
						$scope.$digest();
					}
				});
			} else {
				$scope.noFile = 'Please upload a image with jpeg and png format';
			}
		}
		else {
			$scope.noFile = 'Please upload a file first';
		}
	};

	// change image in profile data for  logged user
	$scope.changeImage = function(){
		if($scope.location === '' || $scope.location === undefined){
			$scope.noFile = 'Please upload a file first';
			focus('image');
			$timeout(function() {
				$scope.noFile = '';
			}, 3000);
			return false;
		}
		var opts = {};
		opts.avatar_url = $scope.location;
		changeUserImageService.update({id: APP.currentUser._id} , opts , function(data){
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.openeditImage = false;
				$scope.editUser.avatar_url = $scope.location;
				APP.currentUser.local.avatar_url = $scope.editUser.avatar_url;
				$scope.location = '';
				$scope.basicProfile();
				$rootScope.$broadcast("updateHeaderDp",{avatar_url : $scope.editUser.avatar_url});
			}
		});
	};
	//Hide preview of image
	$scope.hidePreview = function(){
		$scope.location = '';
		$scope.openeditImage = false;
		$scope.fileModel = null;
		var bucketInstance = new AWS.S3();
		var params = {
			Bucket: 'mindmaxdaffo',
			Key: $scope.imag
		};
		bucketInstance.deleteObject(params, function (err, data) {
		});
	};
	//Cancel edit  box
	$scope.cancelSave = function(index){
		switch(index){
			case 1 :
				$scope.openeditImage = false;
				$scope.basicProfile();
				break;
			case 2 :
				$scope.openeditName = false;
				$scope.basicProfile();
				break;
			case 3 :
				$scope.openeditDob = false;
				$scope.basicProfile();
				break;
			case 4 :
				$scope.openeditEmail = false;
				$scope.basicProfile();
				break;
			case 5 :
				$scope.openeditPassword = false;
				$scope.basicProfile();
				break;
		}
	};
	// Delete Account
	$scope.deleteMyAccount = function(){
		/*deleteAccountService.delete({id: APP.currentUser._id}, function(data){
			if(data.statusCode === 200 && data.message === 'ok' && data.result.message === 'user deleted'){
				localStorage.removeItem('loggedInUser');
				$rootScope.isLoggedIn = false;
				$state.go('home',{},{ reload: true });
			} else {
				$scope.errorDelete = true;
				$scope.delError = frontendSettings.delError;
				$timeout(function() {
					$scope.errorDelete = false;
				}, 2000);
			}
		}, function(error){
		});*/
		APP.currentUser = APP.currentUser;
		var modalInstance = $uibModal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'app/shared/dialogBox/alertView.html',
			controller: 'confirmController',
			scope: $scope,
			backdrop: 'static',
			keyboard : false,
			resolve: {
				message: function () {
					return  "Are you sure to deactivate the  account?";
				},
				item: function(){
					return 'user';
				}
			}
		});
	};
}]);

app.factory('changeNameService', function ($resource) {
   return  $resource(APP.endpoints.changeUserName,{ id: '@id'},{update: { method: "PUT"}});
});
app.factory('changeEmailService', function ($resource) {
   return  $resource(APP.endpoints.changeUserEmail,{ id: '@id'},{update: { method: "PUT"}});
});
app.factory('changePasswordService', function ($resource) {
   return  $resource(APP.endpoints.changeUserPass,{ id: '@id'},{update: { method: "PUT"}});
});
app.factory('changeUserDobService', function ($resource) {
   return  $resource(APP.endpoints.changeUserDob,{ id: '@id'},{update: { method: "PUT"}});
});
app.factory('changeUserImageService', function ($resource) {
   return  $resource(APP.endpoints.changeUserImage,{ id: '@id'},{update: { method: "PUT"}});
});
app.factory('deleteAccountService', function ($resource) {
   return  $resource(APP.endpoints.deleteAccount,{ id: '@id'},{update: { method: "PUT"}});
});

app.controller("resetPasswordController",['$scope','$stateParams','resetPasswordService','$timeout', function($scope, $stateParams, resetPasswordService, $timeout) {
	//$scope.editUser = APP.currentUser.local;
	$scope.tokenId = $stateParams.user;
	$scope.submittedResetForm = false;
	$scope.password = {};
	$scope.resetLoader = false;
	$scope.msgClass = '';
	$scope.showResetForm = true;
	$scope.submitResetPassword = function(){
		$scope.resetError = '';
		$scope.submittedResetForm = true;
		
		if($scope.password.newpass === null || $scope.password.newpass === undefined){
			focus('password1');
			return false;
		}  else if($scope.password.conpass === null || $scope.password.conpass === undefined){
			focus('password2');
			return false;
		} else if($scope.password.conpass !== $scope.password.newpass){
			focus('password2');
			$scope.msgClass = 'error-red';
			$scope.resetError = 'Passwords do not match';
			$timeout(function() {
				$scope.resetError = '';
			}, 3000); 
			return false;
		}
		$scope.resetLoader = true;
		var opts = {};
		opts.access_token = $scope.tokenId;
		opts.password = $scope.password.newpass;
		opts.confirmPassword = $scope.password.newpass;
		resetPasswordService.save(opts, function(data){
			$scope.resetLoader = false;
			if(data.statusCode === 200 && data.message === 'ok'){
				$scope.password = {};
				$scope.submittedResetForm = false;
				$scope.msgClass = 'success-green reset-success';
				$scope.resetError = 'The Password has been changed with Success';
				$scope.showResetForm = false;
				// $timeout(function() {
				// $scope.resetError = '';
				// }, 3000);
			} else if ((data.statusCode === 401 && data.message === 'unauthorized') || (data.statusCode === 422)){
				$scope.msgClass = 'error-red';
				$scope.resetError = 'You are not authorized for reset password';
				$timeout(function() {
					$scope.resetError = '';
				}, 3000);
			} else {
				$scope.msgClass = 'error-red';
				$scope.resetError = 'Error occured in reset password';
				$timeout(function() {
					$scope.resetError = '';
				}, 3000);
			}
		}, function(error){
		});
	};
}]);
app.factory('resetPasswordService', function($resource){
	return $resource(APP.endpoints.resetPassword);
});

app.controller('viewSharedController', ['$scope', '$uibModalInstance', 'getViewActivityService', 'editActivityService', 'likeActivityService', 'unlikeActivityService', 'activityId', '$uibModal', '$timeout', 'notificationShareService', '$rootScope', 'followUserService', 'unfollowUserService',
    function($scope, $uibModalInstance, getViewActivityService, editActivityService, likeActivityService, unlikeActivityService, activityId, $uibModal, $timeout, notificationShareService, $rootScope, followUserService, unfollowUserService) {
        //fab
        $scope.url = 'http://google.com';
        $scope.text = 'testing share';
        $scope.title = 'title1';
        $scope.callback = function(response) {
            if (response !== undefined) {
                if (response.post_id) {
                    var opts = {};
                    opts.activity = activityId;
                    opts.activityOwner = $scope.viewDetailActivity.user._id;
                    opts.shareWith = "facebook";
                    notificationShareService.save(opts, function(data) {});
                }
            }
        };
        $scope.isLike = false;
        $scope.isUnlike = false;
        $scope.editDescription = false;
        $scope.editDescriptionValue = '';
        $scope.activityFormEditSubmitted = false;
        $scope.showLike = false;
        $scope.showEdit = false;
        $scope.fixedComment = false;
        $scope.viewDetailActivity = {};
        $scope.followText = 'Follow';
        $scope.followState = 'hide';
        $scope.activityLoader = true;

        $scope.callViewActivityService = function (callId) {
            $scope.activityLoader = true;
            var opts = {};
            opts.id = callId;

            getViewActivityService.query(opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.viewDetailActivity = data.result;

                    if (!$rootScope.isLoggedIn && !$rootScope.isAdminLoggedIn)
                        $scope.followState = 'hide';
                    else if ($scope.viewDetailActivity.user._id === APP.currentUser._id)
                        $scope.followState = 'hide';
                    else if (APP.currentUser.local.following.filter(function (v) {
                        return v === $scope.viewDetailActivity.user._id;
                    }).length > 0) {
                        $scope.followText = 'Unfollow';
                        $scope.followState = 'show';
                    }
                    else {
                        $scope.followText = 'Follow';
                        $scope.followState = 'show';
                    }

                    $scope.getLike = data.result.likes.indexOf(APP.currentUser._id);
                    if ($scope.getLike === -1) {
                        $scope.showLike = false;
                    } else {
                        $scope.showLike = true;
                    }
                    if (APP.currentUser._id === data.result.user._id) $scope.showEdit = true;
                    $scope.likedPeople = data.result.likes.length;
                    $scope.activityLoader = false;
                }
            });
        };
        $scope.callViewActivityService(activityId);

        //$scope.showLikeView = true;
        $scope.closeModal = function() {
            $scope.viewDetailActivity = {};
            $scope.editDescription = false;
            $scope.editDescriptionValue = '';
            $scope.$emit("CallUpdatedCommunityMethod", {});
            $scope.$emit("CallUpdatedSpaceMethod", {});
            $uibModalInstance.dismiss('cancel');
        };

        /*$timeout(function(){
        $scope.loadMoreComments();
        }, 3000);  */
        $scope.$on("CallHeightMethod", function() {
            $scope.loadMoreComments();
        });
        $scope.loadMoreComments = function() {
            var elmnt = document.getElementById("getHeight");
            var txt = elmnt.scrollHeight;
            if (txt > 365) {
                $scope.fixedComment = true;
            }
        };


        // Like Activity
        $scope.likeActivity = function(id, ownerId, likedPeople) {
            var opts = {};
            opts.activityOwner = ownerId;
            likeActivityService.update({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.isLike = true;
                    $scope.showLike = true;
                    $scope.isUnlike = false;
                    $scope.likedPeople = likedPeople + 1;
                    $scope.activityList[$scope.currentIndex].likes.push(APP.currentUser);
                } else {}
            }, function(error) {});
        };
        //Unlike Activity
        $scope.unlikeActivity = function(id, owner_id, likedPeople) {
            var opts = {};
            opts.activityOwner = owner_id;
            unlikeActivityService.update({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.isLike = false;
                    $scope.showLike = false;
                    $scope.isUnlike = true;
                    $rootScope.$emit('updatingLikes', {});
                    $scope.activityList[$scope.currentIndex].likes=$scope.activityList[$scope.currentIndex].likes.filter(function (v) {
                      return v !== APP.currentUser._id;
                    });

                    if ($scope.likedPeople > 0) {
                        $scope.likedPeople = likedPeople - 1;
                    } else {
                        $scope.likedPeople = likedPeople;
                    }
                } else {}
            }, function(error) {});
        };

        $scope.editDetail = function() {
            $scope.editDescription = true;
            $scope.editDescriptionValue = $scope.viewDetailActivity.description;
        };
        $scope.saveDetail = function(form, id) {
            $scope.activityFormEditSubmitted = true;
            //autosize(document.querySelectorAll('.editpostbox'));
            if ($scope.editDescriptionValue === undefined || $scope.editDescriptionValue === '') {
                focus('description');
                return false;
            }
            var opts = {};
            opts.description = $scope.editDescriptionValue;
            editActivityService.query({
                id: id
            }, opts, function(data) {
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.editDescription = false;
                    $scope.viewDetailActivity.description = $scope.editDescriptionValue;
                }
            });
        };
        $scope.cancel = function() {
            $scope.editDescriptionValue = '';
            $scope.editDescription = false;
        };
        $scope.openModal = function(id) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                size: 'lg',
                templateUrl: 'app/shared/activityDetail/detail.html',
                controller: 'findPeopleController',
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    activityId: function() {
                        return id;
                    }
                }

            });
        };

        $scope.goToModuleViewSharedActivity = function($event, module) {
            $scope.closeModal();
            if ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)
                $rootScope.$emit("callMyModule", {
                    module: module
                });
            else
                $rootScope.$broadcast('ShowModalLogin', {});
        };

        $scope.follow = function() {
            $scope.followState = 'disable';

            if ($scope.followText === 'Follow') {
                followUserService.update({ id: $scope.viewDetailActivity.user._id }, {}, function (data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.followText = 'Unfollow';
                        APP.currentUser.local.following.push($scope.viewDetailActivity.user._id);
                        $scope.followState = 'show';
                    }
                }, function (error) {
                    $scope.followState = 'show';
                });
            }
            else if ($scope.followText === 'Unfollow') {
                unfollowUserService.update({ id: $scope.viewDetailActivity.user._id }, {}, function (data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.followText = 'Follow';
                        APP.currentUser.local.following = APP.currentUser.local.following.filter(function (v) {
                            return v !== $scope.viewDetailActivity.user._id;
                        });
                        $scope.followState = 'show';
                    }
                }, function (error) {
                    $scope.followState = 'show';
                });
            }
        };

        $scope.nextActivity = function () {
            $scope.currentIndex++;
            $scope.viewDetailActivity = $scope.activityList[$scope.currentIndex];
            $rootScope.$broadcast('refreshComments', {
                activityId: $scope.viewDetailActivity._id
            });
            $scope.callViewActivityService($scope.viewDetailActivity._id);
        };

        $scope.prevActivity = function () {
            $scope.currentIndex--;
            $scope.viewDetailActivity = $scope.activityList[$scope.currentIndex];
            $rootScope.$broadcast('refreshComments', {
                activityId: $scope.viewDetailActivity._id
            });
            $scope.callViewActivityService($scope.viewDetailActivity._id);
        };
}]);
app.controller("findPeopleController", ['$rootScope', '$uibModalInstance', '$scope', '$uibModal', '$log', 'viewLikeUserService', 'activityId', function($rootScope, $uibModalInstance, $scope, $uibModal, $log, viewLikeUserService, activityId) {
    $scope.listLikeUser = [];
    $scope.showUserLoader = true;
    viewLikeUserService.query({
        id: activityId
    }, function(data) {
        if (data.statusCode === 200 && data.message === "ok") {
            $scope.listLikeUser = data.result[0].likes;
            $scope.showUserLoader = false;
        }
    }, function() {});
    $scope.closeModal2 = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);

app.directive('adminMenu', function () {
	return {
		templateUrl: 'app/shared/adminMenu/adminMenu.html',
		restrict: 'E',
		controller: ''
	};
});
/*app.controller("adminMenuController",['$rootScope','$scope', function($rootScope, $scope) {
	$scope.moduleExpand = function(index){
		$scope.activeUser = false;
		$scope.activeAction = false;
		$scope.activeProfile = false;
		switch(index){
			case 1: 
				$scope.activeAction = true;
				$scope.activeUser = false;
				$scope.activeProfile = false;
				break;
			case 2 :
				
				$scope.activeAction = false;
				$scope.activeUser = true;
				$scope.activeProfile = false;
				break;
			case 3:
				$scope.activeUser = false;
				$scope.activeAction = false;
				$scope.activeProfile =true;
				break;		
		}
	};
}]);*/
app.controller("commentsController", ['$scope', '$rootScope', 'postCommentService', 'postListCommentService', '$timeout', 'deleteCommentService', 'editCommentService','$uibModal', function($scope, $rootScope, postCommentService, postListCommentService, $timeout, deleteCommentService, editCommentService, $uibModal) {
    $scope.commentFormSubmitted = false;
    $scope.commentList = [];
    $scope.errorComment = false;
    $scope.notificationloader = false;
    $scope.commentProcess = false;
    $scope.hideAllComments = false;
    $scope.remainingComments = 0;
    $scope.avatar = $rootScope.isLoggedIn || $rootScope.isAdminLoggedIn ? APP.currentUser.local.avatar_url : '';
    $scope.commentId = "";
    $scope.activityId = "";
		$scope.index = "";
		$scope.loader = [];

    $rootScope.$on('refreshComments', function (event, args) {
      $scope.activityId = args.activityId;
    });

    // watch the activity id
    $scope.$watch('activityId', function handleIdChange(newValue, oldValue) {
        $scope.activityId = newValue;
        $scope.UserId = APP.currentUser._id;
        $scope.commentList = [];
        $scope.getComments(1);
    });

    //get the comments
    $scope.getComments = function(index) {
        var limit_start = 0;
        var limit = 5;
        var opts;
        if (index == 1) {
            opts = {
                id: $scope.activityId,
                skip: limit_start,
                limit: limit
            };
        } else {
            opts = {
                id: $scope.activityId
            };
        }
        $scope.notificationloader = true;
        postListCommentService.query(opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.notificationloader = false;
                $scope.commentProcess = false;
                $scope.commentList = data.result.comments;
                $timeout(function() {
                    $scope.$emit("CallHeightMethod", {});
                }, 1000);
                if (data.result.totalCount > data.result.comments.length) {
                    $scope.remainingComments = data.result.totalCount - data.result.comments.length;
                }
                if (index == 2) $scope.hideAllComments = true;
            } else {
                $scope.notificationloader = false;
                $scope.commentList = [];
                $scope.commentProcess = false;
            }
        });
    };
    // post the comments
    $scope.postComment = function() {
        $scope.commentFormSubmitted = true;
        if($scope.txtcomment === "" || $scope.txtcomment === null || $scope.txtcomment === undefined) {
          focus('comment');
          return false;
        }
        var opts = {};
        opts.user = APP.currentUser._id;
        $scope.errorComment = false;
        opts.comment = $scope.txtcomment.trim();
        opts.activity = $scope.activityId;
        postCommentService.save({
            id: $scope.activityId
        }, opts, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.commentList.push(data.result[0]);
                $scope.txtcomment = '';
                $scope.commentFormSubmitted = false;
            } else {
                $scope.errorComment = false;
                $scope.errCommentMessage = frontendSettings.errorOccured;
                $timeout(function() {
                    $scope.errorComment = false;
                }, 3000);
            }
        });
    };
    //delete the comment for id
    $scope.deleteComment = function(index, id) {
        $scope.loader[index] = true;
				$scope.loader = $scope.loader;
        $scope.activityId = $scope.activityId;
        $scope.commentId = id;
				$scope.index = index;
				$scope.commentList = $scope.commentList;

				var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return "Are you sure to delete this comment?";
                },
                item: function() {
                    return 'delete-comment';
                }
            }
        });
    };

    $scope.commentLoading = [];
    $scope.editCommentText = [];
    $scope.isEditComment = [];
    $scope.activeCommentEdit = [];
    $scope.commentErrorMsg = [];
    $scope.commentErrorCls = [];
    // open the edit comment box

    $scope.updateComment = function(index, comment) {
        $scope.commentLoading[index] = true;
        $scope.activeCommentEdit = [];
        $scope.commentErrorMsg[index] = '';
        $scope.commentErrorCls[index] = '';
        $scope.isEditComment[index] = false;
        $scope.editCommentText[index] = comment.comment;
        $scope.activeCommentEdit[index] = comment._id;
        $("#commentBox").hide();
    };
    // cancel edit comment
    $scope.cancelPost = function(index) {
        $scope.commentLoading[index] = false;
        $scope.commentLoading = [];
        $scope.editCommentText = [];
        $scope.isEditComment = [];
        $scope.activeCommentEdit = [];
        $scope.commentErrorMsg = [];
        $scope.commentErrorCls = [];
        $("#commentBox").show();
    };
    // edit comment
    $scope.editComment = function(index, comment) {
        var opts = {};
        $scope.commentErrorMsg[index] = '';
        $scope.isEditComment[index] = false;
        var newText = $scope.editCommentText[index];
        if (newText === undefined || newText === '') {
            $scope.commentErrorCls[index] = 'text-red';
            $scope.commentErrorMsg[index] = "Can not save empty comment";
            $timeout(function() {
                $scope.commentErrorCls[index] = '';
                $scope.commentErrorMsg[index] = '';
            }, 8000);
            return false;
        }
        var opts1 = {};
        opts1.comment = newText;
        $scope.isEditComment[index] = true;
        editCommentService.update({
            id: $scope.activityId,
            commentId: comment._id
        }, opts1, function(data) {
            if (data.statusCode === 200 && data.message === "ok") {
                $scope.activeCommentEdit[index] = '';
                $scope.commentErrorCls[index] = '';
                $scope.commentErrorMsg[index] = '';
                $scope.editCommentText[index] = '';
                $scope.commentList[index].comment = newText;
                $scope.isEditComment[index] = false;
                $("#commentBox").show();
            } else {
                $scope.commentInProcess[index] = false;
                $scope.isEditComment[index] = false;
                $scope.commentErrorCls[index] = 'text-red';
                $scope.commentErrorMsg[index] = frontendSettings.errorOccured;
                $("#commentBox").show();
            }
            $timeout(function() {
                $scope.commentErrorCls[index] = '';
                $scope.commentErrorMsg[index] = '';
            }, 8000);
        });
    };
    // show all comments on click on view all comments
    $scope.showAllComments = function() {
        $scope.commentProcess = true;
        $scope.getComments(2);
    };
}]);

app.directive('commentsForm', ['postListCommentService' ,function (postListCommentService) {
	return {
		templateUrl: 'app/shared/comments/commentsView.html',
		restrict: 'E',
		scope : true,
		link: function ($scope, element, $attrs) {
			$scope.activityId = $attrs.activityId;
		},
		controller: 'commentsController'
	};
}]);

app.factory('postCommentService', function ($resource) {
	return  $resource(APP.endpoints.postComment);
});
app.factory('postListCommentService', function ($resource) {
	return  $resource(APP.endpoints.postComment, {},{query: { method: "GET", isArray: false }});
});
app.factory('deleteCommentService', function ($resource) {
	return  $resource(APP.endpoints.updateComment, {id : '@id' ,commentId : '@commentId'},{ delete: { method: "DELETE"}});
});
app.factory('editCommentService', function ($resource) {
	return  $resource(APP.endpoints.updateComment, {id : '@id' ,commentId : '@commentId'},{ update: { method: "PUT"}});
});

app.controller("communityController", ['$scope', '$rootScope', 'getCommunityService', '$uibModal', 'getViewActivityService', 'moduleActivitiesService', 'removeActivityService', 'getActivityCommentService', '$timeout', 'getlessonDetailService', 'getModuleDetailService', 'allActivitiesByModule', '$location','byTagNameService','byUserNameService','followUserService', function($scope, $rootScope, getCommunityService, $uibModal, getViewActivityService, moduleActivitiesService, removeActivityService, getActivityCommentService, $timeout, getlessonDetailService, getModuleDetailService, allActivitiesByModule, $location, byTagNameService, byUserNameService, followUserService) {
    $scope.activityList = [];
    $scope.mycommListShow = false;
    $scope.comments = '';
    $scope.createdAt = [];
    $scope.lessonsz = [];
    $scope.modulesz = [];
    $scope.id = null;
    $scope.url = "";
    $scope.lastLengthComm = -1;
    $scope.lastLengthComm1 = -1;
    $scope.moduleIndex = 0;
    $scope.isLast = null;
    $scope.index = '';
    $scope.name = allActivitiesByModule;
    $scope.currentIndex = null;

    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $rootScope.activeSection = "community";
    $rootScope.myTabs = false;
    // if (!$rootScope.isLast)
    //     $rootScope.activeModule[$rootScope.moduleIndex] = true;
    // else
    //     $rootScope.activeModuleLast[$rootScope.moduleIndex] = true;

    $scope.listResponse = 1;
    $scope.allTotal = 0;

    $scope.showCommunityList = function() {

        if ($scope.lastLengthComm >= $scope.activityList.length && $scope.lastLengthComm != 8 && $scope.activityList.length != 8 && $scope.activityList.length !==0 ){
          return;
        }

        $scope.commloader = true;
        var limit_start = $scope.activityList.length;
        $scope.lastLengthComm = limit_start;
        var limit = 8;
        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse) {
            $scope.listResponse = 0;
            $scope.commloader = true;
            getCommunityService.query({
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                // $scope.commloader = false;
                if (data.statusCode === 200 && data.message === 'ok')
                    if (data.result.activities.length > 0) {
                        $scope.ago(data.result.activities);
                        $scope.commloader = false;
                        $scope.mycommListShow = false;
                        $scope.allTotal = data.result.totalCount;
                        $scope.activityList = $scope.activityList.concat(data.result.activities);
                    } else {
                        $scope.commloader = false;
                        $scope.mycommListShow = true;
                        $scope.activityList = [];
                        $scope.allTotal = 0;
                    }
            }, function(error) {});
        } else
            $scope.commloader = false;
    };

    $scope.ago = function(activities) {
        angular.forEach(activities, function(activity, key) {
            $timeout(function() {
                var a = new Date(activity.createdAt);
                var b = new Date();
                var agoSpan = $scope.getActualTime(b - a);
                $scope.createdAt.push(agoSpan);
            }, 0);

        });
    };

    $scope.getActualTime = function(t) {
        var showSpan = {};
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000,
            d = Math.floor(t / cd),
            h = Math.floor((t - d * cd) / ch),
            m = Math.round((t - d * cd - h * ch) / 60000),
            pad = function(n) {
                return n < 10 ? '0' + n : n;
            };
        if (m === 60) {
            h++;
            m = 0;
        }
        if (h === 24) {
            d++;
            h = 0;
        }
        if (d === 0) {
            showSpan.numberDate = d;
            showSpan.numberSpan = "day";
            return showSpan;
        } else if (d > 0 && d < 30) {
            if (d == 1) {
                showSpan.numberDate = d;
                showSpan.numberSpan = "day";
                return showSpan;
            } else {
                showSpan.numberDate = d;
                showSpan.numberSpan = "days";
                return showSpan;
            }
        } else if (d > 30 || d == 30) {
            var months = parseInt(d / 30);
            if (months > 12 || months == 12) {
                var years = parseInt(months / 12);
                if (years == 1) {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "year";
                    return showSpan;
                } else {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "years";
                    return showSpan;
                }
            } else {
                if (months == 1) {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "month";
                    return showSpan;
                } else {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "months";
                    return showSpan;
                }

            }

        }
    };

    $scope.allTotal1 = 0;
    $scope.listResponse1 = 1;

    $scope.getAllActivities = function(args) {
        if ($scope.lastLengthComm1 >= $scope.activityList.length && $scope.activityList.length !== 0) {
          return;
        }
        if (args !== null && args !== undefined) {
            $scope.module = JSON.parse(JSON.stringify(args.module));
        } else
            args = {
                module: $scope.module
            };

        // $scope.lessonsz = [];
        // $scope.modulesz = [];
        var limit_start = $scope.activityList.length;
        $scope.lastLengthComm1 = limit_start;
        var limit = 8;
        $scope.commloader1 = true;

        if ((($scope.allTotal1 > limit_start) || $scope.allTotal1 === 0) && $scope.listResponse1) {
            $scope.listResponse1 = 0;
            var opts = {};
            opts.skip = limit_start;
            opts.limit = limit;

            $scope.name.query({
                id: args.module._id,
                skip: limit_start,
                limit: limit
            }, opts, function(data) {
                // $scope.commloader1 = false;
                if (data.statusCode === 200 && data.message === 'ok') {
                    $scope.commloader1 = false;
                    $scope.listResponse1 = 1;
                    if (data.result.activities.length > 0) {
                        $scope.mycommListShow = false;
                        $scope.ago(data.result.activities);
                        $scope.commloader1 = false;
                        $scope.mycommListShow = false;
                        $scope.allTotal1 = data.result.totalCount;
                        $scope.activityList = $scope.activityList.concat(data.result.activities);
                    } else {
                        $scope.commloader1 = false;
                        $scope.mycommListShow = true;
                        $scope.activityList = [];
                        $scope.allTotal1 = 0;
                    }
                }

            }, function(error) {});
        } else {
            $scope.commloader1 = false;
        }

    };

    if ((!$rootScope.moduleSelected || $rootScope.isAdminLoggedIn) && !$rootScope.filterActive) {
        $scope.showCommunityList();
     }else {
       if(!$rootScope.filterActive) {
          $scope.getAllActivities();
       }

     }

    $rootScope.$on('updatingLikes', function(event, args) {
        //$scope.activityList = [];
        //$scope.getAllActivities();
    });

    // if ($location.url() == '/home/community' && $rootScope.isLoggedIn) {
    //     $scope.getAllActivities();
    // }
    $rootScope.$on('updateMyCommunity', function(event, args) {
        $scope.activityList = [];
        $scope.module = args.module;
        //$scope.getAllActivities();
    });

    // function for get the community

    $rootScope.$on("callMyCommunity", function(event, args) {

        $scope.showCommunityList();
    });

    $rootScope.$on("callViewActivityFromHome", function(event, args) {
        $scope.viewActivity(args.activityId);
    });

    // Open modal for view full detail of activity in my community
    $scope.viewActivity = function(index, activityId, videoId) {
        $scope.currentIndex = index;
        $scope.activityList = $scope.activityList;
        getActivityCommentService.query({
            id: activityId
        }, function(data) {

            $scope.comments = data.result.totalCount;
        });

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            size: 'lg',
            templateUrl: 'app/shared/activityDetail/viewSharedActivity.html',
            controller: 'viewSharedController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                activityId: function() {
                    return angular.copy(activityId);
                }
            }
        });
    };

    // $on on click on like
    $scope.$on('callViewActivityLike', function(event, args) {
        $scope.id = args.id;

        $scope.viewActivity($scope.id);
    });

    // Scrolling in page
    $scope.loadMoreCommunity = function() {
        if ($rootScope.activeSection !== 'community' && ($rootScope.isAdminLoggedIn || $rootScope.isLoggedIn)){
            return;
        }

        if ($rootScope.isAdminLoggedIn) {
            $scope.showCommunityList();
        } else if ($rootScope.isLoggedIn && $rootScope.moduleSelected) {
          $scope.getAllActivities();
          // if($rootScope.moduleSelected ) {
          //   $scope.getAllActivities();
          // }else {
          //   $scope.showCommunityList();
          // }
        //   if($rootScope.activeModuleLast.length > 0 || $rootScope.activeModule.length > 0)
        //      $scope.getAllActivities();
        //  else
        //      $scope.showCommunityList();

        } else {
          if(!$rootScope.filterActive)
            $scope.showCommunityList();
            else
              $scope.getAllActivities();

        }
    };

    $scope.confirmModal = function(id, url, $event, index) {
        $event.stopPropagation();
        $scope.id = id;
        $scope.url = url;
        $scope.mySpaceList = $scope.mySpaceList;
        $scope.activityList = $scope.activityList;
        $scope.index = index;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                message: function() {
                    return "Are you sure to delete this activity?";
                },
                item: function() {
                    return 'delete-activity';
                }
            }
        });
    };

    $scope.goToModule = function ($event, module) {
        $event.stopPropagation();
        if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn)
            $rootScope.$emit("callMyModule", { module: module });
        else
            $rootScope.$broadcast('ShowModalLogin', {});
    };

    $rootScope.$on("whenNoTag", function (event, args) {
      $scope.listResponse = 1;
      $scope.allTotal = 0;
      $scope.activityList = [];
      $scope.showCommunityList();

    });

    $rootScope.$on("byModuleName", function (event, args) {
      $scope.listResponse1 = 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = allActivitiesByModule;
      $scope.getAllActivities(args);
    });

    $rootScope.$on("byUserName", function (event, args) {
      $scope.listResponse1 = 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = byUserNameService;
      $scope.getAllActivities(args);
    });

    $rootScope.$on("byTagName", function (event, args) {
      $scope.listResponse1= 1;
      $scope.allTotal1 = 0;
      $scope.activityList = [];
      $scope.name = byTagNameService;
      $scope.getAllActivities(args);
    });

}]);

app.directive('community', function() {
    return {
        templateUrl: 'app/shared/community/communityView.html',
        restrict: 'E',
        controller: 'communityController'
    };
});

app.directive('capitalizeFirst', function($parse) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            var capitalize = function(inputValue) {
                if (inputValue === undefined) {
                    inputValue = '';
                }
                var capitalized = inputValue.charAt(0).toUpperCase() +
                    inputValue.substring(1);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            };
            modelCtrl.$parsers.push(capitalize);
            capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
        }
    };
});

app.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];

        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});

app.factory('getCommunityService', function ($resource) {
	return  $resource(APP.endpoints.createActivity,{},{query: { method: "GET", isArray: false }});
});
app.factory('getViewActivityService', function ($resource) {
	return  $resource(APP.endpoints.viewActivity,{},{query: { method: "GET", isArray: false }});
});
app.factory('likeActivityService', function($resource){
	return $resource(APP.endpoints.likeActivity, { id: '@id'}, {update: { method: "PUT"}});
});
app.factory('unlikeActivityService', function($resource){
	return $resource(APP.endpoints.unlikeActivity, { id: '@id'}, {update: { method: "PUT"}});
});
app.factory('removeActivityService', function ($resource) {
	return  $resource(APP.endpoints.deleteActivity,{id: '@id'},{remove: { method: "DELETE"}});
});
app.factory('editActivityService', function ($resource) {
	return  $resource(APP.endpoints.editActivity,{id: '@id'},{query: { method: "PUT"}});
});
app.factory('viewLikeUserService', function ($resource) {
	return  $resource(APP.endpoints.getListLike,{}, {query: { method: "GET",  isArray: false }});
});
app.factory('notificationShareService', function ($resource) {
	return  $resource(APP.endpoints.notiShare);
});
app.factory('moduleActivitiesService', function ($resource) {
	return  $resource(APP.endpoints.moduleActivities,{id : '@id', moduleId : '@moduleId'}, {query :{method : "GET", isArray :false}});
});
app.factory('getActivityCommentService', function ($resource) {
	return  $resource(APP.endpoints.getActivityComments,{},{query: { method: "GET", isArray: false }});
});
app.factory('getlessonDetailService', function ($resource) {
	return  $resource(APP.endpoints.lessonDetail,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getModuleDetailService', function ($resource) {
	return  $resource(APP.endpoints.actionModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('allActivitiesByModule', function ($resource) {
	return  $resource(APP.endpoints.allActivitiesByModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('byTagNameService', function ($resource) {
	return  $resource(APP.endpoints.byTagNameService,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('byUserNameService', function ($resource) {
	return  $resource(APP.endpoints.byUserNameService,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('followUserService', function ($resource) {
	return  $resource(APP.endpoints.followUser,{id: '@id'},{update: { method: "PUT"}});
});
app.factory('unfollowUserService', function ($resource) {
	return  $resource(APP.endpoints.unfollowUser,{id: '@id'},{update: { method: "PUT"}});
});

app.controller("createUserController", ['$scope', '$uibModalInstance', 'moduleId', 'createUserService', 'registerService','$timeout','$rootScope', function($scope, $uibModalInstance, moduleId, createUserService, registerService,$timeout, $rootScope) {
    $scope.user = {};
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        $scope.$emit("CallModulesMethod", {});
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.convert = function(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [day, mnth, date.getFullYear()].join("/");
    };

    $scope.emailValidate = function() {
        $scope.checkingEmail = false;
        $scope.showErrEmail = true;
        $scope.errMsg = frontendSettings.checkingEmail;
        //$scope.showErrEmail = false;
        //$scope.errMsg = '';
        if ($scope.user.email === undefined) {} else {
            registerService.save({
                email: $scope.user.email
            }, function(data) {
                if (data.statusCode == 200 && data.message == "ok" && data.result.message == 'verified') {
                    $scope.showErrEmail = false;
                    $scope.checkingEmail = false;
                } else if (data.statusCode == 422 && data.message == 'email already registered') {

                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.userregistered;
                } else if (data.statusCode == 422 && data.message == 'email should be registered with any email provider') {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                } else {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                }
            });
        }
    };

    $scope.submitRegistration = function() {
        $scope.userSubmitted = true;
        $scope.sucessMessage = false;

        $scope.dob = '';
        if ($scope.dt) {
            $scope.dob = $scope.convert($scope.dt);
        }
        if ($scope.user.name === undefined || $scope.user.name === '') {
            focus('uname');
            return false;
        } else if ($scope.dob === undefined || $scope.dob === '') {
            focus('dob');
            return false;
        } else if ($scope.user.email === undefined || $scope.user.email === '') {
            focus('userEmail');
            return false;
        } else if ($scope.user.password === undefined || $scope.user.password === '') {
            focus('userPassword');
            return false;
        } else if ($scope.showErrEmail === true) {
            $scope.checkingEmail = true;
            focus('email');
            return false;
        }

        $scope.signupStart = true;
        var opts = {};
        opts.email = $scope.user.email;
        opts.password = $scope.user.password;
        opts.dob = $scope.dob;
        opts.name = $scope.user.name;

        createUserService.save(opts, function(data) {
            if (data.statusCode == 200 && data.message === 'ok') {
                $scope.sucessMessage = true;
                $scope.sucessMsg = frontendSettings.sucessMessage;
                $scope.signupStart = false;
                $rootScope.$emit('updateUserList',{});
                $timeout(function() {
                    $scope.closeModal();
                }, 2000);
            } else if (data.statusCode == 422 && data.message.email === false) {
                $scope.signupStart = false;
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.userregistered;
            } else {
                $scope.signupStart = true;
            }
        }, function(error) {
        });

    };
}]);

app.directive('dontFill', function() {

  return {

    restrict: 'A',

    link: function link(scope, el, attrs) {
      // password fields need one of the same type above it (firefox)
      var type = el.attr('type') || 'text';
      // chrome tries to act smart by guessing on the name.. so replicate a shadow name
      var name = el.attr('name') || '';
      var shadowName = name + '_shadow';
      // trick the browsers to fill this innocent silhouette
      var shadowEl = angular.element('<input type="' + type + '" name="' + shadowName + '" style="display: none">');

      // insert before
      el.parent()[0].insertBefore(shadowEl[0], el[0]);
    }

  };

});

app.factory('createUserService', function ($resource) {
	return  $resource(APP.endpoints.createUser);
});
app.factory('registerService', function ($resource) {
	return  $resource(APP.endpoints.validateEmail);
});

app.controller("customBrowseController", ['$rootScope', '$scope', '$uibModalInstance', '$log', function($rootScope, $scope, $uibModalInstance, $log) {

    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.selectImage = function($event) {
        $rootScope.$emit("uploadGroupAvatar", {
            avatar_url: $event.target.src
        });
        $scope.closeModal();
    };


}]);

app.controller("dialogController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'message', function($rootScope, $scope, $timeout, $uibModalInstance, $log, message) {
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };
    if (message == "Your activity is submitted successfully") {
        $scope.message = message;
        $scope.confirm = false;
        $timeout(function() {
            $scope.closeModal();
        }, 500);
        $timeout(function() {
            $rootScope.$emit("moveToProgressBar", {});
        }, 1000);


    }
    $scope.message = message;
    $scope.confirm = false;
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

}]);
app.controller("confirmController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'message', 'deluserService', 'item', 'deleteAccountService', '$state', 'deleteLessonService', 'removeActivityService', 'moduleDelService', 'deleteCommentService', 'delGrpMemberService', 'delGroupService', 'unfollowService', function($rootScope, $scope, $timeout, $uibModalInstance, $log, message, deluserService, item, deleteAccountService, $state, deleteLessonService, removeActivityService, moduleDelService, deleteCommentService, delGrpMemberService, delGroupService, unfollowService) {

    $scope.item = item;
    $scope.message = message;
    $scope.confirm = true;
    $scope.closeModal = function(param) {
        $uibModalInstance.dismiss('cancel');
        // param 1 for press ok button
        if (param === 1) {
            if ($scope.item === 'users') {
                deluserService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.userList.splice($scope.index, 1);
                    }
                });
            }
            if ($scope.item === 'user') {
                deleteAccountService.delete({
                    id: APP.currentUser._id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok' && data.result.message === 'user deleted') {
                        localStorage.removeItem('loggedInUser');
                        $rootScope.isLoggedIn = false;
                        $state.go('home', {}, {
                            reload: true
                        });
                    } else {
                        $scope.errorDelete = true;
                        $scope.delError = frontendSettings.delError;
                        $timeout(function() {
                            $scope.errorDelete = false;
                        }, 2000);
                    }
                });
            }
            if ($scope.item === 'delete-lesson') {
                deleteLessonService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.lesson[$scope.parentId].splice($scope.index, 1);
                        if ($scope.lesson[$scope.parentId].length === 0) {
                            $scope.isnoLesson[$scope.parentId] = true;
                        }
                    } else {
                        $scope.errorDelete = true;
                        $scope.delError = frontendSettings.delError;
                        $timeout(function() {
                            $scope.errorDelete = false;
                        }, 2000);
                    }
                });
            }
            if ($scope.item === 'delete-activity') {
                removeActivityService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        //$scope.mySpaceList.splice($scope.index, 1);
                        $scope.activityList.splice($scope.index, 1);
                        if ($scope.url) {
                            var res = $scope.url.split("com/");
                            var bucketInstance = new AWS.S3();
                            var params = {
                                Bucket: 'mindmaxdaffo',
                                Key: res[1]
                            };
                            bucketInstance.deleteObject(params, function(err, data) {});
                        }
                    }

                });
            }
            if ($scope.item === 'delete-activity-space') {
                removeActivityService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.mySpaceList.splice($scope.index, 1);
                        if ($scope.url) {
                            var res = $scope.url.split("com/");
                            var bucketInstance = new AWS.S3();
                            var params = {
                                Bucket: 'mindmaxdaffo',
                                Key: res[1]
                            };
                            bucketInstance.deleteObject(params, function(err, data) {});
                        }
                    }

                });
            }
            if ($scope.item === 'delete-module') {
                moduleDelService.delete({
                    id: $scope.id
                }, function(data) {
                    if (data.statusCode === 200 && data.message === 'ok') {
                        $scope.showAllModuleList.splice($scope.index, 1);
                    }
                });
            }
            if ($scope.item === 'delete-comment') {
                deleteCommentService.delete({
                    id: $scope.activityId,
                    commentId: $scope.commentId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.commentList.splice($scope.index, 1);
                        $scope.loader[$scope.index] = true;

                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-grpMember') {
                var opts = {};
                opts.user = $scope.userId;
                delGrpMemberService.delete({
                    id: $scope.delMemberId,
                    mid: $scope.userId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.grpMembers.splice($scope.index, 1);

                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-follower') {
                unfollowService.update({
                    id: $scope.id,
                    follower: $scope.follower
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        if ($scope.follower)
                            $scope.followers.splice($scope.index, 1);
                        else
                            $scope.followings.splice($scope.index, 1);
                    }
                }, function(error) {});
            }

            if ($scope.item === 'delete-group') {
                delGroupService.delete({
                    id: $scope.grpId
                }, function(data) {
                    if (data.statusCode === 200 && data.message === "ok") {
                        $scope.myGroupsList.splice($scope.index, 1);
                        $rootScope.$emit('groupsComingInCreatActivity', {});
                        if ($scope.myGroupsList.length === 0 || $scope.myGroupsList.length < 1) {
                            $rootScope.$emit('noGroupsPressent', {});
                            $scope.hasGroups = false;
                        }
                    }
                }, function(error) {});
            }

            if ($scope.item === 'post-without-image') {
                $scope.postActivity();
            }
        } else { // param 2 for press ok button
            angular.element('#fadein').removeClass('color-overlay');
        }
    };

}]);

app.controller("myGroupsController", ['$scope', '$rootScope', 'myGroupsService', '$uibModal', 'delGroupService','$state', function($scope, $rootScope, myGroupsService, $uibModal, delGroupService,$state) {
    $scope.spaceloader = true;
    $scope.myGroupsList = [];
    $scope.mySpaceMsg = true;
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    $scope.grpId = '';
    $scope.index = '';
    $scope.search = '';
    $scope.hasGroups = false;
    $rootScope.activeSection = "groups";
    $rootScope.myTabs = true;

    $scope.showGroupsList = function() {
        var limit_start = $scope.myGroupsList.length;
        var limit = 100;
        var id = APP.currentUser._id;
        //if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && $scope.listResponse === 1) {
            $scope.listResponse = 0;
            $scope.moduleLoader = true;
            myGroupsService.query({
                id: id,
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.spaceloader = false;
                if (data.statusCode === 200 && data.message === "ok") {
                    if (data.result.totalCount === 0) {
                        $scope.hasGroups = false;
                    } else {
                        $scope.hasGroups = true;
                        $scope.moduleLoader = false;
                        $scope.allTotal = data.result.totalCount;
                        $scope.myGroupsList = $scope.myGroupsList.concat(data.result.groups);
                    }
                } else {
                    $scope.moduleLoader = false;
                    $scope.myGroupsList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {});
      //  }
    };

    $scope.showGroupsList();

    $scope.loadMore = function() {
        $scope.showGroupsList();
    };
    //for creating new group
    $scope.createGroup = function() {
        $scope.myGroupsList = $scope.myGroupsList;
        $scope.hasGroups = $scope.hasGroups;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/createGroup/createGroupView.html',
            controller: 'createGroupController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return '';
                }
            }
        });
    };
    $rootScope.$on('groupsComing',function(event, args) {
      $scope.showGroupsList();
    });

    $scope.editGroup = function(id, $event) {
        //$scope.showGroupsList = $scope.showGroupsList;
        $event.stopPropagation();
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/components/editGroup/editGroupView.html',
            controller: 'editGroupController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
                moduleId: function() {
                    return id;
                }
            }
        });
    };

    $scope.deleteMyGroup = function(index, id, $event) {
        $event.stopPropagation();
        $scope.index = index;
        $scope.myGroupsList = $scope.myGroupsList;
        $scope.grpId = id;
        $scope.hasGroups = $scope.hasGroups;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure to delete this group?";
                },
                item: function() {
                    return 'delete-group';
                }
            }
        });

    };
    $rootScope.$on('noGroupsPressent', function(event, args) {
      $scope.hasGroups = false;
    });

    $scope.searchGroup = function() {
      $scope.myGroupsList=[];
        // var limit_start = 0;
        // var limit = 30;
        var id = APP.currentUser._id;

        myGroupsService.query({
            id : id,
            name: $scope.search
        }, function(data) {
            $scope.myGroupsList = data.result.groups;
        });
    };
    $scope.groupActivitiesz = function(id, name) {
      var obj = {};
      obj.id = id;
      obj.name = name;
      $state.go('home.groupActivity', {obj:obj});
      //$state.go('home.community');
        // $rootScope.$emit("groupActivitiesPage", {
        //     id : id,
        //     name : name
        // });
    };
    $rootScope.$on("CallGroupsMethod", function() {
        $scope.myGroupsList = [];
        $scope.showGroupsList();
    });
}]);

app.directive('myGroups', function () {
	return {
		templateUrl: 'app/shared/group/groupView.html',
		restrict: 'E',
		controller: 'myGroupsController'
	};
});
app.factory('myGroupsService', function ($resource) {
   return  $resource(APP.endpoints.myGroups,{},{query: { method: "GET", isArray: false }});
});
app.factory('delGroupService', function ($resource) {
   return  $resource(APP.endpoints.delGroup, {id : '@id'},{query : { method:  "DELETE", isArray: false}});
});

app.controller("groupActivitiesController", ['$rootScope', '$scope', '$log', 'groupActivitiesService','$state',function($rootScope, $scope, $log, groupActivitiesService, $state) {
    $scope.activityloader = false;
    $scope.hasActivities = false;

    if($state.params.obj) {
      $rootScope.name =  $state.params.obj.name;
      $rootScope.id = $state.params.obj.id;
    }else {
      $rootScope.name = $rootScope.name;
      $rootScope.id = $rootScope.id;
    }
    $scope.name = $rootScope.name;
    $scope.id = $rootScope.id;
    
    $scope.getGroupActivities = function() {
        $scope.activityloader = true;
        groupActivitiesService.query({
            id: $rootScope.id
        }, function(data) {
            $scope.activityloader = false;
            if (data.result.totalCount) {

                $scope.hasActivities = true;
                $scope.groupActivities = data.result.activities;
            } else {
                $scope.hasActivities = false;
                $scope.groupActivities = [];
            }
          });
    };
    $scope.getGroupActivities();
}]);

app.directive('groupActivities', function () {
	return {
		templateUrl: 'app/shared/groupActivities/groupActivitiesView.html',
		restrict: 'E',
		controller: 'groupActivitiesController'
	};
});

app.factory('groupActivitiesService', function ($resource) {
   return  $resource(APP.endpoints.groupActivities,{id : '@id'},{query: { method: "GET", isArray: false }});
});

app.controller("headerController", ['$rootScope', '$scope', '$uibModal', '$log', 'getNotificationService', 'viewAllNotificationService', 'viewOneNotificationService', 'rejectGroupInviteService', 'acceptGroupInviteService', '$state', function($rootScope, $scope, $uibModal, $log, getNotificationService, viewAllNotificationService, viewOneNotificationService, rejectGroupInviteService, acceptGroupInviteService, $state) {
    $scope.showRegForm = false;
    $scope.animationsEnabled = true;
    $scope.showDropMenu = false;
    $scope.skip = 0;
    $scope.showAllNotificationList = [];
    $scope.showDp = true;
    $scope.avatar_url = "";
    $scope.showSeeMore = true;

    $scope.rejectId = "";
    //Code for notification section
    $scope.notificationloader = false;

    if ($rootScope.isLoggedIn || $rootScope.isAdminLoggedIn) {
        $scope.avatar_url = APP.currentUser.local.avatar_url;
        $scope.userName = APP.currentUser.local.name;
        if ($scope.avatar_url === null || $scope.avatar_url === "undefined" || $scope.avatar_url === "") {
            $scope.showDp = false;
        }
    }
    $rootScope.$on("updateHeaderDp", function(event, args) {
        $scope.avatar_url = args.avatar_url;

    });
    $rootScope.$on("updateName", function(event, args) {
        $scope.userName = args.name;

    });
    // show registration form
    $scope.showRegisterForm = function() {
        $scope.$broadcast("CallParamsCloseMethod", {});
        $scope.showRegForm = true;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/register/registerView.html',
            controller: 'registerController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false
        });

    };
    $scope.menuAccount = function() {
        $scope.showDropMenu = true;
    };
    $scope.cancel = function() {
        $uibModal.dismiss('cancel');
    };
    // $on in parent method for show login form
    $scope.$on("ShowModalLogin", function() {
        $scope.showRegisterForm();
    });
    $rootScope.$on("ShowModalLogin", function() {
        $scope.showRegisterForm();
    });
    $rootScope.$on("callHeaderController", function(event, value) {
        $scope.backToLessons = value;
    });
    $scope.listResponse = 1;
    $scope.allTotal = 0;
    // Function for get the all notification in list
    $scope.getAllNotificationShow = function(loadMore) {

        if(!loadMore && $scope.allTotal > 0){
          return;
        }

        var limit_start = $scope.showAllNotificationList.length;
        var limit = 5;
         if ((($scope.allTotal > limit_start) || $scope.allTotal === 0) && APP.currentUser._id) {

            $scope.listResponse = 0;
            $scope.notificationloader = true;
            getNotificationService.query({
                id: APP.currentUser._id,
                skip: limit_start,
                limit: limit
            }, function(data) {
                $scope.listResponse = 1;
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.notificationloader = false;
                    $scope.getNotificationShow = true;
                    $scope.allTotal = data.result.totalCount;
                    $scope.showAllNotificationList = $scope.showAllNotificationList.concat(data.result.notifications);
                    if($scope.allTotal <= $scope.showAllNotificationList.length)
                        $scope.showSeeMore = false;
                    else
                        $scope.showSeeMore = true;
                } else {
                    $scope.notificationloader = false;
                    $scope.getNotificationShow = 0;
                    $scope.showAllNotificationList = [];
                    $scope.allTotal = 0;
                }
            }, function(error) {
                console.log("error", error);
            });
        }
    };
    $scope.getAllNotificationShow();
    $rootScope.$on("updateHeader", function(event, args) {
        $scope.avatar_url = args.user.local.avatar_url;
        $scope.userName = args.user.local.name;
        $scope.showAllNotificationList = [];
        $scope.getAllNotificationShow(true);

    });

    $scope.seenAllMessage = function() {
        if ($scope.getTotalNotification > 0) {
            viewAllNotificationService.update({
                id: APP.currentUser._id
            }, function(data) {
                if (data.statusCode === 200 && data.message === "ok") {
                    $scope.$emit("callTotalNotification", {});
                }
            });
        }
    };
    $scope.removeNotification = function(index, grpId, grpOwner, notiId) {
        $scope.showAllNotificationList.splice(index, 1);
        $scope.allTotal = $scope.allTotal - 1;
        var opts = {};
        opts.group = grpId;
        opts.groupOwner = grpOwner;
        opts.notificationId = notiId;
        rejectGroupInviteService.update({
            id: grpId
        }, opts, function(data) {});

        if($scope.allTotal === 0)
            $("#testing-dropdown").dropdown("toggle");
    };

    $scope.acceptNotification = function(index, grpId, grpOwner, notiId) {
        $scope.showAllNotificationList.splice(index, 1);
        $scope.allTotal = $scope.allTotal - 1;
        //$scope.rejectId = grpId;
        var opts = {};
        opts.group = grpId;
        opts.groupOwner = grpOwner;
        opts.notificationId = notiId;
        acceptGroupInviteService.update({
            id: grpId
        }, opts, function(data) {});

        if($scope.allTotal === 0)
            $("#testing-dropdown").dropdown("toggle");
    };

    $scope.viewLikeActivity = function(index, id, notiId) {
        $scope.getNotificationShow = true;
        $scope.showAllNotificationList[index].status = 'seen';
        $("#activity-noti-" + index).removeClass("unread-noti").addClass("read-noti");
        $scope.$broadcast('callViewActivityLike', {
            id: id
        });
        viewOneNotificationService.update({
            id: APP.currentUser._id,
            nid: notiId
        }, function(data) {}, function() {});
    };

    $scope.viewGroup = function(index, notificationList, notiId) {
        $("#testing-dropdown").dropdown("toggle");
        $scope.getNotificationShow = true;
        $scope.showAllNotificationList[index].status = 'seen';
        $("#activity-noti-" + index).removeClass("unread-noti").addClass("read-noti");
        $rootScope.$emit('showMyGroups');
          viewOneNotificationService.update({
            id: APP.currentUser._id,
            nid: notiId
        }, function(data) {}, function() {});
    };

    $scope.loadMoreNoti = function($event) {
        $event.stopPropagation();
        $scope.getAllNotificationShow(true);
    };
    $scope.stopEvent = function($event) {
      $event.stopPropagation();
    };

    $scope.logoClick = function() {
        $state.go('home', {}, { reload: true });
    };

}]);

app.directive('header', function () {
	return {
		templateUrl: 'app/shared/header/headerView.html',
		restrict: 'E',
		controller: 'headerController'
	};
});
app.factory('getStatistics', function ($resource) {
	return  $resource(APP.endpoints.statistics,{}, {query :{method : "GET", isArray :false}});
});

app.factory('getNotificationService', function ($resource) {
	return  $resource(APP.endpoints.getNotification,{}, {query :{method : "GET", isArray :false}});
});
app.factory('viewAllNotificationService', function ($resource) {
	return  $resource(APP.endpoints.viewAllNotification,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('viewOneNotificationService', function ($resource) {
	return  $resource(APP.endpoints.viewOneNotification,{ id: '@id',nid : '@nid'}, {update: { method: "PUT"}});
});
app.factory('rejectGroupInviteService', function ($resource) {
	return  $resource(APP.endpoints.rejectGroupInvite,{ id: '@id'}, {update: { method: "PUT"}});
});
app.factory('acceptGroupInviteService', function ($resource) {
	return  $resource(APP.endpoints.acceptGroupInvite,{ id: '@id'}, {update: { method: "PUT"}});
});

app.controller("lastComponentController", ['$rootScope', '$scope', '$uibModal', '$log', 'moduleActivitiesService', function($rootScope, $scope, $uibModal, $log, moduleActivitiesService) {
    $scope.module = {};
    $scope.allActivities = [];
    $scope.commloader = true;
    $scope.noActivities = false;
    $rootScope.$on("callLastComponent", function(event, args) {
        $scope.module = args.module;
        $scope.getAllActivitiesz();
    });
    $scope.getAllActivitiesz = function() {
        $scope.commloader = true;
        moduleActivitiesService.query({
            id: APP.currentUser._id,
            moduleId: $scope.module._id
        }, function(data) {
            if (data.statusCode == 200 && data.message == "ok") {
                if (data.result.totalCount === 0) {
                    $scope.commloader = false;
                    $scope.noActivities = true;
                } else {
                    $scope.commloader = false;
                    $scope.allActivities = data.result.activities;
                }
            }
        });
    };
}]);

app.directive('lastComponent', function () {
	return {
		templateUrl: 'app/shared/lastComponent/lastComponentView.html',
		restrict: 'E',
		controller: 'lastComponentController'
	};
});

app.factory('moduleActivitiesService', function ($resource) {
	return  $resource(APP.endpoints.moduleActivities,{id : '@id', moduleId : '@moduleId'}, {query :{method : "GET", isArray :false}});
});

app.controller("lessonTypeController", ['$rootScope', '$scope', '$timeout', '$uibModalInstance', '$log', 'moduleId', 'createLessonService', 'type', 'editLessonService', function($rootScope, $scope, $timeout, $uibModalInstance, $log, moduleId, createLessonService, type, editLessonService) {
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });
    $scope.type = type;
    if (type == 'Edit Lesson') {
        $scope.optionModule = moduleId.module;
        $scope.lesson = moduleId;
        $scope.videoTitle = "Change video";
        $scope.videoId = moduleId.url;
        $scope.lesson.location = moduleId.url;
    } else {
        $scope.optionModule = moduleId;
        $scope.lesson = {};
        $scope.videoTitle = "Add video";
    }

    $scope.showVideoButton = true;
    $scope.message = '';

    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };

    $scope.upload = function(element) {
        $scope.noFile = '';
        $scope.delImage = false;
        $scope.imag = '';
        $scope.uploadSuccess = false;
        $scope.showProgressBar = false;
        //$scope.showProgressBar = false;
        $scope.lesson.fileModel = element.files[0];
        $scope.uploadLoader = false;
        if ($scope.lesson.fileModel) {
            if ($scope.lesson.fileModel.type === 'image/jpeg' || $scope.lesson.fileModel.type === 'image/png') {
                $scope.uploadLoader = true;
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'mindmaxdaffo'
                    }
                });
                var imageName = $scope.lesson.fileModel.name + Math.floor(Date.now() / 1000);
                $scope.imag = imageName;
                var params = {
                    Key: imageName,
                    ContentType: $scope.lesson.fileModel.type,
                    Body: $scope.lesson.fileModel
                };
                bucket.upload(params).on('httpUploadProgress', function(evt) {
                    $scope.barShow = parseInt((evt.loaded * 100) / evt.total) + '%';
                    $scope.percentageStyle = {
                        width: $scope.barShow
                    };
                    $scope.$digest();
                }).send(function(err, data) {
                    if (err) {
                        $scope.uploadLoader = false;
                        $scope.noFile = 'Error in uploading file, please try again';
                        $scope.$digest();
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                    } else {
                        $scope.uploadLoader = false;
                        $scope.uploadSuccess = true;
                        $scope.lesson.location = data.Location;
                        if ($scope.delImage) {
                            $scope.hidePreview();
                            $scope.lesson.location = '';
                            $scope.lesson.fileModel = null;
                        }
                        $scope.showProgressBar = true;
                        //$scope.noFile = 'Image uploaded successfully';
                        $timeout(function() {
                            $scope.noFile = '';
                        }, 3000);
                        $scope.barShow = '0%';
                        element.value = null;
                        $scope.$digest();
                    }
                });
            } else {
                $scope.noFile = 'Please upload a image with jpeg and png format';
                $timeout(function() {
                    $scope.noFile = '';
                }, 3000);


            }
        } else {
            $timeout(function() {
                $scope.noFile = 'Please upload a file first';
            }, 3000);

        }

        return $scope.lesson.location;
    };

    $scope.hidePreview = function() {
        $scope.lesson.location = '';
        $scope.lesson.fileModel = null;
        var bucketInstance = new AWS.S3();
        var params = {
            Bucket: 'mindmaxdaffo',
            Key: $scope.imag
        };
        bucketInstance.deleteObject(params, function(err, data) {});
    };
    //videooooooooo
    $scope.addVideo = function() {
        $scope.showVideoFile = true;
        $scope.showVideo = true;

    };


    $scope.doneClick = function($event) {
        $scope.createLessonFormSubmitted = true;
        if ($scope.videoId === null || $scope.videoId === '' || $scope.videoId === undefined) {
            focus('videoIdField');
            return false;
        } else {
            $scope.createLessonFormSubmitted = false;
        }
        $scope.videoTitle = "Change video";
        $scope.showVideo = false;
    };
    $scope.cancelVideo = function() {
        $scope.lesson.url = '';
        $scope.videoTitle = "Add video";
        $scope.videoId = '';
        $scope.showVideo = false;
        $scope.showVideoFile = false;
    };

    //video submit createLessonTypeVideo
    $scope.createLessonTypeVideo = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.videoId === undefined || $scope.videoId === '') {
            focus('videoIdField');
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.url = $scope.videoId;
        opts.type = 'video';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };

    $scope.createLessonTypeImage = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        if ($scope.lesson.location === undefined || $scope.lesson.location === '') {
            $scope.noFile = "Please upload a file first";
            $timeout(function() {
                $scope.noFile = "";
            }, 2000);
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.url = $scope.lesson.location;
        opts.type = 'image';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };
    $scope.createLessonTypeText = function() {
        $scope.createLessonFormSubmitted = true;
        if ($scope.lesson.name === undefined || $scope.lesson.name === '') {
            focus('lessonName');
            return false;
        }
        if ($scope.lesson.basic_description === undefined || $scope.lesson.basic_description === '') {
            focus('lessonDes');
            return false;
        }
        var opts = {};
        opts.module = $scope.optionModule;
        opts.name = $scope.lesson.name;
        opts.basic_description = $scope.lesson.basic_description;
        opts.type = 'text';
        //$scope.loader = true;
        if (type == 'Edit Lesson') {
            $scope.message = frontendSettings.editSucessMessage;
            editLessonService.update({
                id: moduleId._id
            }, opts, function(data) {
                $scope.lessonServiceType(data, $scope.message);
            });
        } else {
            $scope.message = frontendSettings.sucessMessage;
            createLessonService.save(opts, function(data) {
                $scope.totalLesson.push($scope.lesson);
                $scope.lessonServiceType(data, $scope.message);
            });
        }
    };

    $scope.lessonServiceType = function(data, message) {
        if (data.statusCode === 200 && data.message === "ok") {
            $scope.createLessonFormSubmitted = false;
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = message;
            $scope.msgClass = 'success-green';
            $scope.lesson = {};
            $rootScope.$emit("updatelessons", {
                moduleIndex: $scope.moduleIndex,
                moduleId: $scope.optionModule
            });

            $timeout(function() {
                $scope.lessonResultMsg = false;
                $scope.sucessMsg = '';
                $scope.closeModal();

            }, 2000);
        } else if (data.statusCode === 422 && data.message === "lesson name must be unique") {
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = frontendSettings.uniqueLesson;
            $scope.msgClass = 'error-red';
            $scope.createLessonFormSubmitted = false;
            $timeout(function() {
                $scope.resultMsg = false;
                $scope.sucessMsg = '';
            }, 3000);
        } else {
            $scope.lessonResultMsg = true;
            $scope.sucessMsg = frontendSettings.errorOccured;
            $scope.msgClass = 'error-red';
            $scope.createLessonFormSubmitted = false;
            $timeout(function() {
                $scope.lessonResultMsg = false;
                $scope.sucessMsg = '';
            }, 4000);
        }
    };

}]);

app.factory('editLessonService', function ($resource) {
   return  $resource(APP.endpoints.lessonDetail,{id: '@id'},{update: { method: "PUT"}});
});

app.factory('actionService', function ($resource) {
	return  $resource(APP.endpoints.actions);
});
app.factory('actionUserService', function ($resource) {
	return  $resource(APP.endpoints.actionUser,  { id: '@id',actionId:'@actionId' },{query: { method: "PUT"}});
});
app.factory('createActivity', function($resource){
	return $resource(APP.endpoints.createActivity);
});
app.factory('moduleCommunityService', function($resource){
	return $resource(APP.endpoints.moduleCommunity);
});
app.factory('lessonCommunityService', function($resource){
	return $resource(APP.endpoints.lessonCommunity, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
app.factory('completedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson);
});
app.factory('getAllCompletedLessonService', function($resource){
	return $resource(APP.endpoints.completedLesson,  {query :{method : "GET", isArray :false}});
});
app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});
app.factory('lessonActivityService', function ($resource) {
   return  $resource(APP.endpoints.lessonActivity, { id: '@id', lessonId : '@lessonId'}, {query :{method : "GET", isArray :false}});
});

app.factory('getCompletedLessonService', function ($resource) {
   return  $resource(APP.endpoints.getCompletedLesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});

app.controller("modulesController", ['$scope', '$rootScope', '$timeout', 'moduleService', 'lessonService', 'lessonDetailService', 'actionService', 'actionUserService', 'createActivity', '$uibModal', '$state', 'moduleCommunityService', 'lessonCommunityService', 'completedLessonService', '$window', 'lessonActivityService','getCompletedLessonService', function($scope, $rootScope, $timeout, moduleService, lessonService, lessonDetailService, actionService, actionUserService, createActivity, $uibModal, $state, moduleCommunityService, lessonCommunityService, completedLessonService, $window, lessonActivityService, getCompletedLessonService) {
    $scope.modules = [];
    $scope.lesson = '';
    $scope.lessonDetail = '';
    $scope.loader = false;
    $scope.actionlist = false;
    $scope.showActivity = false;
    $scope.activityFormSubmitted = false;
    $scope.lessonSeeMore = false;
    $scope.selectAction = [];
    $scope.activity = {};
    $scope.shareLoader = false;
    $scope.module_id = '';
    $scope.mycommListShow = false;
    $scope.noFile = '';
    $scope.successAct = false;
    $scope.activityError = false;
    $scope.lenAction = false;
    $scope.uploadLoader = false;
    $scope.showLesson = false;
    $scope.delImage = false;
    $scope.uploadSuccess = false;

    $scope.showModule = {};
    $scope.lessons = [];
    $scope.modulesView = true;
    $scope.progressBar = false;
    $scope.current = 0;
    $scope.max = 0;
    $scope.onceClicked = [];
    $scope.showActivity = false;
    $scope.hasActivity = false;
    $scope.completedLessons = [];
    $scope.hasLessons = true;

    //AWS
    AWS.config.region = 'ap-northeast-1'; // Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-northeast-1:13efca28-48cb-445d-9b50-882e3b055c2a',
    });

    $scope.$on("callModuleController", function(event, args) {
        $scope.showModule = args.module;
        $scope.getAllLessons();
        // $scope.getCompletedLessons();

    });
    $scope.getAllLessons = function(module) {
        lessonService.query({
            id: $scope.showModule._id
        }, function(data) {

            if (data.statusCode === 200 && data.message === 'ok') {
                if (data.result.length > 0) {
                  $scope.lessons = data.result;
                  $scope.max = data.result.length;
                  $scope.hasLessons = true;
                } else {
                    $scope.hasLessons = false;
                }
            }
        }, function(error) {
        });
    };


    $scope.onNext = function(index, id, lesson) {
      $scope.completedLessons = [];
        if ($rootScope.isLoggedIn === false && $rootScope.isAdminLoggedIn === false) {
            $scope.$emit("ShowModalLogin", {});
        } else if(lesson.completed) {
            var opts = {};
            opts.user = APP.currentUser._id;
            opts.lesson = id;
            $timeout(function() {
                    angular.forEach($scope.lessons, function(lesson, key) {
                        if (lesson.completed) {
                            $scope.completedLessons.push(lesson);
                        }
                    });
            }, 0);
            $timeout(function() {
              var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'app/shared/progressBar/progressBarView.html',
                    controller: 'progressBarController',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sm',
                    resolve: {
                        max: function() {
                            return $scope.max;
                        },
                        current: function() {
                            return $scope.completedLessons;
                        },
                        lesson: function() {
                            return $scope.lesson;
                        },
                        lessonIndex: function() {
                            return index;
                        },
                        module: function() {
                            return $scope.showModule;
                         }
                    }
                });
            }, 100);

        } else {
            $scope.lessonForActivity = lesson;
            $scope.onceClicked[id] = id;
            //reddirectng to activity page at homeController
            $rootScope.$emit("callHomeControllerForActivity", {
                activity: true,
                lesson: lesson,
                index: index
            });
        }
    };

    $scope.progressModal = function(lesson) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/progressBar/progressBarView.html',
            controller: 'progressBarController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'sm',
            text: {
              value : "text",
               color: '#f00',
               autoStyle: true
            },
            resolve: {
                max: function() {
                    return $scope.max;
                },
                current: function() {
                    return $scope.current;
                },
                lesson: function() {
                    return lesson;
                }
            }
        });
    };
    //close detail of lesson
    $scope.$on("CallParamsCloseMethod", function() {
        $scope.closeLessonModal();
    });
    $scope.closeLessonModal = function() {
        $scope.actionlist = false;
        $scope.showActivity = false;

    };

    // Api call for module list
    var module = moduleService.query(function(data) {
        $scope.modules = data.result.module;
    });
  $scope.getlessonDetail = function($event, id, completeAttr) {
        $scope.noFile = '';
        $scope.activity = {};
        angular.element('#fileUploaded').val(null);
        $scope.activityList = [];
        $scope.mycommListShow = false;
        $scope.activityFormSubmitted = false;
        $event.stopPropagation();
        angular.element('#fadein').addClass('color-overlay');
        $scope.showActivity = true;
        $scope.actionlist = false;
        if (completeAttr === true) {
            $("#completeDft").attr("disabled", "disabled");
        } else {
            $("#completeDft").removeAttr("disabled");
        }
        lessonDetailService.get({
            id: id
        }, function(data) {
            if (data.statusCode == 200) {
                $scope.lessonDetail = data.result;
                $scope.module_id = $scope.lessonDetail.module;
                $scope.Keytakeaways = $scope.lessonDetail.Keytakeaways ? $scope.lessonDetail.Keytakeaways.split(".") : [];
                lessonCommunityService.get({
                    id: id
                }, function(data) {
                    if (data.result.length > 0) {
                        $scope.activityList = data.result;
                        $scope.mycommListShow = false;
                    } else {
                        $scope.mycommListShow = true;
                        $scope.activityList = data.result;
                    }
                });
            } else {
            }
        });
    };

    $(window).click(function(e) {
        try {
            if(!$(e.target).attr('class').includes('video-bg'))
                $scope.modulePlayer.stopVideo();
        }
        catch(e1) {}
    });
}]);

app.directive('modulesList', function() {
    return {
        templateUrl: 'app/shared/modules/modulesView.html',
        restrict: 'E',
        controller: 'modulesController'

    };
});
app.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];

        elm.bind('scroll', function() {
            if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
                scope.$apply(attr.whenScrolled);
            }
        });
    };
});

app.controller("myspaceController", ['$rootScope', '$scope', 'getspaceService', 'getViewActivityService', '$uibModal', 'removeActivityService', '$timeout', 'getlessonDetailService', 'getModuleDetailService', function($rootScope, $scope, getspaceService, getViewActivityService, $uibModal, removeActivityService, $timeout, getlessonDetailService, getModuleDetailService) {
    $scope.spaceloader = true;
    $scope.mySpaceList = [];
    $scope.url = "";
    $scope.id = "";
    $scope.numberDate = "";
    $scope.numberSpan = "";
    $scope.createdAt = [];
    $scope.lessonsz = [];
    $scope.modulesz = [];
    $scope.index = 0;
    $scope.mySpaceMsg = true;
    $scope.lastLength = -1;
    $rootScope.activeSection = "space";
    $rootScope.myTabs = true;
    $scope.currentIndex = null;

    // function for get the community
    $scope.showSpaceList = function() {
        if ($scope.lastLength >= $scope.mySpaceList.length)
            return;

        var limit_start = $scope.mySpaceList.length;
        $scope.lastLength = limit_start;
        var limit = 8;
        var opts = {};
        opts.id = APP.currentUser._id;
        opts.limit = limit;
        opts.skip = limit_start;

        $scope.lessonsz = [];
        $scope.modulesz = [];

        if ((($scope.allTotal > limit_start) || $scope.allTotal === 0)) {
            $scope.spaceloader = true;
            getspaceService.query(opts, function(data) {
                $scope.spaceloader = false;
                if (data.statusCode === 200 && data.message === 'ok') {
                    if (data.result.activities.length > 0) {
                        $scope.ago(data.result.activities);
                        $scope.spaceloader = false;
                        $scope.mySpaceList = $scope.mySpaceList.concat(data.result.activities);
                        $scope.allTotal = data.result.totalCount;
                        $scope.mySpaceMsg = false;
                    } else {
                        $scope.spaceloader = false;
                        $scope.allTotal = 0;
                        $scope.mySpaceList = [];
                    }
                }
            }, function(error) {});
        } else
            $scope.spaceloader = false;
    };

    $rootScope.$on('updatingLikes', function(event, args) {
        $scope.mySpaceList = [];
        $scope.showSpaceList();
    });

    $scope.ago = function(activities) {
        angular.forEach(activities, function(activity, key) {
            $timeout(function() {
                var a = new Date(activity.createdAt);
                var b = new Date();
                var agoSpan = $scope.getActualTime(b - a);
                $scope.createdAt.push(agoSpan);
            }, 0);

        });
    };

    $scope.getActualTime = function(t) {
        var showSpan = {};
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000,
            d = Math.floor(t / cd),
            h = Math.floor((t - d * cd) / ch),
            m = Math.round((t - d * cd - h * ch) / 60000),
            pad = function(n) {
                return n < 10 ? '0' + n : n;
            };
        if (m === 60) {
            h++;
            m = 0;
        }
        if (h === 24) {
            d++;
            h = 0;
        }
        if (d === 0) {
            showSpan.numberDate = d;
            showSpan.numberSpan = "day";
            return showSpan;
        } else if (d > 0 && d < 30) {
            if (d == 1) {
                showSpan.numberDate = d;
                showSpan.numberSpan = "day";
                return showSpan;
            } else {
                showSpan.numberDate = d;
                showSpan.numberSpan = "days";
                return showSpan;
            }
        } else if (d > 30 || d == 30) {
            var months = parseInt(d / 30);
            if (months > 12 || months == 12) {
                var years = parseInt(months / 12);
                if (years == 1) {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "year";
                    return showSpan;
                } else {
                    showSpan.umberDate = years;
                    showSpan.umberSpan = "years";
                    return showSpan;
                }
            } else {
                if (months == 1) {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "month";
                    return showSpan;
                } else {
                    showSpan.numberDate = months;
                    showSpan.numberSpan = "months";
                    return showSpan;
                }

            }

        }
    };

    $scope.showSpaceList();
    $scope.$on("CallUpdatedSpaceMethod", function() {
        $scope.showSpaceList();
    });
    // Open modal for view full detail of activity in my space
    $scope.viewActivitySpace = function(activityId, index) {
        $scope.currentIndex = index;
        $scope.activityList = $scope.mySpaceList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            size: 'lg',
            templateUrl: 'app/shared/activityDetail/viewSharedActivity.html',
            controller: 'viewSharedController',
            scope: $scope,
            resolve: {
                activityId: function() {
                    return angular.copy(activityId);
                }
            }
        });
    };
    $scope.deleteMyActivity = function(index, id, url, $event) {
        $event.stopPropagation();
        $scope.id = id;
        $scope.url = url;
        $scope.index = index;
        $scope.mySpaceList = $scope.mySpaceList;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/shared/dialogBox/alertView.html',
            controller: 'confirmController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
                message: function() {
                    return "Are you sure, you want to delete this activity?";
                },
                item: function() {
                    return "delete-activity-space";
                }
            }
        });

    };

    $scope.loadMoreSpace = function() {
        if ($rootScope.activeSection !== 'space')
            return;

        $scope.showSpaceList();
    };

}]);

app.directive('mySpace', function () {
	return {
		templateUrl: 'app/shared/myspace/myspaceView.html',
		restrict: 'E',
		controller: 'myspaceController'
	};
});
app.factory('getspaceService', function ($resource) {
	return  $resource(APP.endpoints.mySpace,{id: '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getlessonDetailService', function ($resource) {
	return  $resource(APP.endpoints.lessonDetail,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('getModuleDetailService', function ($resource) {
	return  $resource(APP.endpoints.actionModule,{id : '@id'},{query: { method: "GET", isArray: false }});
});
app.factory('removeActivityService', function ($resource) {
	return  $resource(APP.endpoints.deleteActivity,{id: '@id'},{remove: { method: "DELETE"}});
});

app.controller("progressBarController", ['$rootScope', '$scope', '$state', '$uibModalInstance', 'max', 'current', 'lesson', 'lessonIndex', 'module', 'lessonService', '$timeout', function($rootScope, $scope, $state, $uibModalInstance, max, current, lesson, lessonIndex, module, lessonService, $timeout) {
    $scope.max = max;
    $scope.current = current.length;
    $scope.lesson = lesson;
    $scope.module = module;
    $scope.lessonIndex = lessonIndex;
    $scope.nextLesson = {};
    $scope.lessons = [];
    $scope.inCompleteLessons = [];
    $scope.percent = "";

    $scope.getAllLessons = function() {
        $timeout(function() {
            lessonService.query({
                id: $scope.module._id
            }, function(data) {
                $scope.lessons = data.result;
                angular.forEach(data.result, function(lesson, key) {
                    if (!lesson.completed) {
                        $scope.inCompleteLessons.push(lesson);
                    }
                });
                $scope.percent = parseInt((($scope.max-$scope.inCompleteLessons.length)*100)/$scope.max);
            });
        }, 0);

    };
    $scope.getAllLessons();
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };
    $rootScope.$on("closingModal", function(event, args){
        $scope.closeModal();
    });
    $scope.onProgressNext = function() {

        if ($scope.max == $scope.current) {

            $rootScope.$emit("callHomeConLastActiviyComponent", {
                module: $scope.module
            });

        } else {

            $scope.nextLesson = $scope.inCompleteLessons[0];
                $rootScope.$emit("callHomeControllerForActivity", {
                    activity: true,
                    lesson: $scope.nextLesson,
                    module: $scope.module,
                    index: $scope.lessonIndex + 1,
                });


        }
    };


}]);

app.directive('progressBar', function () {
	return {
		templateUrl: 'app/shared/progressBar/progressBarView.html',
		restrict: 'E',
		controller: 'progressBarController'
	};
});
//

app.factory('lessonService', function ($resource) {
   return  $resource(APP.endpoints.lesson, { id: '@id'}, {query :{method : "GET", isArray :false}});
});

app.controller("registerController", ['$scope', '$uibModalInstance', 'registerService', 'focus', 'signupService', 'loginService', 'profileService', '$state', '$rootScope', '$uibModal', 'forgotPasswordService', '$timeout', function($scope, $uibModalInstance, registerService, focus, signupService, loginService, profileService, $state, $rootScope, $uibModal, forgotPasswordService, $timeout) {
    $scope.showLoginForm = true;
    $scope.showRegForm = false;
    $scope.showErrEmail = false;
    $scope.formSubmitted = false;
    $scope.loginFormSubmitted = false;
    $scope.user = {};
    $scope.forgot = {};
    $scope.signupStart = false;
    $scope.userNotFound = false;
    $scope.userPassWrong = false;
    $scope.showForgotPass = false;
    $scope.msgShow = '';
    $scope.forgotLoader = false;
    $scope.forgotmessage = '';
    $scope.checkingEmail = false;
    // show login form
    $scope.showLogin = function() {
        $scope.user = {};
        $scope.dt = '';
        $scope.showLoginForm = true;
        $scope.showRegForm = false;
        $scope.formSubmitted = false;
        $scope.loginFormSubmitted = false;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        showWeeks: 'false'
    };
    $scope.format = 'dd-MMMM-yyyy';
    $scope.disabled = function(date, mode) {
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    };
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.maxDate = new Date();
    $scope.toggleMin = function() {
        $scope.minDate = null;
    };
    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.toggleMin();

    // show registration form
    $scope.showRegister = function() {
        $scope.user = {};
        $scope.dt = '';
        $scope.showRegForm = true;
        $scope.showLoginForm = false;
        $scope.formSubmitted = false;
        $scope.loginFormSubmitted = false;
    };

    // Validate email
    $scope.emailValidate = function() {
        $scope.checkingEmail = false;
        $scope.showErrEmail = true;
        $scope.errMsg = frontendSettings.checkingEmail;
        //$scope.showErrEmail = false;
        //$scope.errMsg = '';
        if ($scope.user.email === undefined) {} else {
            registerService.save({
                email: $scope.user.email
            }, function(data) {
                if (data.statusCode == 200 && data.message == "ok" && data.result.message == 'verified') {
                    $scope.showErrEmail = false;
                    $scope.checkingEmail = false;
                } else if (data.statusCode == 422 && data.message == 'email already registered') {

                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.userregistered;
                } else if (data.statusCode == 422 && data.message == 'email should be registered with any email provider') {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                } else {
                    $scope.showErrEmail = true;
                    $scope.errMsg = frontendSettings.invalidEmail;
                }
            });
        }
    };
    $scope.convert = function(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [day, mnth, date.getFullYear()].join("/");
    };
    $scope.login = function() {

        $scope.loginFormSubmitted = true;
        $scope.loginStart = false;
        if ($scope.user.email === undefined || $scope.user.email === '') {
            focus('uname');
            return false;
        } else if ($scope.user.password === undefined || $scope.user.password === '') {
            focus('password');
            return false;
        }
        var opts1 = {};
        $scope.loginStart = true;

        opts1.email = $scope.user.email;
        opts1.password = $scope.user.password;
        loginService.save(opts1, function(data) {
            if (data.statusCode == 200 && data.message === 'ok') {
                $scope.loginStart = false;
                APP.currentUser = data.result;
                localStorage.setItem("loggedInUser", JSON.stringify(data.result));
                //$rootScope.isLoggedIn = true;

                //APP.currentUser.local.ambassdor = true;
                //angular.element(document.getElementById('headerController')).scope().cancel();
                if (APP.currentUser.local.ambassdor) {
                    $rootScope.isAdminLoggedIn = true;
                } else {
                    $rootScope.isLoggedIn = true;
                }
                $rootScope.$emit("updateHeader", {
                    user: APP.currentUser
                });
                $uibModalInstance.dismiss('cancel');
                //window.location.reload(true);
                $state.go('home', {}, {
                    reload: true
                });
            } else if (data.statusCode == 422 && data.message.email === false && data.message.password === false) {
                $scope.loginStart = false;
                $scope.userNotFound = true;
                $scope.msgShow = 'User not found';
                $timeout(function() {
                    $scope.msgShow = '';
                }, 3000);
            } else if (data.statusCode == 422 && data.message.email === true && data.message.password === false) {
                $scope.loginStart = false;
                $scope.userPassWrong = true;
                $scope.msgShow = 'Password is wrong';
                $timeout(function() {
                    $scope.msgShow = '';
                }, 3000);
            } else {
                $scope.loginStart = true;
            }
        }, function(error) {
        });
    };
    // Forgot Password
    $scope.forgotPassword = function() {
        $scope.showForgotPass = true;
        $scope.showLoginForm = false;
        $scope.showRegForm = false;
    };
    // function for send mail in  forgot password

    $scope.forgotPasswordSave = function() {
        $scope.forgotSubmitted = true;
        $scope.forgotmessage = '';
        if ($scope.forgot.email === undefined || $scope.forgot.email === null) {
            return false;
        }
        $scope.forgotLoader = true;
        var opts = {};
        opts.email = $scope.forgot.email;
        opts.url = APP.resetPasswordUrl;
        forgotPasswordService.save(opts, function(data) {
            $scope.forgotLoader = false;
            if (data.statusCode === 200 && data.message === 'ok') {
                $scope.forgotmessage = 'Email sent successfully on your email-id';
                $scope.msgClass = 'success-green';
                $scope.forgot = {};
                $scope.forgotSubmitted = false;
                /*$timeout(function() {
                $scope.forgotmessage = '';
                }, 15000); */

            } else {
                $scope.forgotmessage = 'Email not registered';
                $scope.msgClass = 'error-red';
                /*$timeout(function() {
                $scope.forgotmessage = '';
                }, 15000); */
            }
        }, function(error) {
        });
    };

    $scope.submitRegistration = function() {
        $scope.formSubmitted = true;
        $scope.sucessMessage = false;

        $scope.dob = '';
        if ($scope.dt) {
            $scope.dob = $scope.convert($scope.dt);
        }
        if ($scope.user.name === undefined || $scope.user.name === '') {
            focus('uname');
            return false;
        } else if ($scope.dob === undefined || $scope.dob === '') {
            focus('dob');
            return false;
        } else if ($scope.user.email === undefined || $scope.user.email === '') {
            focus('email');
            return false;
        } else if ($scope.user.password === undefined || $scope.user.password === '') {
            focus('password');
            return false;
        } else if ($scope.showErrEmail === true) {
          $scope.checkingEmail = true;
            focus('email');
            return false;
        }

        $scope.signupStart = true;
        var opts = {};
        opts.email = $scope.user.email;
        opts.password = $scope.user.password;
        opts.dob = $scope.dob;
        opts.name = $scope.user.name;

        signupService.save(opts, function(data) {
            if (data.statusCode == 200 && data.message === 'ok') {
                $scope.sucessMessage = true;
                $scope.signupStart = false;
                $scope.login();
            } else if (data.statusCode == 422 && data.message.email === false) {
                $scope.signupStart = false;
                $scope.showErrEmail = true;
                $scope.errMsg = frontendSettings.userregistered;
            } else {
                $scope.signupStart = true;
            }
        }, function(error) {
        });
    };
    //  Function for close modal
    $scope.closeModal = function() {
        $uibModalInstance.dismiss('cancel');
        angular.element('#fadein').removeClass('color-overlay');
    };
    // facebookLogin function
    $scope.facebookLogin = function() {
        window.location.href = APP.facebookLogin;
    };
    //google plus login function
    $scope.googlePlusLogin = function() {
        window.location.href = APP.googlePlusLogin;
    };
    //twitter login
    $scope.twitterLogin = function() {
        window.location.href = APP.twitterLogin;
    };
}]);

app.directive('registerForm', function () {
	return {
		templateUrl: 'app/shared/register/registerView.html',
		restrict: 'E',
		controller: 'registerController'
	};
});
app.factory('registerService', function ($resource) {
	return  $resource(APP.endpoints.validateEmail);
});
app.factory('signupService', function ($resource) {
	return  $resource(APP.endpoints.signup);
});
app.factory('loginService', function ($resource) {
	return  $resource(APP.endpoints.login);
});
app.factory('profileService', function ($resource) {
	return  $resource(APP.endpoints.profile ,{}, {query: { method: "GET", isArray: false }});
});
app.factory('logoutService', function ($resource) {
	return  $resource(APP.endpoints.logout);
});
app.factory('isAuthenticated', function ($resource) {
	return  $resource(APP.endpoints.isAuthenticated ,{}, {query: { method: "GET", isArray: false }});
});
app.factory('forgotPasswordService', function($resource){
	return $resource(APP.endpoints.forgotPassword);
});

app.controller('ModalController', ['$scope', '$uibModalInstance',
    function($scope, $uibModalInstance) {

        $scope.$on('closeModal', function() {
            $uibModalInstance.dismiss('cancel');
        });

        $scope.closeModal = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.closePostActivityModal = function() {
            $uibModalInstance.dismiss('cancel');
            angular.element('#fadein').removeClass('color-overlay');
        };
    }
]);


