var Comment = require('../models/comments');

module.exports = {
	populateCommentCount: myPopulate
};

/**
 * To add comments field to each activity recursively
 */
function myPopulate(activities, i, done) {
	if (i >= activities.length)
		return done(null, activities);

	Comment.find({ activity: activities[i]._id, deleted: false }, function (err, comments) {
		if (err)
			return (err, null);

		if (!comments)
			comments = 0;

		var temp = JSON.parse(JSON.stringify(activities[i]));
		temp.comments = comments.length;
		activities[i] = temp;

		myPopulate(activities, i + 1, done);
	});
}