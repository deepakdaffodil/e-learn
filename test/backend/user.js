// Created By Nitesh Jatav  on 10/02/2016
var assert = require('chai').assert;
var request = require('superagent');
var config = require('../../config/config');
var testData = require('./testData');
var url = config.server.host+':'+config.server.port;
var userId = null;

// notifications
describe("Lesson APIs get: "+url+"/api/users/:id/notifications",function(){
	it("should return an object",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
	it("should return object must contain notifications key ",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body.result,'notifications');
			done();
		})
	});
	it("should return object must contain totalCount key ",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body.result,'totalCount');
			done();
		})
	});
	it("should return object must contain notifications key as array",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.isArray(res.body.result.notifications);
			done();
		})
	});
	it("should return object must contain totalCount",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body.result,'totalCount');
			done();
		})
	});
	it("should return object must contain totalCount isNotObject",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.isNotObject(res.body.result.totalCount);
			done();
		})
	});
	it("should return object must contain newNoticationCount",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.property(res.body.result,'newNoticationCount');
			done();
		})
	});
	it("should return object must contain newNoticationCount isNotObject",function(done){
		request
		.get(url+'/api/users/'+testData.mongodb._id+'/notifications')
		.end(function(err, res){
			assert.isNotObject(res.body.result.newNoticationCount);
			done();
		})
	});
});
// get all users.
describe("To get all users APIs get: "+url+"/api/users",function(){
	it("should return an object",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
	
	it("should return object must contain totalCount key ",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			assert.isArray(res.body.result.users);
			done();
		})
	});
});
// get user and edit a user
describe("To get user and edit it APIs get and put: "+url+"/api/users",function(){
	it("should return an object",function(done){
		request
		.get(url+'/api/users')
		.end(function(err, res){
			userId = res.body.result.users[0]._id;
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.put(url+'/api/users/admin/'+userId)
		.send(testData.editUserNew)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	
});
// search users
describe("Search APIs - GET: "+url+"/api/users/search", function() {
	it("should search for users, with a single letter, by email and name, and return their emails",function(done){
		var term = 'a';
		
		request
			.get(`${url}/api/users/search?term=${term}`)
			.end(function(err, res){
				for(var i = 0; i < res.body.result.length; i++)
					assert(res.body.result[i].search(new RegExp(term, 'i') >= 0) || res.body.result[i].search(new RegExp(term, 'i') >= 0));
				done();
			});
	});
	it("should search for users, with valid multiple letters, by email and name, and return their emails",function(done){
		var term = 'anu';
		
		request
			.get(`${url}/api/users/search?term=${term}`)
			.end(function(err, res){
				for(var i = 0; i < res.body.result.length; i++)
					assert(res.body.result[i].search(new RegExp(term, 'i') >= 0) || res.body.result[i].search(new RegExp(term, 'i') >= 0));
				done();
			});
	});
	it("should search for users, with invalid multiple letters, by email and name, and return their emails",function(done){
		var term = 'asdsg3';
		
		request
			.get(`${url}/api/users/search?term=${term}`)
			.end(function(err, res){
				for(var i = 0; i < res.body.result.length; i++){
					assert(res.body.result[i].search(new RegExp(term, 'i') >= 0) || res.body.result[i].search(new RegExp(term, 'i') >= 0));
				}
				done();
			});
	});
});
// search users by name
describe("Search APIs - GET: "+url+"/api/users/search/names", function() {
	it("should search for users, with a single letter, by email and name, and return their names and id",function(done){
		var term = 'a';
		
		request
			.get(`${url}/api/users/search/names?term=${term}`)
			.end(function(err, res){
				assert(res.body.result.length > 0);
				done();
			});
	});
	it("should search for users, with valid multiple letters, by email and name, and return their names and ids",function(done){
		var term = 'anu';
		
		request
			.get(`${url}/api/users/search/names?term=${term}`)
			.end(function(err, res){
				assert(res.body.result.length > 0);
				done();
			});
	});
	it("should search for users, with invalid multiple letters, by email and name, and return empty array",function(done){
		var term = 'asdsg3zzzzzzzzzzzzz';
		
		request
			.get(`${url}/api/users/search/names?term=${term}`)
			.end(function(err, res){
					assert(res.body.result.length <= 0);
				done();
			});
	});
});
// followers
describe("Followers API - GET: " + url + "/api/users/followers", function() {
	it("should return with status code 200",function(done){
		request
			.get(`${url}/api/users/followers?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 200);
				done();
			});
	});
	it("should return with 'ok' message",function(done){
		request
			.get(`${url}/api/users/followers?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.message === 'ok');
				done();
			});
	});
	it("should return with error as false",function(done){
		request
			.get(`${url}/api/users/followers?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(!res.body.error);
				done();
			});
	});
	it("should return array",function(done){
		request
			.get(`${url}/api/users/followers?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert.isArray(res.body.result);
				done();
			});
	});
	it("should return array of users, with property 'local'",function(done){
		request
			.get(`${url}/api/users/followers?id=${testData.sampleUser}`)
			.end(function(err, res){
				for(var i = 0; i < res.body.result.length; i++)
					assert.property(res.body.result[i], 'local');
				done();
			});
	});
});
// following
describe("Following API - GET: " + url + "/api/users/following", function() {
	it("should return with status code 200",function(done){
		request
			.get(`${url}/api/users/following?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 200);
				done();
			});
	});
	it("should return with 'ok' message",function(done){
		request
			.get(`${url}/api/users/following?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.message === 'ok');
				done();
			});
	});
	it("should return with error as false",function(done){
		request
			.get(`${url}/api/users/following?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(!res.body.error);
				done();
			});
	});
	it("should return array",function(done){
		request
			.get(`${url}/api/users/following?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert.isArray(res.body.result);
				done();
			});
	});
	it("should return array of users, with property 'local'",function(done){
		request
			.get(`${url}/api/users/following?id=${testData.sampleUser}`)
			.end(function(err, res){
				for(var i = 0; i < res.body.result.length; i++)
					assert.property(res.body.result[i], 'local');
				done();
			});
	});
});
// add followers
describe('Add Followers API - PUT: ' + url + '/api/users/follow/:id', function() {
	it('sends an statusCode of 422 when following oneself', function(done) {
		request
			.put(`${url}/api/users/follow/${testData.sampleUser}?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 422);
				done();
			});
	});
	it('sends an statusCode of 404 when following unknown user', function(done) {
		request
			.put(`${url}/api/users/follow/asdasdasdasd?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 404);
				done();
			});
	});
	it('sends an statusCode of 200 when following known user', function(done) {
		request
			.put(`${url}/api/users/follow/${testData.sampleUser2}?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 200);
				done();
			});
	});
	it('sends an statusCode of 422 when following known user again', function(done) {
		request
			.put(`${url}/api/users/follow/${testData.sampleUser2}?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 422);
				done();
			});
	});
});
// remove followers
describe('Remove Followers API - PUT: ' + url + '/api/users/unfollow/:id', function() {
	it('sends a statusCode of 404 when unfollowing unknown user', function(done) {
		request
			.put(`${url}/api/users/unfollow/asdasdasdasd?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 404);
				done();
			});
	});
	it('sends a statusCode of 200 when unfollowing known user', function(done) {
		request
			.put(`${url}/api/users/unfollow/${testData.sampleUser2}?id=${testData.sampleUser}`)
			.end(function(err, res){
				assert(res.body.statusCode == 200);
				done();
			});
	});
});