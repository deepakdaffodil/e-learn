// Created By Nitesh Jatav  on 10/02/2016
module.exports = {
	'mongodb' : {
		"_id" : "572c52f76eabfe79492fbdbd"
	},
	'module' : {
		"_id" : "572c52f76eabfe79492fbdbd",
		"name" : "module 8",
		"description" : "description of module eight",
		"deleted" : false,
		"__v" : 0
	},
	'lesson' : {
		"_id" : "572c52f76eabfe79492fbdbd",
		"module" : "56b59d799759cff2207e08d9",
		"name" : "lesson 3",
		"basic_description" : "detailed_description",
		"detailed_description" : "detailed_description ",
		"deleted" : false,
		"__v" : 0
	},
	'login' : {
		"password":"asdasd",
		"email":"anubhav.dhawan@daffodilsw.com"
	},
	'addLessonData' : {
		"module":"56b4456011902bec135bf922","name":"lession 4",
		"basic_description":"lesson  four basic_description",
		"detailed_description":"lesson four detailed_description"
	},
	'localUser' : {
		"name":"Niteshkumar",
		"dob":"2/04/1991",
		"password":"hrhk@12345",
		"email":"demotest@damotest.com",
		"username":"1234abAB@"
	},
	'editUser' : {
		"name":"Nitesh",
		"dob":"2/04/1991",
		"password":"hrhk@1234",
		"email":"demotest@damotest.com"
	},
	'validEmail' : {
		"email": "nitesh.Jatav@daffodilsw.com"
	},
	'invalidEmail' : {
		"email": "test@.com"
	},
	'validUsername' : {
		"username": "1234abAB@"
	},
	'invalidUsername' : {
		"username": "12345"
	},
	'changePassword' : {
		"password":"1234abAB@",
		"email":"demotest@damotest.com",
		"password":"1234abAB@"
	},
	'module' : {
		"name" : "Test Module17",
		"_id": "572c6b7c08e8feb9640ef8ce",
		"description" : "God is everywhere!!!"
	},
	'completedModule': {
		'_id': '575e3fa885a102f8244b8a3d'
	},
	'editModule' : {
		"name" : "edited Module17",

		"description" : "God is everywhere in this universe!!!"
	},
	'editUserNew' : { 
		"dob": "6/23/2012", 			
		"name": "nitesh", 
		"email": "nitesh@gmail.com", 
		"avatar_url": null, 
		"password": "123456", 
		"rev" : "4"  
	},
	'createActivity': {
		"user": "572723363e9960870f776340",
		"lesson": "575fe479204b3b141c3067cb",
		"description": "Rrrrrrrrrrrrrrr",
		"groups": [],
		"type":"image",
		"url":"",
		"module": "575aba877a998dc012b8a260",
		"my_community": "true",
		"tags": ["asdas2d", "asdt", "jR","fasd"]
	},
	'sampleActivity': '5763eddc57551ab419a00e8d',
	'sampleDescription': 'This is something new',
	'sampleUser': '572723363e9960870f776340',
	'sampleUser2': '575ff276e5a059802281b18d'
};