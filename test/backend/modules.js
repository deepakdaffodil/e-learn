// Created By Nitesh Jatav  on 10/02/2016
var assert = require('chai').assert;
var request = require('superagent');
var config = require('../../config/config');
var testData = require('./testData');
var url = config.server.host+':'+config.server.port;
var moduleId = null;

describe("Module APIs get: "+url+"/api/modules",function(){
	it("should return an object",function(done){

		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object with length 4",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.equal(Object.keys(res.body).length,4);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/modules')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
});
describe("Module APIs get: "+url+"/api/modules/:id",function(){
	var modulesId = testData.module._id;
	it("should return an object",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/modules/')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/modules/'+testData.module._id)
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/modules/'+modulesId)
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
});
describe("Module APIs get: "+url+"/api/modules/:id/lessons",function(){
	var modulesId = testData.module._id;
	it("should return an object",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/modules/'+modulesId+'/lessons')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
});
describe("Module APIs get: "+url+"/modules/anything",function(){
	it("should return an object",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value true",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.equal(res.body.error,true);
			done();
		})
	});
	it("should return object must contain statusCode key with value 404",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.equal(res.body.statusCode,404);
			done();
		})
	});
	it("should return object must contain message key with value 'Not Found' ",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.equal(res.body.message,"Not Found");
			done();
		})
	});
	it("should return object must contain result key with object",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.isObject(res.body.result);
			done();
		})
	});
	it("should return object must contain result key with blank object",function(done){
		request
		.get(url+'/api/modules/anything')
		.end(function(err, res){
			assert.equal(Object.keys(res.body.result).length,0);
			done();
		})
	});
});
describe("Module APIs post for adding an module: "+url+"/api/modules",function(){
	it("should return an object",function(done){
		request
		.post(url+'/api/modules')
		.send({
			name: testData.module.name,
			description: testData.module.description
		})
		.end(function(err, res){
			moduleId = res.body.result._id;
			assert.isObject(res.body);
			assert.isNotArray(res.body);
			assert.isObject(res.body.result);
			assert.equal(Object.keys(res.body).length,4);
			assert.property(res.body,'result');
			assert.property(res.body,'message');
			assert.property(res.body,'statusCode');
			assert.property(res.body,'error');
			assert.property(res.body.result,'name');
			assert.property(res.body.result,'description');
			assert.property(res.body.result,'deleted');
			assert.equal(res.body.result.deleted,false);
			assert.equal(res.body.error,false);
			assert.equal(res.body.statusCode,200);
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.post(url+'/api/modules')
		.send(testData.module)
		.end(function(err, res){
			assert.isObject(res.body);
			assert.isNotArray(res.body);
			assert.isObject(res.body.result);
			assert.equal(Object.keys(res.body).length,4);
			assert.property(res.body,'result');
			assert.property(res.body,'message');
			assert.property(res.body,'statusCode');
			assert.property(res.body,'error');
			assert.equal(res.body.error,true);
			assert.equal(res.body.statusCode,422);
			done();
		})
	});
	// to edit a module
	it("should return object must contain edit module object",function(done){
		request
		.put(url+'/api/modules/'+moduleId)
		.send(testData.editModule)
		.end(function(err, res){
			assert.isObject(res.body);
			assert.isNotArray(res.body);
			assert.isObject(res.body.result);
			assert.equal(Object.keys(res.body).length,4);
			assert.property(res.body,'result');
			assert.property(res.body,'message');
			assert.property(res.body,'statusCode');
			assert.property(res.body,'error');
			assert.equal(res.body.error,false);
			assert.equal(res.body.statusCode,200);
			assert.equal(res.body.message,"ok");
			done();
		})
	});
	// to delete a module
	it("should return object must contain delete module object",function(done){
		request
		.delete(url+'/api/modules/'+moduleId)
		.end(function(err, res){
			assert.isObject(res.body);
			assert.isNotArray(res.body);
			assert.isObject(res.body.result);
			assert.equal(Object.keys(res.body).length,4);
			assert.property(res.body,'result');
			assert.property(res.body,'message');
			assert.property(res.body,'statusCode');
			assert.property(res.body,'error');
			assert.equal(res.body.error,false);
			assert.equal(res.body.statusCode,200);
			assert.equal(res.body.message,"ok");
			done();
		})
	});
});
describe("Module Search API - GET: "+url+"/api/module/search", function() {
	it("should search for modules, and return their names and id",function(done){
		var term = 'a';
		
		request
			.get(`${url}/api/modules/search?term=${term}`)
			.end(function(err, res){
				assert(res.body.result.length > 0);
				done();
			});
	});
	it("should search for modules, with invalid multiple letters, and return empty array",function(done){
		var term = 'asdsg3zzzzzzzzzzzzz';
		
		request
			.get(`${url}/api/modules/search?term=${term}`)
			.end(function(err, res){
				assert(res.body.result.length <= 0);
				done();
			});
	});
	it("should give a list of all modules, if no term is specified",function(done){
		var term = '';
		
		request
			.get(`${url}/api/modules/search?term=${term}`)
			.end(function(err, res){
				assert(res.body.result.length > 0);
				done();
			});
	});
});