var assert = require('chai').assert;
var request = require('superagent');
var config = require('../../config/config');
var testData = require('./testData');
var url = config.server.host+':'+config.server.port;

describe("Activity tag API post: " + url + "/activity", function() {
	it("should return an object",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.post(url+'/api/activity/')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.post(url+'/api/activity/')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in result, called tags",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body.result,"tags");
			done();
		});
	});
	it("should contain a property tags as an array",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			assert.isArray(res.body.result.tags);
			done();
		});
	});
	it("should contain a property tags as an array with all the tags specified in lowercase",function(done){
		request
		.post(url+'/api/activity')
        .send(testData.createActivity)
		.end(function(err, res){
			for(var i = 0; i < res.body.result.tags.length; i++) {
                assert(res.body.result.tags[i] === testData.createActivity.tags[i].toLowerCase());
            }
			done();
		});
	});
});
describe("Search tag API get: " + url + "/api/tags", function() {
	it("should return an object",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/tags/')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/tags/')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in result, called tags",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.property(res.body.result,"tags");
			done();
		});
	});
	it("should contain a property tags as an array",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
			assert.isArray(res.body.result.tags);
			done();
		});
	});
	it("should contain a property tags as an array with all elements in lowercase",function(done){
		request
		.get(url+'/api/tags')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.tags.length; i++) {
		        assert(res.body.result.tags[i] === res.body.result.tags[i].toLowerCase());
            }
			done();
		});
	});
	it("should search for tags correctly",function(done){
		request
		.get(url+'/api/tags?term=as')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.tags.length; i++) {
            assert(res.body.result.tags[i].indexOf('as') > -1);
            }
			done();
		});
	});
	it("should obey limit",function(done){
		request
		.get(url+'/api/tags?limit=2')
		.end(function(err, res){
            assert(res.body.result.tags.length <= 2);
			done();
		});
	});
	it("should obey skip",function(done){
		request
		.get(url+'/api/tags?limit=3')
		.end(function(err, res){
            var res1 = res.body.result.tags;
            request
            .get(url+'/api/tags?skip=1&limit=2')
            .end(function(err, res){
                var res2 = res.body.result.tags;
                res1.splice(0, 1);
                assert(res1.length === res2.length);
                for(var i = 0; i < res1.length; i++) {
                    assert(res1[i] === res2[i]);
                }
                done();
            });
		});
	});
});