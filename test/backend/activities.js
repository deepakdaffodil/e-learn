// Created By Nitesh Jatav  on 10/02/2016
var assert = require('chai').assert;
var request = require('superagent');
var config = require('../../config/config');
var testData = require('./testData');
var url = config.server.host+':'+config.server.port;

describe("Lesson APIs get: "+url+"/activity/:id/comments",function(){
	it("should return an object",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		})
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		})
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		})
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		})
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		})
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		})
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		})
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		})
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		})
	});
	it("should return object must contain comments key ",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body.result,'comments');
			done();
		})
	});
	it("should return object must contain totalCount key ",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body.result,'totalCount');
			done();
		})
	});
	it("should return object must contain comments key as array",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.isArray(res.body.result.comments);
			done();
		})
	});
	it("should return object must contain totalCount",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.property(res.body.result,'totalCount');
			done();
		})
	});
	it("should return object must contain totalCount isNotObject",function(done){
		request
		.get(url+'/api/activity/'+testData.mongodb._id+'/comments')
		.end(function(err, res){
			assert.isNotObject(res.body.result.totalCount);
			done();
		})
	});
});
describe("Search activity by tags API - GET: " + url + "/api/activity/tag/:tag", function() {
	it("should return an object",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/activity/tag/a')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/activity/tag/asd')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/activity/tag/asd')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/activity/tag/asd')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in activities, called tags",function(done){
		request
		.get(url+'/api/activity/tag/asd')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.property(res.body.result.activities[i],"tags");
			done();
		});
	});
	it("should contain acitvities with a property tags as an array",function(done){
		request
		.get(url+'/api/activity/tag/asd')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.isArray(res.body.result.activities[i].tags);
			done();
		});
	});
	it("should search for activities with correct tags",function(done){
		request
		.get(url+'/api/activity/tag/asd?term=a')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++) {
                assert(res.body.result.activities[i].tags.filter(function(v) {
                    return v.indexOf('as') > -1;
                }).length > 0);
            }
			done();
		});
	});
	it("should obey limit",function(done){
		request
		.get(url+'/api/activity/tag/asd?limit=2')
		.end(function(err, res){
            assert(res.body.result.activities.length <= 2);
			done();
		});
	});
	it("should obey skip",function(done){
		request
		.get(url+'/api/activity/tag/asd?limit=3')
		.end(function(err, res){
            var res1 = res.body.result.activities;
            request
            .get(url+'/api/activity/tag/asd?skip=1&limit=2')
            .end(function(err, res){
                var res2 = res.body.result.activities;
                res1.splice(0, 1);
                assert(res1.length === res2.length);
                for(var i = 0; i < res1.length; i++) {
                    assert(res1[i]._id === res2[i]._id);
                }
                done();
            });
		});
	});
});
describe("Create activity API - POST: " + url + "/api/activity", function() {
	it("should return an object",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in activities, called tags",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.property(res.body.result,"tags");
			done();
		});
	});
	it("should contain acitvities with a property tags as an array",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert.isArray(res.body.result.tags);
			done();
		});
	});
	it("should search for activities with correct tags",function(done){
		request
		.post(url+'/api/activity')
		.send(testData.createActivity)
		.end(function(err, res){
			assert(res.body.result.tags.filter(function(v) {
				return v.indexOf('as') > -1;
			}).length > 0);
			done();
		});
	});
});
describe("Edit activity API - PUT: " + url + "/api/activity", function() {
	it("should return an object",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.put(url+'/api/activity/'+testData.sampleActivity)
		.send({description: testData.sampleDescription})
		.end(function (err, res) {
			assert.equal(res.body.statusCode, 200);
			done();
		});
	});
});
describe("Get all activity API - GET: " + url + "/api/activity", function() {
	it("should return an object",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in results, called totalCount",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.property(res.body.result,"totalCount");
			done();
		});
	});
	it("should contain a property in results, called activities",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.property(res.body.result,"activities");
			done();
		});
	});
	it("should contain a property activities as an array",function(done){
		request
		.get(url+'/api/activity')
		.end(function(err, res){
			assert.isArray(res.body.result.activities);
			done();
		});
	});
	it("should obey limit",function(done){
		request
		.get(url+'/api/activity?limit=2')
		.end(function(err, res){
            assert(res.body.result.activities.length <= 2);
			done();
		});
	});
	it("should obey skip",function(done){
		request
		.get(url+'/api/activity?limit=3')
		.end(function(err, res){
            var res1 = res.body.result.activities;
            request
            .get(url+'/api/activity?skip=1&limit=2')
            .end(function(err, res){
                var res2 = res.body.result.activities;
                res1.splice(0, 1);
                assert(res1.length === res2.length);
                for(var i = 0; i < res1.length; i++) {
                    assert(res1[i]._id === res2[i]._id);
                }
                done();
            });
		});
	});
});
describe("Get an activity API - GET: " + url + "/api/activity/:id", function() {
	it("should return an object",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url+'/api/activity/'+testData.sampleActivity)
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it('should return correct activity', function(done) {
		request
		.get(url + '/api/activity/' + testData.sampleActivity)
		.end(function(err, res) {
			assert.equal(res.body.result._id, testData.sampleActivity);
			done();
		});
	});
});
describe("Get all activity of a user API - GET: " + url + "/api/activity/search/user/:id", function() {
	it("should return an object",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.isObject(res.body);
			done();
		});
	});
	it("should return object not array",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.isNotArray(res.body);
			done();
		});
	});
	it("should return object must contain result key",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.property(res.body,'result');
			done();
		});
	});
	it("should return object must contain message key",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.property(res.body,'message');
			done();
		});
	});
	it("should return object must contain statusCode key",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.property(res.body,'statusCode');
			done();
		});
	});
	it("should return object must contain error key",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.property(res.body,'error');
			done();
		});
	});
	it("should return object must contain error key with value false",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.equal(res.body.error,false);
			done();
		});
	});
	it("should return object must contain statusCode key with value 200",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.equal(res.body.statusCode,200);
			done();
		});
	});
	it("should return object must contain message key with value 'ok' ",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.equal(res.body.message,"ok");
			done();
		});
	});
	it("should contain a property in results, called totalCount",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.property(res.body.result,"totalCount");
			done();
		});
	});
	it("should contain a property in results, called activities",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
            for(var i = 0; i < res.body.result.activities.length; i++)
			    assert.property(res.body.result,"activities");
			done();
		});
	});
	it("should contain a property activities as an array",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser)
		.end(function(err, res){
			assert.isArray(res.body.result.activities);
			done();
		});
	});
	it("should obey limit",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser + '?limit=2')
		.end(function(err, res){
            assert(res.body.result.activities.length <= 2);
			done();
		});
	});
	it("should obey skip",function(done){
		request
		.get(url + '/api/activity/search/user/' + testData.sampleUser + '?limit=3')
		.end(function(err, res){
            var res1 = res.body.result.activities;
            request
			.get(url + '/api/activity/search/user/' + testData.sampleUser + '?limit=2&skip=1')
            .end(function(err, res){
                var res2 = res.body.result.activities;
                res1.splice(0, 1);
                assert(res1.length === res2.length);
                for(var i = 0; i < res1.length; i++) {
                    assert(res1[i]._id === res2[i]._id);
                }
                done();
            });
		});
	});
});