describe('E-LEARNING: createActivityController Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout;
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "lessons": ['abcd']
        }
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [{
                activvity: "abcd"
            }],
            "totalCount": 1
        }
    };
    var postActivity = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "__v": 0,
            "createdAt": "2016-05-30T12:18:18.918Z",
            "updatedAt": "2016-05-30T12:18:18.918Z",
            "user": "574b2fe43189345b473c5978",
            "lesson": "573dac2eefaaa1ca1535336f",
            "type": "image",
            "description": "Dnddddddddddddddddddddddddddddddddddddddddddddddddd",
            "url": "",
            "module": "573a6fe89120137b221e99be",
            "_id": "574c2f8a933c88871bfd7ed8",
            "deleted": false,
            "likes": [],
            "my_community": true
        },
        "error": false
    };
    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "sports",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": true
        }, {
            "_id": "56bdc7163aa2778521d65fd1",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "cricket",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": false
        }],
        "error": false
    };
    /**
    /**{"statusCode":200,"message":"ok","result":{"activities":['abcd']}}
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('createActivityController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout
        });
    }));

    //  it('$on call for callCreateActivityController :: $on call for callCreateActivityController ', inject(function() {
    //
    // }));
    // it('showCommunityList method :: should have a showCommunityList function', inject(function(){
    //     expect(angular.isFunction($scope.showCommunityList)).toBe(true);
    // }));
    //
    it('getAllLessons method :: should have a getAllLessons function', inject(function() {
        expect(angular.isFunction($scope.getAllLessons)).toBe(true);
    }));
    it('getAllLessons method :: should have a getAllLessons function definition', inject(function() {
        console.log("iiiiiiii");
        $scope.module._id = "1"
        $scope.getAllLessons();
        $httpBackend.when('GET', '/api/modules/1/lessons').respond(getLesson);
        //$httpBackend.flush();
    }));
    it('upload method :: should have a upload function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $rootScope.isLoggedIn = false;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        // $scope.activity.fileModel = "abc.jpeg";
        // $scope.activity.fileModel.type = 'image/jpeg';

        $scope.upload(element);
    }));

    it('hidePreview  method :: should have a hidePreview  function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('hidePreview method :: should have a hidePreview function definition', inject(function() {
        //$rootScope.isLoggedIn = ture;
        $scope.hidePreview();
    }));
    it('createActivityItem  method :: should have a createActivityItem  function', inject(function() {
        expect(angular.isFunction($scope.createActivityItem)).toBe(true);
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        //$rootScope.isLoggedIn = ture;
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $rootScope.isLoggedIn = false;
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $scope.activity.description = undefined;
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $scope.activity.description = "hello";
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $scope.uploadLoader = false;
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $rootScope.isLoggedIn = true;
        $scope.uploadLoader = true;
        $scope.createActivityItem();
    }));
    it('createActivityItem method :: should have a createActivityItem function definition', inject(function() {
        $scope.uploadSuccess = true;
        $scope.createActivityItem();
    }));
    it('$scope.postActivity  method :: should have a $scope.postActivity  function', inject(function() {
        expect(angular.isFunction($scope.postActivity)).toBe(true);
    }));
    it('$scope.postActivity  method :: should have a $scope.postActivity  definition', inject(function() {
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
        $scope.postActivity();
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
    }));
    it('$scope.postActivity  method :: should have a $scope.postActivity  definition', inject(function() {
       $scope.activity.location = "abcd.jpeg";
       $scope.myCommunityCheck = true;
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
        $scope.postActivity();
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
    }));
    it('$scope.postActivity  method :: should have a $scope.postActivity  definition', inject(function() {
        $scope.activity.location = "abcd.jpeg";
        $scope.myCommunityCheck = false;
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
        $scope.postActivity();
        $httpBackend.when('POST', '/api/activity').respond(postActivity);
    }));

    it('$emit-"moveToProgressBar" method :: should have a moveToProgressBar  definition', inject(function() {
        $rootScope.$emit("moveToProgressBar", {});
    }));
    it('$emit-"callCreateActivityController" method :: should have a callCreateActivityController  definition', inject(function() {
        var lesson = {};
        lesson.Keytakeaways = "hello";
        var lessonIndex = 0;
        var module = "123456";
        $rootScope.$broadcast("callCreateActivityController", {
            lesson: lesson,
            module: module,
            lessonIndex: lessonIndex
        });
    }));
    it('updateSelection method :: should have a updateSelection  function', inject(function() {
        expect(angular.isFunction($scope.updateSelection)).toBe(true);
    }));
    it('updateSelection method :: should have a updateSelection function definition', inject(function() {
        var mockEvent = {};
        mockEvent = {
            stopPropagation: jasmine.createSpy('stopPropagation')
        };
        $scope.checkbox = {};
        $scope.checkbox.checked = true;
        $scope.updateSelection(mockEvent, 1);
    }));
    it('stopEvent method :: should have a stopEvent  function', inject(function() {
        expect(angular.isFunction($scope.stopEvent)).toBe(true);
    }));
    it('stopEvent method :: should have a stopEvent function definition', inject(function() {
        var mockEvent = {};
        mockEvent = {
            stopPropagation: jasmine.createSpy('stopPropagation')
        };
        $scope.stopEvent(mockEvent);
    }));
    it('openProgressModal method :: should have a openProgressModal  function', inject(function() {
        expect(angular.isFunction($scope.openProgressModal)).toBe(true);
    }));
    it('openProgressModal method :: should have a openProgressModal function definition', inject(function() {
        $scope.openProgressModal();

    }));
    it('onNext method :: should have a onNext  function', inject(function() {
        expect(angular.isFunction($scope.onNext)).toBe(true);
    }));
    it('onNext method :: should have a onNextttt function definition', inject(function($timeout) {

        //GET /api/groups/users?limit=100&skip=0
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $scope.onNext();
        $timeout.flush();
        $httpBackend.when('GET', '/api/modules/1/lessons').respond(getLesson);
        $httpBackend.flush();


    }));
    // it('$scope.postActivity  method :: should have a $scope.postActivity  definition when $scope.uploadLoader = true', inject(function() {
    //     $rootScope.isLoggedIn = true;
    //     $scope.uploadLoader = true;
    //     $scope.postActivity();
    //     $httpBackend.when('POST', '/api/groups/users?limit=100&skip=0').respond(postActivity);
    //     $httpBackend.flush();
    // }));

    // it('should fetch activity in community', function(){
    //     //$httpBackend.when('GET', '/api/activity').respond(comm);
    //     $scope.showCommunityList();
    //     //$httpBackend.flush();
    // });

    // it('should fetch activity in community', function(){
    //     $httpBackend.when('GET', '/api/activity?limit=50&skip=0').respond(comm1);
    //     $scope.showCommunityList();
    //     $httpBackend.flush();
    // });
    // it('viewActivity method :: should have a viewActivity function', inject(function(){
    //     expect(angular.isFunction($scope.viewActivity)).toBe(true);
    // }));
    // it('viewActivity call method :: should have a viewActivity defination', inject(function($rootScope){
    //     $rootScope.isLoggedIn = false;
    //     $scope.viewActivity();
    // }));
    // it('loadMore call method :: should have a loadMore defination', inject(function(){
    //     $scope.loadMore();
    // }));
    // it('callViewActivityLike $on :: should have a callViewActivityLike defination', inject(function(){
    //     $scope.$broadcast('callViewActivityLike',{ id: '56ssss98192d3f379fd13eabdd4' });
    // }));
    // it('viewActivitySpace method :: should have a viewActivitySpace body', function() {
    //     $httpBackend.when('GET', '/api/activity?limit=50&skip=0').respond(comm);
    //     $scope.viewActivity('56ssss98192d3f379fd13eabdd4');
    //     //$httpBackend.when('GET', '/api/activity/56ssss98192d3f379fd13eabdd4/comments').respond({"statusCode":200,"message":"ok","result":{"comments":['abcd'],"totalCount":1}});
    //     $httpBackend.when('GET', 'app/shared/activityDetail/viewSharedActivity.html').respond();
    //     //$httpBackend.flush();
    // });
});
