describe('E-LEARNING: dialogController Controller test', function(){
    var ctrl, $scope, httpBackend;
    var message = 'test';
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, $rootScope,$httpBackend, $controller, $rootScope){
        httpBackend = $httpBackend;
        $rootScope = $rootScope;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
    	$uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('dialogController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            message : message
        });
    }));

    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));

    it('closeModal method :: should have a closeModal body', inject(function() {
        $scope.closeModal();
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
    }));
});
'use strict';
describe('E-LEARNING: confirmController Controller test', function(){
    var ctrl, $scope, $httpBackend , $state,$timeout;
    var message = 'test';
    var item = 'users';
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));

    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, $rootScope,_$timeout_,_$httpBackend_,_$state_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;
        $rootScope = $rootScope;
          $state = _$state_;
          $timeout = _$timeout_;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('confirmController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            message : message,
            item : item
        });
    }));

    it('closeModal method :: should have a closeModal function empty', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));

    it('closeModal method :: should have a closeModal body 1', inject(function() {
        $scope.id = '56dab3598e0f0fa40d98b3c7';
        $scope.userList = [{},{}];$scope.closeModal(1);
        $httpBackend.expect('DELETE','/api/users/56dab3598e0f0fa40d98b3c7').respond({"statusCode":200,"message":"ok"});
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
        $httpBackend.flush();
    }));

    it('closeModal method :: should have a closeModal body 2', inject(function() {
        $scope.id = '56dab3598e0f0fa40d98b3c7';
        $scope.closeModal(2);
        $httpBackend.when('GET', 'app/shared/dialogBox/alertView.html').respond();
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
    }));

    it('closeModal method :: should have a closeModal body 1 user', inject(function() {
        APP.currentUser = APP.currentUser.result;
        $scope.item = 'user';
        $scope.appId = '56dff82e37bca6b31a4168be';
        $scope.closeModal(1);
        $httpBackend.when('DELETE', '/api/users/56dff82e37bca6b31a4168be').respond({"statusCode":201});
        spyOn($state, 'go');
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('closeModal method :: should have a closeModal body', inject(function() {
        $scope.item = 'user';
        $scope.closeModal(1);
        $httpBackend.when('DELETE', '/api/users/56dff82e37bca6b31a4168be').respond({"statusCode":200,"message":"ok","result":{"message":"user deleted"}});
        spyOn($state, 'go');
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
        $httpBackend.flush();
    }));

});
