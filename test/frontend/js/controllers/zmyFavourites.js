'use strict';
describe('E-LEARNING: FavouritesController Controller test', function() {
    var ctrl, $scope, $httpBackend, $timeout;
    var followers = {
        error: false,
        message: "ok",
        result: [{
            _id: "57299f0d782f0092722a4581",
            createdAt: "2016-05-04T07:04:45.859Z"
        }],
        statusCode: 200
    };
    var failFollowers = {
        error: true,
        message: "ok",
        result: [{
            _id: "57299f0d782f0092722a4581",
            createdAt: "2016-05-04T07:04:45.859Z"
        }],
        statusCode: 400
    };
    var loginResult = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "__v": 0,
            "_id": "56dff82e37bca6b31a4168be",
            "createdAt": "2016-03-09T10:17:19.071Z",
            "updatedAt": "2016-03-09T10:17:19.071Z",
            "local": {
                "dob": "01/03/2016",
                "email": "preeti.sachdeva@daffodilsw.com",
                "name": "Preeti Sachdeva",
                "password": "$2a$08$wJfg24K9Q4b6x5xfS2.dnup1Pr5M.u21eLbMWdt3xu0jpgU5HR71a",
                "deleted": false,
                "rev": 0,
                "ambassdor": false,
                "lastlogin_at": "2016-03-09T10:17:18.972Z",
                "token": '12232423454363433'
            }
        },
        "error": false
    };


    /**
     * Load Sixthcontinent module before execute any test
     */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, _$httpBackend_, _$timeout_, $controller, $rootScope) {
        $httpBackend = _$httpBackend_;
        $timeout = _$timeout_;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('myFavouritesController', {
            $scope: $scope,
            $timeout: $timeout
        });
    }));
    it('variables followingsLoader test :: should have a equall value', inject(function() {
        expect($scope.followingsLoader).toEqual(true);
    }));
    it('variables followersLoader test :: should have a equall value', inject(function() {
            expect($scope.followersLoader).toEqual(true);
        }))
        // it('variables  test :: should have a watch function called', inject(function() {
        //     $scope.activityId = '56ssss98192d3f379fd13eabdd4';
        //     $httpBackend.when('GET', '/api/activity/56ssss98192d3f379fd13eabdd4/comments?limit=6&skip=0').respond(getComment);
        //     $httpBackend.flush();
        //
        // }));
    it('following method :: should have a following function', inject(function() {
        expect(angular.isFunction($scope.following)).toBe(true);
    }));
    it('following method :: should have a following function definition', inject(function() {
        $scope.following();
    }));
    it('unfollow method :: should have a unfollow function', inject(function() {
        expect(angular.isFunction($scope.unfollow)).toBe(true);
    }));
    it('unfollow method :: should have a unfollow function definition', inject(function() {
        $scope.unfollow();
    }));
    it('unfollow method :: should have a unfollow function definition', inject(function() {
        $scope.follower = [{
            _id: "57299f0d782f0092722a4581",
            createdAt: "2016-05-04T07:04:45.859Z"
        }];
        $scope.unfollow(0,'57299f0d782f0092722a4581',$scope.follower);
    }));
    it('uploadFiles method :: should have a uploadFiles function', inject(function() {
        expect(angular.isFunction($scope.uploadFiles)).toBe(true);
    }));
    it('uploadFiles method :: should have a uploadFiles function definition when have files', inject(function() {
        var files = [{
            lastModified: "1467020822540",
            lastModifiedDate: "Mon Jun 27 2016",
            type: "video"
        }];
        $scope.uploadFiles(files);
    }));
    it('uploadFiles method :: should have a uploadFiles function definition when no files', inject(function() {
        var files1 = "";
        $scope.uploadFiles(files1);
        //$timeout.flush();
    }));
    it('follow/following service :: should have a follow service function definition', inject(function() {
        $scope.user = {};
        $scope.user.email = 'preeti.sachdeva@daffodilsw.com';
        $scope.user.password = '1234567';
        $httpBackend.when('POST', '/api/login').respond(loginResult);
        $httpBackend.when('GET', '/api/users/followers').respond(followers);
        $httpBackend.when('GET', '/api/users/following').respond(followers);
        $scope.followers();
        $httpBackend.flush();
    }));
    it('follow/following service :: should have a follow service function definition with failure', inject(function() {
        $scope.user = {};
        $scope.user.email = 'preeti.sachdeva@daffodilsw.com';
        $scope.user.password = '1234567';
        $httpBackend.when('POST', '/api/login').respond(loginResult);
        $httpBackend.when('GET', '/api/users/followers').respond(failFollowers);
        $httpBackend.when('GET', '/api/users/following').respond(failFollowers);
        $scope.followers();
        $httpBackend.flush();
    }));
    //$watch testing
    // it('test using $digest', function() {
    //   // make an initial selection
    //   $scope.locationVideo = 'Hi';
    //   $scope.$digest();
    //
    //   // make another one
    //   $scope.locationVideo = 'New';
    //   $httpBackend.when('GET', '/api/users/followers').respond(followers);
    //   $httpBackend.when('GET', '/api/users/following').respond(followers);
    //   $httpBackend.flush();
    //   $scope.$digest();
    //
    //   // simulate a ng-change which should revert to the previous value
    //   // ctrl.changeSelection(true);
    //
    //   expect(scope.locationVideo).toEqual('Hi');
    // });

});
