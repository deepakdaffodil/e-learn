describe('E-LEARNING: editGroupController Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout, $uibModalInstance;
    var id = '56dab3598e0f0fa40d98b3c7';
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "lessons": ['abcd']
        }
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [{
                activvity: "abcd"
            }],
            "totalCount": 1
        }
    };
    var postActivity = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "__v": 0,
            "createdAt": "2016-05-30T12:18:18.918Z",
            "updatedAt": "2016-05-30T12:18:18.918Z",
            "user": "574b2fe43189345b473c5978",
            "lesson": "573dac2eefaaa1ca1535336f",
            "type": "image",
            "description": "Dnddddddddddddddddddddddddddddddddddddddddddddddddd",
            "url": "",
            "module": "573a6fe89120137b221e99be",
            "_id": "574c2f8a933c88871bfd7ed8",
            "deleted": false,
            "likes": [],
            "my_community": true
        },
        "error": false
    };
    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "sports",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": true
        }, {
            "_id": "56bdc7163aa2778521d65fd1",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "cricket",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": false
        }],
        "error": false
    };
    var users = {
            "statusCode": 200,
            "message": "ok",
            "result": [{
                "_id": "570e3044215c168c4eef9dfb",
                "createdAt": "2016-04-13T11:40:52.182Z",
                "updatedAt": "2016-04-13T11:40:52.182Z",
                "__v": 0,
                "local": {
                    "dob": "2/23/2012",
                    "name": "kaberi"
                }
            }],
            "error": false
        };
    /**
    /**{"statusCode":200,"message":"ok","result":{"activities":['abcd']}}
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('editGroupController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout,
            $uibModalInstance: $uibModalInstance,
            moduleId: id
        });
        $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode":200,"message":"ok","result":{"members":[],"totalCount":0},"error":false});
      // $httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
    }));


    it('getGroup method :: should have a getGroup function', inject(function() {
        expect(angular.isFunction($scope.getGroup)).toBe(true);
    }));
    it('getGroup method :: should have a getGroup function definition', inject(function() {
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode":200,"message":"ok","result":{"members":[],"totalCount":0},"error":false});
        $scope.getGroup();
        //$httpBackend.flush();
    }));
    it('getGroup method :: should have a getGroup function definition', inject(function() {
        $scope.group.owner = "12345";
        $scope.location = "abcd.png";
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond(comm1);
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond(comm1);
        $scope.getGroup();
        $httpBackend.flush();
    }));
    it('getGroup method :: should have a getGroup function definition', inject(function() {
        $scope.group.owner = 12345;
        $scope.location = '';
        APP.currentUser._id = 123456;
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond(comm1);
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond(comm1);
        $scope.getGroup();
          $scope.location = undefined;
        $httpBackend.flush();
    }));
    it('getGroup method :: should have a getGroup function definition', inject(function() {
        $scope.group.owner = 12345;
        $scope.location = 'hello.png';
        APP.currentUser._id = 123456;
        $scope.getGroup();
        $httpBackend.flush();
    }));
    it('loadMatchingEmails method :: should have a loadMatchingEmails function', inject(function() {
        expect(angular.isFunction($scope.loadMatchingEmails)).toBe(true);
    }));
    it('loadMatchingEmails method :: should have a loadMatchingEmails function definition', inject(function() {
        var query = "a";
        $scope.loadMatchingEmails(query);
        $httpBackend.when('GET', '/api/users/search?term=a').respond(users);
        $httpBackend.flush();
      }));
    //tagAdded
    it('tagAdded method :: should have a tagAdded function', inject(function() {
        expect(angular.isFunction($scope.tagAdded)).toBe(true);
    }));
    it('tagAdded method :: should have a tagAdded function definition', inject(function() {
        var tag = {};
        tag.text = "mindmax";
        $scope.tagAdded(tag);
    }));
    //tagRemoved
    it('tagRemoved method :: should have a tagRemoved function', inject(function() {
        expect(angular.isFunction($scope.tagRemoved)).toBe(true);
    }));
    it('tagRemoved method :: should have a tagRemoved function definition', inject(function() {
        $scope.tagRemoved();
    }));
    //closeModal
    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));
    it('closeModal method :: should have a closeModal function definition', inject(function() {
        $scope.closeModal();
    }));
    //editAvatar
    it('editAvatar method :: should have a editAvatar function', inject(function() {
        expect(angular.isFunction($scope.editAvatar)).toBe(true);
    }));
    it('editAvatar method :: should have a editAvatar function definition', inject(function() {
      var mockEvent = {};
      mockEvent = {
          stopPropagation: jasmine.createSpy('stopPropagation')
      };
        $scope.editAvatar(mockEvent);
    }));
    //upload
    it('upload method :: should have a upload function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $rootScope.isLoggedIn = false;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
      $scope.fileModel = '';
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/abcd"
            }]
        };
        $scope.fileModel = '';
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition failure', inject(function() {
        var element = {
            "files": [{
                "type": "image/abcd"
            }]
        };
        $scope.fileModel = '';
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(400);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    //hidePreview
    it('hidePreview method :: should have a hidePreview function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('hidePreview method :: should have a hidePreview function definition', inject(function() {
      $scope.group.avatar_url = '';
        $scope.hidePreview();
    }));
    it('hidePreview method :: should have a hidePreview function definition', inject(function() {
      $scope.group.avatar_url = "abbcd.png";
        $scope.hidePreview();
    }));
    //openEditForm
    it('openEditForm method :: should have a openEditForm function', inject(function() {
        expect(angular.isFunction($scope.openEditForm)).toBe(true);
    }));
    it('openEditForm method :: should have a openEditForm function definition', inject(function() {
        $scope.openEditForm(1);
    }));
    it('openEditForm method :: should have a openEditForm function definition', inject(function() {
        $scope.openEditForm(2);
    }));
    it('openEditForm method :: should have a openEditForm function definition', inject(function() {
        $scope.openEditForm(3);
    }));
    it('openEditForm method :: should have a openEditForm function definition', inject(function() {
        $scope.openEditForm(4);
    }));
    it('openEditForm method :: should have a openEditForm function definition', inject(function() {
      $scope.editUser = {};
      $scope.editUser.password = "";
        $scope.openEditForm(5);
    }));
    //changeImage
    it('changeImage  method :: should have a changeImage function', inject(function() {
        expect(angular.isFunction($scope.changeImage)).toBe(true);
    }));
    it('changeImage  method :: should have a changeImage function', inject(function() {
        $scope.location = '';
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});

        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/avatar').respond({
            "statusCode": 200,
            "message": "ok",
            "result": {
                "ok": 1,
                "nModified": 1,
                "n": 1
            },
            "error": false
        });
        $scope.changeImage();
        $timeout.flush();
        $httpBackend.flush();

    }));
    it('changeImage  method :: should have a changeImage function', inject(function() {
        $scope.location = "abcd.jpeg"
      //  $httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/avatar').respond({"statusCode": 200, "message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeImage();
        $timeout.flush();
        $httpBackend.flush();
    }));
    //changeName
    it('changeName  method :: should have a changeName function', inject(function() {
        expect(angular.isFunction($scope.changeName)).toBe(true);
    }));
    it('changeName  method :: should have a changeName function', inject(function() {
        $scope.group.name='';
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/name').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeName();
        //$timeout.flush();
        $httpBackend.flush();

    }));
    it('changeName  method :: should have a changeName function', inject(function() {
        $scope.group.name = "gunjan";

        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/name').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeName();
        $httpBackend.flush();
    }));
    it('changeName  method :: should have a changeName function error', inject(function() {
        $scope.group.name = "gunjan";

        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/name').respond({"statusCode": 400,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeName();
        $httpBackend.flush();
    }));
    //changeDes
    it('changeDes  method :: should have a changeDes function', inject(function() {
        expect(angular.isFunction($scope.changeDes)).toBe(true);
    }));
    it('changeDes  method :: should have a changeDes function', inject(function() {
      $scope.group.description = "";
        $scope.changeDes();
        $timeout.flush();
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/description').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $httpBackend.flush();

    }));
    it('changeDes  method :: should have a changeDes function', inject(function() {
        $scope.group.description= "Mindmax";
        $scope.changeDes();
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/description').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $httpBackend.flush();
    }));
    it('changeDes  method :: should have a changeDes function api error', inject(function() {
        $scope.group.description= "Mindmax";
        $scope.changeDes();
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/description').respond({"statusCode": 400,"message": "","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $httpBackend.flush();
    }));
  //changeMembers

    it('changeMembers  method :: should have a changeMembers function', inject(function() {
        expect(angular.isFunction($scope.changeMembers)).toBe(true);
    }));
    it('changeMembers  method :: should have a changeMembers function', inject(function() {
    $scope.list = "";
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeMembers();
        //$timeout.flush();
        $httpBackend.flush();

    }));
    it('changeMembers  method :: should have a changeMembers function', inject(function() {
        $scope.list=[1234,12345];
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode": 200,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeMembers();
        $httpBackend.flush();
    }));
    it('changeMembers  method :: should have a changeMembers function api error', inject(function() {
        $scope.list=[1234,12345];
        //$httpBackend.when('GET', ' /api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        $httpBackend.when('PUT', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode": 400,"message": "ok","result": {"ok": 1,"nModified": 1,"n": 1},"error": false});
        $scope.changeMembers();
        $httpBackend.flush();
    }));
    //cancelSave
    it('cancelSave method :: should have a cancelSave function', inject(function() {
        expect(angular.isFunction($scope.cancelSave)).toBe(true);
    }));
    it('cancelSave method :: should have a cancelSave function definition', inject(function() {
        $scope.cancelSave(1);
    }));
    it('cancelSave method :: should have a cancelSave function definition', inject(function() {
        $scope.cancelSave(2);
    }));
    it('cancelSave method :: should have a cancelSave function definition', inject(function() {
        $scope.cancelSave(3);
    }));
    it('cancelSave method :: should have a cancelSave function definition', inject(function() {
        $scope.cancelSave(4);
    }));
    it('cancelSave method :: should have a cancelSave function definition', inject(function() {
        $scope.cancelSave(5);
    }));
 //membersForm
 it('membersForm method :: should have a membersForm function', inject(function() {
     expect(angular.isFunction($scope.membersForm)).toBe(true);
 }));
 it('membersForm method :: should have a membersForm function definition', inject(function() {
     $scope.membersForm(1);
 }));
 //detailsForm
 it('detailsForm method :: should have a detailsForm function', inject(function() {
     expect(angular.isFunction($scope.detailsForm)).toBe(true);
 }));
 it('detailsForm method :: should have a detailsForm function definition', inject(function() {
     $scope.detailsForm(1);
 }));
 //confirmModal
 it('confirmModal method :: should have a confirmModal function', inject(function() {
     expect(angular.isFunction($scope.confirmModal)).toBe(true);
 }));
 it('confirmModal method :: should have a confirmModal function definition', inject(function() {
     $scope.confirmModal(1);
 }));
 //uploadGroupAvatar
 it('uploadGroupAvatar rootscope method :: should have a uploadGroupAvatar rootscope function', inject(function() {
     $rootScope.$emit("uploadGroupAvatar", {
         avatar_url: "abcd.jpg"
     });
 }));

});
