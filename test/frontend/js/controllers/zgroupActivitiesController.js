describe('E-LEARNING: groupActivities Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout, $uibModalInstance;
    var moduleObj = {};
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "lessons": ['abcd']
        }
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [{
                activvity: "abcd"
            }],
            "totalCount": 1
        }
    };
    var lessons = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce"
        }],
        "error": false
    };
    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [
              {
                "_id": "56bdc6e93aa2778521d65fd0",
                "module": "56bdc69a3aa2778521d65fce",
                "name": "sports",
                "basic_description": "lesson  two basic_description",
                "detailed_description": "lesson two detailed_description",
                "__v": 0,
                "deleted": false,
                "completed": true
            },
            {
                "_id": "56bdc7163aa2778521d65fd1",
                "module": "56bdc69a3aa2778521d65fce",
                "name": "cricket",
                "basic_description": "lesson  two basic_description",
                "detailed_description": "lesson two detailed_description",
                "__v": 0,
                "deleted": false,
                "completed": false
            }],
            "totalCount" : 2
        },
        "error": false
    };
    var getNoLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [],
            "totalCount" : 0
        },
        "error": false
    };

    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));

    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('groupActivitiesController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout,
            $uibModalInstance: $uibModalInstance

        });
    }));

    it('getGroupActivities method :: should have a getGroupActivities function', inject(function() {
        expect(angular.isFunction($scope.getGroupActivities)).toBe(true);
    }));
    it('getGroupActivities method :: should have a getGroupActivities function definition', inject(function() {
        $scope.getGroupActivities();
        $httpBackend.when('GET', '/api/groups//activities').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $httpBackend.flush();
    }));
    it('getGroupActivities method :: should have a getGroupActivities function definition', inject(function() {
        $scope.getGroupActivities();
        $httpBackend.when('GET', '/api/groups//activities').respond(getNoLesson);
        $rootScope.isLoggedIn = true;
        $httpBackend.flush();
    }));


    it('groupActivitiesPageId rootscope method :: should have a groupActivitiesPageId rootscope function', inject(function() {
        $rootScope.$broadcast("groupActivitiesPageId", {
            module: moduleObj
        });
    }));

});
