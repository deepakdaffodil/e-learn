'use strict';
describe('E-LEARNING: manageModuleController Controller test', function(){
    var ctrl, $scope, $httpBackend, state, $uibModalInstance, $timeout;
    var comm = {"statusCode":200,"message":"ok","result":[{"createdAt":"2016-03-23T06:40:02.496Z"}]};
    var comm1 = {"statusCode":422,"message":{"message":"Module name must be unique"}};
    var id= '56dab3598e0f0fa40d98b3c7';
    /**
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector,$state, _$httpBackend_, _$timeout_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('manageModuleController', {
            $scope: $scope,
            state : $state,
            $uibModalInstance: $uibModalInstance,
            moduleId :id,
            $timeout : $timeout
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.resultMsg).toEqual(false);
        expect($scope.loader).toEqual(false);
    }));
    it('getTotalLesson method :: should have a getTotalLesson function', inject(function() {
        expect(angular.isFunction($scope.getTotalLesson)).toBe(true);
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        expect(angular.isFunction($scope.createModule)).toBe(true);
    }));
    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));
    it('editForm method :: should have a editForm function', inject(function() {
        expect(angular.isFunction($scope.editForm)).toBe(true);
    }));
    it('editForm method :: should have a editForm function', inject(function() {
        $scope.editForm();
        //$httpBackend.when('GET', '/api/modules').respond({"statusCode":200,"message":"ok"});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({"statusCode":200,"message":"ok"});
        //$httpBackend.when("GET","/api/modules/56dab3598e0f0fa40d98b3c7").respond({"statusCode":200,"message":"ok"});
        //$httpBackend.flush();
    }));
    it('createModuleForm method :: should have a createModuleForm function', inject(function() {
        expect(angular.isFunction($scope.createModuleForm)).toBe(true);
    }));
    it('editModuleForm method :: should have a editModuleForm function', inject(function() {
        expect(angular.isFunction($scope.editModuleForm)).toBe(true);
    }));
    it('createLesson method :: should have a createLesson function', inject(function() {
        expect(angular.isFunction($scope.createLesson)).toBe(true);
    }));
    it('updateModule method :: should have a updateModule function', inject(function() {
        expect(angular.isFunction($scope.updateModule)).toBe(true);
    }));
    it('createLesson method :: should have a createLesson function', inject(function() {
        $scope.createLesson();
    }));
    it('closeModal method :: should have a closeModal function', inject(function() {
        $scope.closeModal();
    }));
    it('createModuleForm method :: should have a createModuleForm body', inject(function() {
        $scope.createModuleForm();
    }));
     it('editModuleForm method :: should have a editModuleForm body', inject(function() {
        $scope.editModuleForm();
    }));

    it('createModule method :: should have a createModule function', inject(function() {
       $scope.createModule();
    }));

    it('createModule method :: should have a createModule function', inject(function() {
       $scope.module.name = '';
       $scope.createModule();
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        $scope.module.name = 'test';
        $scope.module.description = '';
       $scope.createModule();
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        $scope.module.name = 'test';
        $scope.module.description = 'test description';
       $scope.createModule();
    }));
    it('updateModule method :: should have a updateModule function with blank name', inject(function() {
       $scope.editFormData = {};
       $scope.editFormData.name ='';
       $scope.updateModule();
    }));
    it('updateModule method :: should have a updateModule function with blank description', inject(function() {
       $scope.editFormData = {};
       $scope.editFormData.name ='test';
       $scope.editFormData.description ='';
       $scope.updateModule();
    }));
    it('updateModule method :: should have a updateModule function with api call', inject(function() {
        $scope.editFormData = {};
        $scope.editFormData.name ='test';
        $scope.editFormData.description ='test';
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({});
        $scope.updateModule();
        //$httpBackend.when('PUT', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({"statusCode":200,"message":"ok"});
        //$httpBackend.flush();
        //$timeout.flush();
    }));
    it('updateModule method :: should have a updateModule function with api call', inject(function() {
        $scope.editFormData = {};
        $scope.editFormData.name ='test';
        $scope.editFormData.description ='test';
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({});
        $scope.updateModule();
        //$httpBackend.when('PUT', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({"statusCode":201});
        //$httpBackend.flush();
        //$timeout.flush();
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        $scope.module.name = 'test';
        $scope.module.description = 'test description';
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({});
        $scope.createModule();
        //$httpBackend.when('POST', '/api/modules').respond({"statusCode":200,"message":"ok"});
        //$httpBackend.flush();
        //$timeout.flush();
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        $scope.module.name = 'test';
        $scope.module.description = 'test description';
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({});
        $scope.createModule();
        //$httpBackend.when('POST', '/api/modules').respond(comm1);
        //$httpBackend.flush();
        //$timeout.flush();
    }));
    it('createModule method :: should have a createModule function', inject(function() {
        $scope.module.name = 'test';
        $scope.module.description = 'test description';
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7').respond({});
        //$httpBackend.when('GET', '/api/modules/56dab3598e0f0fa40d98b3c7/lessons').respond({});
        $scope.createModule();
        //$httpBackend.when('POST', '/api/modules').respond({"statusCode":201});
        //$httpBackend.flush();
        //$timeout.flush();
    }));
});
