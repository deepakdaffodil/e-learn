'use strict';
describe('E-LEARNING: lessonModuleController Controller test', function(){
    var ctrl, $scope, $httpBackend, state, $rootScope ;
    var comm = {"statusCode":200,"message":"ok","result":[{"createdAt":"2016-03-23T06:40:02.496Z"}]};
    var comm1 = {"statusCode":200,"message":"ok","result":[]};
    /**
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector,$state, _$httpBackend_, $controller,  _$rootScope_){
        $httpBackend = _$httpBackend_;
          $rootScope = _$rootScope_;
        state = $state;

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('lessonModuleController', {
          $rootScope: $rootScope,
            $scope: $scope,
            state : $state
        });
    }));
    it('getModuleList method :: should have a getModuleList function', inject(function() {
        expect(angular.isFunction($scope.getModuleList)).toBe(true);
    }));
    it('searchUser method :: should have a searchModule function', inject(function() {
        expect(angular.isFunction($scope.searchModule)).toBe(true);
    }));
    it('searchUser method :: should have a searchModule function', inject(function() {
        expect(angular.isFunction($scope.sortMyModules)).toBe(true);
    }));
    it('searchUser method :: should have a searchModule function defination', inject(function() {
        $scope.sortModules = true;
        $scope.showAllModuleList = comm.result;
        $httpBackend.when('GET',"/api/modules?limit=30&skip=0").respond(comm1);
        $httpBackend.when('GET', '/api/modules?name=a').respond(comm1);

    }));
    it('searchUser method :: should have a deleteLesson function', inject(function() {
        expect(angular.isFunction($scope.deleteLesson)).toBe(true);
    }));
    it('searchUser method :: should have a deleteLesson function defination', inject(function() {
        $scope.deleteLesson(1,0)
    }));
    it('searchUser method :: should have a editLesson function', inject(function() {
        expect(angular.isFunction($scope.editLesson)).toBe(true);
    }));
    it('searchUser method :: should have a deleteLesson function defination', inject(function() {
        $scope.editLesson(1)
    }));
    it('searchUser method :: should have a searchUser function defination', inject(function() {
        $scope.search ='p';
        /*$httpBackend.when('GET', '/modules').respond({});
        $scope.searchUser();*/
        $httpBackend.when('GET',"/api/modules?limit=30&skip=0").respond({"statusCode":200,"message":"ok","result":{"totalcount":0}});
        $httpBackend.when('GET', '/api/modules?name=a').respond({"statusCode":200,"message":"ok","result":{"totalcount":0}  });
        $httpBackend.flush();
    }));
    it('showLesson method :: should have a showLesson function', inject(function() {
        expect(angular.isFunction($scope.showLesson)).toBe(true);
    }));
    it('should fetch modules in modules api', function(){
        $httpBackend.when('GET', '/api/modules?limit=30&skip=0').respond({});
        $scope.getModuleList();
        $httpBackend.flush();
    });
    it('should call hideLesson method', function(){
        $httpBackend.when('GET', '/api/modules?limit=30&skip=0').respond({});
        $scope.hideLesson(0);
        $httpBackend.flush();
    });
    it('should fetch lesson for module with result', function(){
        $httpBackend.when('GET', '/api/modules?limit=30&skip=0').respond({});
        $httpBackend.when('GET','/api/modules/56b5e059b7d52a8145c13824/lessons').respond({"statusCode":200,"message":"ok","result":[{}]})
        $scope.showLesson(1,'56b5e059b7d52a8145c13824');
        $httpBackend.flush();
    });
    it('should fetch lesson for module with blank result', function(){
        $httpBackend.when('GET', '/api/modules?limit=30&skip=0').respond({});
        $httpBackend.when('GET','/api/modules/56b5e059b7d52a8145c13824/lessons').respond({"statusCode":200,"message":"ok","result":[]})
        $scope.showLesson(1,'56b5e059b7d52a8145c13824');
        $httpBackend.flush();
    });
    // it('should call deleteModule', function(){
    //     $httpBackend.when('GET', '/api/modules?limit=12&skip=0').respond({"statusCode":200,"message":"ok","result":[{"_id":1},{"_id":1}]});
    //
    //     // $httpBackend.when('GET', '/modules').respond({});
    //     $httpBackend.when('DELETE','/api/modules/56b5e059b7d52a8145c13824').respond({"statusCode":200,"message":"ok"})
    //
    //     $scope.deleteModule('56b5e059b7d52a8145c13824', 1);
    //
    //     $httpBackend.flush();
    // });
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.showCreateForm).toEqual(false);
    }));
    it('broadcast  test :: should have a broadcast call', inject(function() {
        $scope.$broadcast('CallModulesMethod');
    }));
    it('createModule  test :: should have a createModule call', inject(function() {
        $scope.createModule();
    }));
    it('editModule  test :: should have a editModule call', inject(function() {
        $scope.editModule();
    }));
    it('showLesson method :: should have a showLesson function', inject(function() {
        expect(angular.isFunction($scope.showLesson)).toBe(true);
    }));
    it('showLesson method :: should have a showLesson body', inject(function() {
       $scope.showLesson();
    }));
    it('hideLesson method :: should have a hideLesson function', inject(function() {
        expect(angular.isFunction($scope.hideLesson)).toBe(true);
    }));
    it('hideLesson method :: should have a hideLesson body', inject(function() {
       $scope.hideLesson();
    }));
    it('loadMore method :: should have a loadMore function', inject(function() {
        expect(angular.isFunction($scope.loadMore)).toBe(true);
    }));
    it('loadMore method :: should have a loadMore function defination', inject(function() {
      $httpBackend.when('GET', '/api/modules?limit=30&skip=0').respond({});
        $scope.getModuleList();
    }));
    it('$rootScope.$emit method :: should have a rootscope.CallModulesMethod function', inject(function() {
      $rootScope.$emit("CallModulesMethod", {});
    }));
    /*it('variables  test :: should have a equal value', inject(function() {
        expect($scope.allTotal).toEqual('0');
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.listResponse).toEqual('1');
    }));*/


});
