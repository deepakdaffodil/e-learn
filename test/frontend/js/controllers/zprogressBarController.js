describe('E-LEARNING: progressBar Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout, $uibModalInstance;
    var id = '56dab3598e0f0fa40d98b3c7';
    var max = 10;
    var current = 2;
    var lesson = {};
    var lessonIndex = 3;
    var moduleObj = {};
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "lessons": ['abcd']
        }
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [{
                activvity: "abcd"
            }],
            "totalCount": 1
        }
    };
    var lessons = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce"
        }],
        "error": false
    };
    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "sports",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": true
        }, {
            "_id": "56bdc7163aa2778521d65fd1",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "cricket",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": false
        }],
        "error": false
    };
    /**
    /**{"statusCode":200,"message":"ok","result":{"activities":['abcd']}}
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('progressBarController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout,
            $uibModalInstance: $uibModalInstance,
            max: max,
            current: current,
            lesson: lesson,
            lessonIndex: lessonIndex,
            module: moduleObj
        });
    }));

    it('getAllLessons method :: should have a getAllLessons function', inject(function() {
        expect(angular.isFunction($scope.getAllLessons)).toBe(true);
    }));
    it('getAllLessons method :: should have a getAllLessons function definition', inject(function() {
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode":200,"message":"ok","result":{"members":[],"totalCount":0},"error":false});
        $httpBackend.when('GET', '/api/modules//lessons').respond(lessons);
        $scope.getAllLessons();
        $timeout.flush();



        $httpBackend.flush();
    }));
    it('getAllLessons method :: should have a getAllLessons function definition when any not completed', inject(function() {
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond({  "statusCode": 200, "message": "ok",  "result": [{"_id": "570f34ab9f118e485ff46638",}, {"_id": "570f349c9f118e485ff46636",}],"error": false});
        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond({"statusCode":200,"message":"ok","result":{"members":[],"totalCount":0},"error":false});
        $httpBackend.when('GET', '/api/modules//lessons').respond(getLesson);
        $scope.getAllLessons();
        $timeout.flush();



        $httpBackend.flush();
    }));
    //    it('getGroup method :: should have a getGroup function definition', inject(function() {
    //        $scope.group.owner = "12345";
    //        $scope.location = "abcd.png";
    //        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7').respond(comm1);
    //        // $httpBackend.when('GET', '/api/groups/56dab3598e0f0fa40d98b3c7/members').respond(comm1);
    //        $scope.getGroup();
    //        $httpBackend.flush();
    //    }));
    //    it('loadMatchingEmails method :: should have a loadMatchingEmails function', inject(function() {
    //        expect(angular.isFunction($scope.loadMatchingEmails)).toBe(true);
    //    }));
    //    it('loadMatchingEmails method :: should have a loadMatchingEmails function definition', inject(function() {
    //        var query = "a";
    //        $scope.loadMatchingEmails();
    //        $httpBackend.when('GET', '/api/users/search?term=a').respond({
    //    "statusCode":200,
    //    "message":"ok",
    //    "result":["abc@gmail.com","gunjan@gmail.com"],
    //    "error":false
    //    });
    //
    //    }));
    //    //tagAdded
    //    it('tagAdded method :: should have a tagAdded function', inject(function() {
    //        expect(angular.isFunction($scope.tagAdded)).toBe(true);
    //    }));
    //    it('tagAdded method :: should have a tagAdded function definition', inject(function() {
    //        var tag = {};
    //        tag.text = "mindmax";
    //        $scope.tagAdded(tag);
    //    }));
    //    //tagRemoved
    //    it('tagRemoved method :: should have a tagRemoved function', inject(function() {
    //        expect(angular.isFunction($scope.tagRemoved)).toBe(true);
    //    }));
    //    it('tagRemoved method :: should have a tagRemoved function definition', inject(function() {
    //        $scope.tagRemoved();
    //    }));
    //closeModal
    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));
    it('closeModal method :: should have a closeModal function definition', inject(function() {
        $scope.closeModal();
    }));
    //onProgressNext
    it('onProgressNext method :: should have a onProgressNext function', inject(function() {
        expect(angular.isFunction($scope.onProgressNext)).toBe(true);
    }));
    it('onProgressNext method :: should have a onProgressNext function definition', inject(function() {
        $scope.onProgressNext();
    }));
    it('onProgressNext method :: should have a onProgressNext function definition when max==current', inject(function() {
        $scope.max = 5;
        $scope.current = 5;
        $scope.onProgressNext();
    }));
    //closingModal
    it('closingModal rootscope method :: should have a closingModal rootscope function', inject(function() {
        $rootScope.$broadcast("closingModal", {});
    }));

});
