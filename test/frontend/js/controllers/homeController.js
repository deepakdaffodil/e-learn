'use strict';
describe('E-LEARNING: homeController Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope;
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "createdAt": "2016-03-23T06:40:02.496Z"
        }]
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": []
    };
    /**
     * Load Sixthcontinent module before execute any test
     */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_) {
        $httpBackend = _$httpBackend_;
        state = $state;

        //create a new scope that's a child scope of $rootscope
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('homeController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.oneAtATime).toEqual(true);
        expect($scope.showSpace).toEqual(false);
        expect($scope.showGroup).toEqual(false);
        expect($scope.showCommunity).toEqual(true);
    }));
    it('should fetch activity in community', function() {
        //$httpBackend.when('GET', '/api/activity').respond(comm);
        $scope.showMycommunity();
        $rootScope.$on('updateMyCommunity', function(event, args) {
            $scope.getAllActivities(args);
        });
        //$httpBackend.flush();
    });
    it('showMycommunity method :: should have a showMycommunity function', inject(function() {
        expect(angular.isFunction($scope.showMycommunity)).toBe(true);
    }));
    it('showMyGroups method :: should have a showMyGroups function', inject(function() {
        expect(angular.isFunction($scope.showMyGroups)).toBe(true);
    }));
    it('showMyspace method :: should have a showMyspace function', inject(function() {
        expect(angular.isFunction($scope.showMyspace)).toBe(true);
    }));


    it('showSpace method :: should have a showMyspace function', inject(function() {
        $scope.showMyspace();
        expect($scope.showGroups).toEqual(false);
        expect($scope.showCommunity).toEqual(false);
        expect($scope.showSpace).toEqual(true);
    }));
    it('showMycommunity method params :: should have a showMycommunity function', inject(function() {
        $scope.showMycommunity();
        expect($scope.showGroups).toEqual(false);
        expect($scope.showCommunity).toEqual(true);
        expect($scope.showSpace).toEqual(false);
    }));

    it('showMyGroups method :: should have a showMyGroups function', inject(function() {
        $scope.showMyGroups();
        expect($scope.showGroups).toEqual(true);
        expect($scope.showCommunity).toEqual(false);
        expect($scope.showSpace).toEqual(false);
    }));
    it('showMyspace method :: should have a viewActivity function', inject(function() {
        expect(angular.isFunction($scope.viewActivity)).toBe(true);
    }));
    it('showMyspace method :: should have a viewActivity function definition', inject(function() {
        $scope.comments = 0;
        $rootScope.isLoggedIn = true;
        $scope.$emit("ShowModalLogin", {});
    }));
    it('showMyspace method :: should have a Rootscope.groupActivitiesPage', inject(function() {
        $rootScope.$emit("groupActivitiesPage", {
            grpId: '1'
        });
    }));

    it('moduleClicked method :: should have a moduleClicked function', inject(function() {
        expect(angular.isFunction($scope.moduleClicked)).toBe(true);
    }));
    it('moduleClicked method :: should have a moduleClicked function definition', inject(function() {
        $scope.moduleClicked(0, 1);
        $rootScope.$on("callModuleController", function(event, args) {
            $scope.showModule = args.module;
            $scope.getAllLessons();

        });
    }));
    it('callHomeConLastActiviyComponent method :: should have a callHomeConLastActiviyComponent function definition', inject(function() {
        $rootScope.$emit("callHomeConLastActiviyComponent", {
            module: "abc"
        });

    }));
    it('callHomeControllerForActivity method :: should have a callHomeControllerForActivity function definition', inject(function() {
        $rootScope.$emit("callHomeControllerForActivity", {
            activity: true,
            lesson: "abc",
            index: 0
        });

    }));
    it('getModules method :: should have a getModules function', inject(function() {
        expect(angular.isFunction($scope.getModules)).toBe(true);
    }));
    it('getModules method :: should have a getModules function definition', inject(function() {
      $scope.totalModules = 5;
      $scope.modules = [];
        $scope.getModules();
        $httpBackend.when('GET', "/api/modules?limit=60&skip=0").respond(comm1);
        $httpBackend.flush();
    }));

});
