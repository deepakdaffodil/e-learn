'use strict';
describe('E-LEARNING: ModalController Controller test', function(){
    var ctrl, $scope, $httpBackend , $timeout,state, 	$uibModalInstance;
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));

    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector,$state, $rootScope,_$httpBackend_,_$timeout_,$controller){
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $rootScope = $rootScope;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
    	$uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('myspaceController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            state : $state
        });
    }));

    it('variables  test :: should have a equall value', inject(function() {
       expect($scope.spaceloader).toEqual(false);
    }));
    it('fucntion get activity in my space :: should have a activity in my space', inject(function() {
        $httpBackend.when('GET', '/api/activity/users').respond({"statusCode":200,"message":"ok","result":{"activities": []}});
        // $httpBackend.flush();
    }));

    it('CallUpdatedSpaceMethod method :: should have a CallUpdatedSpaceMethod $on event', inject(function($rootScope) {
        $rootScope.$broadcast('CallUpdatedSpaceMethod');
    }));
    it('viewActivitySpace method :: should have a viewActivitySpace function', inject(function() {
        expect(angular.isFunction($scope.viewActivitySpace)).toBe(true);
    }));
    it('deleteMyActivity method :: should have a confirmModal function', inject(function() {
        expect(angular.isFunction($scope.deleteMyActivity)).toBe(true);
    }));
    it('deleteMyActivity method :: should have a body', inject(function() {
        $scope.deleteMyActivity({}, {}, {}, {stopPropagation: function () {}});
    }));
    it('viewActivitySpace method :: should have a viewActivitySpace body', function() {
        //$httpBackend.when('GET', '/api/activity/users').respond({"statusCode":200,"message":"ok","result":{"activities": []}});
        $scope.viewActivitySpace('56ssss98192d3f379fd13eabdd4');
        //$httpBackend.when('GET', '/api/activity/56ssss98192d3f379fd13eabdd4').respond({"statusCode":200,"message":"ok","result":{"likes":[APP.currentUser._id]}});
        $httpBackend.when('GET', 'app/shared/activityDetail/viewSharedActivity.html').respond();
        //$httpBackend.flush();
    });
    it('viewActivitySpace method :: should have a viewActivitySpace body functionality', function() {
        $httpBackend.when('GET', '/api/activity/users').respond({"statusCode":200,"message":"ok","result":{"activities": []}});
        $scope.viewActivitySpace('56ssss98192d3f379fd13eabdd4');
        $httpBackend.when('GET', '/api/activity/56ssss98192d3f379fd13eabdd4').respond({"statusCode":200,"message":"ok","result":{"likes":['rr34344']}});
        $httpBackend.when('GET', 'app/shared/activityDetail/viewSharedActivity.html').respond();
        // $httpBackend.flush();
    });
    // it('deleteMyActivity method :: should have a deleteMyActivity body', inject(function() {
    //     $httpBackend.when('GET', '/api/activity/users/'+ APP.currentUser._id).respond({"statusCode":200,"message":"ok","result":{"activities": []}});
    //     var e = jasmine.createSpyObj('e', [ 'stopPropagation' ]);
    //     $scope.deleteMyActivity(e,'56ssss98192d3f379fd13eabdd4','');
    //     $httpBackend.when('DELETE', '/activity/56ssss98192d3f379fd13eabdd4').respond({"statusCode":200,"message":"ok"});
    //     $httpBackend.flush();
    // }));
    // it('deleteMyActivity method :: should have a deleteMyActivity body', inject(function() {
    //     $httpBackend.when('GET', '/api/activity/users/'+ APP.currentUser._id).respond({"statusCode":200,"message":"ok","result":{"activities": []}});
    //     var e = jasmine.createSpyObj('e', [ 'stopPropagation' ]);
    //     $scope.deleteMyActivity(e,'56ssss98192d3f379fd13eabdd4','pic.com');
    //     $httpBackend.when('DELETE', '/api/activity/56ssss98192d3f379fd13eabdd4').respond({"statusCode":200,"message":"ok"});
    //     $httpBackend.flush();
    // }));
    it('showSpaceList is working', inject( function () {
        $scope.allTotal = 0;
        $scope.mySpaceList = ['1', '2'];
        $scope.lastLength = 1;
        $scope.showSpaceList();
    }));
    it('updateLikes working', inject( function ($rootScope) {
        $rootScope.$broadcast('updatingLikes', {});
    }));
    it('ago working', inject( function () {
        $scope.ago(['asd', '123']);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(0);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24 * 12);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24 * 30);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24 * 30 * 12);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24 * 30 * 6);
    }));
    it('getActualTime working', inject( function () {
        $scope.getActualTime(1000 * 60 * 60 * 24 * 30 * 46);
    }));
    it('loadMoreSpace working', inject( function () {
        $scope.loadMoreSpace();
    }));
    it('loadMoreSpace working', inject( function ($rootScope) {
        $rootScope.activeSection = '';
        $scope.loadMoreSpace();
    }));
});
