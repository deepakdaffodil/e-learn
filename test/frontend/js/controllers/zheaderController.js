'use strict';
describe('E-LEARNING: headerController Controller test', function() {
    var ctrl, $scope, $httpBackend, modalInstance, $rootScope;
    
    var noti_result = {
        "statusCode": 200,
        "message": "ok",
        "result": [{}],
        "error": false
    };
    var noti_result_fail = {
        "statusCode": 201,
        "message": ""
    };
    var user = {
        "local": {
            "avatar_url": "abcd.png",
            "name": "gunjan",
        }
    };
    /**
     * Load Sixthcontinent module before execute any test
     */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, _$httpBackend_, $controller, _$rootScope_, $uibModal) {
        $httpBackend = _$httpBackend_;

        //create a new scope that's a child scope of $rootscope
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        //$modalInstance: modalInstance;
        //create contorller
        ctrl = $controller('headerController', {
            $rootScope: $rootScope,
            $scope: $scope,
            $uibModal: $uibModal
        });
    }));
    // $scope.bodyClasses = 'default';
    it('variables  test :: should have a equall value', inject(function() {
        expect($scope.animationsEnabled).toEqual(true);
        $rootScope.$broadcast('updateHeaderDp', {
            avatar_url: "www.emedinexus.com"
        });
    }));
    it('showRegisterForm method :: should have a showRegisterForm function', inject(function() {
        expect(angular.isFunction($scope.showRegisterForm)).toBe(true);
    }));
    it('showRegisterForm method body :: should have a showRegisterForm function', inject(function() {
        $scope.showRegisterForm();
        //  expect($scope.selected).toEqual('56dff82e37bca6b31a4168be');
        expect($scope.showRegForm).toEqual(true);
    }));
    it('menuAccount method :: should have a menuAccount function', inject(function() {
        expect(angular.isFunction($scope.menuAccount)).toBe(true);
    }));
    it('seenAllMessage method :: should have a seenAllMessage function', inject(function() {
        expect(angular.isFunction($scope.seenAllMessage)).toBe(true);
    }));
    it('loadMoreNoti method :: should have a loadMoreNoti function', inject(function() {
        expect(angular.isFunction($scope.loadMoreNoti)).toBe(true);
    }));
    it('loadMoreNoti method :: should have a loadMoreNoti bodyClasses', inject(function() {
      var mockEvent = {};
      mockEvent = {
          stopPropagation: jasmine.createSpy('stopPropagation')
      };
        $scope.loadMoreNoti(mockEvent);
    }));
    it('viewLikeActivity method :: should have a viewLikeActivity function', inject(function() {
        expect(angular.isFunction($scope.viewLikeActivity)).toBe(true);
    }));
    it('viewLikeActivity method :: viewLikeActivity function working', inject(function() {
        $scope.showAllNotificationList = [{
            "status": ''
        }];
        $scope.viewLikeActivity(0, '56dff52e37bca6b31a4168be', '24dff52e37bca6b31a4168be');
        $httpBackend.when('PUT', '/api/users/1/notifications/24dff52e37bca6b31a4168be/seen').respond({
            "statusCode": 200,
            "message": "ok"
        });
    }));
    it('seenAllMessage method :: seenAllMessage function working', inject(function() {
        $scope.getTotalNotification = 3;

        $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
        $httpBackend.when('PUT', '/api/users//notifications/unseen').respond(noti_result);
        $scope.seenAllMessage();
        $httpBackend.flush();
    }));
    it('seenAllMessage method :: seenAllMessage function working', inject(function() {
        $scope.getTotalNotification = 0;

        $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
        $httpBackend.when('PUT', '/api/users//notifications/unseen').respond(400);
        $scope.seenAllMessage();
        //$httpBackend.flush();
    }));

    it('removeNotification method :: should have a removeNotification function', inject(function() {
        expect(angular.isFunction($scope.removeNotification)).toBe(true);
    }));

    it('removeNotification method :: removeNotification function working', inject(function() {
        //APP.currentUser._id = 1;
        $scope.allTotal = 5;
        $scope.showAllNotificationList = [{
            "status": ''
        }];
        $scope.rejectId = 123456;
        $scope.removeNotification(0, '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be');
        $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
        $httpBackend.when('PUT', '/api/groups/123456/rejected').respond(noti_result);

    }));
    // it('removeNotification method :: removeNotification function working', inject(function() {
    //     //APP.currentUser._id = 1;
    //     $scope.allTotal = 1;
    //     // $scope.showAllNotificationList = [{
    //     //     "status": ''
    //     // }];
    //     // $scope.rejectId = 123456;
    //     $scope.removeNotification(0, '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be');
    //     // $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
    //     // $httpBackend.when('PUT', '/api/groups/123456/rejected').respond(noti_result);
    // }));
    //acceptNotification
    it('acceptNotification method :: should have a acceptNotification function', inject(function() {
        expect(angular.isFunction($scope.acceptNotification)).toBe(true);
    }));

    it('acceptNotification method :: acceptNotification function working', inject(function() {
        // APP.currentUser._id = 1;
        $scope.allTotal = 5;
        $scope.showAllNotificationList = [{
            "status": ''
        }];
        $scope.rejectId = 123456;
        $scope.acceptNotification(0, '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be');
        $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
        $httpBackend.when('PUT', '/api/groups/123456/accepted').respond(noti_result);

    }));
    // it('acceptNotification method :: acceptNotification function working', inject(function() {
    //     // APP.currentUser._id = 1;
    //     $scope.allTotal = 1;
    //     // $scope.showAllNotificationList = [{
    //     //     "status": ''
    //     // }];
    //     // $scope.rejectId = 123456;
    //     $scope.acceptNotification(0, '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be');
    //     // $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
    //     // $httpBackend.when('PUT', '/api/groups/123456/accepted').respond(noti_result);

    // }));
    //viewGroup
    it('viewGroup method :: should have a viewGroup function $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$', inject(function() {
        expect(angular.isFunction($scope.viewGroup)).toBe(true);
    }));

    // it('viewGroup method :: viewGroup function working', inject(function() {
    //     $scope.allTotal = 5;
    //     $scope.showAllNotificationList = [{
    //         "status": ''
    //     }];
    //     $scope.rejectId = 123456;
    //     $scope.viewGroup(0, '56dff82e37bca6b31a4168be', '56dff82e37bca6b31a4168be');
    //     $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);
    //     $httpBackend.when('PUT', '/api/users/1/notifications/56dff82e37bca6b31a4168be/seen').respond(noti_result);

    // }));
    //updateName
    it('updateName rootscope method :: should have a updateName rootscope function', inject(function() {
        $rootScope.$broadcast("updateName", { name: "Gunjan" });
    }));
    //getAllNotificationShow
    it('getAllNotificationShow method :: should have a getAllNotificationShow function', inject(function() {
        expect(angular.isFunction($scope.getAllNotificationShow)).toBe(true);
    }));
    it('getAllNotificationShow method :: should have a getAllNotificationShow failure', inject(function() {
        $httpBackend.when('GET', '/api/users//notifications?limit=5&skip=0').respond(noti_result_fail);
        $scope.getAllNotificationShow();

        //$httpBackend.flush();
    }));

    it('getAllNotificationShow method :: should have a getAllNotificationShow body', inject(function() {
        $scope.getNotificationShow = true;
        $scope.getAllNotificationShow();
        $httpBackend.when('GET', '/api/users/1/notifications?limit=5&skip=0').respond(noti_result);

    }));

    it('getAllNotificationShow method :: should have a getAllNotificationShow body', inject(function() {
        $scope.getAllNotificationShow();
        $httpBackend.when('GET', '/api/users//notifications?limit=5&skip=0').respond(noti_result);
        //$httpBackend.flush();
    }));
    it('getAllNotificationShow method :: should have a getAllNotificationShow body', inject(function() {
        $scope.getAllNotificationShow();
        $httpBackend.when('GET', '/api/users//notifications?limit=5&skip=0').respond(noti_result_fail);
        //$httpBackend.flush();
    }));
    it('getAllNotificationShow method :: should have a getAllNotificationShow body', inject(function() {
        $scope.getAllNotificationShow();
        $httpBackend.when('GET', '/api/users//notifications?limit=5&skip=0').respond(400);
        //$httpBackend.flush();
    }));

    it('getAllNotificationShow method :: getAllNotificationShow function return when no pagination available', inject(function() {
        $scope.listResponse = 17;
        $scope.getAllNotificationShow();
    }));
    it('getAllNotificationShow method :: getAllNotificationShow function return when no pagination available', inject(function() {
        $scope.allTotal = 4;
        $scope.getAllNotificationShow();
    }));
    it('getAllNotificationShow method :: getAllNotificationShow function return when no pagination available', inject(function() {
        $scope.allTotal = 10;
        APP.currentUser._id = '123';
        $scope.getAllNotificationShow();
        $httpBackend.when('GET', '/api/users/123/notifications?limit=5&skip=0').respond(noti_result);
        // $httpBackend.flush();
    }));
    //updateHeader

    it('updateHeader rootscope method :: updateHeader rootscope function', inject(function() {
        //$scope.avatar_url= "abcd.png";
        // var user = {};
        // var user.local = {};
        // user.local.avatar_url = "abcd.png";

        $rootScope.$emit("updateHeader", {
            user: user
        });
    }));
    it('menuAccount method :: should have a cancel function', inject(function() {
        expect(angular.isFunction($scope.cancel)).toBe(true);
    }));
    it('emit event :: should have a callHeaderController $on event', inject(function($rootScope) {
        $rootScope.$emit('callHeaderController');
    }));
    it('emit event :: should have a ShowModalLogin $on event', inject(function($rootScope) {
        $scope.$emit('ShowModalLogin');
    }));
    it('menuAccount method :: should have a cancel function body', function() {

        //expect($uibModal.dismiss).toHaveBeenCalled();
        // $scope.cancel();
    });
    it('menuAccount method body :: should have a menuAccount function', inject(function() {
        $scope.menuAccount();
        expect($scope.showDropMenu).toEqual(true);
    }));

});
