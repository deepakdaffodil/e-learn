'use strict';
describe('E-LEARNING: lessonTypeController Controller test', function() {
    var ctrl, $scope, $httpBackend, $timeout, $uibModalInstance;
    var followers = {
        error: false,
        message: "ok",
        result: [{
            _id: "57299f0d782f0092722a4581",
            createdAt: "2016-05-04T07:04:45.859Z"
        }],
        statusCode: 200
    };
    var failFollowers = {
        error: true,
        message: "ok",
        result: [{
            _id: "57299f0d782f0092722a4581",
            createdAt: "2016-05-04T07:04:45.859Z"
        }],
        statusCode: 400
    };
    var loginResult = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "__v": 0,
            "_id": "56dff82e37bca6b31a4168be",
            "createdAt": "2016-03-09T10:17:19.071Z",
            "updatedAt": "2016-03-09T10:17:19.071Z",
            "local": {
                "dob": "01/03/2016",
                "email": "preeti.sachdeva@daffodilsw.com",
                "name": "Preeti Sachdeva",
                "password": "$2a$08$wJfg24K9Q4b6x5xfS2.dnup1Pr5M.u21eLbMWdt3xu0jpgU5HR71a",
                "deleted": false,
                "rev": 0,
                "ambassdor": false,
                "lastlogin_at": "2016-03-09T10:17:18.972Z",
                "token": '12232423454363433'
            }
        },
        "error": false
    };
    var type = "";
    var moduleId = "1234567";

    /**
     * Load Sixthcontinent module before execute any test
     */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, _$httpBackend_, _$timeout_, $controller, $rootScope) {
        $httpBackend = _$httpBackend_;
        $timeout = _$timeout_;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create contorller
        ctrl = $controller('lessonTypeController', {
            $scope: $scope,
            $timeout: $timeout,
            type: type,
            $uibModalInstance: $uibModalInstance,
            moduleId: moduleId
        });
    }));
    // it('closeModal method test :: should have a equall value', inject(function() {
    //     expect($scope.followingsLoader).toEqual(true);
    // }));
    // it('variables followersLoader test :: should have a equall value', inject(function() {
    //     expect($scope.followersLoader).toEqual(true);
    // }));
    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));
    it('closeModal method :: should have a closeModal function definition', inject(function() {
        $scope.closeModal();
    }));
    it('uploadFiles method :: should have a uploadFiles function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('uploadFiles method :: should have a uploadFiles function definition when have files', inject(function() {
        var element = {
            files: [{
                lastModified: "1467020822540",
                lastModifiedDate: "Mon Jun 27 2016",
                type: "image/jpeg"
            }]
        };
        $scope.upload(element);
    }));
    it('hidePreview method :: should have a hidePreview function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('hidePreview method :: should have a hidePreview function definition', inject(function() {
        $scope.hidePreview();
    }));
    it('addVideo method :: should have a addVideo function', inject(function() {
        expect(angular.isFunction($scope.addVideo)).toBe(true);
    }));
    it('addVideo method :: should have a addVideo function definition', inject(function() {
        $scope.addVideo();
    }));
    it('doneClick method :: should have a doneClick function', inject(function() {
        expect(angular.isFunction($scope.doneClick)).toBe(true);
    }));
    it('doneClick method :: should have a doneClick function definition', inject(function() {
        $scope.doneClick();
    }));
    it('doneClick method :: should have a doneClick function definition when videoId', inject(function() {
        $scope.videoId = "12345678901";
        $scope.doneClick();
    }));
    it('cancelVideo method :: should have a cancelVideo function', inject(function() {
        expect(angular.isFunction($scope.cancelVideo)).toBe(true);
    }));
    it('cancelVideo method :: should have a cancelVideo function definition', inject(function() {
        $scope.cancelVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function', inject(function() {
        expect(angular.isFunction($scope.createLessonTypeVideo)).toBe(true);
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        // $scope.lesson.basic_description = "Hello! how are you?";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        // $scope.lesson.basic_description = "Hello! how are you?";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        // $scope.lesson.basic_description = "Hello! how are you?";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "Hello! how are you?";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        // type = 'Edit Lesson';
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "Hello! how are you?";
        $scope.videoId = "";
        $scope.createLessonTypeVideo();
    }));
    it('createLessonTypeVideo method :: should have a createLessonTypeVideo function definition', inject(function() {
        // type = 'Edit Lesson';
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "Hello! how are you?";
        $scope.videoId = "12345678901";
        $scope.optionModule = "46273547234";
        $scope.createLessonTypeVideo();
        $httpBackend.when('POST', '/api/lessons').respond(followers);

        // $httpBackend.flush();
    }));



    it('createLessonTypeImage method :: should have a createLessonTypeImage function', inject(function() {
        expect(angular.isFunction($scope.createLessonTypeImage)).toBe(true);
    }));
    it('createLessonTypeImage method :: should have a createLessonTypeImage function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "Hello! how are you?";
        $scope.lesson.location = "";
        $scope.createLessonTypeImage();
    }));
    it('createLessonTypeImage method :: should have a createLessonTypeImage function definition', inject(function() {
        $scope.lesson.name = "";
        $scope.lesson.basic_description = "Hello! how are you?";
        $scope.createLessonTypeImage();
    }));
    it('createLessonTypeImage method :: should have a createLessonTypeImage function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "";
        $scope.createLessonTypeImage();
    }));
    it('createLessonTypeImage method :: should have a createLessonTypeImage function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "hello how are you?";
        $scope.lesson.location = "hello";
        $scope.createLessonTypeImage();
    }));



    it('createLessonTypeText method :: should have a createLessonTypeText function', inject(function() {
        expect(angular.isFunction($scope.createLessonTypeText)).toBe(true);
    }));
    it('createLessonTypeText method :: should have a createLessonTypeText function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "hello how are you?";
        $scope.createLessonTypeText();
    }));
    it('createLessonTypeText method :: should have a createLessonTypeText function definition', inject(function() {
        $scope.lesson.name = "";
        $scope.lesson.basic_description = "";
        $scope.createLessonTypeText();
    }));
    it('createLessonTypeText method :: should have a createLessonTypeText function definition', inject(function() {
        $scope.lesson.name = "Gunjan Jain";
        $scope.lesson.basic_description = "";
        $scope.createLessonTypeText();
    }));


    it('lessonServiceType method :: should have a lessonServiceType function', inject(function() {
        expect(angular.isFunction($scope.lessonServiceType)).toBe(true);
    }));
    // it('lessonServiceType method :: should have a lessonServiceType function definition', inject(function() {
    //     $scope.lessonServiceType();
    // }));
    // it('unfollow method :: should have a unfollow function definition', inject(function() {
    //     $scope.follower = [{
    //         _id: "57299f0d782f0092722a4581",
    //         createdAt: "2016-05-04T07:04:45.859Z"
    //     }];
    //     $scope.unfollow(0, '57299f0d782f0092722a4581', $scope.follower);
    // }));
    //
    // it('uploadFiles method :: should have a uploadFiles function definition when no files', inject(function() {
    //     var files1 = "";
    //     $scope.uploadFiles(files1);
    //     //$timeout.flush();
    // }));
    // it('follow/following service :: should have a follow service function definition', inject(function() {
    //     $scope.user = {};
    //     $scope.user.email = 'preeti.sachdeva@daffodilsw.com';
    //     $scope.user.password = '1234567';
    //     $httpBackend.when('POST', '/api/login').respond(loginResult);
    //     $httpBackend.when('GET', '/api/users/followers').respond(followers);
    //     $httpBackend.when('GET', '/api/users/following').respond(followers);
    //     $scope.followers();
    //     $httpBackend.flush();
    // }));
    // it('follow/following service :: should have a follow service function definition with failure', inject(function() {
    //     $scope.user = {};
    //     $scope.user.email = 'preeti.sachdeva@daffodilsw.com';
    //     $scope.user.password = '1234567';
    //     $httpBackend.when('POST', '/api/login').respond(loginResult);
    //     $httpBackend.when('GET', '/api/users/followers').respond(failFollowers);
    //     $httpBackend.when('GET', '/api/users/following').respond(failFollowers);
    //     $scope.followers();
    // }));


});
