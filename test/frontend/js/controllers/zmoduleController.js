'use strict';
describe('E-LEARNING: modulesController Controller test', function(){
    var ctrl, $scope, $httpBackend, state , $timeout;
    var moduleRes = { "statusCode": 200, "message": "ok", "result": [{
            "description": "sfgsdfgsd",
            "name": "Nitesh",
            "_id": "56b5e059b7d52a8145c13824",
            "__v": 0,
            "deleted": false
        }] };
    var loginResult = {
    "statusCode": 200,
    "message": "ok",
    "result": {
        "__v": 0,
        "_id": "56dff82e37bca6b31a4168be",
        "createdAt": "2016-03-09T10:17:19.071Z",
        "updatedAt": "2016-03-09T10:17:19.071Z",
        "local": {
            "dob": "01/03/2016",
            "email": "preeti.sachdeva@daffodilsw.com",
            "name": "Preeti Sachdeva",
            "password": "$2a$08$wJfg24K9Q4b6x5xfS2.dnup1Pr5M.u21eLbMWdt3xu0jpgU5HR71a",
            "deleted": false,
            "rev": 0,
            "ambassdor": false,
            "lastlogin_at": "2016-03-09T10:17:18.972Z"
        }
    },
    "error": false
};
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, _$httpBackend_, $controller, $rootScope, $uibModal,_$timeout_){
        $httpBackend = _$httpBackend_;
        $timeout = _$timeout_;
        state = jasmine.createSpyObj('$state', ['go']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
       //  spyOn(window, 'confirm').andReturn(true);
        var windowMock = { confirm: function(msg) { return false } }
        //create contorller
        ctrl = $controller('modulesController', {
            $scope: $scope,
            state: state,
            $uibModal : $uibModal,
            $rootscope : $rootScope,
            $window: windowMock
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.lesson).toEqual('');
        expect($scope.showActivity).toEqual(false);
        expect($scope.actionlist).toEqual(false);
    }));

     it('CallParamsCloseMethod event :: should have a CallParamsCloseMethod $on event', inject(function($rootScope) {
        $rootScope.$broadcast('CallParamsCloseMethod');
    }));
    it('getlessonDetail method :: should have a getlessonDetail function', inject(function() {
        expect(angular.isFunction($scope.getlessonDetail)).toBe(true);
    }));
    it('onNext method :: should have a onNext function', inject(function() {
        expect(angular.isFunction($scope.onNext)).toBe(true);
    }));
    it('progressModal method :: should have a progressModal function', inject(function() {
        expect(angular.isFunction($scope.progressModal)).toBe(true);
    }));

    it('variable  module :: call modules api', inject(function() {
        $httpBackend.when('GET', '/api/modules').respond(moduleRes);
        expect($scope.modules).toEqual([]);
        $httpBackend.flush();
    }));
    // it('createActivity method :: should have a createActivity function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity.description = 'activity description';
    //     $scope.uploadLoader = true;
    //     $scope.createActivityItem();
    //     $scope.postActivity();
    // }));
    // it('createActivity method :: should have a createActivity function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity.location = undefined;
    //     $scope.activity.description = 'activity description';
    //     $scope.uploadLoader = true;
    //     $scope.createActivityItem();
    //     $scope.postActivity();
    // }));
    //
    // it('createActivityItem method :: should have a createActivityItem with rootscope isLoggedIn false function', inject(function($rootScope) {
    //     $scope.activity.description = '';
    //     $scope.createActivityItem();
    // }));
    // it('createActivityItem method :: should have a createActivityItem with rootscope isLoggedIn false function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity.description = undefined;
    //     $scope.createActivityItem();
    // }));
    //  it('createActivity method :: should have a createActivity with rootscope isLoggedIn false function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn = false;
    //     $scope.createActivityItem();
    // }));
    // it('createActivity method :: should have a createActivity function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity ={};
    //     $httpBackend.when('GET', '/modules').respond({});
    //     $scope.activity.location = 'www.test.com';
    //     $scope.activity.description = 'activity description';
    //     $scope.uploadLoader = true;
    //     $scope.createActivityItem('form','56b5e075b7d52a8145c13825','56b5e075b7d52a8145c13825');
    //     $httpBackend.when('POST', '/activity').respond({"statusCode":200,"message":"ok"});
    //     $scope.postActivity('form',false);
    //     $httpBackend.when('GET', 'app/shared/shareActivity/shareActivityView.html').respond();
    //     $httpBackend.when('GET', '/modules/56b5e075b7d52a8145c13825/lessons').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.when('GET', '/activity/modules/56b5e075b7d52a8145c13825').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.flush();
    //    // $timeout.flush();
    // }));
    //   it('createActivity method :: should have a createActivity function', inject(function($rootScope) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity ={};
    //     $httpBackend.when('GET', '/modules').respond({});
    //     $scope.activity.location = 'www.test.com';
    //     $scope.activity.description = 'activity description';
    //     $scope.uploadLoader = true;
    //     $scope.createActivityItem('form','56b5e075b7d52a8145c13825','56b5e075b7d52a8145c13825');
    //     $httpBackend.when('POST', '/activity').respond({"statusCode":200,"message":"ok"});
    //     $scope.postActivity('form',true);
    //     $httpBackend.when('GET', 'app/shared/shareActivity/shareActivityView.html').respond();
    //     $httpBackend.when('GET', '/modules/56b5e075b7d52a8145c13825/lessons').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.when('GET', '/activity/modules/56b5e075b7d52a8145c13825').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.flush();
    //    // $timeout.flush();
    // }));
    // it('createActivity method :: should have a createActivity function', inject(function($rootScope, $timeout) {
    //     $rootScope.isLoggedIn =true;
    //     $scope.activity ={};
    //     $httpBackend.when('GET', '/modules').respond({});
    //     $scope.activity.location = 'www.test.com';
    //     $scope.activity.description = 'activity description';
    //     $scope.uploadLoader = true;
    //     $scope.createActivityItem('form','56b5e075b7d52a8145c13825','56b5e075b7d52a8145c13825');
    //     $httpBackend.when('GET', 'app/shared/shareActivity/shareActivityView.html').respond();
    //
    //     $httpBackend.when('POST', '/activity').respond({"statusCode":201});
    //     $scope.postActivity('form',true);
    //     $scope.expandLesson('56b5e075b7d52a8145c13825');
    //     $httpBackend.when('GET', '/modules/56b5e075b7d52a8145c13825/lessons').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.when('GET', '/activity/modules/56b5e075b7d52a8145c13825').respond({"message":"ok" , "result":[{}]});
    //     $httpBackend.flush();
    //     $timeout.flush();
    // }));
    it('callModuleController works', inject(function () {
        $scope.$emit('callModuleController', {});
    }));
    it('onNext works', inject(function ($rootScope) {
        $rootScope.isLoggedIn = false;
        $rootScope.isAdminLoggedIn = false;
        $scope.onNext({}, {}, {});
    }));
    it('onNext works', inject(function ($rootScope) {
        $rootScope.isLoggedIn = true;
        $rootScope.isAdminLoggedIn = false;
        $scope.onNext({}, {}, {});
    }));
    it('onNext works', inject(function ($rootScope) {
        $rootScope.isLoggedIn = true;
        $rootScope.isAdminLoggedIn = false;
        $scope.onNext({}, {}, {completed: true});
    }));
    it('progressModal works', inject(function () {
        $scope.progressModal();
    }));
    it('getLessonDetail works', inject(function () {
        $scope.getlessonDetail();
    }));
});
