describe('E-LEARNING: lastComponent Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout, $uibModalInstance;
    var moduleObj = {};
    var comm = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "lessons": ['abcd']
        }
    };
    var comm1 = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "activities": [{
                activvity: "abcd"
            }],
            "totalCount": 1
        }
    };
    var lessons = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce"
        }],
        "error": false
    };
    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "sports",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": true
        }, {
            "_id": "56bdc7163aa2778521d65fd1",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "cricket",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": false
        }],
        "error": false
    };
    /**
    /**{"statusCode":200,"message":"ok","result":{"activities":['abcd']}}
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
     * Inject required dependencies as $httpBackend, $controller and $rootscope etc
     */
    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('lastComponentController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout,
            $uibModalInstance: $uibModalInstance

        });
    }));

    it('getAllActivitiesz method :: should have a getAllActivitiesz function', inject(function() {
        expect(angular.isFunction($scope.getAllActivitiesz)).toBe(true);
    }));
    it('getAllActivitiesz method :: should have a getAllActivitiesz function definition', inject(function() {
        $scope.getAllActivitiesz();
        $timeout.flush();
        $httpBackend.flush();
    }));

    it('callLastComponent rootscope method :: should have a callLastComponent rootscope function', inject(function() {
        $rootScope.$broadcast("callLastComponent", {
            module: moduleObj
        });
    }));

});
