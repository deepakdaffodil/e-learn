'use strict';
describe('E-LEARNING: lessonModuleController Controller test', function(){
    var ctrl, $scope, $httpBackend, state, $uibModalInstance;
    var comm = {"statusCode":200,"message":"ok","result":[{"createdAt":"2016-03-23T06:40:02.496Z"}]};
    var comm1 = {"statusCode":200,"message":"ok","result":[]};
    var id= '56dab3598e0f0fa40d98b3c7';
    /**
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector,$state, _$httpBackend_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;
        state = $state;

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('lessonModuleController', {
            $scope: $scope,
            state : $state,
            $uibModalInstance: $uibModalInstance,
            moduleId :id
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.showCreateForm).toEqual(false);
    }));
    it('broadcast  test :: should have a broadcast call', inject(function() {
        $scope.$broadcast('CallModulesMethod');
    }));
    it('createModule  test :: should have a createModule call', inject(function() {
        $scope.createModule();
    }));
    it('editModule  test :: should have a editModule call', inject(function() {
        $scope.editModule();
    }));
    it('showLesson method :: should have a showLesson function', inject(function() {
        expect(angular.isFunction($scope.showLesson)).toBe(true);
    }));
    it('showLesson method :: should have a showLesson body', inject(function() {
       $scope.showLesson();
    }));
    it('hideLesson method :: should have a hideLesson function', inject(function() {
        expect(angular.isFunction($scope.hideLesson)).toBe(true);
    }));
    it('hideLesson method :: should have a hideLesson body', inject(function() {
       $scope.hideLesson();
    }));
});    

