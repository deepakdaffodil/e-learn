'use strict';
describe('E-LEARNING: viewSharedController Controller test', function(){
    var ctrl, $scope, $httpBackend, obj, $uibModalInstance;
    var res= { "statusCode": 200,
    "message": "ok",
    "result": {"_id":'56dff82e37bca6b31a4168be',"likes": []},
    "error": false
    };
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));

    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, _$httpBackend_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
    	  $uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);
        var obj = document.createElement('div');
        obj.id = "getHeight";
        obj.setAttribute("style", "height:600px;");
        spyOn(document,'getElementById').and.returnValue(obj);
        //create contorller
        ctrl = $controller('viewSharedController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            activityId : '56dab3598e0f0fa40d98b3c7'
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
       expect($scope.isLike).toEqual(false);
    }));
    it('closeModal method body :: should have a closeModal function', inject(function() {
       $scope.closeModal();
    }));
      it('closeModal method body :: should have a closeModal function', inject(function() {
       $scope.cancel();
    }));
    it('editDetail method body :: should have a editDetail function', inject(function() {
       $scope.editDetail();
    }));
    it('likeActivity method body :: should have a likeActivity body',function() {
     // console.log("ggg", APP.currentUser._id);
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond(res);
      $scope.likeActivity('56dab3598e0f0fa40d98b3c7','5e4ssss98192d3f379fd13eabdd4' ,1);
      $httpBackend.when('PUT','/api/activity/56dab3598e0f0fa40d98b3c7/likes').respond({"statusCode":200,"message":"ok"})
      $httpBackend.flush();
    });
    it('callback method :: should have a callback function', inject(function() {
        expect(angular.isFunction($scope.callback)).toBe(true);
    }));
    it('likeActivity method body :: should have a likeActivity body',function() {
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.likeActivity('5e4ssss98192d3f379fd13eabdd4' ,1);
      $httpBackend.when('PUT','/api/activity/5e4ssss98192d3f379fd13eabdd4/likes').respond({"statusCode":201})
      $httpBackend.flush();
    });
    it('unlikeActivity method body :: should have a likeActivity body',function() {
      $scope.likedPeople = -1;
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.unlikeActivity('5e4ssss98192d3f379fd13eabdd4' ,1);
      $httpBackend.when('PUT','/api/activity/5e4ssss98192d3f379fd13eabdd4/unlikes').respond({"statusCode":200,"message":"ok"})
      $httpBackend.flush();
    });
     it('unlikeActivity method body :: should have a likeActivity body',function() {
      $scope.likedPeople = 1;
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.unlikeActivity('5e4ssss98192d3f379fd13eabdd4' ,1);
      $httpBackend.when('PUT','/api/activity/5e4ssss98192d3f379fd13eabdd4/unlikes').respond({"statusCode":200,"message":"ok"})
      $httpBackend.flush();
    });
    it('unlikeActivity method body :: should have a unlikeActivity body',function() {
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.unlikeActivity('5e4ssss98192d3f379fd13eabdd4' ,1);
      $httpBackend.when('PUT','/api/activity/5e4ssss98192d3f379fd13eabdd4/unlikes').respond({"statusCode":201})
      $httpBackend.flush();
    });
    it('saveDetail method body :: should have a likeActivity body',function() {
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.editDescriptionValue = undefined;
      $scope.saveDetail(' ','5e4ssss98192d3f379fd13eabdd4');
    });
    it('saveDetail method body :: should have a likeActivity body',function() {
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.editDescriptionValue = '';
      $scope.saveDetail(' ','5e4ssss98192d3f379fd13eabdd4');
    });
    it('saveDetail method body :: should have a likeActivity body',function() {
      $httpBackend.when('GET', '/api/activity/56dab3598e0f0fa40d98b3c7').respond({});
      $scope.editDescriptionValue = '111111';
      $scope.saveDetail(' ','5e4ssss98192d3f379fd13eabdd4');
      $httpBackend.when('PUT','/api/activity/5e4ssss98192d3f379fd13eabdd4').respond({"statusCode":200,"message":"ok"})
      $httpBackend.flush();
    });

    it('CallHeightMethod method :: should have a CallHeightMethod $on event', inject(function($rootScope) {
      $scope.$emit('CallHeightMethod');
    }));
     it('openModal method :: should have a openModal $on event', inject(function($rootScope) {
      $scope.openModal('5e3ssss98192d3f379fd13eabdd4');
    }));
    it('openModal method body :: should have a openModal function', inject(function() {
      $scope.openModal('5e4ssss98192d3f379fd13eabdd4');
    }));
    it('callback method body :: should have a callback body', inject(function() {
        $scope.callback(undefined);
    }));
    it('callback method body :: should have a callback body', inject(function() {
        $scope.viewDetailActivity= {"user":{"_id":'5e3ssss98192d3f379fd13eabdd1'}};
        $scope.callback({'post_id':'5e4ssss98192d3f379fd13eabdd4'});
    }));
});
