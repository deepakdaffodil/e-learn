'use strict';
describe('E-LEARNING: findPeopleController Controller test', function(){
    var ctrl, $scope, $httpBackend, $uibModalInstance;
    var id= '56dab3598e0f0fa40d98b3c7';
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, $rootScope,_$httpBackend_, $controller){
        $httpBackend = _$httpBackend_;
        $rootScope = $rootScope;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();
    	$uibModalInstance = jasmine.createSpyObj('$uibModalInstance',  ['dismiss']);

        //create contorller
        ctrl = $controller('findPeopleController', {
            $scope: $scope,
            $uibModalInstance: $uibModalInstance,
            activityId:  id
        });
    }));
    it('fucntion  getUserActions :: should have a getUserActions function', inject(function() {
        expect($scope.showUserLoader).toEqual(true);
        var activityId = activityId;
        $httpBackend.when('GET','/api/activity/56dab3598e0f0fa40d98b3c7/likes').respond({"statusCode":200,"message":"ok","result":[{"likes":0}]});
    	$httpBackend.flush();
    }));
    it('fucntion  getUserActions :: should have a getUserActions function', inject(function() {
    	$scope.closeModal2();
    }));
});
