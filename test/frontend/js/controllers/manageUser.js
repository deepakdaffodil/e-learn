'use strict';
describe('E-LEARNING: adminUserController Controller test', function(){
    var ctrl, $scope, $httpBackend, state , $timeout;
    var comm = {"statusCode":200,"message":"ok","result":[{"createdAt":"2016-03-23T06:40:02.496Z"}]};
    var comm1 = {"statusCode":200,"message":"ok","result":[]};
    /**
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector,$state, _$httpBackend_,_$timeout_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('adminUserController', {
            $scope: $scope,
            state : $state
        });
    }));
    it('variables  test :: should have a equal value', inject(function() {
        expect($scope.loader).toEqual(true);
    }));
    it('loadMore  test :: should have a loadMore call', inject(function() {
       $scope.loadMore();
    }));
    it('sortMyUser  test :: should have a sortMyUser call', inject(function() {
        $scope.sortUser = false;
        $scope.sortMyUser();
    }));
    it('sortMyUser  test :: should have a sortMyUser call', inject(function() {
        $scope.sortUser = true;
        $scope.sortMyUser();
    }));
    it('open1 method :: should have a open1 function', inject(function() {
        expect(angular.isFunction($scope.open1)).toBe(true);
    }));
    it('editUser method :: should have a editUser function', inject(function() {
        expect(angular.isFunction($scope.editUser)).toBe(true);
    }));
    it('cancelSave method :: should have a cancelSave function', inject(function() {
        expect(angular.isFunction($scope.cancelSave)).toBe(true);
    }));
    it('hidePreview method :: should have a hidePreview function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('upload method :: should have a upload function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('updateUser method :: should have a updateUser function', inject(function() {
        expect(angular.isFunction($scope.updateUser)).toBe(true);
    }));
    it('open1 method :: should have a open1 body', inject(function() {
        $scope.open1();
    }));
    it('disabled method :: should have a disabled body', inject(function() {
        $scope.disabled();
    }));
    it('getUserList method :: should have a getUserList body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200 , "message":"ok","result":{"totalcount":0}})
        $scope.getUserList();
        $httpBackend.flush();
    }));
    it('getUserList method :: should have a getUserList body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.getUserList();
        $httpBackend.flush();
    }));
    it('deleteUser method :: should have a deleteUser body', inject(function() {
        $scope.userList =[{"_id":'56ssss98192d3f379fd13eabdd4'},{"_id":'56ssss98192d3f379fd13eabd24'}];
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200 , "message":"ok","result":[{"_id":'56ssss98192d3f379fd13eabdd4'},{"_id":'56ssss98192d3f379fd13eabd24'}]})
        $scope.deleteUser(0,'56ssss98192d3f379fd13eabdd4');
        $httpBackend.when('DELETE',"/api/users/56ssss98192d3f379fd13eabdd4").respond({"statusCode":200 , "message":"ok"})
        $httpBackend.flush();
    }));
    it('editUser method :: should have a editUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.editUser(0,{"_id":'56ssss98192d3f379fd13eabdd4',"local":{"email":"pr@gmail.com"}});
        //$httpBackend.flush();
    }));
     it('editUser method :: should have a editUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.editUser(0,{"_id":'56ssss98192d3f379fd13eabdd4',"local":{"email":null}});
        //$httpBackend.flush();
    }));
    it('cancelSave method :: should have a cancelSave body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.cancelSave(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"","dob":"2/1/2016","rev":0,"email":"preeti.sachdeva@daffodilsw.com"}});
        //$httpBackend.flush();
    }));
    it('updateUser method :: should have a updateUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.updateUser(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"","dob":"2/1/2016","rev":0,"email":"preeti.sachdeva@daffodilsw.com"}});
        //$httpBackend.flush();
    }));
    it('updateUser method :: should have a updateUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.updateUser(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"ss","dob":"2/1/2016","rev":0,"email":''}});
    }));
    it('updateUser method :: should have a updateUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.updateUser(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"ss","dob":"","rev":0,"email":'d'}});
    }));
    it('updateUser method :: should have a updateUser body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.updateUser(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"ss","dob":"1/2/2014","rev":0,"email":'d',"password" : "wowwwwwww"}});
    }));

    it('updateUser method :: should have a updateUser body', inject(function() {
        $scope.getOriginalEmail= 'test@gmail.com';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":201})
        $scope.updateUser(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"ss","dob":"1/2/2014","rev":0,"email":'test@gmail.com',"password" : "testinghere"}});
    }));
    it('hidePreview method :: should have a hidePreview body', inject(function() {
        $scope.hidePreview();
    }));
    it('upload method :: should have a upload body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok"});
        var element = {"files":[{"type":"image/jpeg"}]};
        $scope.upload(element);
        $timeout.flush();
    }));
     it('upload method :: should have a upload body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok"});
        var element = {"files":[{"type":"image/png"}]};
        $scope.upload(element);

    }));
    it('upload method :: should have a upload body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok"});
        var element = {"files":[{"type":"image/ptng"}]};
        $scope.upload(element);
    }));
    it('upload method :: should have a upload body', inject(function() {
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok"});
        var element = {"files":[{undefined:undefined}]};
        $scope.fileModel = '';
        $scope.upload(element);
    }));
    it('searchUser method :: should have a searchUser function', inject(function() {
        expect(angular.isFunction($scope.searchUser)).toBe(true);
    }));
    it('searchUser method :: should have a searchUser function', inject(function() {
        $scope.search ='pree';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":{"totalcount":0}});

        $httpBackend.when('GET','/api/users?name=pree').respond({"statusCode":200,"message":"ok","result":{"totalcount":0}});
        $scope.searchUser();$httpBackend.flush();
    }));

    it('openModal method :: should have a openModal function', inject(function() {
        $scope.openModal();
    }));
    it('openConfirmModal method :: should have a openConfirmModal function', inject(function() {
        $scope.openConfirmModal();
    }));
    it('emailValidate method body :: emailValidate habe a body', inject(function(){
        $scope.location =[];
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":[{"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com"}}]});
        $scope.emailValidate(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com","avatar_url":''}});
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":200,"message":"ok","result":{"message":"verified"}});
        $httpBackend.when('PUT','/api/users/admin/56b5e059b7d52a8145c13824').respond({"statusCode":200,"message":"ok","result":{}})
        $httpBackend.flush();
    }));
    it('emailValidate method body :: emailValidate habe a body', inject(function(){
        $scope.location[0] = null;
        $scope.formData.password = '1234567';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":[{"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com"}}]});
        $scope.emailValidate(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com","avatar_url":'ht'}});
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":200,"message":"ok","result":{"message":"verified"}});
        $httpBackend.when('PUT','/api/users/admin/56b5e059b7d52a8145c13824').respond({"statusCode":201});
         $httpBackend.when('GET', 'app/shared/dialogBox/alertView.html').respond();
        $httpBackend.flush();
    }));
    it('emailValidate method body :: emailValidate habe a body', inject(function(){
        $scope.location[0] = null;
        $scope.formData.password = '1234567';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":[{"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com"}}]});
        $scope.emailValidate(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com","avatar_url":'ht'}});
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":422,"message":"email already registered"});
        $httpBackend.when('GET', 'app/shared/dialogBox/alertView.html').respond();
        $httpBackend.flush();
    }));
    it('emailValidate method body :: emailValidate habe a body', inject(function(){
        $scope.location[0] = null;
        $scope.formData.password = '1234567';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":[{"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com"}}]});
        $scope.emailValidate(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com","avatar_url":'ht'}});
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":422,"message":"email should be registered with any email provider"});
        $httpBackend.when('GET', 'app/shared/dialogBox/alertView.html').respond();
        $httpBackend.flush();
    }));
    it('emailValidate method body :: emailValidate habe a body', inject(function(){
        $scope.location[0] = null;
        $scope.formData.password = '1234567';
        $httpBackend.when('GET',"/api/users?limit=200&skip=0").respond({"statusCode":200,"message":"ok","result":[{"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com"}}]});
        $scope.emailValidate(0, {"_id":'56b5e059b7d52a8145c13824',"local":{"name":"test","dob":"2/1/2016","rev":0,"email":"'preeti.sachdeva@daffodilsw.com","avatar_url":'ht'}});
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":123});
        $httpBackend.when('GET', 'app/shared/dialogBox/alertView.html').respond();
        $httpBackend.flush();
    }));

});
