describe('E-LEARNING: createGroupController Controller test', function() {
    var ctrl, $scope, $httpBackend, state, $rootScope, $timeout, $uibModalInstance;
    var id = '56dab3598e0f0fa40d98b3c7';


    var getLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": [{
            "_id": "56bdc6e93aa2778521d65fd0",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "sports",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": true
        }, {
            "_id": "56bdc7163aa2778521d65fd1",
            "module": "56bdc69a3aa2778521d65fce",
            "name": "cricket",
            "basic_description": "lesson  two basic_description",
            "detailed_description": "lesson two detailed_description",
            "__v": 0,
            "deleted": false,
            "completed": false
        }],
        "error": false
    };
    var createLesson = {
        "statusCode": 200,
        "message": "ok",
        "result": {
            "__v": 0,
            "createdAt": "2016-04-06T10:31:57.833Z"
        },
        "error": false
    };
    var createLessonError = {
        "statusCode": 409,
        "message": "group name should be unique for a user",
        "result": {
            "__v": 0,
            "createdAt": "2016-04-06T10:31:57.833Z"
        },
        "error": false
    };
    var createLessonError2 = {
        "statusCode": 400,
        "message": "",
        "result": {
            "__v": 0,
            "createdAt": "2016-04-06T10:31:57.833Z"
        },
        "error": false
    };
    var users = {
            "statusCode": 200,
            "message": "ok",
            "result": [{
                "_id": "570e3044215c168c4eef9dfb",
                "createdAt": "2016-04-13T11:40:52.182Z",
                "updatedAt": "2016-04-13T11:40:52.182Z",
                "__v": 0,
                "local": {
                    "dob": "2/23/2012",
                    "name": "kaberi"
                }
            }],
            "error": false
        };

    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));

    beforeEach(inject(function($injector, $state, _$httpBackend_, $controller, _$rootScope_, _$timeout_) {
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        state = $state;
        $timeout = _$timeout_;
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);

        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('createGroupController', {
            $rootScope: $rootScope,
            $scope: $scope,
            state: $state,
            $timeout: $timeout,
            $uibModalInstance: $uibModalInstance,
            moduleId: id
        });
    }));


    it('createGroup method :: should have a createGroup function', inject(function() {
        expect(angular.isFunction($scope.createGroup)).toBe(true);
    }));
    it('createGroup method :: should have a createGroup function definition', inject(function() {
        $scope.createGroup();
    }));
    it('createGroup method :: should have a createGroup function definition', inject(function() {
        $scope.group.name = "gunjan";
        $scope.group.description = undefined;
        $scope.createGroup();
    }));
    it('createGroup method :: should have a createGroup function definition', inject(function() {
        $scope.group.name = "gunjan";
        $scope.group.description = null;
        $scope.createGroup();
    }));
    it('createGroup method :: should have a createGroup function definition', inject(function() {
        $scope.group.description = "hello gunjan";
        $scope.createGroup();
    }));
    it('createGroup method :: should have a createGroup function definition with api call', inject(function() {
        $scope.group.description = "hello gunjan";
        $scope.group.name = "gunjan";
        $scope.location = "abcd";
        $scope.list = [1234, 12345];
        $scope.myGroupsList = [];
        $scope.createGroup();
        $httpBackend.when('POST', '/api/groups').respond(createLesson);
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('createGroup method :: should have a createGroup function definition with api call when error', inject(function() {
        $scope.group.description = "hello gunjan";
        $scope.group.name = "gunjan";
        $scope.location = "abcd";
        $scope.list = [1234, 12345];
        $scope.myGroupsList = [];
        $scope.createGroup();
        $httpBackend.when('POST', '/api/groups').respond(createLessonError);
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('createGroup method :: should have a createGroup function definition with api call when error else part', inject(function() {
        $scope.group.description = "hello gunjan";
        $scope.group.name = "gunjan";
        $scope.location = "abcd";
        $scope.list = [1234, 12345];
        $scope.myGroupsList = [];
        $scope.createGroup();
        $httpBackend.when('POST', '/api/groups').respond(createLessonError2);
        $httpBackend.flush();
        $timeout.flush();
    }));
    // loadMatchingEmails
    it('loadMatchingEmails method :: should have a loadMatchingEmails function', inject(function() {
        expect(angular.isFunction($scope.loadMatchingEmails)).toBe(true);
    }));
    it('loadMatchingEmails method :: should have a loadMatchingEmails function definition', inject(function() {
        var query = "a";
        $scope.loadMatchingEmails(query);
        $httpBackend.when('GET', '/api/users/search?term=a').respond(users);
        $httpBackend.flush();
      }));
    //tagAdded
    it('tagAdded method :: should have a tagAdded function', inject(function() {
        expect(angular.isFunction($scope.tagAdded)).toBe(true);
    }));
    it('tagAdded method :: should have a tagAdded function definition', inject(function() {
        var tag = {};
        tag.text = "mindmax";
        $scope.tagAdded(tag);
    }));
    //tagRemoved
    it('tagRemoved method :: should have a tagRemoved function', inject(function() {
        expect(angular.isFunction($scope.tagRemoved)).toBe(true);
    }));
    it('tagRemoved method :: should have a tagRemoved function definition', inject(function() {
        $scope.tagRemoved();
    }));
    //closeModal
    it('closeModal method :: should have a closeModal function', inject(function() {
        expect(angular.isFunction($scope.closeModal)).toBe(true);
    }));
    it('closeModal method :: should have a closeModal function definition', inject(function() {
        $scope.closeModal();
    }));
    //editAvatar
    it('editAvatar method :: should have a editAvatar function', inject(function() {
        expect(angular.isFunction($scope.editAvatar)).toBe(true);
    }));
    it('editAvatar method :: should have a editAvatar function definition', inject(function() {
        $scope.editAvatar();
    }));
    //upload
    it('upload method :: should have a upload function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $rootScope.isLoggedIn = false;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/jpeg"
            }]
        };
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition', inject(function() {
        var element = {
            "files": [{
                "type": "image/abcd"
            }]
        };
        $scope.fileModel = '';
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(getLesson);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    it('upload method :: should have a upload function definition failure', inject(function() {
        var element = {
            "files": [{
                "type": "image/abcd"
            }]
        };
        $scope.fileModel = '';
        $httpBackend.when('GET', '/api/groups/users?limit=100&skip=0').respond(400);
        $rootScope.isLoggedIn = true;
        $scope.upload(element);
    }));
    //hidePreview
    it('hidePreview method :: should have a hidePreview function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('hidePreview method :: should have a hidePreview function definition', inject(function() {
        $scope.hidePreview();
    }));
    //uploadGroupAvatar
    it('uploadGroupAvatar rootscope method :: should have a uploadGroupAvatar rootscope function', inject(function() {
        $rootScope.$emit("uploadGroupAvatar", {
            avatar_url: "abcd.jpg"
        });
    }));

});
