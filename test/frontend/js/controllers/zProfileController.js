'use strict';
describe('E-LEARNING: profileController Controller test', function(){
    var ctrl, $scope, $httpBackend, $timeout, $state;
    /**
    * Load Sixthcontinent module before execute any test
    */
    beforeEach(module('eLearning'));
    beforeEach(module('stateMock'));
    var loginResult = {
    "statusCode": 200,
    "message": "ok",
    "result": {
        "__v": 0,
        "_id": "56dff82e37bca6b31a4168be",
        "createdAt": "2016-03-09T10:17:19.071Z",
        "updatedAt": "2016-03-09T10:17:19.071Z",
        "local": {
            "dob": "01/03/2016",
            "email": "preeti.sachdeva@daffodilsw.com",
            "name": "Preeti Sachdeva",
            "password": "$2a$08$wJfg24K9Q4b6x5xfS2.dnup1Pr5M.u21eLbMWdt3xu0jpgU5HR71a",
            "deleted": false,
            "rev": 0,
            "avatar_url": "https://mindmaxdaffo.s3-ap-northeast-1.amazonaws.com/26026225-3d-rojo-letra-W-fondo-blanco-aislado-Foto-de-archivo.jpg1460114314",
            "ambassdor": false,
            "lastlogin_at": "2016-03-09T10:17:18.972Z"
        }
    },
    "error": false
};
    /**
    * Inject required dependencies as $httpBackend, $controller and $rootscope etc
    */
    beforeEach(inject(function($injector, _$httpBackend_,_$state_, _$timeout_, $controller, $rootScope){
        $httpBackend = _$httpBackend_;
        $timeout = _$timeout_;
        $state = _$state_
        spyOn( $state, 'go' );
        spyOn( $state, 'transitionTo' );
        //create a new scope that's a child scope of $rootscope
        $scope = $rootScope.$new();

        //create contorller
        ctrl = $controller('profileController', {
            $scope: $scope
        });
    }));
    it('variables  test :: should have a equall value', inject(function() {
        expect($scope.activeAction).toEqual(false);
    }));
    it('open1 method :: should have a open1 function', inject(function() {
        expect(angular.isFunction($scope.open1)).toBe(true);
    }));
    it('changeImage method :: should have a changeImage function', inject(function() {
        expect(angular.isFunction($scope.changeImage)).toBe(true);
    }));
    it('openEditForm method :: should have a openEditForm function', inject(function() {
        expect(angular.isFunction($scope.openEditForm)).toBe(true);
    }));
    it('changeName method :: should have a changeName function', inject(function() {
        expect(angular.isFunction($scope.changeName)).toBe(true);
    }));
    it('open1 method :: should have a open1 body', inject(function() {
        $scope.open1();
    }));
    it('cancelSave method :: should have a cancelSave function', inject(function() {
        expect(angular.isFunction($scope.cancelSave)).toBe(true);
    }));
    it('changeEmail method :: should have a changeEmail function', inject(function() {
        expect(angular.isFunction($scope.changeEmail)).toBe(true);
    }));
    it('upload method :: should have a upload function', inject(function() {
        expect(angular.isFunction($scope.upload)).toBe(true);
    }));
    it('changePassword method :: should have a changePassword function', inject(function() {
        expect(angular.isFunction($scope.changePassword)).toBe(true);
    }));
    it('deleteMyAccount method :: should have a deleteMyAccount function', inject(function() {
        expect(angular.isFunction($scope.deleteMyAccount)).toBe(true);
    }));
    it('profile service method :: should have a service called', inject(function() {
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.flush();
    }));
    it('openEditForm method :: should have a openEditForm body', inject(function() {
        $scope.openEditForm(1);
    }));
    it('openEditForm method :: should have a openEditForm body', inject(function() {
        $scope.openEditForm(2);
    }));
    it('openEditForm method :: should have a openEditForm body', inject(function() {
        $scope.openEditForm(3);
    }));
    it('openEditForm method :: should have a openEditForm body', inject(function() {
        $scope.openEditForm(4);
    }));
    it('openEditForm method :: should have a openEditForm body', inject(function() {
        $scope.editUser = {"password":''};
        $scope.openEditForm(5);
    }));
    it('cancelSave method :: should have a cancelSave body', inject(function() {
        $scope.cancelSave(1);
    }));
    it('cancelSave method :: should have a cancelSave body', inject(function() {
        $scope.cancelSave(2);
    })); it('cancelSave method :: should have a cancelSave body', inject(function() {
        $scope.cancelSave(3);
    })); it('cancelSave method :: should have a cancelSave body', inject(function() {
        $scope.cancelSave(4);
    }));
    it('cancelSave method :: should have a cancelSave body', inject(function() {
        $scope.cancelSave(5);
    }));
    it('hidePreview method :: should have a hidePreview function', inject(function() {
        expect(angular.isFunction($scope.hidePreview)).toBe(true);
    }));
    it('changeName method :: should have a changeName function', inject(function() {
        $scope.editUser = {};
        $scope.editUser.name = undefined;
        $scope.changeName();
    }));
    it('changeName method :: should have a changeName function', inject(function() {
        $scope.editUser = {};
        $scope.editUser.name = '';
        $scope.changeName();
    }));
    it('changeName method :: should have a changeName function', inject(function() {
        $scope.editUser = {};
        $scope.editUser.name = 'Preeti Sachdeva';
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('PUT', '/api/users/56dff82e37bca6b31a4168be/name').respond({"statusCode":200,"message":"ok"});
        $scope.changeName();
        $httpBackend.flush();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};$scope.editUser.email = undefined; $scope.changeEmail();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};$scope.editUser.email = ''; $scope.changeEmail();
    }));
     it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};$scope.editUser.email = 'preeti.sachdeva@daffodilsw.com';
        $scope.showErrEmail = true;
        //$httpBackend.when('GET', '/profile').respond(loginResult);
       // $httpBackend.when('PUT', '/users/56dff82e37bca6b31a4168be/email').respond({"statusCode":200,"message":"ok","result":{"message":"verified"}});
        $scope.changeEmail();
       // $httpBackend.flush();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};
        $scope.editUser.email = 'preeti.sachdeva@daffodilsw.com';
        $scope.showErrEmail = false;
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":200,"message":"ok","result":{"message":"verified"}});
        $httpBackend.when('PUT', '/api/users/56dff82e37bca6b31a4168be/email').respond({"statusCode":200,"message":"ok"});
        $scope.changeEmail();
        $httpBackend.flush();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};
        $scope.editUser.email = 'preeti.sachdeva@dafefodilsw.com';
        $scope.showErrEmail = false;
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":422,"message":"email already registered"});
        $scope.changeEmail();
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};
        $scope.editUser.email = 'preeti.sachdeva@dafefodilsw.com';
        $scope.showErrEmail = false;
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":422,"message":"email should be registered with any email provider"});
        $scope.changeEmail();
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('changeEmail method :: should have a changeEmail body', inject(function() {
        $scope.editUser = {};
        $scope.editUser.email = 'preeti.sachdeva@dafefodilsw.com';
        $scope.showErrEmail = false;
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('POST', '/api/auth/validateEmail').respond({"statusCode":42,"message":"email should be registered with any email provider"});
        $scope.changeEmail();
        $httpBackend.flush();
        $timeout.flush();
    }));
    it('changePassword method :: should have a changePassword function', inject(function() {
        $scope.editUser = {};$scope.editUser.password = undefined; $scope.changePassword();
    }));
    it('changePassword method :: should have a changePassword function', inject(function() {
        $scope.editUser = {};$scope.editUser.password = ''; $scope.changePassword();
    }));
    it('changePassword method :: should have a changePassword function', inject(function() {
        $scope.editUser = {};$scope.editUser.password = '1234567';
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('PUT', '/api/users/56dff82e37bca6b31a4168be/password').respond({"statusCode":200,"message":"ok"});
        $scope.changePassword();
        $httpBackend.flush();
    }));
    it('hidePreview method :: should have a hidePreview body', inject(function() {
        $scope.hidePreview();
    }));
    it('changeImage method :: should have a changeImage function', inject(function() {
        $scope.location = '';
        $scope.changeImage();
    }));
    it('changeImage method :: should have a changeImage function', inject(function() {
        $scope.location = undefined;
         $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $scope.changeImage();
        $timeout.flush();
    }));
    it('changeImage method :: should have a changeImage function', inject(function() {
        $scope.location = 'https://mindmaxdaffo.s3-ap-northeast-1.amazonaws.com/26026225-3d-rojo-letra-W-fondo-blanco-aislado-Foto-de-archivo.jpg1460114314';
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $scope.changeImage();
        $httpBackend.when('PUT', '/api/users/56dff82e37bca6b31a4168be/avatar').respond({"statusCode":200,"message":"ok"});
         $httpBackend.flush();
    }));

    it('upload method :: should have a upload function', inject(function() {
       var element = {"files":[{"type":"image/jpeg"}]};
       $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $scope.upload(element);
        $timeout.flush();
    }));
    /*it('upload method :: should have a upload function', inject(function() {
       var element = {};
       $httpBackend.when('GET', '/profile').respond(loginResult);
        $scope.upload(element);
        $timeout.flush();
    })); */
    it('changeDob method :: should have a changeDob function', inject(function() {
        $scope.editUser = {};$scope.dt = undefined;
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $scope.changeDob();
    }));
     it('changeDob method :: should have a changeDob function', inject(function() {
        $scope.editUser = {};
        $scope.dt = "Thu Mar 03 2016 00:00:00 GMT+0530 (IST)";
        $httpBackend.when('GET', '/api/profile').respond(loginResult);
        $httpBackend.when('PUT', '/api/users/56dff82e37bca6b31a4168be/dob').respond({"statusCode":200,"message":"ok"});
        $scope.changeDob();
        $httpBackend.flush();
    }));
    it('deleteMyAccount method :: should have a deleteMyAccount body', inject(function() {
    //    $httpBackend.when('GET', '/profile').respond(loginResult);
       // $httpBackend.when('DELETE', '/users/56dff82e37bca6b31a4168be').respond({"statusCode":200,"message":"ok","result":{"message":"user deleted"}});
      //  spyOn($state, 'go');
     //   $scope.changeDob();

        $scope.deleteMyAccount();
       // $httpBackend.flush();

    }));
    it('deleteMyAccount method :: should have a deleteMyAccount body', inject(function() {
///$httpBackend.when('GET', '/profile').respond(loginResult);
      //  $httpBackend.when('DELETE', '/users/56dff82e37bca6b31a4168be').respond({"statusCode":200});
       // $scope.changeDob();$scope.deleteMyAccount();
      //  $httpBackend.flush();
    //    $timeout.flush();
    }));
});
