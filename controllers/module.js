"use strict";
var Module = require('../models/modules');
var Lesson = require('../models/lessons');
var CompletedLesson = require('../models/completedLesson');
var helper = require('../helper/response');
var validator = require('validator');
var q = require('q');
var _ = require('lodash');

module.exports = {
	addModule : function(req, res, next){
		Module.findOne({name:{ $regex: new RegExp("^" + req.body.name.toLowerCase(), "i") },deleted:false})
		.then(function(module){
			if(!module){
				(new Module(req.body)).save(function(err, module){
					if(err){
						res.json(helper.responseObject(422, err, null, true));
					}else {
						req.result = module;
						next();				
					}
				});
			}else{
				throw({message:"Module name must be unique"});
			}
		})
		.catch(function(err){
			res.json(helper.responseObject(422, err, null, true));
		});
	},
	getModule : function(req, res, next){
		if(validator.isMongoId(req.params.id)){
			Module.findOne({_id:req.params.id,deleted:false})
			.then(function(module){
				req.result = module;
				next();
			})
			.catch(function(err){
				res.json(helper.responseObject(422, err, null, true));
			})
		}else{
			res.json(handler.handleError(404, "Not Found"));
		}
	},
	getAllModule : function(req, res, next){

		let limit = req.query.limit ? parseInt(req.query.limit) : 5;
		let skip = req.query.skip ? parseInt(req.query.skip) : 0;

		q.all([
			Module.count({deleted:false}, {}, { sort: { 'createdAt': -1 } }),
			Module.find({deleted:false,name:new RegExp(req.query.name,"i")}, {}, { sort: { 'createdAt': -1 } }).skip(skip).limit(limit)
		]).spread(function(totalCount, models){
			let result = {};
			result.module = models;
			result.totalCount = totalCount;
			req.result = result;
			next();
		}).then(function(err){
			res.json(helper.responseObject(422, err, null, true));
		})
	},
	getModuleLessions: function(req, res, next){
		let limit = req.query.limit ? parseInt(req.query.limit) : 50;
		let skip = req.query.skip ? parseInt(req.query.skip) : 0;
		if (req.isAuthenticated()){
			q.all([
				Lesson.find({module:req.params.id,name:new RegExp(req.query.name,"i"), deleted: false}, {}, { sort: { 'createdAt': -1 } }).skip(skip).limit(limit),
				CompletedLesson.find({user:req.user._id}, {}, { sort: { 'createdAt': -1 } })	
			]).spread(function(lessons, completedLessons){
				var userLessons = [];
				var userLesson;
				lessons.forEach(function(lesson, index) {
					if(_.find(completedLessons, ['lesson', lesson._id])){
						userLesson = lessons[index].toObject();
						userLesson.completed = true;
						userLessons.push(userLesson);
					}else{
						userLesson = lessons[index].toObject();
						userLesson.completed = false;
						userLessons.push(userLesson);
					}
				});
				req.result = userLessons;
				next();
			});
		}else{
			Lesson.find({module:req.params.id,name:new RegExp(req.query.name,"i")},function(err, lessons){
				if(err)
					res.json(helper.responseObject(422, err, null, true));
				req.result = lessons;
				next();
			});
		}
	},
	editModule: function(req, res, next){
		if(validator.isMongoId(req.params.id)){
			var editModule = {
				name:req.body.name,
				description:req.body.description
			}
			Module.update({_id:req.params.id},{ $set : editModule},function(err, change){
				if(err)
					res.json(helper.handleError(422, err));
				if(change.ok === 1 && change.n === 1){
					req.result = change;
					next();
				}else{
					res.json(helper.handleError(422, "module not found sorry"));
				}
			})
		}else{
			res.json(handler.handleError(404, "Not Found"));
		}
	},
	deleteModule : function(req, res, next){
		Module.update({ _id: req.params.id }, { $set: {deleted: true } }, function(err, change) {
			if(err)
				res.json(helper.handleError(422, err));
			if(!(change.ok === 1 && change.n === 1)) {
				res.json(helper.handleError(422, "module not found sorry"));
			}
			req.result = change;
			next();
		});
	},
	searchModule : function(req, res, next){
		let term = req.query.term ? req.query.term : '';
		
		Module.find({ deleted: false, name: new RegExp(req.query.term, "i") }, 'name', { sort: { 'createdAt': -1 } }, function(err, modules) {
			req.result = modules;
			next();
		});
	}
}