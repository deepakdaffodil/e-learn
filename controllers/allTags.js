var AllTags = require('../models/allTags');
var helper = require('../helper/response');

module.exports = {
    addTags: function(tags, done) {
        if(!tags)
            tags = [];
        
        AllTags.update({}, { $addToSet: { tags: { $each: tags } } }, { upsert: true }, function(err, numAffected) {
            if(err)
                return done(err);
            
            done();
        });
    },
    searchTags: function(req, res, next) {
        var limit = req.query.limit ? parseInt(req.query.limit) : 12;
        var skip = req.query.skip ? parseInt(req.query.skip) : 0;
        var term = req.query.term ? req.query.term : '';
        
        AllTags.findOne(function(err, allTags) {
            if (err)
                res.json(helper.handleError(422, err));
            if(!allTags)
                allTags = {
                    tags: []
                };
            var result = {
                tags: allTags.tags.filter(function(tag) {
                    return tag.indexOf(term) > -1;
                })
            };
            result.totalCount = result.tags.length;
            result.tags = result.tags.splice(skip, limit);
            
            req.result = result;
            next();
        });
    }
};