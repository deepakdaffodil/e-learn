"use strict";
var User = require('../models/users');
var ActivityNotification = require('../models/activityNotifications');
var GroupNotification = require('../models/groupNotifications');
var helper = require('../helper/response');
var validator = require('validator');
var authHelper = require('../helper/authentication');
var config = require('../config/config');
var q = require('q');
var _ = require('lodash');
var config = require('../config/config');

module.exports = {

    editUser: function(req, res, next) {

        User.findById(req.user._id, function(err, user) {
            if (err)
                res.json(helper.responseObject(422, err, null, true));
            if (req.body.rev && (user.local.rev == req.body.rev)) {
                var update = {
                    local: {}
                };
                update.local.avatar_url = req.body.avatar_url ? req.body.avatar_url : user.local.avatar_url;
                update.local.name = req.body.name ? req.body.name : user.local.name;
                update.local.dob = req.body.dob ? req.body.dob : user.local.dob;
                update.local.rev = Number(req.body.rev) ? Number(req.body.rev) + 1 : Number(user.local.rev) + 1;
                update.local.password = user.local.password;
                update.local.email = user.local.email;

                update.local.lastlogin_at = user.local.lastlogin_at;
                update.local.deleted = user.local.deleted;

                User.update({
                    _id: req.user._id
                }, {
                    $set: update
                }, {
                    multi: true
                }, function(err, user) {
                    if (err)
                        res.json(helper.responseObject(422, err, null, true));
                    if (user.ok == 1) {
                        User.findById(req.user._id, function(err, updatedUser) {
                            if (err)
                                res.json(helper.responseObject(422, err, null, true));
                            if (updatedUser) {
                                req.user = updatedUser;
                                req.result = updatedUser;
                                next();
                            }
                        })
                    }
                })
            } else {
                res.json(helper.responseObject(409, {
                    message: 'someone else updated the value just before',
                    user: user
                }, null, true));
            }
        })
    },

    editUserByAdmin: function(req, res, next) {
        User.findById(req.params.id, function(err, user) {
            if (err)
                res.json(helper.responseObject(422, err, null, true));
            if (req.body.rev && user.local.rev == req.body.rev) {
                var update = {
                    local: {}
                };
                update.local.avatar_url = req.body.avatar_url ? req.body.avatar_url : user.local.avatar_url;
                update.local.name = req.body.name ? req.body.name : user.local.name;
                update.local.dob = req.body.dob ? req.body.dob : user.local.dob;
                update.local.rev = Number(req.body.rev) ? Number(req.body.rev) + 1 : Number(user.local.rev) + 1;
                update.local.password = req.body.password ? user.generateHash(req.body.password) : user.local.password;
                update.local.email = req.body.email ? req.body.email : user.local.email;
                var newPassword = req.body.password ? req.body.password : "not change";
                update.local.deleted = user.local.deleted;
                update.local.lastlogin_at = user.local.lastlogin_at;
                update.local.ambassdor = user.local.ambassdor;
                update.local.active = user.local.active;

                User.update({
                    _id: req.params.id
                }, {
                    $set: update
                }, {
                    multi: true
                }, function(err, change) {
                    if (err)
                        res.json(helper.responseObject(422, err, null, true));
                    if (change.ok == 1) {
                        User.findById(req.params.id, function(err, updatedUser) {
                            if (err)
                                res.json(helper.responseObject(422, err, null, true));
                            if (updatedUser) {
                                var url = "http://" + config.server.host + ":" + config.server.port;
                                var mailOptions = {
                                    from: "MIND MAX " + config.mailer.user,
                                    to: user.local.email,
                                    subject: "Profile updated",
                                    text: "",
                                    html: "<div>Your Profile is updated by admin <div>Here is your new updates</div><div>name: " + update.local.name + "</div><div>BOD: " + update.local.dob + "</div><div>email:" + update.local.email + "</div><div>password: " + newPassword + "</div><span>To login click here " + url + "</span></div>" // html body
                                }

                                var smtpTransport = helper.mailer();
                                smtpTransport.sendMail(mailOptions, function(err, response) {
                                    if (err)
                                        res.json(helper.responseObject(422, err, null, true));
                                    smtpTransport.close();
                                });
                                req.result = updatedUser;
                                next();
                            }
                        })
                    }
                })
            } else {
                res.json(helper.responseObject(409, {
                    message: 'someone else updated the value just before',
                    user: user
                }, null, true));
            }
        })
    },

    deleteUser: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.findOne({
                _id: req.params.id
            }, function(err, user) {
                if (err)
                    res.json(helper.handleError(422, err));

                else if (!user)
                    res.json(helper.responseObject(422, {
                        message: "Given email is not register"
                    }, null, true));

                user.local.deleted = true;
                user.save(function(err) {
                    if (err)
                        res.json(helper.responseObject(422, {
                            message: "user not found"
                        }, null, true));

                    if (req.user.local.ambassdor) {
                        var mailOptions = {
                            from: "MIND MAX " + config.mailer.user,
                            to: user.local.email,
                            subject: "Account deleted",
                            text: "",
                            html: helper.deleteTemplate(user.local.name)
                        };

                        var smtpTransport = helper.mailer();
                        smtpTransport.sendMail(mailOptions, function(err, response) {
                            if (err)
                                console.log('Mail couldn\'t be sent: ', err);
                            else {
                                console.log('Mail sent successfully');
                            }
                            smtpTransport.close();
                        });
                    }

                    req.result = {
                        message: "user deleted"
                    };
                    next();
                });
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    deleteUserPhysically: function(req, res, next) {
        User.findOne({
            _id: req.params.id
        }, function(err, user) {
            if (err)
                res.json(helper.responseObject(422, err, null, true));
            else if (user) {
                user.remove(function(err, removed) {
                    if (err)
                        res.json(helper.responseObject(422, err, null, true));
                    if (removed) {
                        req.result = {
                            message: "user deleted"
                        };
                        next();
                    } else {
                        req.result = {
                            message: "Not deleted"
                        };
                        next();
                    }
                })
            } else
                res.json(helper.responseObject(422, {
                    message: "user not found"
                }, null, true));
        });
    },
    getNotification: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            var limit = req.query.limit ? parseInt(req.query.limit) : 12;
            var skip = req.query.skip ? parseInt(req.query.skip) : 0;
            q.all([
                GroupNotification.count({
                    to: req.params.id,
                    status: 'deliver',
                    deleted: false
                }),
                ActivityNotification.count({
                    activityOwner: req.params.id,
                    deleted: false,
                    status: 'deliver',
                    from: {
                        $ne: req.params.id
                    }
                }),
                GroupNotification.count({
                    to: req.params.id,
                    deleted: false
                }),
                ActivityNotification.count({
                    activityOwner: req.params.id,
                    deleted: false,
                    from: {
                        $ne: req.params.id
                    }
                }),
                GroupNotification.find({
                    to: req.params.id,
                    deleted: false
                }, {}, {
                    sort: {
                        'createdAt': -1
                    }
                }).populate('from').populate('group', 'name'),
                ActivityNotification.find({
                    activityOwner: req.params.id,
                    deleted: false,
                    from: {
                        $ne: req.params.id
                    }
                }, {}, {
                    sort: {
                        'createdAt': -1
                    }
                }).populate('activity').populate('from')
            ]).spread(function(grpNtnCount, actNtnCount, grpNtnTCount, actNtnTCount, groupNotifications, activityNotifications) {
                let result = {};
                for (var i = 0; i < groupNotifications.length; i++) {
                    if (!groupNotifications[i].group) {
                        groupNotifications.splice(i--, 1);
                        grpNtnTCount--;
                    }
                }
                for (var j = 0; j < activityNotifications.length; j++) {
                    if (!activityNotifications[j].activity) {
                        activityNotifications.splice(j--, 1);
                        actNtnTCount--;
                    }
                }

                let grpAndActivitynotifications = groupNotifications.concat(activityNotifications);
                grpAndActivitynotifications.sort(function(a, b) {
                    return (a.createdAt > b.createdAt) ? -1 : ((b.createdAt > a.createdAt) ? 1 : 0);
                });
                grpAndActivitynotifications.splice(0, skip);
                grpAndActivitynotifications.splice(5, grpAndActivitynotifications.length);

                result.notifications = grpAndActivitynotifications;
                result.totalCount = grpNtnTCount + actNtnTCount;
                result.newNoticationCount = grpNtnCount + actNtnCount;
                req.result = result;
                next();
            }).catch(function(err) {
                res.json(helper.handleError(422, err));
            });
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    changePassword: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            var user = new User();
            var password = user.generateHash(req.body.password);
            User.update({
                _id: req.params.id
            }, {
                $set: {
                    'local.password': password
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "user not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    changeName: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.update({
                _id: req.params.id
            }, {
                $set: {
                    'local.name': req.body.name
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "user not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    changeEmail: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.update({
                _id: req.params.id
            }, {
                $set: {
                    'local.email': req.body.email
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "user not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    changeDob: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.update({
                _id: req.params.id
            }, {
                $set: {
                    'local.dob': req.body.dob
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "user not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    changeAvatar: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.update({
                _id: req.params.id
            }, {
                $set: {
                    'local.avatar_url': req.body.avatar_url
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "user not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },

    getAllUser: function(req, res, next) {
        let limit = req.query.limit ? parseInt(req.query.limit) : 5;
        let skip = req.query.skip ? parseInt(req.query.skip) : 0;

        q.all([
            User.count({
                'local.ambassdor': false,
                'local.deleted': false
            }, {}, {
                sort: {
                    'createdAt': -1
                }
            }),
            User.find({
                'local.ambassdor': false,
                'local.deleted': false,
                'local.name': new RegExp(req.query.name, "i")
            }, {}, {
                sort: {
                    'createdAt': -1
                }
            }).skip(skip).limit(limit)
        ]).spread(function(totalCount, users) {
            let result = {};
            result.totalCount = totalCount;
            result.users = users;
            req.result = result;
            next();
        }).then(function(err) {
            res.json(helper.responseObject(422, err, null, true));

        })
    },

    getUserByEmail: function(req, res, next) {
        User.find({
            "local.email": new RegExp(req.query.email, "i")
        }, {}, {
            sort: {
                'createdAt': -1
            }
        }, function(err, users) {
            if (err)
                res.json(helper.responseObject(422, err, null, true));
            else {
                req.result = users;
                next();
            }
        });
    },
    getUserById: function(req, res, next) {
        if (validator.isMongoId(req.params.id)) {
            User.find({
                _id: req.params.id
            }, {}, {
                sort: {
                    'createdAt': -1
                }
            }, function(err, user) {
                if (err)
                    res.json(helper.responseObject(422, err, null, true));
                else {
                    req.result = user;
                    next();
                }
            });
        }
    },
    getUserByName: function(req, res, next) {
        User.find({
            "local.name": new RegExp(req.query.name, "i")
        }, {}, {
            sort: {
                'createdAt': -1
            }
        }, function(err, users) {
            if (err)
                res.json(helper.responseObject(422, err, null, true));
            else {
                req.result = users;
                next();
            }
        });
    },
    searchUserByName: function(req, res, next) {
        User.find({
                $or: [{
                    "local.name": new RegExp(req.query.term, "i")
                }, {
                    "local.email": new RegExp(req.query.term, "i")
                }],
                'local.deleted': false
            },
            'local.email local.ambassdor local.name',
            function(err, foundUsers) {
                if (err)
                    res.json(helper.responseObject(422, err, null, true));
                else {
                    var result = [];
                    if (foundUsers.length <= 0) {
                        req.result = [];
                        next();
                    }

                    for (var i = 0; i < foundUsers.length; i++) {
                        if (req.user && foundUsers[i].local.email === req.user.local.email || foundUsers[i].local.ambassdor)
                            foundUsers.splice(i--, 1);
                        else {
                            if(req.url.indexOf('/names') <= -1)
                                result[i] = foundUsers[i].local.email;
                            else
                                result[i] = {
                                    name: foundUsers[i].local.name,
                                    _id: foundUsers[i]._id
                                };
                        }

                        if (i + 1 == foundUsers.length) {
                            req.result = result;
                            next();
                        }
                    }
                }
            });
    },
    seenNotification: function(req, res, next) {
        if (validator.isMongoId(req.params.id) && validator.isMongoId(req.params.nid)) {
            ActivityNotification.update({
                _id: req.params.nid
            }, {
                $set: {
                    'status': 'seen'
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "notification not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    unseenNotification: function(req, res, next) {
        q.all([
            ActivityNotification.update({
                activityOwner: req.params.id,
                'status': 'deliver'
            }, {
                $set: {
                    'status': 'unseen'
                }
            }, {
                multi: true
            }),
            GroupNotification.update({
                to: req.params.id,
                'status': 'deliver'
            }, {
                $set: {
                    'status': 'unseen'
                }
            }, {
                multi: true
            })
        ]).spread(function(changeAct, changeGro) {
            req.result = changeAct;
            next();
        }).catch(function(err) {
            res.json(helper.handleError(422, err));
        });
    },
    getAllNotification: function(req, res, next) {
        q.all([
            ActivityNotification.find(),
            GroupNotification.find()
        ]).spread(function(activityNotifications, groupNotifications) {
            req.result = activityNotifications.concat(groupNotifications);
            next();
        }).catch(function(err) {
            res.json(helper.handleError(422, err));
        });
    },
    seenGrpNotification: function(req, res, next) {
        if (validator.isMongoId(req.params.id) && validator.isMongoId(req.params.nid)) {
            GroupNotification.update({
                _id: req.params.nid
            }, {
                $set: {
                    'status': 'seen'
                }
            }, function(err, change) {
                if (err)
                    res.json(helper.handleError(422, err));
                if (change.ok === 1 && change.n === 1) {
                    req.result = change;
                    next();
                } else {
                    res.json(helper.handleError(422, "notification not found sorry"));
                }
            })
        } else {
            res.json(handler.handleError(404, "Not Found"));
        }
    },
    adminUserRegister: function(req, res, next) {
        User.findOne({ 'local.email':  req.body.email, 'local.deleted': false }, function(err, user) {
            if (err)
                return next(err);
            if (user)
                return res.json(helper.handleError(422, "Email already registered"));
            var newUser = new User();
            newUser.local.email = req.body.email;
            newUser.local.password = newUser.generateHash(req.body.password);
            newUser.local.name = req.body.name;
            newUser.local.dob = req.body.dob;

            newUser.save(function(err) {
                if (err)
                    return next(err);
                var result = {};
                req.result = newUser;
                next();
            });
        });
    },
    getFollowers: function (req, res, next) {
        if(process.env.TEST)
            req.user = {
                _id: req.query.id
            };
        
        User.findOne({ _id: req.user._id }, 'local.followers').populate('local.followers').exec(function (err, followers) {
            if (err)
                return res.json(helper.handleError(422, err));
            req.result = followers.local.followers;
            next();
        });
    },
    getFollowing: function (req, res, next) {
        if(process.env.TEST)
            req.user = {
                _id: req.query.id
            };
        
        User.findOne({ _id: req.user._id }, 'local.following').populate('local.following').exec(function (err, following) {
            if (err)
                return res.json(helper.handleError(422, err));
            req.result = following.local.following;
            next();
        });
    },
    addFollowing: function (req, res, next) {
        if(process.env.TEST)
            req.user = {
                _id: req.query.id,
                local: {
                    following: []
                }
            };
        
        if (!validator.isMongoId(req.params.id)) {
            return res.json(helper.handleError(404, 'Not found'));
        }
        if(req.params.id === req.user._id)
            return res.json(helper.handleError(422, 'Cannot follow oneself'));
        
        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err)
                return res.json(helper.handleError(422, err));
            if (!user)
                return res.json(helper.handleError(422, 'No user found'));

            if (user.local.followers.filter(function (v) {
                return v.toString() === req.user._id.toString();
            }).length > 0 || req.user.local.following.filter(function (v) {
                return v.toString() === user._id.toString();
            }).length > 0)
                return res.json(helper.handleError(422, 'Already following'));

            user.local.followers.push(req.user);
            user.save(function (err) {
                if (err)
                    return res.json(helper.handleError(422, err));

                req.user.local.following.push(user);
                req.user.save(function (err) {
                    if (err)
                        return res.json(helper.handleError(422, err));
                    next();
                });
            });
            next();
        });
    },
    removeFollowing: function (req, res, next) {
        if(process.env.TEST)
            req.user = {
                _id: req.query.id,
                local: {
                    following: []
                },
                save: function (next) {
                    next();
                }
            };
        
        if (!validator.isMongoId(req.params.id))
            return res.json(helper.handleError(404, 'Not found'));

        User.findOne({ _id: req.params.id }, function (err, user) {
            if (err)
                return res.json(helper.handleError(422, err));
            if (!user)
                return res.json(helper.handleError(422, 'No user found'));
                
            if (req.query.follower && req.query.follower.toString() === 'true') {
                user.local.following = user.local.following.filter(function (v) {
                    return v.toString() !== req.user._id.toString();
                });
                req.user.local.followers = req.user.local.followers.filter(function (v) {
                    return v.toString() !== user._id.toString();
                });
            }
            else {
                user.local.followers = user.local.followers.filter(function (v) {
                    return v.toString() !== req.user._id.toString();
                });
                req.user.local.following = req.user.local.following.filter(function (v) {
                    return v.toString() !== user._id.toString();
                });
            }

            user.save(function (err) {
                if (err)
                    return res.json(helper.handleError(422, err));

                req.user.save(function (err) {
                    if (err)
                        return res.json(helper.handleError(422, err));
                    next();
                });
            });
        });
    }
};
