var Lesson = require('../models/lessons');
var helper = require('../helper/response');
var validator = require('validator');
var CompletedLesson = require('../models/completedLesson');

module.exports = {
	addLesson: function (req, res, next) {
		Lesson.findOne({ name: { $regex: new RegExp("^" + req.body.name.toLowerCase(), "i") }, module: req.body.module, deleted: false })
			.then(function (lesson) {
				if (!lesson) {
					if (req.body.type === 'activity') {
						if (!req.body.Keytakeaways)
							return res.json(helper.responseObject(422, 'Keytakeaways required'));
						if (!req.body.activity)
							return res.json(helper.responseObject(422, 'Activity required'));
						if (!req.body.detail_description)
							return res.json(helper.responseObject(422, 'Detail description required'));
					}
					else if (req.body.type === 'image' || req.body.type === 'video') {
						if (!req.body.url)
							return res.json(helper.responseObject(422, 'URL required'));
					}
					else if (req.body.type !== 'text')
						return res.json(helper.responseObject(422, 'Invalid type'));

					(new Lesson(req.body)).save(function (err, lesson) {
						if (err) {
							res.json(helper.responseObject(422, err, null, true));
						} else {
							req.result = lesson;
							next();
						}
					});
				} else {
					throw ({ message: "lesson name must be unique" });
				}
			})
			.catch(function (err) {
				res.json(helper.responseObject(422, err.message, null));
			});
	},
	getAllLesson: function (req, res, next) {
		Lesson.find({ deleted: false })
			.then(function (lessons) {
				req.result = lessons;
				next();
			})
			.catch(function (err) {
				res.json(helper.responseObject(422, err, null, true));
			});
	},
	getLesson: function (req, res, next) {

		if (validator.isMongoId(req.params.id)) {
			Lesson.findOne({ _id: req.params.id, deleted: false })
				.then(function (lesson) {
					req.result = lesson;
					next();
				})
				.catch(function (err) {
					res.json(helper.responseObject(422, err, null, true));
				});
		} else {
			res.json(handler.handleError(404, "Not Found"));
		}
	},
	editLesson: function (req, res, next) {
		if (validator.isMongoId(req.params.id)) {
			if (req.body.type === 'activity') {
				if (!req.body.Keytakeaways)
					return res.json(helper.responseObject(422, 'Keytakeaways required'));
				if (!req.body.activity)
					return res.json(helper.responseObject(422, 'Activity required'));
				if (!req.body.detail_description)
					return res.json(helper.responseObject(422, 'Detail description required'));
			}
			else if (req.body.type === 'image' || req.body.type === 'video') {
				if (!req.body.url)
					return res.json(helper.responseObject(422, 'URL required'));
			}
			else if (req.body.type !== 'text')
				return res.json(helper.responseObject(422, 'Invalid type'));

			var editLesson = {
				name: req.body.name,
				activity: req.body.activity,
				Keytakeaways: req.body.Keytakeaways,
				basic_description: req.body.basic_description,
				detail_description: req.body.detail_description,
				url: req.body.url,
				type: req.body.type
			};

			Lesson.update({ _id: req.params.id }, { $set: editLesson }, function (err, change) {
				if (err)
					res.json(helper.handleError(422, err));
				if (change.ok === 1 && change.n === 1) {
					req.result = change;
					next();
				} else {
					res.json(helper.handleError(422, "lesson not found sorry"));
				}
			});
		} else {
			res.json(handler.handleError(404, "Not Found"));
		}
	},
	deleteLesson: function (req, res, next) {
		Lesson.update({ _id: req.params.id }, { $set: { deleted: true } }, function (err, change) {
			if (err)
				res.json(helper.handleError(422, err));
			if (!(change.ok === 1 && change.n === 1)) {
				res.json(helper.handleError(422, "lesson not found sorry"));
			}
			req.result = change;
			next();
		});
	},
	addCompletedLesson: function (req, res, next) {
		if (validator.isMongoId(req.body.lesson) && validator.isMongoId(req.body.user) && validator.isMongoId(req.body.module)) {
			CompletedLesson.findOne({ user: req.body.user, lesson: req.body.lesson }, function (err, completedLesson) {
				if (err)
					res.json(helper.responseObject(422, err, null, true));
				else if (completedLesson) {
					req.result = { message: "Lesson Already Comleted" };
					next();
				} else {
					(new CompletedLesson(req.body)).save(function (err, completedLesson) {
						if (err)
							res.json(helper.responseObject(422, err, null, true));
						req.result = { message: "Lesson Comleted" };
						next();
					});
				}
			});
		} else {
			res.json(handler.handleError(404, "Not Found"));
		}
    },
    getCompletedLessons: function (req, res, next) {

		CompletedLesson.find({}, function (err, completedLesson) {
			if (err)
				res.json(helper.responseObject(422, err, null, true));
			req.result = completedLesson;
			next();
		});
    },
    getCompletedLesson: function (req, res, next) {

		if (validator.isMongoId(req.params.id)) {
			CompletedLesson.findOne({ _id: req.params.id }, function (err, completedLesson) {
				if (err)
					res.json(helper.responseObject(422, err, null, true));
				req.result = completedLesson;
				next();
			});
		} else {
			res.json(handler.handleError(404, "Not Found"));
		}
    },
    deleteCompletedLesson: function (req, res, next) {
		CompletedLesson.remove({ _id: req.params.id })
			.then(function (completedLesson) {
				req.result = completedLesson;
				next();
			})
			.catch(function (err) {
				res.json(helper.responseObject(422, err, null, true));
			});
    },
	getCompletedLessonByModule: function (req, res, next) {
		if(process.env.TEST)
			req.user = {
				_id: req.query.id
			};
		
		if (validator.isMongoId(req.params.id)) {
			CompletedLesson.find({ user: req.user._id, module: req.params.id }, function (err, completedLesson) {
				if (err)
					res.json(helper.responseObject(422, err, null, true));
				req.result = completedLesson;
				next();
			});
		} else {
			res.json(handler.handleError(404, "Not Found"));
		}
	}
};
