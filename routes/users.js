// Created By Nitesh Jatav  on 10/02/2016
var express = require('express');
var userController = require('../controllers/user');
var authHelper = require('../helper/authentication');
var helper = require('../helper/response');
var router = express.Router();


router.put('/:id/password',authHelper.isLoggedIn,userController.changePassword,helper.handleSuccess);
router.put('/:id/name',authHelper.isLoggedIn,userController.changeName,helper.handleSuccess);
router.put('/:id/email',authHelper.isLoggedIn,userController.changeEmail,helper.handleSuccess);
router.put('/:id/dob',authHelper.isLoggedIn,userController.changeDob,helper.handleSuccess);
router.put('/:id/dob',authHelper.isLoggedIn,userController.changeDob,helper.handleSuccess);
router.put('/:id/avatar',authHelper.isLoggedIn,userController.changeAvatar,helper.handleSuccess);
router.get('/',[userController.getAllUser,helper.handleSuccess]);

//user personal edit
router.put('/',[authHelper.isLoggedIn, userController.editUser,helper.handleSuccess]);
//admin user add
router.post('/admin/register',[authHelper.isAdmin, userController.adminUserRegister,helper.handleSuccess]);
//admin user edit
router.put('/admin/:id',[authHelper.isAdmin, userController.editUserByAdmin,helper.handleSuccess]);
router.get('/emails',userController.getUserByEmail,helper.handleSuccess);
router.get('/name',userController.getUserByName,helper.handleSuccess);
router.get('/byId/:id',userController.getUserById,helper.handleSuccess);

// followers/following
router.get('/followers', authHelper.isLoggedIn, userController.getFollowers, helper.handleSuccess);
router.get('/following', authHelper.isLoggedIn, userController.getFollowing, helper.handleSuccess);
router.put('/follow/:id', authHelper.isLoggedIn, userController.addFollowing, helper.handleSuccess);
router.put('/unfollow/:id', authHelper.isLoggedIn, userController.removeFollowing, helper.handleSuccess);

router.delete('/:id',[authHelper.isLoggedIn, userController.deleteUser,helper.handleSuccess]);
//notification
router.get('/notifications',userController.getAllNotification,helper.handleSuccess);
router.get('/:id/notifications',userController.getNotification,helper.handleSuccess);
router.put('/:id/notifications/:nid/seen',userController.seenNotification,helper.handleSuccess);
router.put('/:id/notifications/unseen',userController.unseenNotification,helper.handleSuccess);
router.put('/:id/groupNotifications/:nid/seen',userController.seenGrpNotification,helper.handleSuccess);

// search users
router.get('/search',userController.searchUserByName,helper.handleSuccess);
router.get('/search/names',userController.searchUserByName,helper.handleSuccess);

module.exports = router;
