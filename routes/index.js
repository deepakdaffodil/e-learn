// Created By Nitesh Jatav  on 14/02/2016
var users = require('./users');
var auth = require('./authentication');
var modules = require('./modules');
var lessons = require('./lessons');
var actions = require('./actions');
var activity = require('./activities');
var groups = require('./groups');
var completedLessons = require('./completedLessons');
var notifications = require('./notifications');
var allTags = require('./allTags');

module.exports = function(app){
	
	// app.get('/',function(req, res, next){
	// 	res.render('index');
	// })
	app.use('/api/auth',auth);
	app.use('/api/users', users);
	app.use('/api/modules', modules);
	app.use('/api/lessons', lessons);
	app.use('/api/actions',actions);
	app.use('/api/activity',activity);
	app.use('/api/completedLessons',completedLessons);
	app.use('/api/groups', groups);
	app.use('/api/notifications',notifications);
	app.use('/api/tags', allTags);
};