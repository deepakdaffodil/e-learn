// Created By Nitesh Jatav  on 10/02/2016
var express = require('express');
var moduleController = require('../controllers/module');
var authHelper = require('../helper/authentication');
var helper = require('../helper/response');
var router = express.Router();
/* GET users listing. */
router.get('/',[moduleController.getAllModule,helper.handleSuccess]);

// search module by name
router.get('/search', [moduleController.searchModule, helper.handleSuccess]);

router.get('/:id',[moduleController.getModule,helper.handleSuccess]);
router.get('/:id/lessons',[moduleController.getModuleLessions,helper.handleSuccess]);
router.post('/',[authHelper.isAdmin, moduleController.addModule,helper.handleSuccess]);
router.put('/:id',[authHelper.isAdmin, moduleController.editModule,helper.handleSuccess]);
router.delete('/:id',[authHelper.isAdmin, moduleController.deleteModule,helper.handleSuccess]);

module.exports = router;