var express = require('express');
var allTagsController = require('../controllers/allTags');
var authHelper = require('../helper/authentication');
var helper = require('../helper/response');
var router = express.Router();

/**
 * Get and search for tags
 */
router.get('/',[allTagsController.searchTags, helper.handleSuccess]);

module.exports = router;