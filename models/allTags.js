var timestamps = require('mongoose-timestamp');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagsSchema = Schema ({
	tags : { type: [{ type: String }], default: [] }
});

tagsSchema.plugin(timestamps);

module.exports = mongoose.model('AllTags', tagsSchema);