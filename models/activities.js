var timestamps = require('mongoose-timestamp');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var activitySchema = Schema({
  my_community : { type: Boolean, default: true },
  user : { type: Schema.Types.ObjectId, ref: 'Users', required: true},
  lesson : { type: Schema.Types.ObjectId, ref: 'Lessons', required: true },
  module : { type: Schema.Types.ObjectId, ref: 'Modules', required: true},
  description : { type: String, required: true},
  type : { type: String, required: true},
  url : { type: String},
  likes : [{ type: Schema.Types.ObjectId, ref: 'Users' }],
  deleted : { type: Boolean, default: false },
  tags: { type: [{type: String }], default: [], set: function(v) {
    for (var i = 0; i < v.length; i++) {
      v[i] = v[i].toLowerCase();
    }
    return v;
  }}
});

activitySchema.plugin(timestamps);

module.exports = mongoose.model('Activities',activitySchema);
