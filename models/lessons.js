var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lessonSchema = Schema({
  module: { type: Schema.Types.ObjectId, ref: 'Modules', required: true },
  name: { type: String, required: true },
  activity: { type: String },
  Keytakeaways: { type: String },
  basic_description: { type: String, required: true },
  detail_description: { type: String },
  url: { type: String },
  type: { type: String, enum: ['text', 'image', 'video', 'activity'], default: 'activity', required: true },
  deleted: { type: Boolean, default: false }
});

module.exports = mongoose.model('Lessons', lessonSchema);